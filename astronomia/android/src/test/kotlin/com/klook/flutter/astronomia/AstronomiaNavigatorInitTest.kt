package com.klook.flutter.astronomia

import android.content.Context
import org.junit.After
import org.junit.Test
import org.mockito.Mockito.mock

class AstronomiaNavigatorInitTest {

    @After
    fun tearDown() {
        AstronomiaNavigator.instance.clear()
    }

    @Test
    fun `should call init when call push`() {
        try {
            AstronomiaNavigator.instance.push("/page/1")
        } catch (e: IllegalStateException) {
            assert(e.message == "The AstronomiaNavigator has not been initialized, pls call init first!")
        }
    }

    @Test
    fun `should call init when call pop`() {
        try {
            AstronomiaNavigator.instance.pop(mock(Context::class.java))
        } catch (e: IllegalStateException) {
            assert(e.message == "The AstronomiaNavigator has not been initialized, pls call init first!")
        }
    }

    @Test
    fun `should call init when call popUntilNamed`() {
        try {
            AstronomiaNavigator.instance.popUntilNamed("/page/1")
        } catch (e: IllegalStateException) {
            assert(e.message == "The AstronomiaNavigator has not been initialized, pls call init first!")
        }
    }
}