package io.flutter.embedding.android

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import com.klook.flutter.astronomia.*
import com.klook.flutter.astronomia.FlutterEngineManager.Companion.ASTRONOMIA_ENGINE_ID
import io.flutter.embedding.android.FlutterActivityLaunchConfigs.EXTRA_BACKGROUND_MODE
import io.flutter.embedding.android.FlutterActivityLaunchConfigs.EXTRA_CACHED_ENGINE_ID
import io.flutter.embedding.engine.renderer.FlutterUiDisplayListener
import io.flutter.plugin.common.MethodChannel
import timber.log.Timber

open class AstronomiaFlutterActivity : ThawFlutterFragmentActivity(), FlutterUiDisplayListener {

    companion object {
        @VisibleForTesting
        const val EXTRA_ROUTE_SETTINGS_KEY = "extra_route_settings_key"
        @VisibleForTesting
        const val EXTRA_POP_RESULT_KEY = "extra_pop_result_key"
        @VisibleForTesting
        const val EXTRA_FORCE_POP_KEY = "extra_force_pop_key"
        @VisibleForTesting
        const val EXTRA_NAVIGATION_OP_KEY = "extra_navigation_op_key"
        @VisibleForTesting
        const val NAVIGATION_OP_NONE = 1000
        @VisibleForTesting
        const val NAVIGATION_OP_PUSH = 2000
        @VisibleForTesting
        const val NAVIGATION_OP_PUSH_REPLACEMENT = 3000
        @VisibleForTesting
        const val NAVIGATION_OP_POP = 4000
        @VisibleForTesting
        const val NAVIGATION_OP_MAYBE_POP = 5000
        @VisibleForTesting
        const val NAVIGATION_OP_POP_AND_FINISH_ACTIVITY = 6000
        @VisibleForTesting
        const val NAVIGATION_OP_POP_UNTIL = 7000

        internal fun createIntent(
            context: Context,
            navigationOp: Int,
            activityClass: Class<out AstronomiaFlutterActivity>? = null,
            backgroundMode: FlutterActivityLaunchConfigs.BackgroundMode = FlutterActivityLaunchConfigs.BackgroundMode.opaque
        ): Intent {
            return Intent(context, activityClass).apply {
                putExtra(EXTRA_NAVIGATION_OP_KEY, navigationOp)
                putExtra(EXTRA_CACHED_ENGINE_ID, ASTRONOMIA_ENGINE_ID)

                putExtra(EXTRA_BACKGROUND_MODE, backgroundMode.name)
            }
        }

        fun pushOnly(context: Context, launchConfigs: AstronomiaFlutterActivityLaunchConfigs? = null) {
            val intent = createIntent(
                    context,
                    NAVIGATION_OP_NONE,
                    launchConfigs?.activityClass,
                    launchConfigs?.backgroundMode ?: FlutterActivityLaunchConfigs.BackgroundMode.opaque)

            context.startActivity(intent)
        }

        fun popResult(result: Any?) {
            Timber.e("pop: $result")
            val currentPageRoute = NavigationStackManager.instance.pageRouteStack.last()
            Timber.e("currentPageRoute: $currentPageRoute")
            currentPageRoute.interceptedPopRouteResult?.success(result)
        }
    }

    private var isPushInitialRoute = false

    private lateinit var flutterEngineWrapper: FlutterEngineWrapper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        flutterEngineWrapper = FlutterEngineManager.instance.getEngineWrapper(this)
    }

    override fun onNewIntent(intent: Intent) {
        isPushInitialRoute = false
        Timber.e("onNewIntent")
        pushRoute(
                intent.getIntExtra(EXTRA_NAVIGATION_OP_KEY, NAVIGATION_OP_PUSH),
                intent.getParcelableExtra(EXTRA_ROUTE_SETTINGS_KEY),
                intent.getSerializableExtra(EXTRA_POP_RESULT_KEY))
        super.onNewIntent(intent)
    }

    override fun onFlutterUiDisplayed() {
        pushRoute(
                intent.getIntExtra(EXTRA_NAVIGATION_OP_KEY, NAVIGATION_OP_PUSH),
                intent.getParcelableExtra(EXTRA_ROUTE_SETTINGS_KEY),
                intent.getSerializableExtra(EXTRA_POP_RESULT_KEY))
    }

    override fun onFlutterUiNoLongerDisplayed() {
    }

    private fun pushRoute(navigationOp: Int, routeSetting: RouteSettings?, result: Any?) {
        if (!isPushInitialRoute) {
            flutterEngineWrapper.executePendingFlutterUIDisplayAction()

            // TODO(littlegnal): Will remove this bloc in the future.
            when (navigationOp) {
//                NAVIGATION_OP_PUSH -> {
//                    Timber.e("NAVIGATION_OP_PUSH")
////                    val currentPageRoute = NavigationStackManager.instance.pageRouteStack.last()
////                    val currentPageRouteSettings = currentPageRoute.routeSettings
////                    flutterEngineWrapper.routeChannel.push(
////                            routeName = currentPageRouteSettings.routeName,
////                            arguments = currentPageRouteSettings.arguments,
////                            callback = currentPageRoute.popRouteResult)
//
//                    routeSetting?.apply {
//
//                        val currentPageRoute = NavigationStackManager.instance.pageRouteStack.last()
////                        flutterEngineWrapper.routeChannel.push(
////                                routeName = routeName,
////                                arguments = arguments,
////                                callback = currentPageRoute.popRouteResult)
//
//                        flutterEngineWrapper.routeChannel.pushFromNative(routeName, arguments, currentPageRoute.popRouteResult)
//                    }
//                }
//                NAVIGATION_OP_POP -> {
//                    Timber.e("NAVIGATION_OP_POP")
//                    flutterEngineWrapper.routeChannel.pop(result, object : MethodChannel.Result {
//                        override fun notImplemented() {
//
//                        }
//
//                        override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
//                        }
//
//                        override fun success(result: Any?) {
//                            removeLastRouteFromStack()
//                        }
//
//                    })
//                }
//                NAVIGATION_OP_MAYBE_POP -> {
//                    Timber.e("NAVIGATION_OP_MAYBE_POP")
//                    val lastPageRoute = getLastRouteFromStack()
//                    flutterEngineWrapper.routeChannel.maybePop(result, object : MethodChannel.Result {
//                        override fun notImplemented() {
//                            lastPageRoute.maybePopRouteResult?.notImplemented()
//                        }
//
//                        override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
//                            lastPageRoute.maybePopRouteResult?.error(errorCode, errorMessage, errorDetails)
//                        }
//
//                        override fun success(result: Any?) {
//                            lastPageRoute.maybePopRouteResult?.success(result)
//                            if (result is Boolean && result) {
//                                removeLastRouteFromStack()
//                            }
//                        }
//
//                    })
//                }
//                NAVIGATION_OP_POP_AND_FINISH_ACTIVITY -> {
//                    Timber.e("NAVIGATION_OP_POP_AND_FINISH_ACTIVITY")
//                    flutterEngineWrapper.routeChannel.pop(result = result, callback = object : MethodChannel.Result {
//                        override fun notImplemented() {
//
//                        }
//
//                        override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
//                        }
//
//                        override fun success(result: Any?) {
//                            if (result is Boolean && result) {
//                                Timber.e("NAVIGATION_OP_POP_AND_FINISH_ACTIVITY Success")
//                                removeLastRouteFromStack()
//                                finish()
//                            }
//                        }
//
//                    })
//                }
//                NAVIGATION_OP_POP_UNTIL -> {
//                    Timber.e("NAVIGATION_OP_POP_UNTIL")
//                    routeSetting?.apply {
//                        popUntilNamed(routeName)
//                    }
//                }
                else -> {

                }
            }

            isPushInitialRoute = true
        }
    }

    /**
     * This function will be used by [NavigationDelegate.popAll]
     */
    fun popUntilNamed(routeName: String) {
        flutterEngineWrapper.routeChannel.popUntilNamed(
                routeName = routeName,
                callback = object : MethodChannel.Result {
                    override fun notImplemented() {

                    }

                    override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
                    }

                    override fun success(result: Any?) {
                    }
                })
    }


    override fun onBackPressed() {
        flutterEngineWrapper.routeChannel.maybePopFromNative()
    }

    private fun removeLastRouteFromStack() {
        NavigationStackManager.instance.pageRouteStack.removeAt(
                NavigationStackManager.instance.pageRouteStack.size - 1)
    }

    private fun getLastRouteFromStack(): PageRoute {
        return NavigationStackManager.instance.pageRouteStack.last()
    }
}