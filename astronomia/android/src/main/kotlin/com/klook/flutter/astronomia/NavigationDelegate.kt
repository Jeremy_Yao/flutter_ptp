package com.klook.flutter.astronomia

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import io.flutter.embedding.android.AstronomiaFlutterActivity
import io.flutter.embedding.android.FlutterActivityLaunchConfigs
import io.flutter.plugin.common.MethodChannel
import timber.log.Timber
import java.io.Serializable

class NavigationDelegate(
    private val routeInterceptor: RouteInterceptor,
    private val navigationStackManager: NavigationStackManager,
    internal val activityStack: ActivityStack,
    internal val launchConfigs: AstronomiaFlutterActivityLaunchConfigs?,
    private val routeChannel: RouteChannel,
    private val flutterEngineWrapper: FlutterEngineWrapper
) : RouteChannelMethodHandler {

  fun pushFromNative(
      routeName: String,
      arguments: Any?,
      popResultCallback: PopResultCallback? = null
  ) {
    val popRouteResult: MethodChannel.Result? = if (popResultCallback != null) {
      object : MethodChannel.Result {
        override fun notImplemented() {
        }

        override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
        }

        override fun success(result: Any?) {
          popResultCallback.invoke(result)
        }
      }
    } else {
      null
    }

    val topActivity = activityStack.topActivity ?: return

    val hasFlutterActivity = activityStack.getActivityStack().any { it is AstronomiaFlutterActivity }
    if (!hasFlutterActivity) {
      // Push route after flutter ui displayed
      flutterEngineWrapper.postOnFlutterUIDisplay(Runnable {
        routeChannel.pushFromNative(
                routeName,
                arguments,
                topActivity !is AstronomiaFlutterActivity,
                popRouteResult)
      })

      Timber.d("Push route: $routeName on flutter ui display.")
    } else {
      routeChannel.pushFromNative(
              routeName,
              arguments,
              topActivity !is AstronomiaFlutterActivity,
              popRouteResult)
    }

    if (topActivity !is AstronomiaFlutterActivity) {
      AstronomiaFlutterActivity.pushOnly(
              context = topActivity,
              launchConfigs = launchConfigs)
    }
  }

  fun popUntilNamed(routeName: String) {
    routeChannel.popUntilNamedFromNative(routeName)
  }

  override fun intercept(routeName: String, arguments: Any?, popRouteResult: MethodChannel.Result?): Boolean {
    activityStack.refreshActivityStackAfterFinish()
    val topActivity = activityStack.topActivity ?: return false
    val pageRouteStack = navigationStackManager.pageRouteStack
    val routeSettings = RouteSettings(routeName = routeName, arguments = arguments as? Serializable)
    if (routeInterceptor.intercept(activityContext = topActivity, routeSettings = routeSettings)) {
      if (topActivity is AstronomiaFlutterActivity) {
        // The first intercepted page can invoke the interceptedPopRouteResult by using AstronomiaNavigator.pop
        val lastPageRoute = pageRouteStack.last()
        pageRouteStack[pageRouteStack.size - 1] = lastPageRoute.copy(
                interceptedPopRouteResult = popRouteResult)
      }

      return true
    }

    popRouteResult?.success(null)
    return false
  }

  override fun didPush(routeName: String, arguments: Any?, isFirstRoute: Boolean) {
//    if (routeName == "/") return
    val pageRouteStack = navigationStackManager.pageRouteStack
    val routeSettings = RouteSettings(routeName = routeName, arguments = arguments as? Serializable)

    pageRouteStack.add(PageRoute(
            routeSettings = routeSettings,
            isFirstRoute = isFirstRoute))

    if (BuildConfig.DEBUG) {
      val stackOutput = pageRouteStack.joinToString("\n") {
        "routeName: ${it.routeSettings.routeName}, arguments: ${it.routeSettings.arguments}, isFirstRoute: ${it.isFirstRoute}"
      }
      Timber.d("After didPush:\n $stackOutput")
    }
  }

  @SuppressLint("BinaryOperationInTimber")
  override fun didPop(routeName: String, arguments: Any?, popUntilNamedName: String?) {
    val pageRouteStack = NavigationStackManager.instance.pageRouteStack
    if (pageRouteStack.isEmpty()) {
      Timber.d("The pageRouteStack is empty, skip this route, " +
              "routeName: $routeName, " +
              "arguments: $arguments, " +
              "popUntilNamedName: $popUntilNamedName")
      return
    }

    val activityStack = activityStack.getActivityStack()
    if (activityStack.isEmpty()) {
      Timber.d("The activityStack is empty, skip this route, " +
              "routeName: $routeName, " +
              "arguments: $arguments, " +
              "popUntilNamedName: $popUntilNamedName")
      return
    }

    val currentRoute = pageRouteStack.last()
    val previousRoute = pageRouteStack.getOrNull(pageRouteStack.size - 2)
    val isSameRoute = currentRoute.routeSettings.routeName == routeName
    if (!isSameRoute) {
      Timber.d("The top route: $currentRoute, is not the same route with " +
              "routeName: $routeName, " +
              "arguments: $arguments, " +
              "popUntilNamedName: $popUntilNamedName")
      return
    }

    var targetActivityIndex = activityStack.size - 1

    while (targetActivityIndex >= 0) {
      val activity = activityStack[targetActivityIndex]
      if (activity !is AstronomiaFlutterActivity) {
        --targetActivityIndex
        continue
      }

      break
    }

    if (currentRoute.isFirstRoute) {
      --targetActivityIndex
    }

    if (popUntilNamedName != null && popUntilNamedName == previousRoute?.routeSettings?.routeName) {
      while (targetActivityIndex >= 0) {
        val activity = activityStack[targetActivityIndex]
        if (activity !is AstronomiaFlutterActivity) {
          --targetActivityIndex
          continue
        }

        break
      }
    }

    for (i in activityStack.size - 1 downTo 0) {
      if (i == targetActivityIndex) break
      activityStack[i].finish()
    }

    if (targetActivityIndex != activityStack.size - 1) {
      this.activityStack.refreshActivityStackAfterFinish()
    }

    NavigationStackManager.instance.pageRouteStack.removeAt(
            NavigationStackManager.instance.pageRouteStack.size - 1)

    if (BuildConfig.DEBUG) {
      val stackOutput = pageRouteStack.joinToString("\n") {
        "routeName: ${it.routeSettings.routeName}, arguments: ${it.routeSettings.arguments}, isFirstRoute: ${it.isFirstRoute}"
      }
      Timber.d("After didPop:\n $stackOutput")
    }
  }

  /**
   * Finish all [AstronomiaFlutterActivity]s, and pop the flutter route to `/`
   */
  fun popAll() {
    val activityStack = activityStack.getActivityStack().reversed()
    navigationStackManager.pageRouteStack.clear()

    // Find the first AstronomiaFlutterActivity, popUntil flutter route `/`
    // and finish all the AstronomiaFlutterActivity
    var isTopmostFlutterActivity = false
    for (activity in activityStack) {
      if (activity is AstronomiaFlutterActivity) {
        if (!isTopmostFlutterActivity) {
          isTopmostFlutterActivity = true
          activity.popUntilNamed("/")
        }

        activity.finish()
      }
    }
  }

  fun popToRootNavigatorActivity() {
    val activityStack = activityStack.getActivityStack()

    navigationStackManager.pageRouteStack.removeAll { it.routeSettings.routeName != "/" }

    // Find the root AstronomiaFlutterActivity, popUntil flutter route `/`
    // and finish all the AstronomiaFlutterActivity that on the top of the root AstronomiaFlutterActivity.
    var isRootFlutterActivity = false
    for (activity in activityStack) {
      if (activity is AstronomiaFlutterActivity) {
        if (!isRootFlutterActivity) {
          isRootFlutterActivity = true
          activity.popUntilNamed("/")
        } else {
          activity.finish()
        }
      }
    }
  }
}

fun NavigationDelegate.createRootNavigatorActivityIntent(
  context: Context,
  activityClass: Class<out AstronomiaFlutterActivity>? = null,
  backgroundMode: FlutterActivityLaunchConfigs.BackgroundMode = FlutterActivityLaunchConfigs.BackgroundMode.opaque
): Intent {
  return AstronomiaFlutterActivity.createIntent(
    context = context,
    navigationOp = AstronomiaFlutterActivity.NAVIGATION_OP_NONE,
    activityClass = activityClass,
    backgroundMode = backgroundMode)
}