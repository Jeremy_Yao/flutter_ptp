package com.klook.flutter.astronomia

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import java.lang.ref.WeakReference
import java.util.*

class ActivityStack : Application.ActivityLifecycleCallbacks {

    @VisibleForTesting
    val activities: MutableList<WeakReference<Activity>> = mutableListOf()

    val topActivity: Activity?
        get() = getActivityStack().lastOrNull()

    override fun onActivityPaused(activity: Activity) {
        if (activity.isFinishing) {
            val activityIterator = activities.iterator()
            while (activityIterator.hasNext()) {
                if (activityIterator.next().get() === activity) {
                    activityIterator.remove()
                }
            }
        }
    }

    override fun onActivityResumed(activity: Activity) {
    }

    override fun onActivityStarted(p0: Activity) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
    }

    override fun onActivityStopped(p0: Activity) {
    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) {
        activity.apply { activities.add(WeakReference(this)) }
    }

    fun getActivityStack(): List<Activity> {
        val tempActivities: MutableList<Activity> = ArrayList(activities.size)
        val activityIterator = activities.iterator()
        while (activityIterator.hasNext()) {
            val activity = activityIterator.next().get()
            if (activity == null) {
                activityIterator.remove()
            } else {
                tempActivities.add(activity)
            }
        }
        return tempActivities
    }

    fun refreshActivityStackAfterFinish() {
        val activityIterator = activities.iterator()
        while (activityIterator.hasNext()) {
            if (activityIterator.next().get()?.isFinishing == true) {
                activityIterator.remove()
            }
        }
    }
}