package com.klook.flutter.astronomia

import io.flutter.plugin.common.MethodChannel

data class PageRoute(
    val routeSettings: RouteSettings,
    val isIntercepted: Boolean = false,
    val isFirstRoute: Boolean = false,
    val popRouteResult: MethodChannel.Result? = null,
    val maybePopRouteResult: MethodChannel.Result? = null,
    val interceptedPopRouteResult: MethodChannel.Result? = null
)