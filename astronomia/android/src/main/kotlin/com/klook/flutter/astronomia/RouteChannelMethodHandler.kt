package com.klook.flutter.astronomia

import io.flutter.plugin.common.MethodChannel

interface RouteChannelMethodHandler {

  fun intercept(routeName: String, arguments: Any?, popRouteResult: MethodChannel.Result?): Boolean

  fun didPush(routeName: String, arguments: Any?, isFirstRoute: Boolean)

  fun didPop(routeName: String, arguments: Any?, popUntilNamedName: String?)
}