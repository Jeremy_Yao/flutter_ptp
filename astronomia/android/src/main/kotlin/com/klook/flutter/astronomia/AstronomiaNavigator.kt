package com.klook.flutter.astronomia

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.annotation.VisibleForTesting
import com.klook.flutter.astronomia.FlutterEngineManager.Companion.ASTRONOMIA_ENGINE_ID
import io.flutter.embedding.android.AstronomiaFlutterActivity
import io.flutter.embedding.android.FlutterActivityLaunchConfigs
import io.flutter.embedding.android.FlutterActivityLaunchConfigs.BackgroundMode
import io.flutter.embedding.engine.FlutterEngine
import timber.log.Timber

typealias PopResultCallback = (result: Any?) -> Unit

data class AstronomiaFlutterActivityLaunchConfigs(
    val flutterEngine: FlutterEngine? = null,
    val activityClass: Class<out AstronomiaFlutterActivity>? = null,
    val backgroundMode: BackgroundMode = BackgroundMode.opaque
)

class AstronomiaNavigator private constructor() {

  private object Holder {
    val instance =
        AstronomiaNavigator()
  }

  companion object {

    val instance: AstronomiaNavigator by lazy { Holder.instance }
  }

  private var application: Application? = null

  private var isInitialized = false

  private val routeInterceptors = mutableSetOf<RouteInterceptor>()

  private lateinit var flutterEngineWrapper: FlutterEngineWrapper

  @VisibleForTesting
  val flutterEngineManager = FlutterEngineManager.instance

  @VisibleForTesting
  val activityStack = ActivityStack()

  internal lateinit var navigationDelegate: NavigationDelegate

  fun init(context: Context, launchConfigs: AstronomiaFlutterActivityLaunchConfigs? = null) {
    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }

    isInitialized = true
    application = context.applicationContext as? Application
    application?.registerActivityLifecycleCallbacks(activityStack)
    val flutterEngine = launchConfigs?.flutterEngine
    if (flutterEngine != null) {
      flutterEngineManager.addEngineWrapper(
              engineId = ASTRONOMIA_ENGINE_ID,
              engineWrapper = FlutterEngineWrapper(ASTRONOMIA_ENGINE_ID, flutterEngine))
    }
    flutterEngineWrapper = flutterEngineManager.getEngineWrapper(context)

    navigationDelegate = NavigationDelegate(
            routeInterceptor = object : RouteInterceptor {
              override fun intercept(activityContext: Context, routeSettings: RouteSettings): Boolean {
                return routeInterceptors.any { it.intercept(activityContext, routeSettings) }
              }
            },
            navigationStackManager = NavigationStackManager.instance,
            activityStack = activityStack,
            launchConfigs = launchConfigs,
            routeChannel = flutterEngineWrapper.routeChannel,
            flutterEngineWrapper = flutterEngineWrapper
    )

    flutterEngineWrapper.routeChannel.routeChannelMethodHandler = navigationDelegate
  }

  fun clear() {
    isInitialized = false
    application?.unregisterActivityLifecycleCallbacks(activityStack)
    application = null
    flutterEngineManager.clear()
    NavigationStackManager.instance.pageRouteStack.clear()
  }

  private fun checkIsInitialized() {
    check(isInitialized) {
      "The AstronomiaNavigator has not been initialized, pls call init first!"
    }
  }

  fun addRouteInterceptor(routeInterceptor: RouteInterceptor) {
    routeInterceptors.add(routeInterceptor)
  }

  fun removeRouteInterceptor(routeInterceptor: RouteInterceptor) {
    if (routeInterceptors.contains(routeInterceptor)) {
      routeInterceptors.remove(routeInterceptor)
    }
  }

  fun push(routeName: String, arguments: Any? = null, popResultCallback: PopResultCallback? = null) {
    checkIsInitialized()
    navigationDelegate.pushFromNative(
            routeName = routeName,
            arguments = arguments,
            popResultCallback = popResultCallback)
  }

  fun pop(from: Context, result: Any? = null) {
    checkIsInitialized()

    if (from is Activity && from !is AstronomiaFlutterActivity) {
      AstronomiaFlutterActivity.popResult(result)
      from.finish()
    }
  }

  fun popUntilNamed(routeName: String) {
    checkIsInitialized()
    navigationDelegate.popUntilNamed(routeName)
  }

  /**
   * Finish all [AstronomiaFlutterActivity]s, and pop the flutter route to `/`
   */
  fun popAll() {
    navigationDelegate.popAll()
  }

  fun popToRootNavigatorActivity() {
    navigationDelegate.popToRootNavigatorActivity()
  }
}

fun AstronomiaNavigator.createRootNavigatorActivityIntent(
  context: Context,
  activityClass: Class<out AstronomiaFlutterActivity>? = null,
  backgroundMode: BackgroundMode = BackgroundMode.opaque
): Intent {
  return navigationDelegate.createRootNavigatorActivityIntent(
          context = context,
          activityClass = activityClass,
          backgroundMode = backgroundMode)
}