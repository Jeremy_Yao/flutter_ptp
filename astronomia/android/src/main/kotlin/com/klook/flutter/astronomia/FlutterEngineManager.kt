package com.klook.flutter.astronomia

import android.content.Context
import androidx.annotation.VisibleForTesting

class FlutterEngineManager {

  private object Holder {
    val instance =
        FlutterEngineManager()
  }

  companion object {

    @VisibleForTesting
    const val ASTRONOMIA_ENGINE_ID = "astronomia_engine_id"

    val instance: FlutterEngineManager by lazy { Holder.instance }
  }

  private val engineWrappers = mutableMapOf<String, FlutterEngineWrapper>()

  fun getEngineWrapper(
      context: Context,
      engineId: String = ASTRONOMIA_ENGINE_ID
  ): FlutterEngineWrapper {
    var engineWrapper = engineWrappers[engineId]
    if (engineWrapper != null) return engineWrapper

    engineWrapper = FlutterEngineWrapper(context.applicationContext, engineId)
    engineWrappers[engineId] = engineWrapper
    return engineWrapper
  }

  fun clear() {
    for (engine in engineWrappers) {
      engine.value.destroy()
    }

    engineWrappers.clear()
  }

  fun addEngineWrapper(engineId: String, engineWrapper: FlutterEngineWrapper) {
    engineWrappers[engineId] = engineWrapper
  }
}