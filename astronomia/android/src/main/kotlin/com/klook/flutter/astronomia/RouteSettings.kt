package com.klook.flutter.astronomia

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class RouteSettings(
    val routeName: String,
    // TODO(littlegnal): Try change it to `Any` later
    val arguments: Serializable?
) : Parcelable