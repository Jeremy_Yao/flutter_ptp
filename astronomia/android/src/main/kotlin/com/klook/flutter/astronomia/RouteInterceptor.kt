package com.klook.flutter.astronomia

import android.content.Context

interface RouteInterceptor {

  fun intercept(activityContext: Context, routeSettings: RouteSettings): Boolean
}