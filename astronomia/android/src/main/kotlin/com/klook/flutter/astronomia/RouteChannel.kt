package com.klook.flutter.astronomia

import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class RouteChannel(binaryMessenger: BinaryMessenger): MethodChannel.MethodCallHandler {

  private val channel: MethodChannel = MethodChannel(binaryMessenger, "astronomia").apply {
    setMethodCallHandler(this@RouteChannel)
  }

  var routeChannelMethodHandler: RouteChannelMethodHandler? = null

  override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
    when (call.method) {
      "intercept" -> {
        val routeName = call.argument<String>("routeName") ?: ""
        val arguments = call.argument<Any?>("arguments")
        routeChannelMethodHandler?.intercept(routeName, arguments, result)
      }
      "didPush" -> {
        val routeName = call.argument<String>("routeName") ?: ""
        val arguments = call.argument<Any?>("arguments")
        val isFirstRoute = call.argument<Boolean?>("isFirstRoute") ?: false
        routeChannelMethodHandler?.didPush(routeName, arguments, isFirstRoute)
        result.success(true)
      }
      "didPop" -> {
        val routeName = call.argument<String>("routeName") ?: ""
        val arguments = call.argument<Any?>("arguments")
        val popUntilNamedName = call.argument<String>("popUntilNamedName")
        routeChannelMethodHandler?.didPop(routeName, arguments, popUntilNamedName)
        result.success(true)
      }
      else -> {
        result.notImplemented()
      }
    }
  }

  fun popUntilNamed(routeName: String, callback: MethodChannel.Result) {
    channel.invokeMethod("popUntilNamed", routeName, callback)
  }

  fun pushFromNative(routeName: String, arguments: Any?, isFirstRoute: Boolean = true ,callback: MethodChannel.Result? = null) {
    val arg = mapOf("routeName" to routeName, "arguments" to arguments, "isFirstRoute" to isFirstRoute)
    if (callback != null) {
      channel.invokeMethod("pushFromNative", arg, callback)
      return
    }
    channel.invokeMethod("pushFromNative", arg)
  }

  fun maybePopFromNative(result: Any? = null, callback: MethodChannel.Result? = null) {
    if (callback != null) {
      channel.invokeMethod("maybePopFromNative", result, callback)
      return
    }
    channel.invokeMethod("maybePopFromNative", result)
  }

  fun popUntilNamedFromNative(routeName: String, callback: MethodChannel.Result? = null) {
    if (callback != null) {
      channel.invokeMethod("popUntilNamedFromNative", routeName, callback)
      return
    }
    channel.invokeMethod("popUntilNamedFromNative", routeName)
  }
}