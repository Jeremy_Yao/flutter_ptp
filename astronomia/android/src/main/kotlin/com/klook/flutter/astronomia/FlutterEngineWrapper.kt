package com.klook.flutter.astronomia

import android.content.Context
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor

class FlutterEngineWrapper(
  private val engineId: String,
  flutterEngine: FlutterEngine,
  routeChannel: RouteChannel? = null
) {

  constructor(context: Context, engineId: String): this(engineId, FlutterEngine(context.applicationContext))

  var flutterEngine: FlutterEngine? = null
    private set

  var routeChannel: RouteChannel
    private set

  private var pendingFlutterUIDisplayAction: Runnable? = null

  init {
    this.flutterEngine = flutterEngine
    this.flutterEngine!!.dartExecutor.executeDartEntrypoint(
        DartExecutor.DartEntrypoint.createDefault()
    )
    FlutterEngineCache.getInstance().put(engineId, flutterEngine)

    this.routeChannel = routeChannel ?: RouteChannel(flutterEngine.dartExecutor.binaryMessenger)
  }

  fun postOnFlutterUIDisplay(action: Runnable) {
    pendingFlutterUIDisplayAction = action
  }

  fun executePendingFlutterUIDisplayAction() {
    pendingFlutterUIDisplayAction?.run()
    pendingFlutterUIDisplayAction = null
  }

  fun destroy() {
    flutterEngine?.destroy()
    flutterEngine = null
    routeChannel.routeChannelMethodHandler = null
    FlutterEngineCache.getInstance().put(engineId, null)
  }
}