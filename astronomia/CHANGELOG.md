# Change logs

## 2.0.0

* Upgrade to flutter 2.0.1

## 1.1.2

* 修复iOS 14 原生和flutter混合几次后，来回进出几次flutter时setViewControllers把整个栈清空了

## 1.1.1+1

* Fix can not push route from cold-launching

## 1.1.0

* Push first route when flutter ui displayed.

## 1.0.0

* Initial release
