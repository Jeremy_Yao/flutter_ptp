# astronomia

**astronomia** is a navigator for Flutter Add2App.

## Contributing
Make sure all the tests passed by running following command:
``` 
cd astronomia
flutter test
bash run-native-ui-test.sh
```