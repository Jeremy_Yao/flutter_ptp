#!/usr/bin/env bash

set -e

ROOT_PATH=$(pwd)
ANDROID_TEST_PATH=$ROOT_PATH/example/android
IOS_TEST_PATH=$ROOT_PATH/example/ios

cd $ANDROID_TEST_PATH
# Reference https://pub.dev/packages/espresso
./gradlew app:connectedAndroidTest -Ptarget=`pwd`/../test_driver/app.dart

cd $IOS_TEST_PATH
# Borrow from https://gist.github.com/KrauseFx/26c99e394c8fb7bbad7f
xcodebuild \
    -workspace Runner.xcworkspace \
    -scheme Runner \
    -sdk iphonesimulator \
    -destination 'platform=iOS Simulator,name=iPhone 8,OS=13.3' \
    test