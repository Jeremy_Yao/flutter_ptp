import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class AstronomiaNavigatorObserverDelegate {
  final RouteObserver<PageRoute> _routeObserver = RouteObserver<PageRoute>();

  @visibleForTesting
  final _AstronomiaNavigatorObserverForwarder
      astronomiaNavigatorObserverForwarder =
      _AstronomiaNavigatorObserverForwarder();

  List<NavigatorObserver> get observers =>
      [_routeObserver, astronomiaNavigatorObserverForwarder];

  void subscribeRouteAware(RouteAware routeAware, Route route) {
    _routeObserver.subscribe(routeAware, route);
  }

  void unsubscribeRouteAware(RouteAware routeAware) {
    _routeObserver.unsubscribe(routeAware);
  }

  /// Add a [NavigatorObserver].
  VoidCallback addNavigatorObserver(NavigatorObserver navigatorObserver) {
    astronomiaNavigatorObserverForwarder
        .addNavigatorObserver(navigatorObserver);

    return () {
      removeNavigatorObserver(navigatorObserver);
    };
  }

  /// Remove a [NavigatorObserver].
  void removeNavigatorObserver(NavigatorObserver navigatorObserver) {
    astronomiaNavigatorObserverForwarder
        .removeNavigatorObserver(navigatorObserver);
  }
}

class _AstronomiaNavigatorObserverForwarder extends NavigatorObserver {
  @visibleForTesting
  final List<NavigatorObserver> navigatorObservers = [];

  void addNavigatorObserver(NavigatorObserver navigatorObserver) {
    navigatorObservers.add(navigatorObserver);
  }

  void removeNavigatorObserver(NavigatorObserver navigatorObserver) {
    navigatorObservers.remove(navigatorObserver);
  }

  @override
  void didPush(Route route, Route previousRoute) {
    super.didPush(route, previousRoute);

    navigatorObservers.forEach((element) {
      _tryCatch(() {
        element.didPush(route, previousRoute);
      });
    });
  }

  @override
  void didPop(Route route, Route previousRoute) {
    super.didPop(route, previousRoute);

    navigatorObservers.forEach((element) {
      _tryCatch(() {
        element.didPop(route, previousRoute);
      });
    });
  }

  @override
  void didRemove(Route route, Route previousRoute) {
    super.didRemove(route, previousRoute);

    navigatorObservers.forEach((element) {
      _tryCatch(() {
        element.didRemove(route, previousRoute);
      });
    });
  }

  @override
  void didReplace({Route newRoute, Route oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);

    navigatorObservers.forEach((element) {
      _tryCatch(() {
        element.didReplace(newRoute: newRoute, oldRoute: oldRoute);
      });
    });
  }

  @override
  void didStartUserGesture(Route route, Route previousRoute) {
    super.didStartUserGesture(route, previousRoute);

    navigatorObservers.forEach((element) {
      _tryCatch(() {
        element.didStartUserGesture(route, previousRoute);
      });
    });
  }

  @override
  void didStopUserGesture() {
    super.didStopUserGesture();

    navigatorObservers.forEach((element) {
      _tryCatch(() {
        element.didStopUserGesture();
      });
    });
  }

  /// Catch exception for _AstronomiaNavigatorObserverForwarder on release mode, to
  /// avoid affect to the `AstronomiaNavigatorObserverDelegate._routeObserver` and
  /// others.
  void _tryCatch(VoidCallback callback) {
    if (kDebugMode) {
      callback();
    } else {
      try {
        callback();
      } catch (e) {
        // TODO(littlegnal): Should report the exception to firebase.
      }
    }
  }
}
