import 'package:astronomia/src/route_channel.dart';
import 'package:flutter/widgets.dart';

// typedef GetPopUntilNamedNameCallback = V;

class AstronomiaNavigatorSyncLifecycleObserver extends NavigatorObserver {
  AstronomiaNavigatorSyncLifecycleObserver(
      this._routeChannel, this._getPopUntilNamedName)
      : assert(_routeChannel != null),
        assert(_getPopUntilNamedName != null);

  final RouteChannel _routeChannel;
  final ValueGetter _getPopUntilNamedName;

  @override
  void didPop(Route route, Route previousRoute) {
    _routeChannel.notifyDidPop(route.settings, _getPopUntilNamedName());
    super.didPop(route, previousRoute);
  }

  @override
  void didPush(Route route, Route previousRoute) {
    _routeChannel.notifyDidPush(route.settings);
    super.didPush(route, previousRoute);
  }
}
