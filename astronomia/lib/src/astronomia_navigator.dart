import 'package:astronomia/src/observer/astronomia_navigator_observer.dart';
import 'package:astronomia/src/observer/astronomia_navigator_sync_lifecycle_observer.dart';
import 'package:astronomia/src/route_channel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

@immutable
class AstronomiaRouteSettings extends RouteSettings {
  const AstronomiaRouteSettings(
      {String name, Object arguments, this.isFirstRoute})
      : super(name: name, arguments: arguments);

  final bool isFirstRoute;

  @override
  String toString() =>
      '${objectRuntimeType(this, 'AstronomiaRouteSettings')}("$name", $arguments, $isFirstRoute)';
}

class AstronomiaNavigator extends StatefulWidget {
  const AstronomiaNavigator({
    Key key,
    @required this.child,
    @required AstronomiaNavigatorObserverDelegate navigatorObserverDelegate,
  })  : _navigatorObserverDelegate = navigatorObserverDelegate,
        assert(child is Navigator),
        super(key: key);

  final Navigator child;

  final AstronomiaNavigatorObserverDelegate _navigatorObserverDelegate;

  @override
  State<StatefulWidget> createState() => AstronomiaNavigatorState();

  static Future<T> pushNamed<T extends Object>(
    BuildContext context,
    String routeName, {
    Object arguments,
  }) {
    return AstronomiaNavigator.of(context)
        .pushNamed<T>(routeName, arguments: arguments);
  }

  static void pop<T extends Object>(BuildContext context, [T result]) {
    AstronomiaNavigator.of(context).pop<T>(result);
  }

  // TODO(littlegnal): Add unit test
  static Future<bool> maybePop<T extends Object>(BuildContext context,
      [T result]) {
    return AstronomiaNavigator.of(context).maybePop<T>(result);
  }

  static void popUntilNamed(BuildContext context, String routeName) {
    AstronomiaNavigator.of(context).popUntilNamed(routeName);
  }

  static AstronomiaNavigatorState of(BuildContext context) {
    final navigator =
        context.findAncestorStateOfType<AstronomiaNavigatorState>();
    assert(navigator != null);
    return navigator;
  }

  static TransitionBuilder builder(
      {Key astronomiaNavigatorKey,
      AstronomiaNavigatorObserverDelegate navigatorObserverDelegate}) {
    return (context, child) {
      assert(child is Navigator,
          'The AstronomiaNavigator can only work with MaterialApp, WidgetsApp at this time.');
      final navigator = child as Navigator;
      debugPrint('create NavigatorWidget');
      return AstronomiaNavigator(
        key: astronomiaNavigatorKey,
        navigatorObserverDelegate: navigatorObserverDelegate,
        child: navigator,
      );
    };
  }
}

class AstronomiaNavigatorState extends State<AstronomiaNavigator>
    implements RouteMethodHandler {
  @visibleForTesting
  AstronomiaNavigatorObserverDelegate navigatorObserverDelegate;

  RouteChannel _routeChannel;
  @visibleForTesting
  RouteChannel get routeChannel => _routeChannel;

  AstronomiaNavigatorSyncLifecycleObserver
      _astronomiaNavigatorSyncLifecycleObserver;

  ValueGetter<String> _getPopUntilNamedName;
  String _popUntilNamedName;

  @override
  void initState() {
    navigatorObserverDelegate = widget._navigatorObserverDelegate;
    _routeChannel = RouteChannel(this);
    _getPopUntilNamedName ??= () {
      return _popUntilNamedName;
    };

    _astronomiaNavigatorSyncLifecycleObserver ??=
        AstronomiaNavigatorSyncLifecycleObserver(
            _routeChannel, _getPopUntilNamedName);
    widget._navigatorObserverDelegate
        .addNavigatorObserver(_astronomiaNavigatorSyncLifecycleObserver);

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

    if (_astronomiaNavigatorSyncLifecycleObserver != null) {
      widget._navigatorObserverDelegate
          .removeNavigatorObserver(_astronomiaNavigatorSyncLifecycleObserver);
      _astronomiaNavigatorSyncLifecycleObserver = null;
    }
    if (_getPopUntilNamedName != null) {
      _getPopUntilNamedName = null;
    }
  }

  Future<T> pushNamed<T extends Object>(
    String routeName, {
    Object arguments,
  }) async {
    final result =
        await _routeChannel.intercepted<T>(routeName, arguments: arguments);
    final intercepted = result != null;

    assert(() {
      if (intercepted) {
        debugPrint(
            "The route with routeName: $routeName, arguments: $arguments has been intercepted.");
      }

      return true;
    }());

    // 原生不拦截，flutter 内部跳转
    if (!intercepted) {
      return _pushNamedInternal<T>(routeName, arguments: arguments);
    }

    return result;
  }

  void pop<T extends Object>([T result]) {
    _popInternal<T>(result);
  }

  Future<bool> maybePop<T extends Object>([T result]) {
    return _maybePopInternal<T>(result);
  }

  void popUntilNamed(String routeName) {
    popUntilNamedInternal(routeName);
  }

  /// Add a [NavigatorObserver].
  VoidCallback addNavigatorObserver(NavigatorObserver navigatorObserver) {
    navigatorObserverDelegate.addNavigatorObserver(navigatorObserver);

    return () {
      removeNavigatorObserver(navigatorObserver);
    };
  }

  /// Remove a [NavigatorObserver].
  void removeNavigatorObserver(NavigatorObserver navigatorObserver) {
    navigatorObserverDelegate.removeNavigatorObserver(navigatorObserver);
  }

  @override
  Widget build(BuildContext context) => widget.child;

  @protected
  Future<bool> _maybePopInternal<T extends Object>([T result]) async {
    return _getNavigatorState().maybePop(result);
  }

  void _popInternal<T extends Object>([T result]) {
    final navigatorState = _getNavigatorState();
    if (navigatorState == null) {
      debugPrint('NavigatorWidgetState getState null');
    }
    navigatorState.pop(result);
  }

  @protected
  @override
  void popUntilNamedInternal(String routeName) {
    final navigatorState = _getNavigatorState();
    assert(_popUntilNamedName == null);
    _popUntilNamedName = routeName;
    navigatorState.popUntil(ModalRoute.withName(routeName));
    _popUntilNamedName = null;
  }

  Future<T> _pushNamedInternal<T extends Object>(String routeName,
      {Object arguments, bool isFirstRoute = false}) {
    final navigatorState = _getNavigatorState();
    if (navigatorState == null) {
      debugPrint('NavigatorWidgetState getState null');
      return Future.value();
    }

    debugPrint("navigatorState: $navigatorState");

    return navigatorState.push<T>(_routeNamed<T>(routeName,
        arguments: arguments, isFirstRoute: isFirstRoute));
  }

  NavigatorState _getNavigatorState() {
    if (widget.child.key == null) {
      return null;
    }
    final key = widget.child.key;
    assert(key is GlobalKey<NavigatorState>);
    return (key as GlobalKey<NavigatorState>).currentState;
  }

  @protected
  @override
  Future<T> pushFromNativeInternal<T extends Object>(String routeName,
      {Object arguments}) {
    assert(arguments is Map &&
        arguments.containsKey('arguments') &&
        arguments.containsKey('isFirstRoute'));

    debugPrint(
        "pushFromNativeInternal routeName: $routeName, arguments: $arguments");
    final argMap = arguments as Map;
    final bool isFirstRoute = argMap['isFirstRoute'];
    final actualArguments = argMap['arguments'];
    return _pushNamedInternal<T>(routeName,
        arguments: actualArguments, isFirstRoute: isFirstRoute);
  }

  @protected
  @override
  Future<bool> maybePopFromNativeInternal<T extends Object>([T result]) {
    return _maybePopInternal<T>(result);
  }

  /// A copy of `Navigator._routeNamed`, which use to add the custom [RouteSettings] to the [Navigator.onGenerateRoute] function.
  // TODO(littlegnal): Find a better way to handle this, instead of copy the flutter source code.
  Route<T> _routeNamed<T>(String name,
      {@required Object arguments,
      bool isFirstRoute = false,
      bool allowNull = false}) {
    assert(name != null);
    if (allowNull && widget.child.onGenerateRoute == null) return null;
    assert(() {
      if (widget.child.onGenerateRoute == null) {
        throw FlutterError(
            'Navigator.onGenerateRoute was null, but the route named "$name" was referenced.\n'
            'To use the Navigator API with named routes (pushNamed, pushReplacementNamed, or '
            'pushNamedAndRemoveUntil), the Navigator must be provided with an '
            'onGenerateRoute handler.\n'
            'The Navigator was:\n'
            '  $this');
      }
      return true;
    }());
    final AstronomiaRouteSettings settings = AstronomiaRouteSettings(
        name: name, arguments: arguments, isFirstRoute: isFirstRoute);
    var route = widget.child.onGenerateRoute(settings) as Route<T>;
    if (route == null && !allowNull) {
      assert(() {
        if (widget.child.onUnknownRoute == null) {
          throw FlutterError.fromParts(<DiagnosticsNode>[
            ErrorSummary(
                'Navigator.onGenerateRoute returned null when requested to build route "$name".'),
            ErrorDescription(
                'The onGenerateRoute callback must never return null, unless an onUnknownRoute '
                'callback is provided as well.'),
            DiagnosticsProperty<NavigatorState>(
                'The Navigator was', _getNavigatorState(),
                style: DiagnosticsTreeStyle.errorProperty),
          ]);
        }
        return true;
      }());
      route = widget.child.onUnknownRoute(settings) as Route<T>;
      assert(() {
        if (route == null) {
          throw FlutterError.fromParts(<DiagnosticsNode>[
            ErrorSummary(
                'Navigator.onUnknownRoute returned null when requested to build route "$name".'),
            ErrorDescription(
                'The onUnknownRoute callback must never return null.'),
            DiagnosticsProperty<NavigatorState>(
                'The Navigator was', _getNavigatorState(),
                style: DiagnosticsTreeStyle.errorProperty),
          ]);
        }
        return true;
      }());
    }
    assert(route != null || allowNull);
    return route;
  }
}

/// An mixin that implement the [RouteAware], so you can override the `didPush`, `didPop` etc,
/// to receive the navigator lifecycle callback.
///
/// NOTE: The `AstronomiaRouteAwareMixin` subscribe/unsubscribe the [RouteAware] internally, which
///  will cause unexpected `didChangeDependencies` called when the route be poped, so you can judge the [routeDidPop]
/// to avoid some operation when `didChangeDependencies` be called again. e.g.,
///
/// ```dart
/// class _DemoPage extends StatefulWidget {
///   @override
///   State<StatefulWidget> createState() => _DemoPageState();
/// }
///
/// class _DemoPageState extends State<_DemoPage> with AstronomiaRouteAwareMixin {
///
///   @override
///   void didChangeDependencies() {
///     super.didChangeDependencies();
///     if (!routeDidPop) {
///       // Do something...
///     }
///   }
/// ```
mixin AstronomiaRouteAwareMixin<T extends StatefulWidget> on State<T>
    implements RouteAware {
  bool _routeDidPop = false;

  bool get routeDidPop => _routeDidPop;

  AstronomiaNavigatorObserverDelegate _astronomiaNavigatorObserverDelegate;

  @mustCallSuper
  @override
  void didChangeDependencies() {
    _astronomiaNavigatorObserverDelegate =
        AstronomiaNavigator.of(context).navigatorObserverDelegate;
    _astronomiaNavigatorObserverDelegate.subscribeRouteAware(
        this, ModalRoute.of(context));
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _astronomiaNavigatorObserverDelegate.unsubscribeRouteAware(this);
    super.dispose();
  }

  @override
  void didPop() {
    _routeDidPop = true;
  }

  @override
  void didPopNext() {}

  @override
  void didPush() {}

  @override
  void didPushNext() {}
}
