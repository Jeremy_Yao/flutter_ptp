import 'package:astronomia/astronomia.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

abstract class RouteMethodHandler {
  void popUntilNamedInternal(String routeName);

  Future<T> pushFromNativeInternal<T extends Object>(
    String routeName, {
    Object arguments,
  });

  Future<bool> maybePopFromNativeInternal<T extends Object>([T result]);
}

class RouteChannel {
  RouteChannel(this._routeMethodHandler) {
    astronomiaChannel.setMethodCallHandler(methodCallHandler);
  }

  @visibleForTesting
  final MethodChannel astronomiaChannel = const MethodChannel('astronomia');

  final RouteMethodHandler _routeMethodHandler;

  @visibleForTesting
  Future<dynamic> methodCallHandler(MethodCall call) {
    debugPrint(
        'RouteChannel recieve call: ${call.method}, arguments: ${call.arguments}');
    switch (call.method) {
      case 'popUntilNamed':
        debugPrint('RouteChannel popUntilNamed routeName: ${call.arguments}');
        _routeMethodHandler.popUntilNamedInternal(call.arguments);
        return Future.value(true);
      case 'pushFromNative':
        final map = call.arguments;
        final routeName = map['routeName'];
        return _routeMethodHandler.pushFromNativeInternal(routeName,
            arguments: map);
      case 'maybePopFromNative':
        _routeMethodHandler.maybePopFromNativeInternal();
        break;
      case 'popUntilNamedFromNative':
        _routeMethodHandler.popUntilNamedInternal(call.arguments);
        return Future.value(true);
      default:
        break;
    }
    return Future.value(false);
  }

  void notifyDidPop(RouteSettings settings, String popUntilNamedName) {
    if (settings is! AstronomiaRouteSettings) {
      return;
    }
    final args = {
      'routeName': settings.name,
      'arguments':
          isAcceptableType(settings.arguments) ? settings.arguments : null,
      'popUntilNamedName': popUntilNamedName
    };

    // TODO(RUIJIN): channel 暂时不处理
    //astronomiaChannel.invokeMethod('didPop', args);
  }

  void notifyDidPush(RouteSettings settings) {
    if (settings is! AstronomiaRouteSettings && settings.name != '/') {
      return;
    }

    bool isFirstRoute = false;
    if (settings is AstronomiaRouteSettings) {
      isFirstRoute = settings.isFirstRoute;
    } else if (settings.name == '/') {
      // We force mark `isFirstRoute = true` for route `/`.
      isFirstRoute = true;
    }

    final arguments = {
      'routeName': settings.name,
      'arguments':
          isAcceptableType(settings.arguments) ? settings.arguments : null,
      'isFirstRoute': isFirstRoute
    };

    // TODO(RUIJIN): channel 暂时不处理
    //astronomiaChannel.invokeMethod('didPush', arguments);
  }

  Future<T> intercepted<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    final args = {
      'routeName': routeName,
      'arguments': isAcceptableType(arguments) ? arguments : null
    };

    // TODO(RUIJIN): channel 暂时使用死数据
    return Future.value(null);
    return astronomiaChannel.invokeMethod<T>('intercept', args);
  }

  @visibleForTesting
  bool isAcceptableType(Object arguments) {
    if (arguments == null) {
      return true;
    } else if (arguments is num) {
      return true;
    } else if (arguments is bool) {
      return true;
    } else if (arguments is String) {
      return true;
    } else if (arguments is List) {
      var isAcceptableList = true;
      for (final v in arguments) {
        isAcceptableList &= isAcceptableType(v);
        if (!isAcceptableList) break;
      }

      return isAcceptableList;
    } else if (arguments is Map) {
      var isAcceptableMap = true;
      for (final v in arguments.values) {
        isAcceptableMap &= isAcceptableType(v);
        if (!isAcceptableMap) break;
      }

      return isAcceptableMap;
    } else {
      return false;
    }
  }
}
