import 'package:astronomia/astronomia.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class _TestApp extends StatelessWidget {
  const _TestApp(this.astronomiaNavigatorKey);

  final Key astronomiaNavigatorKey;

  @override
  Widget build(BuildContext context) {
    final navigatorObserverDelegate = AstronomiaNavigatorObserverDelegate();
    return MaterialApp(
      home: _TestHomePage(),
      // TODO(littlegnal): Remove this when this issuse fixed:
      // https://bitbucket.org/klook/klook-app-flutter/issues/7/astronomia-unsupported-operation-cannot
      // ignore: prefer_const_literals_to_create_immutables
      navigatorObservers: navigatorObserverDelegate.observers,
      builder: AstronomiaNavigator.builder(
          astronomiaNavigatorKey: astronomiaNavigatorKey,
          navigatorObserverDelegate: navigatorObserverDelegate),
      onGenerateRoute: (routeSettings) {
        debugPrint('onGenerateRoute routeSettings: $routeSettings');
        final argument = routeSettings.arguments;
        return MaterialPageRoute(
            settings: routeSettings,
            builder: (context) => _TestPage(routeSettings.name, argument));
      },
    );
  }
}

class _TestHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ElevatedButton(
          onPressed: () async {
            AstronomiaNavigator.of(context).pushNamed('/page/1');
          },
          child: const Text('push /page/1'),
        ),
        ElevatedButton(
          onPressed: () async {
            AstronomiaNavigator.of(context).pushNamed('/page/1', arguments: 1);
          },
          child: const Text('push /page/1, argument: 1'),
        )
      ],
    );
  }
}

class _TestPage extends StatefulWidget {
  const _TestPage(this._routeName, this._argument);

  final String _routeName;
  final dynamic _argument;

  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<_TestPage> {
  String _pushText;
  String _resultText = '';

  @override
  void initState() {
    super.initState();

    _pushText = 'route: ${widget._routeName}, argument: ${widget._argument}';
  }

  @override
  Widget build(BuildContext context) {
    final index = (int.tryParse(widget._routeName.split('/').last) ?? 0) + 1;
    final nextPageRoute = '/page/$index';
    final nextPageRouteArgument = widget._argument != null
        ? '$nextPageRoute:${widget._argument}'
        : nextPageRoute;
    return Column(
      children: <Widget>[
        Text(_pushText),
        Text(_resultText),
        ElevatedButton(
          onPressed: () async {
            final result = await AstronomiaNavigator.of(context)
                .pushNamed(nextPageRoute, arguments: nextPageRouteArgument);
            setState(() {
              _resultText = result?.toString() ?? 'Poped but no result';
            });
          },
          child: Text('push $nextPageRoute, arguments: $nextPageRouteArgument'),
        ),
        ElevatedButton(
          onPressed: () async {
            AstronomiaNavigator.of(context).pop();
          },
          child: const Text('pop'),
        ),
        ElevatedButton(
          onPressed: () async {
            AstronomiaNavigator.of(context)
                .pop('pop result: ${widget._routeName}');
          },
          child: Text('pop result: ${widget._routeName}'),
        ),
        ElevatedButton(
          onPressed: () async {
            AstronomiaNavigator.of(context).popUntilNamed('/page/1');
          },
          child: const Text('popUntilNamed /page/1'),
        )
      ],
    );
  }
}

class _ObjectTypeParam {
  const _ObjectTypeParam(this.stringParam, this.intParam);
  final String stringParam;
  final int intParam;

  @override
  String toString() {
    return '[stringParam: $stringParam, intParam: $intParam]';
  }
}

typedef InterceptedCallback<T> = Future<T> Function(
  String routeName, {
  Object arguments,
});

void main() {
  GlobalKey<AstronomiaNavigatorState> astronomiaNavigatorState;

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    astronomiaNavigatorState = GlobalKey<AstronomiaNavigatorState>();
  });

  tearDown(() {
    astronomiaNavigatorState = null;
  });

  Finder raisedButtonWidgetFinder(String textData) {
    return find.byWidgetPredicate((widget) =>
        widget is ElevatedButton &&
        widget.child is Text &&
        (widget.child as Text).data == textData);
  }

  /// Simulate the (invoke native channel method) -> (invoke flutter channel method) call flow.
  void _mockRouteChannelMethodCall<T>(
      InterceptedCallback<T> interceptedCallback,
      {VoidCallback onDidPush,
      VoidCallback onDidPop}) {
    final routeChannel = astronomiaNavigatorState.currentState.routeChannel;
    routeChannel.astronomiaChannel.setMockMethodCallHandler((call) {
      switch (call.method) {
        case 'didPop':
          onDidPop?.call();
          break;
        case 'didPush':
          onDidPush?.call();
          break;
        case 'intercept':
          assert(call.arguments is Map);
          final routeName = call.arguments['routeName'];
          final arguments = call.arguments['arguments'];
          return interceptedCallback(routeName, arguments: arguments);
          break;
        default:
          break;
      }
      return Future.value();
    });
  }

  void _resetMockRouteChannelMethodCall() {
    final routeChannel = astronomiaNavigatorState.currentState.routeChannel;
    routeChannel.astronomiaChannel.setMockMethodCallHandler(null);
  }

  group('AstronomiaNavigator', () {
    testWidgets('pushNamed routeName = /page/1', (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      bool isDidPushCalled = false;
      bool isDidPopCalled = false;
      _mockRouteChannelMethodCall<String>(interceptedCallback, onDidPush: () {
        isDidPushCalled = true;
      }, onDidPop: () {
        isDidPopCalled = true;
      });

      await tester.tap(raisedButtonWidgetFinder('push /page/1'));

      await tester.pumpAndSettle();

      expect(isDidPushCalled, true);
      expect(isDidPopCalled, false);

      expect(find.text('route: /page/1, argument: null'), findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets('pushNamed routeName = /page/1, arguments = 1',
        (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall<String>(interceptedCallback);

      await tester.tap(raisedButtonWidgetFinder('push /page/1, argument: 1'));

      await tester.pumpAndSettle();

      expect(find.text('route: /page/1, argument: 1'), findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets(
        'pushNamed routeName = /page/1, arguments = _ObjectTypeParam("aString", 10)',
        (WidgetTester tester) async {
      final navigatorObserverDelegate = AstronomiaNavigatorObserverDelegate();
      await tester.pumpWidget(MaterialApp(
        home: Builder(builder: (context) {
          return ElevatedButton(
            onPressed: () async {
              AstronomiaNavigator.of(context).pushNamed('/page/1',
                  arguments: const _ObjectTypeParam('aString', 10));
            },
            child: const Text('push'),
          );
        }),
        // TODO(littlegnal): Remove this when this issuse fixed:
        // https://bitbucket.org/klook/klook-app-flutter/issues/7/astronomia-unsupported-operation-cannot
        // ignore: prefer_const_literals_to_create_immutables
        navigatorObservers: navigatorObserverDelegate.observers,

        builder: AstronomiaNavigator.builder(
            astronomiaNavigatorKey: astronomiaNavigatorState,
            navigatorObserverDelegate: navigatorObserverDelegate),
        onGenerateRoute: (routeSettings) {
          debugPrint('onGenerateRoute routeSettings: $routeSettings');
          final argument = routeSettings.arguments;
          return MaterialPageRoute(
              settings: routeSettings,
              builder: (context) => Builder(builder: (context) {
                    return Text(argument.toString());
                  }));
        },
      ));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall(interceptedCallback);

      await tester.tap(raisedButtonWidgetFinder('push'));
      await tester.pumpAndSettle();

      expect(find.text('[stringParam: aString, intParam: 10]'), findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets(
        'pushNamed routeName = /page/1, arguments = {"objectTypeWithMap": _ObjectTypeParam("aString", 10)}',
        (WidgetTester tester) async {
      final navigatorObserverDelegate = AstronomiaNavigatorObserverDelegate();
      await tester.pumpWidget(MaterialApp(
        home: Builder(builder: (context) {
          return ElevatedButton(
            onPressed: () async {
              AstronomiaNavigator.of(context).pushNamed('/page/1', arguments: {
                'objectTypeWithMap': const _ObjectTypeParam('aString', 10)
              });
            },
            child: const Text('push'),
          );
        }),
        // TODO(littlegnal): Remove this when this issuse fixed:
        // https://bitbucket.org/klook/klook-app-flutter/issues/7/astronomia-unsupported-operation-cannot
        // ignore: prefer_const_literals_to_create_immutables
        navigatorObservers: navigatorObserverDelegate.observers,
        builder: AstronomiaNavigator.builder(
            astronomiaNavigatorKey: astronomiaNavigatorState,
            navigatorObserverDelegate: navigatorObserverDelegate),
        onGenerateRoute: (routeSettings) {
          final argument = routeSettings.arguments;
          return MaterialPageRoute(
              settings: routeSettings,
              builder: (context) => Builder(builder: (context) {
                    final t = argument.toString();
                    return Text(t);
                  }));
        },
      ));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall(interceptedCallback);

      await tester.tap(raisedButtonWidgetFinder('push'));
      await tester.pumpAndSettle();

      expect(
          find.text(
              '{objectTypeWithMap: [stringParam: aString, intParam: 10]}'),
          findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets(
        'pushNamed routeName = /page/1, arguments = _ObjectTypeParam("aString", 10), then pop',
        (WidgetTester tester) async {
      final navigatorObserverDelegate = AstronomiaNavigatorObserverDelegate();
      await tester.pumpWidget(MaterialApp(
        home: Builder(builder: (context) {
          return ElevatedButton(
            onPressed: () async {
              AstronomiaNavigator.of(context).pushNamed('/page/1',
                  arguments: const _ObjectTypeParam('aString', 10));
            },
            child: const Text('push'),
          );
        }),
        // TODO(littlegnal): Remove this when this issuse fixed:
        // https://bitbucket.org/klook/klook-app-flutter/issues/7/astronomia-unsupported-operation-cannot
        // ignore: prefer_const_literals_to_create_immutables
        navigatorObservers: navigatorObserverDelegate.observers,
        builder: AstronomiaNavigator.builder(
            astronomiaNavigatorKey: astronomiaNavigatorState,
            navigatorObserverDelegate: navigatorObserverDelegate),
        onGenerateRoute: (routeSettings) {
          debugPrint('onGenerateRoute routeSettings: $routeSettings');
          // final argument = routeSettings.arguments;
          return MaterialPageRoute(
              settings: routeSettings,
              builder: (context) => Builder(builder: (context) {
                    return Builder(builder: (context) {
                      return ElevatedButton(
                        onPressed: () async {
                          AstronomiaNavigator.of(context).pop();
                          // AstronomiaNavigator.of(context).pushNamed('/page/1',
                          //     arguments: const _ObjectTypeParam('aString', 10));
                        },
                        child: const Text('pop'),
                      );
                    });
                  }));
        },
      ));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall(interceptedCallback);

      await tester.tap(raisedButtonWidgetFinder('push'));
      await tester.pumpAndSettle();

      await tester.tap(raisedButtonWidgetFinder('pop'));
      await tester.pumpAndSettle();

      _resetMockRouteChannelMethodCall();
    });

    testWidgets('pop', (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      bool isDidPushCalled = false;
      bool isDidPopCalled = false;
      _mockRouteChannelMethodCall<String>(interceptedCallback, onDidPush: () {
        isDidPushCalled = true;
      }, onDidPop: () {
        isDidPopCalled = true;
      });

      await tester.tap(raisedButtonWidgetFinder('push /page/1, argument: 1'));
      await tester.pumpAndSettle();

      await tester
          .tap(raisedButtonWidgetFinder('push /page/2, arguments: /page/2:1'));
      await tester.pumpAndSettle();

      await tester.tap(raisedButtonWidgetFinder('pop'));
      await tester.pumpAndSettle();

      expect(isDidPushCalled, true);
      expect(isDidPopCalled, true);

      expect(find.text('Poped but no result'), findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets('pop with result', (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall<String>(interceptedCallback);

      await tester.tap(raisedButtonWidgetFinder('push /page/1, argument: 1'));
      await tester.pumpAndSettle();

      await tester
          .tap(raisedButtonWidgetFinder('push /page/2, arguments: /page/2:1'));
      await tester.pumpAndSettle();

      await tester.tap(raisedButtonWidgetFinder('pop result: /page/2'));
      await tester.pumpAndSettle();

      expect(find.text('pop result: /page/2'), findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets('popUntilNamed', (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall<String>(interceptedCallback);

      await tester.tap(raisedButtonWidgetFinder('push /page/1, argument: 1'));
      await tester.pumpAndSettle();

      await tester
          .tap(raisedButtonWidgetFinder('push /page/2, arguments: /page/2:1'));
      await tester.pumpAndSettle();

      await tester.tap(raisedButtonWidgetFinder(
          'push /page/3, arguments: /page/3:/page/2:1'));
      await tester.pumpAndSettle();

      await tester.tap(raisedButtonWidgetFinder('popUntilNamed /page/1'));
      await tester.pumpAndSettle();

      astronomiaNavigatorState.currentState.navigatorObserverDelegate
          .addNavigatorObserver(null);

      expect(find.text('route: /page/1, argument: 1'), findsOneWidget);
      _resetMockRouteChannelMethodCall();
    });

    testWidgets('addNavigatorObserver', (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall<String>(interceptedCallback);

      final forwarder = astronomiaNavigatorState.currentState
          .navigatorObserverDelegate.astronomiaNavigatorObserverForwarder;

      // The initial one is AstronomiaNavigatorState._astronomiaNavigatorSyncLifecycleObserver
      expect(forwarder.navigatorObservers.length, 1);

      astronomiaNavigatorState.currentState
          .addNavigatorObserver(NavigatorObserver());

      expect(forwarder.navigatorObservers.length, 2);

      _resetMockRouteChannelMethodCall();
    });

    testWidgets('removeNavigatorObserver', (WidgetTester tester) async {
      await tester.pumpWidget(_TestApp(astronomiaNavigatorState));
      final interceptedCallback = (String routeName, {Object arguments}) async {
        return null;
      };
      _mockRouteChannelMethodCall<String>(interceptedCallback);

      final forwarder = astronomiaNavigatorState.currentState
          .navigatorObserverDelegate.astronomiaNavigatorObserverForwarder;

      final navigatorObserver = NavigatorObserver();
      astronomiaNavigatorState.currentState
          .addNavigatorObserver(navigatorObserver);
      expect(forwarder.navigatorObservers.length, 2);

      astronomiaNavigatorState.currentState
          .removeNavigatorObserver(navigatorObserver);
      expect(forwarder.navigatorObservers.length, 1);

      _resetMockRouteChannelMethodCall();
    });
  });
}
