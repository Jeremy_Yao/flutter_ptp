import 'package:astronomia/src/route_channel.dart';
import 'package:flutter_test/flutter_test.dart';

class _ObjectTypeParam {
  const _ObjectTypeParam(this.stringParam, this.intParam);
  final String stringParam;
  final int intParam;

  @override
  String toString() {
    return '_ObjectTypeParam [stringParam: $stringParam, intParam: $intParam]';
  }
}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  RouteChannel routeChannel;

  setUp(() {
    routeChannel = RouteChannel(null);
  });
  group('isAcceptableType', () {
    test('null', () {
      expect(routeChannel.isAcceptableType(null), true);
    });

    test('Map with null value', () {
      expect(routeChannel.isAcceptableType({'key1': 'value1', 'key2': null}),
          true);
    });

    test('num', () {
      expect(routeChannel.isAcceptableType(10), true);
    });

    test('String', () {
      expect(routeChannel.isAcceptableType('aString'), true);
    });

    test('bool', () {
      expect(routeChannel.isAcceptableType(true), true);
    });

    test('List', () {
      expect(routeChannel.isAcceptableType(['aString']), true);
    });

    test('List<_ObjectTypeParam>', () {
      expect(
          routeChannel
              .isAcceptableType([const _ObjectTypeParam('aString', 10)]),
          false);
    });

    test('Map', () {
      expect(routeChannel.isAcceptableType({'key': '10'}), true);
    });

    test('Map<_ObjectTypeParam>', () {
      expect(
          routeChannel.isAcceptableType(
              {'objectType': const _ObjectTypeParam('aString', 10)}),
          false);
    });
  });
}
