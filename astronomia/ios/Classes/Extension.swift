//
//  UINavigationControllerExtension.swift
//  astronomia
//
//  Created by klook on 2020/5/8.
//

import Foundation

extension UIApplication {
  func topNavigationController() -> UINavigationController? {
    var topNavigationController: UINavigationController?
    var topViewController = delegate?.window??.rootViewController
    while true {
      if topViewController?.isKind(of: UINavigationController.self) ?? false {
        topNavigationController = topViewController as? UINavigationController
      }
      if let presentedViewController = topViewController?.presentedViewController {
        topViewController = presentedViewController
      } else if (topViewController?.isKind(of: UITabBarController.self) ?? false) {
        let tabBarController = topViewController as? UITabBarController
        topViewController = tabBarController?.selectedViewController
      } else {
        break
      }
    }
    return topNavigationController
  }
}

extension Array where Element: Equatable {
  mutating func remove(object: Element) {
    guard let index = firstIndex(of: object) else { return }
    remove(at: index)
  }
  
  mutating func removeUntil(include: Bool = false, predicate: (Element) -> Bool) {
    var size = count
    while (size > 0) {
      let lastRoute = last!
      if (predicate(lastRoute)) {
        if (include) {
          remove(object: lastRoute)
        }
        break
      } else {
        remove(object: lastRoute)
      }

      size -= 1
    }
  }
  
  func getOrNull(index: Int) -> Element? {
    guard index <= count - 1 && index >= 0 else {
      return nil
    }
    
    return self[index]
  }
}

extension Optional where Wrapped == Any {
  func equals(_ other : Any?) -> Bool {
      guard self is AnyHashable else { return false }
      guard other is AnyHashable else { return false }
      return (self as! AnyHashable) == (other as! AnyHashable)
  }
}
