//
//  AstronomiaFlutterEngine.swift
//  astronomia
//
//  Created by klook on 2020/5/7.
//

import Flutter
import Foundation

class AstronomiaFlutterEngine {
  let engine: FlutterEngine
  private(set) var routeChannel: RouteChannel?

  init(flutterEngine: FlutterEngine? = nil) {
    if flutterEngine != nil {
      engine = flutterEngine!
    } else {
      engine = FlutterEngine.init(name: "astronomia")
      engine.run()
    }

    routeChannel = RouteChannel(engine.binaryMessenger)
  }

  func get() -> FlutterEngine {
    return engine
  }

  func attachTo(viewController: AstronomiaFlutterViewController) {
    if engine.viewController != viewController {
      engine.viewController = viewController
    }
  }
}
