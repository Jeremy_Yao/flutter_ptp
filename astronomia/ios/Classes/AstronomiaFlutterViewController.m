//
//  AstronomiaFlutterViewController.m
//  astronomia
//
//  Created by zhao on 2020/7/12.
//

#import "AstronomiaFlutterViewController.h"
#import <astronomia/astronomia-Swift.h>

@interface AstronomiaFlutterViewController ()

@end

@implementation AstronomiaFlutterViewController

- (void)viewWillAppear:(BOOL)animated {  //For new page we should attach flutter view in view will appear
    //for better performance.
    [self attatchFlutterEngine];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    //Ensure flutter view is attached.
    [self attatchFlutterEngine];
    [self updateSurface];
    [super viewDidAppear:animated];
}

- (void)attatchFlutterEngine {
    [AstronomiaNavigator.shared attachEngineToViewController: self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    /* 父类实现如下
     - (void)viewDidDisappear:(BOOL)animated {
       TRACE_EVENT0("flutter", "viewDidDisappear");
       if ([_engine.get() viewController] == self) {
         [self surfaceUpdated:NO];
         [[_engine.get() lifecycleChannel] sendMessage:@"AppLifecycleState.paused"];
         [self flushOngoingTouches];
         [_engine.get() notifyLowMemory];
       }

       [super viewDidDisappear:animated];
     }
     */
    if (self.engine.viewController == self) {
        // flutterVC 会在viewDidDisappear判断engine.viewController == self，然后做一些事情，所以不能在viewWillDisappear 中detach
        // 如果是从flutterVC跳转到flutterVC，engine已经attach到新的flutterVC，无需下面这一步
        [AstronomiaNavigator.shared detach];
    } else {
        // flutterVC在viewWillDisappear和viewWillAppear都发送AppLifecycleState.inactive事件
        // flutter端不能依赖inactive事件，所以这里要把AppLifecycleState.paused 事件发送过去
        [self.engine.lifecycleChannel sendMessage:@"AppLifecycleState.paused"];
    }
}

- (void)updateSurface {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wundeclared-selector"
    [self performSelector:@selector(surfaceUpdated:) withObject:@YES];
    #pragma clang diagnostic pop
}
- (void)didPopToFirstRoute {}
- (void)didPushFirstRoute {}
- (void)didPushFirstRouteNext {}

@end
