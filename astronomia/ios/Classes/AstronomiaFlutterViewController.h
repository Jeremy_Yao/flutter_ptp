//
//  AstronomiaFlutterViewController.h
//  astronomia
//
//  Created by zhao on 2020/7/12.
//

#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

@interface AstronomiaFlutterViewController : FlutterViewController

/// 在当前VC pop到第一个flutter页面时会被调用
- (void)didPopToFirstRoute;

/// 在当前VC push第一个flutter页面时会被调用
- (void)didPushFirstRoute;

/// 在当前VC push第二个flutter页面时会被调用
- (void)didPushFirstRouteNext;

@end

NS_ASSUME_NONNULL_END
