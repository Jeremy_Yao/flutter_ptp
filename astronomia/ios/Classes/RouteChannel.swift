//
//  RouteChannel.swift
//  astronomia
//
//  Created by klook on 2020/5/7.
//

import Flutter
import Foundation

typealias RoutePopCallback = (_ result: Any?) -> Void

class RouteChannel {
  private let channel: FlutterMethodChannel

  init(_ binaryMessenger: FlutterBinaryMessenger) {
    channel = FlutterMethodChannel(name: "astronomia", binaryMessenger: binaryMessenger)
    channel.setMethodCallHandler { (call, result) in
      switch call.method {
      case "didPop":
        if let dict = call.arguments as? [String: Any?],
          let routeName = dict["routeName"] as? String,
          let arguments = dict["arguments"],
          let popUntilNamedName = dict["popUntilNamedName"] as? String? {
          AstronomiaNavigator.shared.flutterDidPop(
            routeName: routeName,
            arguments: arguments,
            popUntilNamedName: popUntilNamedName)
        }
        
      case "didPush":
        if let dict = call.arguments as? [String: Any?],
          let routeName = dict["routeName"] as? String,
          let arguments = dict["arguments"],
          let isFirstRoute = (dict["isFirstRoute"] as? Bool?) ?? false {
            AstronomiaNavigator.shared.flutterDidPush(
              routeName: routeName,
              arguments: arguments,
              isFirstRoute: isFirstRoute)
        }
        
      case "intercept":
        if let dict = call.arguments as? [String: Any?],
          let routeName = dict["routeName"] as? String,
          let arguments = dict["arguments"] {
          AstronomiaNavigator.shared.nativeRouteIntercept(
            routeName: routeName,
            arguments: arguments,
            popRouteResult: result)
        }

      default:
        break
      }
    }
  }

  func popUntilNamed(routeName: String, callback: @escaping RoutePopCallback) {
    channel.invokeMethod(
      "popUntilNamed",
      arguments: routeName,
      result: callback)
  }
  
  func pushFromNative(routeName: String, arguments: Any?, isFirstRoute: Bool, callback: @escaping RoutePopCallback) {
    let arg = ["routeName" : routeName, "arguments" : arguments, "isFirstRoute" : isFirstRoute]
    channel.invokeMethod("pushFromNative", arguments: arg, result: callback)
  }
  
  func popUntilNamedFromNative(routeName: String, callback: @escaping RoutePopCallback) {
    channel.invokeMethod(
      "popUntilNamedFromNative",
      arguments: routeName,
      result: callback)
  }

  func maybePopFromNative() {
    channel.invokeMethod("maybePopFromNative", arguments: nil, result: nil)
  }
}
