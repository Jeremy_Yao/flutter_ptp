//
//  AstronomiaNavigator.swift
//  astronomia
//
//  Created by klook on 2020/5/7.
//

import Foundation

public typealias RouteInterceptor = (_ routeName: String, _ argumnet: Any?) -> Bool
public typealias RoutePopResultCallback = (_: Any?) -> Void

@objcMembers public class AstronomiaNavigator: NSObject {
  var shouldWaitForFlutterEngine = true
  public var topNavigationControllerBuilder: () -> UINavigationController? = UIApplication
    .shared
    .topNavigationController

  public var flutterViewControllerBuilder:
    (_ engine: FlutterEngine, _ routeName: String, _ argumnet: Any?) ->
    AstronomiaFlutterViewController = { (engine, routeName, arguments) in
      let viewController = AstronomiaFlutterViewController(
        engine: engine, nibName: nil, bundle: nil)
      viewController.hidesBottomBarWhenPushed = true
      return viewController
    }


  private var topNavigationController: UINavigationController? {
    return topNavigationControllerBuilder()
  }

  private var routeInterceptors = [RouteInterceptor]()
  private lazy var pageRoutes = [PageRoute]()

  public func addRouteInterceptor(interceptor: @escaping RouteInterceptor) {
    routeInterceptors.append(interceptor)
  }

  private var lazyEngine: AstronomiaFlutterEngine? = nil

  var engine: AstronomiaFlutterEngine {
    return lazyEngine!
  }

  // fix crash when flutter vc is not the vc of engine, but vc send message to engine
  private var dumpViewController: AstronomiaFlutterViewController?

  public func attachEngineTo(viewController: AstronomiaFlutterViewController) {
    engine.attachTo(viewController: viewController)
  }

  public func detach() {
    engine.attachTo(viewController: dumpViewController!)
  }
  
  public static let shared: AstronomiaNavigator = {
    let navigator = AstronomiaNavigator()
    return navigator
  }()

  public func startup(flutterEngine: FlutterEngine? = nil) {
    lazyEngine = AstronomiaFlutterEngine.init(flutterEngine: flutterEngine)
    dumpViewController = AstronomiaFlutterViewController(engine: engine.get(), nibName: nil, bundle: nil)
    dumpViewController?.loadViewIfNeeded()
  }

  public func push(
    _ routeName: String, arguments: Any? = nil, popResultCallback: RoutePopResultCallback? = nil
  ) {
    pushFlutter(routeName, arguments: arguments, popResultCallback: popResultCallback)
  }

  private func pushFlutter(
    _ routeName: String, arguments: Any? = nil, popResultCallback: RoutePopResultCallback? = nil
  ) {
    let topViewController = topNavigationController?.topViewController
    if topViewController is AstronomiaFlutterViewController && (
      topNavigationController?.viewControllers.count ?? 0
    ) > 1 {
      let callback = { (result: Any?) -> Void in
        popResultCallback?(result)
      }
      engine.routeChannel?.pushFromNative(routeName: routeName, arguments: arguments, isFirstRoute: false, callback: callback)
    } else {
      let viewController = flutterViewControllerBuilder(engine.get(), routeName, arguments)
      let callback = { (result: Any?) -> Void in
        popResultCallback?(result)
      }
      if self.shouldWaitForFlutterEngine {
        viewController.setFlutterViewDidRenderCallback {
          self.engine.routeChannel?.pushFromNative(routeName: routeName, arguments: arguments, isFirstRoute: true, callback: callback)
          self.shouldWaitForFlutterEngine = false
        }
      } else {
        self.engine.routeChannel?.pushFromNative(routeName: routeName, arguments: arguments, isFirstRoute: true, callback: callback)
      }
      self.topNavigationController?.pushViewController(viewController, animated: true)
    }
  }
  
  func flutterDidPop(routeName: String, arguments: Any?, popUntilNamedName: String?) {
    if pageRoutes.isEmpty {
      debugPrint("The pageRoutes is empty, skip this route, " +
        "routeName: \(routeName), " +
        "arguments: \(String(describing: arguments)), " +
        "popUntilNamedName: \(String(describing: popUntilNamedName))")
      return
    }
    
    let viewControllers = self.topNavigationController?.viewControllers ?? []
    if viewControllers.isEmpty {
      debugPrint("The topNavigationController.viewControllers is empty, skip this route, " +
        "routeName: \(routeName), " +
        "arguments: \(String(describing: arguments)), " +
        "popUntilNamedName: \(String(describing: popUntilNamedName))")
      return
    }
  
    
    let currentRoute = pageRoutes.last!
    let previousRoute = pageRoutes.getOrNull(index: pageRoutes.count - 2)

    let rootRouter = pageRoutes.first!
    let isPopToRoot = popUntilNamedName != nil && rootRouter.routeSettings.routeName == popUntilNamedName
    
    let isSameRoute = currentRoute.routeSettings.routeName == routeName
    if !isSameRoute {
      debugPrint("The top route: \(currentRoute), is not the same route with " +
        "routeName: \(routeName), " +
        "arguments: \(String(describing: arguments)), " +
        "popUntilNamedName: \(String(describing: popUntilNamedName))")
      return
    }
    
    var targetViewControllerIndex = viewControllers.count - 1

    while (targetViewControllerIndex >= 0) {
      let viewcontroller = viewControllers[targetViewControllerIndex]
      if !(viewcontroller is AstronomiaFlutterViewController) {
        targetViewControllerIndex -= 1
        continue
      }

      break
    }

    if (currentRoute.isFirst) {
      targetViewControllerIndex -= 1
    }


    if popUntilNamedName != nil && popUntilNamedName == previousRoute?.routeSettings.routeName {
      while (targetViewControllerIndex >= 0) {
        let viewcontroller = viewControllers[targetViewControllerIndex]
        if !(viewcontroller is AstronomiaFlutterViewController) {
          targetViewControllerIndex -= 1
          continue
        }

        break
      }
    }

    if (targetViewControllerIndex != viewControllers.count - 1) {
      let newViewControllers = Array.init(viewControllers.dropLast(
        viewControllers.count - 1 - targetViewControllerIndex))
      if (newViewControllers.count > 0 && !isPopToRoot) {
        // sleeping for 0.3 seconds after detach to resolve issue of seeing last screen after pop
        self.detach()
        usleep(300000)
        // iOS 14 原生和flutter混合几次后，来回进出几次flutter时setViewControllers把整个栈清空了，因此改用popToViewController
        self.topNavigationController?.popToViewController(newViewControllers.last!, animated: true)
      }
    }

    pageRoutes.removeLast()
    if (!pageRoutes.isEmpty) {
      // 当pop的PageRoute为VC的第二个页面时，VC回到了第一个 PageRoute ，触发VC的didPushFirstRoute
      if (!currentRoute.isFirst && pageRoutes.last?.isFirst == true) {
        if let viewController = topNavigationController?.topViewController as? AstronomiaFlutterViewController {
          viewController.didPopToFirstRoute()
        }
      }
    }
    debugPrint("After didPop: ")
    debugPrint( { () -> String in
      let pageStackOutput = pageRoutes.map { (pageRoute) -> String in
        "routeName: \(pageRoute.routeSettings.routeName), arguments: \(String(describing: pageRoute.routeSettings.arguments)), isFirstRoute: \(pageRoute.isFirst)"
      }.joined(separator: "\n")

      return pageStackOutput
    }())
  }

  func flutterDidPush(routeName: String, arguments: Any?, isFirstRoute: Bool) {
    if (isFirstRoute) {
      // 当push的PageRoute为VC的第一个页面时，触发VC的didPushFirstRoute
        if let viewController = topNavigationController?.topViewController as? AstronomiaFlutterViewController {
            viewController.didPushFirstRoute()
        }
    } else {
      // 当push的PageRoute为VC的第二个页面时，触发VC的didPushFirstRouteNext，之后的PageRoute都忽略
        if !pageRoutes.isEmpty && pageRoutes.last?.isFirst == true {
            if let viewController = topNavigationController?.topViewController as? AstronomiaFlutterViewController {
                viewController.didPushFirstRouteNext()
            }
        }
    }
    pageRoutes.append(PageRoute(
      routeSettings: RouteSettings(routeName: routeName, arguments: arguments),
      isFirst: isFirstRoute))
    
    debugPrint("After didPush: ")
    debugPrint( { () -> String in
      let pageStackOutput = pageRoutes.map { (pageRoute) -> String in
        "routeName: \(pageRoute.routeSettings.routeName), arguments: \(String(describing: pageRoute.routeSettings.arguments)), isFirstRoute: \(pageRoute.isFirst)"
      }.joined(separator: "\n")

      return pageStackOutput
    }())
  }
  
  func nativeRouteIntercept(
    routeName: String,
    arguments: Any?,
    popRouteResult: @escaping RoutePopCallback) {
    for interceptor in routeInterceptors.reversed() {
      if interceptor(routeName, arguments) {
        if let pageRoute = pageRoutes.last {
          pageRoutes.removeLast()
          pageRoutes.append(
            PageRoute(
              routeSettings: pageRoute.routeSettings, isFirst: pageRoute.isFirst,
              popResultCallback: pageRoute.popResultCallback,
              interceptedPopResultCallback: popRouteResult))
        }
        return
      }
    }
    
    popRouteResult(nil)
    return
  }

  private func isPageRouteInTopmostViewController(pageRoutes: [PageRoute], routeName: String)
    -> Bool
  {
    let popUntilRouteIndex = pageRoutes.lastIndex(
      where: { e in
        e.routeSettings.routeName == routeName
      }) ?? -1
    if popUntilRouteIndex == -1 {
      return false
    }

    let firstRouteIndex = pageRoutes.lastIndex(
      where: { e in
        e.isFirst
      }) ?? -1
    if firstRouteIndex == -1 {
      return true
    }

    return popUntilRouteIndex >= firstRouteIndex
  }

  public func popUntil(routeName: String) {
    engine.routeChannel?.popUntilNamedFromNative(routeName: routeName, callback: { (_) in
      
    })
  }

  /// pop result from native to flutter
  /// only use this method when native ViewContrller is pushed by routeInterceptors
  public func popResultFromNative(_ result: Any? = nil) {
    if !(topNavigationController?.topViewController is AstronomiaFlutterViewController) {
      topNavigationController?.popViewController(animated: true)
      pageRoutes.last?.interceptedPopResultCallback?(result)
    }
  }
    
  /// call flutter maybePop
  public func maybePopFromNative() {
    if topNavigationController?.topViewController is AstronomiaFlutterViewController {
      engine.routeChannel?.maybePopFromNative()
    }
  }

  /// if flutter is root ViewController
  /// pass the routeName parameter
  public func popToRoot(_ routeName: String = "/") {
    pageRoutes.removeUntil { (pageRoute) -> Bool in
      return pageRoute.routeSettings.routeName == routeName
    }
    engine.routeChannel?.popUntilNamed(
      routeName: routeName,
      callback: { _ in })
  }

}

extension AstronomiaNavigator {
  public func createViewControllerAsRoot(_ routeName: String, arguments: Any? = nil)
    -> AstronomiaFlutterViewController
  {
    shouldWaitForFlutterEngine = true
    let viewController = flutterViewControllerBuilder(engine.get(), routeName, arguments)
    viewController.setFlutterViewDidRenderCallback {
      self.shouldWaitForFlutterEngine = false
    }
    // trigger flutter render, or it will crash when pop to root
    viewController.loadViewIfNeeded()
    return viewController
  }
}
