//
//  RouteSettings.swift
//  astronomia
//
//  Created by klook on 2020/5/11.
//

import Foundation

struct RouteSettings: Equatable {
  static func == (lhs: RouteSettings, rhs: RouteSettings) -> Bool {
    return lhs.routeName == rhs.routeName && lhs.arguments.equals(rhs.arguments)
  }
  
  let routeName: String
  let arguments: Any?

  init(routeName: String, arguments: Any? = nil) {
    self.routeName = routeName
    self.arguments = arguments
  }
}
