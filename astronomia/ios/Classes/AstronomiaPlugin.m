#import "AstronomiaPlugin.h"
#if __has_include(<astronomia/astronomia-Swift.h>)
#import <astronomia/astronomia-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "astronomia-Swift.h"
#endif

@implementation AstronomiaPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAstronomiaPlugin registerWithRegistrar:registrar];
}
@end
