//
//  PageRoute.swift
//  astronomia
//
//  Created by klook on 2020/5/15.
//

import Foundation

struct PageRoute: Equatable {
  static func == (lhs: PageRoute, rhs: PageRoute) -> Bool {
    return lhs.isFirst == rhs.isFirst &&
      lhs.routeSettings == rhs.routeSettings
  }
  
  let isFirst: Bool
  let routeSettings: RouteSettings
  let popResultCallback: RoutePopResultCallback?
  let interceptedPopResultCallback: RoutePopResultCallback?

  init(
    routeSettings: RouteSettings, isFirst: Bool, popResultCallback: RoutePopResultCallback? = nil,
    interceptedPopResultCallback: RoutePopResultCallback? = nil
  ) {
    self.routeSettings = routeSettings
    self.isFirst = isFirst
    self.popResultCallback = popResultCallback
    self.interceptedPopResultCallback = interceptedPopResultCallback
  }
}
