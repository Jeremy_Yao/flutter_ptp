package com.klook.flutter.astronomia_example

import android.content.Intent
import android.net.Uri
import androidx.test.espresso.flutter.EspressoFlutter
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class AstronomiaNavigatorDeeplinkTest {

    // Ref:
    // https://stackoverflow.com/questions/42951216/how-to-write-tests-for-deep-links-in-android
    // https://stackoverflow.com/questions/44074173/automating-deep-linking-using-android-espresso/47813474#47813474
    @Rule
    @JvmField
    var activityRule: ActivityTestRule<DeeplinkActivity> =
            object : ActivityTestRule<DeeplinkActivity>(DeeplinkActivity::class.java, false) {
                override fun getActivityIntent(): Intent {
                    val intent = Intent(InstrumentationRegistry.getInstrumentation()
                            .targetContext, DeeplinkActivity::class.java)
                    intent.action = Intent.ACTION_VIEW
                    intent.data = Uri.parse("astronomia://flutter")
                    return intent
                }
            }

    @Test
    fun shouldOpenByDeeplink() {
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(AstronomiaNavigatorUITest.INTERACT_FLUTTER_DELAY)
        EspressoFlutter.onFlutterWidget(FlutterMatchers.withText("route: /page/1, index: null"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }
}