package com.klook.flutter.astronomia_example

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.flutter.EspressoFlutter.onFlutterWidget
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


typealias FlutterAssertions = androidx.test.espresso.flutter.assertion.FlutterAssertions
typealias FlutterActions = androidx.test.espresso.flutter.action.FlutterActions
typealias FlutterMatchers = androidx.test.espresso.flutter.matcher.FlutterMatchers


@LargeTest
@RunWith(AndroidJUnit4::class)
class AstronomiaNavigatorUITest {

    companion object {
        const val INTERACT_FLUTTER_DELAY = 500L
    }

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun pushFromNative_pushFlutter() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(2000L)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push flutter"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("Can pop this page"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushFromNative_pushFlutter_pushNative() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/3")))
    }

    @Test
    fun pushFromNative_pushFlutter_popFlutter() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("pop flutter")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
        onView(withId(R.id.tv_route_pop_result)).check(matches(withText("pop from 2")))
    }

    @Test
    fun pushFromNative_pushFlutter_pressBack() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        pressBack()

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
        onView(withId(R.id.tv_route_pop_result)).check(matches(withText("null")))
    }

    @Test
    fun pushFromNative_pushFlutter_maybePopFlutter_canPop() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("maybePop flutter")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_pressBack_canPop() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        pressBack()

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_maybePopFlutter_cantPop() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("Can pop this page"))
                .perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("maybePop flutter")).perform(FlutterActions.click())

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushFromNative_pushFlutter_pressBack_cantPop() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("Can pop this page")).perform(FlutterActions.click())
        pressBack()

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushFromNative_pushFlutter_popUntilPage2() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push flutter")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("route: /page/3, index: 3"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("popUntil /page/2")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushFromNative_showDialog_closeDialog_popFlutter() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("show dialog")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("close dialog"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

        onFlutterWidget(FlutterMatchers.withText("close dialog")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

        onFlutterWidget(FlutterMatchers.withText("pop flutter")).perform(FlutterActions.click())
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))

    }

    @Test
    fun pushFromNative_showDialog_closeDialog_pressBack() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("show dialog")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("close dialog"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

        onFlutterWidget(FlutterMatchers.withText("close dialog")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

        pressBack()
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_showFlushbar_showFlushbar_showFlushbar_showFlushbar_popFlutter() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())

        onFlutterWidget(FlutterMatchers.withText("pop flutter")).perform(FlutterActions.click())
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_showFlushbar_showFlushbar_showFlushbar_showFlushbar_pressBack() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("show flushbar")).perform(FlutterActions.click())

        // Because the FlushBar will pop after the animation, we can only press back after detention.
        Thread.sleep(1000L)
        pressBack()
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_showDialog_pressBack_pressBack() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("show dialog")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("close dialog"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

        pressBack()
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

        pressBack()
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_pushNative_popNative() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/3")))

        onView(withId(R.id.btn_pop_native)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

    }

    @Test
    fun pushFromNative_pushFlutter_pushNative_pressBack() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/3")))

        pressBack()

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushFromNative_pushFlutter_pushNative_popNativeWithResult() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/3")))

        onView(withId(R.id.btn_pop_native_result)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("pop from native /page/3"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))

    }

    @Test
    fun pushFromNative_pushFlutter_pusFlutter_pushNative_popUntilPage2() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push flutter")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/4")))

        onView(withId(R.id.btn_pop_until)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushFromNative_pushFlutter_pusFlutter_pushNative_popUntilPage2_popFlutter() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push flutter")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/4")))

        onView(withId(R.id.btn_pop_until)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("pop flutter")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_pusFlutter_pushNative_popUntilPage2_pressBack() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        onFlutterWidget(FlutterMatchers.withText("push flutter")).perform(FlutterActions.click())
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/4")))

        onView(withId(R.id.btn_pop_until)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("route: /page/2, index: 2"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
        pressBack()

        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_pushNative_pushFlutter_pushNative_popAllFlutter() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.btn_push_flutter)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.btn_pop_all)).perform(click())
        onView(withId(R.id.btn_pop_native)).perform(click())
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/3")))
        onView(withId(R.id.btn_pop_native)).perform(click())
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/1")))
    }

    @Test
    fun pushFromNative_pushFlutter_pushNative_pushFlutter_pushNative_popToRootNavigatorActivity() {
        onView(withId(R.id.btn_push_from_native)).perform(click())
        onView(withId(R.id.btn_push_flutter)).perform(click())
        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.btn_push_flutter)).perform(click())

        onFlutterWidget(FlutterMatchers.withText("push native")).perform(FlutterActions.click())

        onView(withId(R.id.btn_pop_to_root)).perform(click())
        onView(withId(R.id.btn_pop_native)).perform(click())
        onView(withId(R.id.tv_route_name)).check(matches(withText("/page/3")))
        onView(withId(R.id.btn_pop_native)).perform(click())
        // The flutter route: "/"
        onFlutterWidget(FlutterMatchers.withText("push flutter"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

    @Test
    fun pushRootFlutterActivity() {
        onView(withId(R.id.btn_push_root_flutter_activity)).perform(click())

        // TODO(littlegnal): Use IdlingResource instead in the future.
        // https://medium.com/azimolabs/wait-for-it-idlingresource-and-conditionwatcher-602055f32356
        // https://gist.github.com/dGorod/ac8d413c8804f2ebb1a0
        Thread.sleep(INTERACT_FLUTTER_DELAY)
        onFlutterWidget(FlutterMatchers.withText("push flutter"))
                .check(FlutterAssertions.matches(FlutterMatchers.isExisting()))
    }

}