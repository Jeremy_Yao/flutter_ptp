package com.klook.flutter.astronomia_example

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.klook.flutter.astronomia.AstronomiaNavigator

class DeeplinkActivity  : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val deeplink = intent.data?.toString()
        if (deeplink?.isNotEmpty() == true && deeplink == "astronomia://flutter") {
            AstronomiaNavigator.instance.push("/page/1" )
        }
    }
}