package com.klook.flutter.astronomia_example

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.klook.flutter.astronomia.AstronomiaNavigator

class PushFromNativeActivity : AppCompatActivity() {

  companion object {
    private const val EXTRA_INDEX_KEY = "extra_index_key"

    fun openActivity(context: Context, index: Int) {
      val intent = Intent(context, PushFromNativeActivity::class.java).apply {
        putExtra(EXTRA_INDEX_KEY, index)
//        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
      }

      context.startActivity(intent)
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val index = intent.getIntExtra("extra_index_key", 1)
    val text = "/page/$index"
    findViewById<AppCompatTextView>(R.id.tv_route_name).text = text

    findViewById<AppCompatButton>(R.id.btn_push_flutter).setOnClickListener {
      AstronomiaNavigator.instance.push("/page/${index + 1}", index + 1) {
        findViewById<AppCompatTextView>(R.id.tv_route_pop_result).text = it.toString()
      }

//      AstronomiaNavigator.instance.push("/page/${index + 1}", index + 1)
    }

    findViewById<AppCompatButton>(R.id.btn_push_native).setOnClickListener {
      openActivity(this, index + 1)
    }

    findViewById<AppCompatButton>(R.id.btn_pop_native).setOnClickListener {
      finish()
    }

    findViewById<AppCompatButton>(R.id.btn_pop_native_result).setOnClickListener {
      AstronomiaNavigator.instance.pop(this, "pop from native $text")
    }

    findViewById<AppCompatButton>(R.id.btn_pop_until).setOnClickListener {
      AstronomiaNavigator.instance.popUntilNamed("/page/2")
    }

    findViewById<AppCompatButton>(R.id.btn_pop_all).setOnClickListener {
      AstronomiaNavigator.instance.popAll()
    }

    findViewById<AppCompatButton>(R.id.btn_pop_to_root).setOnClickListener {
      AstronomiaNavigator.instance.popToRootNavigatorActivity()
    }
  }
}
