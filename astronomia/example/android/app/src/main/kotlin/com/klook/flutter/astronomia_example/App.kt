package com.klook.flutter.astronomia_example

import android.app.Application
import android.content.Context
import android.content.Intent
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.android.utils.FlipperUtils
import com.facebook.flipper.core.FlipperClient
import com.facebook.flipper.plugins.inspector.DescriptorMapping
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin
import com.facebook.soloader.SoLoader
import com.klook.flutter.astronomia.*
import com.klook.flutter.astronomia.BuildConfig
import io.flutter.embedding.android.FlutterActivityLaunchConfigs

class App: Application() {

  override fun onCreate() {
    super.onCreate()

    SoLoader.init(this, false)

    if (BuildConfig.DEBUG && FlipperUtils.shouldEnableFlipper(this)) {
      val client: FlipperClient = AndroidFlipperClient.getInstance(this)
      client.addPlugin(InspectorFlipperPlugin(this, DescriptorMapping.withDefaults()))
      client.start()
    }

    val launchConfigs = AstronomiaFlutterActivityLaunchConfigs(
            activityClass = CustomFlutterActivity::class.java,
            backgroundMode = FlutterActivityLaunchConfigs.BackgroundMode.opaque)
    AstronomiaNavigator.instance.init(this, launchConfigs = launchConfigs)
    AstronomiaNavigator.instance.addRouteInterceptor(object : RouteInterceptor {
      override fun intercept(activityContext: Context, routeSettings: RouteSettings): Boolean {
        if (routeSettings.routeName.startsWith("native:")) {
          val index = routeSettings.arguments as? Int ?: 1
          PushFromNativeActivity.openActivity(activityContext, index)
          return true
        } else if (routeSettings.routeName == "/push_flutter_finish_native") {
          activityContext.startActivity(Intent(activityContext, PushNativePushFlutterFinishNativeActivity::class.java))
          return true
        }
        return false
      }
    })
  }
}