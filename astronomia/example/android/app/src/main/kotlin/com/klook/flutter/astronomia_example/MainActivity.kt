package com.klook.flutter.astronomia_example

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.klook.flutter.astronomia.AstronomiaNavigator
import com.klook.flutter.astronomia.createRootNavigatorActivityIntent
import io.flutter.embedding.android.FlutterActivityLaunchConfigs
import io.flutter.embedding.android.RenderMode
import io.flutter.embedding.android.TransparencyMode

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_home)

    findViewById<AppCompatButton>(R.id.btn_push_root_flutter_activity).setOnClickListener {
      startActivity(AstronomiaNavigator.instance.createRootNavigatorActivityIntent(
        this,
        CustomRootNavigatorActivity::class.java,
        FlutterActivityLaunchConfigs.BackgroundMode.opaque
      ))
    }

    findViewById<AppCompatButton>(R.id.btn_push_from_native).setOnClickListener {
      startActivity(Intent(this, PushFromNativeActivity::class.java))
    }

    findViewById<AppCompatButton>(R.id.btn_push_flutter_finish_native).setOnClickListener {
      startActivity(AstronomiaNavigator.instance.createRootNavigatorActivityIntent(
              this,
              CustomRootNavigatorActivity::class.java,
              FlutterActivityLaunchConfigs.BackgroundMode.opaque
      ))
    }
  }
}
