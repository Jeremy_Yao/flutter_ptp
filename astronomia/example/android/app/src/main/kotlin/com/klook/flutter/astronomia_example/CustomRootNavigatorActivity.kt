package com.klook.flutter.astronomia_example

import android.app.Activity
import android.os.Build
import android.view.View
import io.flutter.embedding.android.AstronomiaFlutterActivity

class CustomRootNavigatorActivity: AstronomiaFlutterActivity() {

    override fun onPostResume() {
        super.onPostResume()
        setStatusBarMode(true)
    }
}

private fun Activity.setStatusBarMode(isLightStatusBar: Boolean) {
    if (Build.VERSION.SDK_INT >= 23) {
        var vis = window.decorView.systemUiVisibility
        vis = if (isLightStatusBar) {
            vis or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            vis and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        }

        window.decorView.systemUiVisibility = vis
    }
}