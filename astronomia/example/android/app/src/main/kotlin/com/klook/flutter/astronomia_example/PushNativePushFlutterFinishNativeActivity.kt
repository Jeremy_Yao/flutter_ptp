package com.klook.flutter.astronomia_example

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.klook.flutter.astronomia.AstronomiaNavigator

class PushNativePushFlutterFinishNativeActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.push_native_push_flutter_finish_native);

        findViewById<AppCompatButton>(R.id.btn_finish_native_push_flutter).setOnClickListener {
            AstronomiaNavigator.instance.push("/page/2", 1)
            finish()
        }
    }
}