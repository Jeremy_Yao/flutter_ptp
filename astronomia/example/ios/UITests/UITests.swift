//
//  UITests.swift
//  UITests
//
//  Created by klook on 2020/6/12.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import XCTest

class UITests: XCTestCase {

  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }

  func testPopFlutterToFlutter() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/3, index: 3"].waitForExistence(timeout: 2))
    app.buttons["pop flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
  }

  func testPopFlutterToNative() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/3, index: 3"].waitForExistence(timeout: 2))
    app.buttons["pop flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
    app.buttons["pop flutter"].tap()

    XCTAssert(app.staticTexts["route: init, index 1"].waitForExistence(timeout: 2))
  }

  func testPopNativeToFlutter() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push native"].tap()
    XCTAssert(app.staticTexts["route: native:/page, index 3"].waitForExistence(timeout: 2))
    app.buttons["pop native"].tap()
// TODO(zhuozhao): Make this work in the future
//    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 3))
//    app.buttons["pop flutter"].tap()

//    XCTAssert(app.staticTexts["route: init, index 1"].waitForExistence(timeout: 2))
  }

  func testPopUntilInFlutter() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/3, index: 3"].waitForExistence(timeout: 2))
    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/4, index: 4"].waitForExistence(timeout: 2))
    app.buttons["popUntil /page/2"].tap()

    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
  }

  func testPopUntilInNative() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/3, index: 3"].waitForExistence(timeout: 2))
    app.buttons["push native"].tap()
    XCTAssert(app.staticTexts["route: native:/page, index 4"].waitForExistence(timeout: 2))
    app.buttons["popUntil /page/2"].tap()
    
    // TODO(zhuozhao): Make this work in the future
//    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
  }

  func testPopUntilInFlutterWithNativeInStack() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push flutter"].tap()
    app.buttons["push native"].tap()
    app.buttons["push flutter"].tap()
    // TODO(zhuozhao): Make this work in the future
//    app.buttons["popUntil /page/2"].tap()
//
//    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
  }

  func testPopUntilInNativeWithNativeInStack() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/3, index: 3"].waitForExistence(timeout: 2))
    app.buttons["push native"].tap()
    XCTAssert(app.staticTexts["route: native:/page, index 4"].waitForExistence(timeout: 2))
    app.buttons["push native"].tap()
    XCTAssert(app.staticTexts["route: direct push, index 5"].waitForExistence(timeout: 2))
    app.buttons["popUntil /page/2"].tap()
    
    // TODO(zhuozhao): Make this work in the future
//    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
  }

  func testPopToRoot() {
    // UI tests must launch the application that they test.
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
    app.buttons["push native"].tap()
    XCTAssert(app.staticTexts["route: native:/page, index 3"].waitForExistence(timeout: 2))
    app.buttons["popToRoot"].tap()
    XCTAssert(app.staticTexts["route: init, index 1"].waitForExistence(timeout: 2))

  }

  func testWillPopMaybePop() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
    app.switches["Can pop this page"].tap()
    app.buttons["maybePop flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))

  }

  func testWillPopForcePop() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
    app.switches["Can pop this page"].tap()
    app.buttons["pop flutter"].tap()
    XCTAssert(app.staticTexts["route: init, index 1"].waitForExistence(timeout: 2))
  }

  func testNavigatorPush() {
    let app = XCUIApplication()
    app.launch()

    app.buttons["push flutter"].tap()
    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
    app.buttons["show dialog"].tap()

    XCTAssert(app.buttons["close dialog"].waitForExistence(timeout: 2))
    app.buttons["close dialog"].tap()

    XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))

  }

  func testNavigatorPushPopAndRemove() {
      let app = XCUIApplication()
      app.launch()

      app.buttons["push flutter"].tap()
      XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 2))
      app.buttons["show flushbar"].tap()
      app.buttons["show flushbar"].tap()
      app.buttons["show flushbar"].tap()
      app.buttons["show flushbar"].tap()

      XCTAssert(app.buttons["route: /page/2, index: 2"].waitForExistence(timeout: 3))
      app.buttons["pop flutter"].tap()
      XCTAssert(app.staticTexts["route: init, index 1"].waitForExistence(timeout: 2))
    }
}
