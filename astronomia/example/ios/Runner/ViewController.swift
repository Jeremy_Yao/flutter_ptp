//
//  ViewController.swift
//  Runner
//
//  Created by klook on 2020/4/28.
//  Copyright © 2020 klook. All rights reserved.
//

import UIKit
import astronomia

class ViewController: UIViewController {
  var index = 1
  var route = "init"
  var fromFlutter = false

  convenience init(route: String, index: Int = 1, fromFlutter: Bool = false) {
    self.init()
    self.index = index
    self.route = route
    self.fromFlutter = fromFlutter
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    hidesBottomBarWhenPushed = route != "init"
    let title = UILabel()
    title.text = "route: \(route), index \(index)"
    title.sizeToFit()
    title.center = CGPoint(x: view.center.x, y: view.center.y - 300)
    view.addSubview(title)

    self.view.backgroundColor = UIColor.white
    // Do any additional setup after loading the view.
    let button1 = UIButton(type: UIButton.ButtonType.custom)
    button1.setTitle("push flutter", for: UIControl.State.normal)
    button1.setTitleColor(UIColor.red, for: UIControl.State.normal)
    button1.sizeToFit()
    button1.center = CGPoint(x: view.center.x, y: view.center.y - 200)
    button1.addTarget(self, action: #selector(pushFlutter), for: UIControl.Event.touchUpInside)
    view.addSubview(button1)

    let button2 = UIButton(type: UIButton.ButtonType.custom)
    button2.setTitle("push native", for: UIControl.State.normal)
    button2.setTitleColor(UIColor.red, for: UIControl.State.normal)
    button2.sizeToFit()
    button2.center = CGPoint(x: view.center.x, y: view.center.y - 100)
    button2.addTarget(self, action: #selector(pushNative), for: UIControl.Event.touchUpInside)
    view.addSubview(button2)

    let button3 = UIButton(type: UIButton.ButtonType.custom)
    button3.setTitle("pop native", for: UIControl.State.normal)
    button3.setTitleColor(UIColor.red, for: UIControl.State.normal)
    button3.sizeToFit()
    button3.center = view.center
    button3.addTarget(self, action: #selector(pop), for: UIControl.Event.touchUpInside)
    view.addSubview(button3)

    let button4 = UIButton(type: UIButton.ButtonType.custom)
    button4.setTitle("popToRoot", for: UIControl.State.normal)
    button4.setTitleColor(UIColor.red, for: UIControl.State.normal)
    button4.sizeToFit()
    button4.center = CGPoint(x: view.center.x, y: view.center.y + 100)
    button4.addTarget(self, action: #selector(popToRoot), for: UIControl.Event.touchUpInside)
    view.addSubview(button4)

    let button5 = UIButton(type: UIButton.ButtonType.custom)
    button5.setTitle("popUntil /page/2", for: UIControl.State.normal)
    button5.setTitleColor(UIColor.red, for: UIControl.State.normal)
    button5.sizeToFit()
    button5.center = CGPoint(x: view.center.x, y: view.center.y + 200)
    button5.addTarget(self, action: #selector(popUntil), for: UIControl.Event.touchUpInside)
    view.addSubview(button5)
  }

  @objc func pushFlutter() {
    AstronomiaNavigator.shared.push(
      "/page/\(index + 1)", arguments: (index + 1),
      popResultCallback: { r in
        if let r = r {
          debugPrint(r)
        } else {
          debugPrint("pop nil")
        }
      })
  }

  @objc func pushNative() {
    self.navigationController?.pushViewController(
      ViewController(route: "direct push", index: index+1), animated: true)
  }

  @objc func pop() {
    if fromFlutter {
      AstronomiaNavigator.shared.popResultFromNative("pop from \(index)")
    } else {
      self.navigationController?.popViewController(animated: true)
    }
  }

  @objc func popToRoot() {
    self.navigationController?.popToRootViewController(animated: true)
    AstronomiaNavigator.shared.popToRoot()
  }

  @objc func popUntil() {
    AstronomiaNavigator.shared.popUntil(routeName: "/page/2")
  }
}
