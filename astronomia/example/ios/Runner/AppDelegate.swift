//
//  AppDelegate.swift
//  Runner
//
//  Created by klook on 2020/4/28.
//  Copyright © 2020 klook. All rights reserved.
//

import UIKit
import astronomia

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    // Override point for customization after application launch.
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = UIColor.white
    AstronomiaNavigator.shared.flutterViewControllerBuilder = { (engine, routeName, arguments) in
      let viewController = AstronomiaFlutterViewController(
        engine: engine, nibName: nil, bundle: nil)
      viewController.hidesBottomBarWhenPushed = routeName != "/"
      return viewController
    }
    AstronomiaNavigator.shared.startup()
    let navigationController1 = UINavigationController(
      rootViewController: ViewController(route: "init"))
    navigationController1.setNavigationBarHidden(true, animated: false)
    navigationController1.title = "native home"

    let tabBarController = UITabBarController()
    tabBarController.viewControllers = [navigationController1]

    window?.rootViewController = tabBarController
    window?.makeKeyAndVisible()
    AstronomiaNavigator.shared.addRouteInterceptor { (routeName, argument) in
      if routeName.starts(with: "native:") {
        (tabBarController.selectedViewController as! UINavigationController).pushViewController(
          ViewController(route: routeName, index: argument as! Int, fromFlutter: true),
          animated: true)
        return true
      }
      return false
    }

    return true
  }
}
