import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:astronomia/astronomia.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    final navigatorObserverDelegate = AstronomiaNavigatorObserverDelegate();
    return MaterialApp(
      home: _HomePage(),
      navigatorObservers: navigatorObserverDelegate.observers,
      builder: AstronomiaNavigator.builder(
          navigatorObserverDelegate: navigatorObserverDelegate),
      onGenerateRoute: (routeSettings) {
        debugPrint('onGenerateRoute routeSettings: $routeSettings');
        final arguments = routeSettings.arguments;
        final index = arguments;
        return MaterialPageRoute(
            settings: routeSettings,
            builder: (context) => _DemoPage(routeSettings.name, index));
      },
    );
  }
}

class _HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('home'),
      ),
      body: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Builder(builder: (context) {
            return Column(children: [
              ElevatedButton(
                onPressed: () async {
                  final result = await AstronomiaNavigator.of(context)
                      .pushNamed('/page/1', arguments: 1);
                  debugPrint('pop result: $result');
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text('result: $result',
                          style: const TextStyle(fontSize: 20))));
                },
                child: const Text('push flutter'),
              ),
              ElevatedButton(
                onPressed: () async {
                  AstronomiaNavigator.of(context)
                      .pushNamed('/push_flutter_finish_native', arguments: 1);
                },
                child: const Text('push a native and finish imidiatly'),
              ),
            ]);
          }),
        ],
      )),
    );
  }
}

class _DemoPage extends StatefulWidget {
  const _DemoPage(this._routeName, this._index);
  final String _routeName;
  final int _index;

  @override
  State<StatefulWidget> createState() => _DemoPageState();
}

class _DemoPageState extends State<_DemoPage> with AstronomiaRouteAwareMixin {
  bool _canPop = true;
  String _popResult = "";

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    debugPrint(
        'didChangeDependencies: routeName: ${widget._routeName}, index: ${widget._index}, mouted: $mounted');

    if (!routeDidPop) {
      debugPrint(
          'didChangeDependencies routeDidPop: routeName: ${widget._routeName}, index: ${widget._index}, mouted: $mounted');
    }
  }

  @override
  void didPush() {
    debugPrint(
        'didPush: routeName: ${widget._routeName}, index: ${widget._index}');
    super.didPush();
  }

  @override
  void didPop() {
    debugPrint(
        'didPop: routeName: ${widget._routeName}, index: ${widget._index}');
    super.didPop();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        debugPrint('WillPopScope _canPop: $_canPop');
        return _canPop;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Flutter'),
          leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                AstronomiaNavigator.of(context).pop();
              }),
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () async {},
              child:
                  Text('route: ${widget._routeName}, index: ${widget._index}'),
            ),
            ElevatedButton(
              onPressed: () async {},
              child: Text(_popResult ?? ''),
            ),
            const SizedBox(height: 20),
            Builder(
                builder: (context) => ElevatedButton(
                      onPressed: () async {
                        final result = await AstronomiaNavigator.of(context)
                            .pushNamed('/page/${widget._index + 1}',
                                arguments: widget._index + 1);
                        debugPrint('pop result: $result');
                        setState(() {
                          _popResult = result;
                        });
                      },
                      child: const Text('push flutter'),
                    )),
            const SizedBox(height: 20),
            Builder(
                builder: (context) => ElevatedButton(
                      onPressed: () async {
                        final result = await AstronomiaNavigator.of(context)
                            .pushNamed('native:/page',
                                arguments: widget._index + 1);
                        debugPrint('pop result: $result');
                        setState(() {
                          _popResult = result;
                        });
                      },
                      child: const Text('push native'),
                    )),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  AstronomiaNavigator.of(context)
                      .pop('pop from ${widget._index}');
                },
                child: const Text('pop flutter')),
            const SizedBox(height: 20),
            Builder(
              builder: (context) {
                return ElevatedButton(
                    onPressed: () async {
                      final maybePopResult =
                          await AstronomiaNavigator.of(context)
                              .maybePop('pop from ${widget._index}');
                      // Scaffold.of(context).showSnackBar(SnackBar(
                      //     content: Text('maybePop: $maybePopResult',
                      //         style: const TextStyle(fontSize: 20))));
                      setState(() {
                        _popResult = 'maybePop: $maybePopResult';
                      });
                    },
                    child: const Text('maybePop flutter'));
              },
            ),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  AstronomiaNavigator.of(context).popUntilNamed('/page/2');
                },
                child: const Text('popUntil /page/2')),
            ElevatedButton(
                onPressed: () {
                  showGeneralDialog(
                      context: context,
                      barrierDismissible: false,
                      transitionDuration: const Duration(milliseconds: 300),
                      pageBuilder: (context, _, __) {
                        return Container(
                          color: Colors.white,
                          child: Center(
                              child: ElevatedButton(
                            onPressed: () {
                              AstronomiaNavigator.of(context).pop();
                            },
                            child: const Text('close dialog'),
                          )),
                        );
                      });
                },
                child: const Text('show dialog')),
            ElevatedButton(
                onPressed: () {
                  Flushbar(
                    title: "test",
                    message: "test",
                    duration: const Duration(seconds: 1),
                  ).show(context);
                },
                child: const Text('show flushbar')),
            SwitchListTile(
                title: const Text('Can pop this page'),
                value: _canPop,
                onChanged: (_) {
                  setState(() {
                    _canPop = !_canPop;
                  });
                })
          ],
        )),
      ),
    );
  }
}
