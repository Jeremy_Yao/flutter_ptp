# Changlog

## 2.1.0

* Update [built_value](https://pub.dev/packages/built_value) to `^8.0.4`
* Update [built_collection](https://pub.dev/packages/built_collection) to `^5.0.0`
* Update `l10n_cms` to `l10n_cms-3.1.0`

## 2.0.1

* Upgrade `l10n_cms` -> `l10n_cms-3.0.0`

## 2.0.0

* Upgrade to flutter 2.0.1

## 1.13.0

* optimize share and add share listener

## 1.12.2

* kltracker 任意模块监听曝光范围，并在sendevent前删除null数据

## 1.12.1

* kltracker assert fix and rewrite trackingData hashcode method

## 1.12.0

* optimus integration

## 1.11.0
* Initialize `AppLocalizations` in `DIProvider`

## 1.10.1

* add @nullable in component model

## 1.10.0

* add some field in component model

## 1.9.6

* 请求header`x_country_code`改为`X-Country-Code`

## 1.9.5

* 1. kltracker 添加 sync_config_data channel 方法给native更新config数据
* 2. kltracker 模块点击是获取曝光范围，并放到事件的数据中

## 1.9.4

* 请求header添加X-Klook-Kepler-Id、x_country_code

## 1.9.3

* change astronomia ref to 1.1.1+1

## 1.9.2

* addNavigatorObserver with kltracker and change kltracker ref to 1.2.0

## 1.9.1

* change kltracker ref to 1.1.1

## 1.9.0

* CommonText 新增textStyle命名构造方法，方便对接design_token

## 1.8.0

* add a optional params in tracker trackScreen method

## 1.7.2

* Add request header filed: X-Klook-Tint

## 1.7.1

* Deprecated "Remove liveshow write iOS calender method" instead of Remove

## 1.7.0

* Add A/B Test
* Remove liveshow write iOS calender method

## 1.6.8

* Update ref for klook-flutter-plugin/kltracker: 3e97c0a

## 1.6.7

* Add request header filed: X-Klook-Host

## 1.6.6

* CommonApp `title` support Widget, add new property `titleWidget`

## 1.6.5

* Fix in house tracking navigator observer not correctly registered

## 1.6.2

* change in house tracking ref to: `133e8d9`

## 1.6.1

* Fix crash due to `develop_setting` return null in `AppInfo`. 

## 1.6.0

* Remove `PlatformHomeCommonNavigatorDelegate`
* Update `UserInfo`
* Update `astronomia` hash to `e47607c`

## 1.5.0

* Add `DevelopSetting` to `AppInfo` to support get develop settings from native side
* Add preview mode parameter for all request for previewing navigation configs and home page configs

## 1.4.1

* update `scrollable_positioned_list` version to `0.1.8`

## 1.4.0

* Add `common_share` to open share panel.

## 1.3.0

* Upgrade `l10n_cms` version to `l10n_cms-1.2.2`
* Add `error_handling` dependency
* Deprecated `errorToException` and friends


## 1.2.1

* Upgrade `l10n_cms` version to `l10n_cms-1.2.1`

## 1.2.0

* Upgrade `l10n_cms` version to `l10n_cms-1.2.0`
* Export `HeaderInterceptor`, and make create headers logic reusable

## 1.1.1

Upgrade l10n_cms version to `l10n_cms-1.0.1`.

## 1.1.0

Remove `flutter_crashlytics` dependency.

## 1.0.0

Initial release.