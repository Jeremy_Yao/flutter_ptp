import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

/// 【Class Summary】
/// 这个基于第三方库的[CachedNetworkImage]类进行封装，实现了常用的一些功能
/// 目前只支持 `png`,`jpg`,`webp`等格式
///
/// 【Feature】
///  1. 支持设置控件大小,如果设置了`size`为以此为准，如果`size`为`null`该组件的大小会以父控件的决定
///  2. 支持设置圆角，默认没有
///  3. 支持自定义占位颜色
///
/// 【参数说明】
/// * 必须参数 `url`, 图片的url地址
/// * 可选参数 `cornerRadius`, 控件的圆角大小，默认为`0`,无圆角
/// * 可选参数 `placeHolderColor`, 占位组件，默认为`Color(0xFFE9E9E9)`的占位Container,一般无需设置
/// * 可选参数 `fit`, 图片填充模式，默认 `BoxFit.cover`
///
/// 【Example】
///  ```
///  // 一般用法
///  NetworkImageWidget(
///   url: "some url",
///  );
///
///  // 设置url + 圆角
///  NetworkImageWidget(
///   url: "some url",
///   cornerRadius: 6,
///  );
///
///  // 设置url + 圆角 + size
///  NetworkImageWidget(
///   size: Size(60, 60),
///   url: "some url",
///   cornerRadius: 6,
///  );
///  ```
class NetworkImageWidget extends StatefulWidget {
  const NetworkImageWidget({
    Key key,
    @required this.url,
    this.size,
    this.cornerRadius = 0,
    this.placeholder,
    this.fit = BoxFit.cover,
  })  : assert(url != null),
        super(key: key);

  final String url;
  final Size size;
  final double cornerRadius;
  final PlaceholderWidgetBuilder placeholder;
  final BoxFit fit;

  @override
  _NetworkImageWidgetState createState() => _NetworkImageWidgetState();
}

class _NetworkImageWidgetState extends State<NetworkImageWidget> {
  @override
  Widget build(BuildContext context) {
    // 设置默认占位颜色placeholder
    PlaceholderWidgetBuilder _placeholder = widget.placeholder;
    _placeholder ??= (context, url) {
      return Container(color: const Color(0xFFE9E9E9));
    };

    Widget child = CachedNetworkImage(
      imageUrl: widget.url,
      width: widget.size?.width,
      height: widget.size?.height,
      fit: widget.fit,
      placeholder: _placeholder,
    );

    if (widget.cornerRadius > 0) {
      child = ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(widget.cornerRadius)),
        child: child,
      );
    }
    return child;
  }
}
