import 'package:common/src/locale/app_localizations.dart';
import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:common/assets.dart';

@Deprecated('use CommonNoNetworkWidget instead')
class CommonNoInternetWight extends StatelessWidget {
  const CommonNoInternetWight({VoidCallback onRefreshTap})
      : _onRefreshTap = onRefreshTap;
  final VoidCallback _onRefreshTap;

  @override
  Widget build(BuildContext context) {
    final _translations = Provider.of<AppLocalizations>(context, listen: false);
    return Container(
      margin: const EdgeInsets.all(50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset(CommonAssets.icon_nowifi_72,
              package: CommonAssets.package),
          Container(
              padding: const EdgeInsets.all(16),
              child: Text(
                _translations.text("jrpass_no_internet"),
                style: const TextStyle(fontSize: 16, color: ColorUtil.BW2),
                textAlign: TextAlign.center,
              )),
          OutlineButton(
            onPressed: _onRefreshTap,
            borderSide: const BorderSide(color: ColorUtil.BW1),
            child: Text(_translations.text("jrpass_refresh")),
          ),
        ],
      ),
    );
  }
}
