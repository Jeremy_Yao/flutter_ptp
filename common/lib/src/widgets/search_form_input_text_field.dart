import 'package:common/assets.dart';
import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SearchFormInputTextField extends StatelessWidget {
  const SearchFormInputTextField(
    String name, {
    String value,
    int maxLinesOfValue,
    bool showArrow = false,
    double borderWidth = 0,
    Color borderColor = ColorUtil.BW8,
    Color backgroundColor = ColorUtil.BW8,
    GestureTapCallback onTap,
  })  : assert(name != null),
        _name = name,
        _value = value,
        _maxLinesOfValue = maxLinesOfValue ?? 1,
        _showArrow = showArrow,
        _borderWidth = borderWidth,
        _borderColor = borderColor,
        _backgroundColor = backgroundColor,
        _onTap = onTap;

  const SearchFormInputTextField.failed(
    String name, {
    String value,
    bool showArrow = false,
    GestureTapCallback onTap,
  })  : assert(name != null),
        _name = name,
        _value = value,
        _maxLinesOfValue = 1,
        _showArrow = showArrow,
        _borderWidth = 1,
        _borderColor = ColorUtil.orange,
        _backgroundColor = const Color(0x19ff5722),
        _onTap = onTap;

  const SearchFormInputTextField.succeed(
    String name, {
    String value,
    bool showArrow = false,
    GestureTapCallback onTap,
  })  : assert(name != null),
        _name = name,
        _value = value,
        _maxLinesOfValue = 1,
        _showArrow = showArrow,
        _borderWidth = 1,
        _borderColor = ColorUtil.green2,
        _backgroundColor = const Color(0x1924b985),
        _onTap = onTap;

  /// The name and placeholder of field
  final String _name;

  /// The detail string of the field
  final String _value;

  /// The max line of value field text, default is 1
  final int _maxLinesOfValue;

  /// Control the arrow icon at right of field
  final bool _showArrow;

  /// Border width of field,
  /// normal: 0,
  /// failed: 1,
  /// successed: 1,
  final double _borderWidth;

  /// Border color of field,
  /// normal: ColorUtil.BW8,
  /// failed: ColorUtil.orange,
  /// successed: ColorUtil.green2,
  final Color _borderColor;

  /// Background color of field,
  /// normal: ColorUtil.BW8,
  /// failed: ColorUtil.orange * 0.1, Color(0x19ff5722)
  /// successed: ColorUtil.green2 * 0.1, Color(0x1924b985)
  final Color _backgroundColor;

  /// Tap action callback
  final GestureTapCallback _onTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: GestureDetector(
            onTap: _onTap,
            child: ConstrainedBox(
              constraints: const BoxConstraints(minHeight: 48.0),
              child: Container(
                decoration: BoxDecoration(
                  color: _backgroundColor,
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  border: Border.all(
                    width: _borderWidth,
                    color: _borderColor,
                  ),
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                child: Row(
                  children: <Widget>[
                    _textfield(),
                    if (_showArrow) _tailArrow(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Content of field
  /// If the [_value] argument is null of empty, show placeholder only
  Widget _textfield() {
    if (_value == null || _value.isEmpty) {
      return Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              _name,
              style: const TextStyle(
                color: ColorUtil.BW3,
                fontSize: 14.0,
              ),
            ),
          ],
        ),
      );
    } else {
      return Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              _name,
              style: const TextStyle(
                color: ColorUtil.BW3,
                fontSize: 10.0,
              ),
            ),
            const SizedBox(height: 2),
            Text(
              _value,
              maxLines: _maxLinesOfValue,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                color: ColorUtil.BW1,
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );
    }
  }

  /// The arrow icon at right of field
  Widget _tailArrow() {
    return SvgPicture.asset(
      CommonAssets.icon_expandmore,
      width: 24,
      height: 24,
      package: CommonAssets.package,
    );
  }
}
