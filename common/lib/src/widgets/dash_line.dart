import 'package:flutter/material.dart';

///
/// 自定义画虚线
///

class DashLine extends StatelessWidget {
  const DashLine(
      {double dashWidth = 2,
      double dashSpace = 2,
      double thickness = 1,
      Color dashColor = Colors.grey,
      Axis axis = Axis.horizontal})
      : assert(dashWidth > 0),
        assert(dashSpace > 0),
        assert(thickness > 0),
        assert(dashColor != null),
        _dashWidth = dashWidth,
        _dashSpace = dashSpace,
        _thickness = thickness,
        _dashColor = dashColor,
        _axis = axis;

  final double _dashWidth;
  final double _dashSpace;
  final double _thickness;
  final Color _dashColor;
  final Axis _axis;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        painter: _DashedLinePainter(
            _dashWidth, _dashSpace, _thickness, _dashColor, _axis));
  }
}

class _DashedLinePainter extends CustomPainter {
  _DashedLinePainter(this._dashWidth, this._dashSpace, this._thickness,
      this._dashColor, this._axis);

  final double _dashWidth;
  final double _dashSpace;
  final double _thickness;
  final Color _dashColor;
  final Axis _axis;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = _dashColor
      ..strokeWidth = _thickness;
    if (_axis == Axis.horizontal) {
      double startX = 0;
      while (startX < size.width) {
        canvas.drawLine(
            Offset(startX, 0), Offset(startX + _dashWidth, 0), paint);
        startX += _dashWidth + _dashSpace;
      }
    } else {
      double startY = 0;
      while (startY < size.height) {
        canvas.drawLine(
            Offset(0, startY), Offset(0, startY + _dashWidth), paint);
        startY += _dashWidth + _dashSpace;
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
