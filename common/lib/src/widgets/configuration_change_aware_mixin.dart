import 'package:common/local_event.dart';
import 'package:common/src/channels/event/native_event_dispatcher.dart';
import 'package:flutter/cupertino.dart';

/// A [State] that can handle the configuration change (such as
/// language/currency changed), and rebuild the child widget.
/// ```
/// class _ExampleWidgetState extends State<ExampleWidget> with ConfigurationChangeAwareMixin {
///   @override
///   void refreshUI() {
///     _bloc.getData();
///   }
///
///   @override
///   Future<void> refreshUserInfo() async {
///     final userInfo = await _nativeInfoManager.provideUserInfo();
///     // Update your state
///   }
///
///   @override
///   Widget build(BuildContext context) {
///     return Container();
///   }
/// }
///
/// ```
///
mixin ConfigurationChangeAwareMixin<T extends StatefulWidget> on State<T> {
  final List<VoidCallback> _removeReceiverCallbacks = [];

  /// Allow set a mock LocalEventManager to the LocalEventReceiverMixin for testing purpose.
  @visibleForTesting
  LocalEventManager mockLocalEventManager() => null;

  LocalEventManager get _getLocalEventManager =>
      mockLocalEventManager() ?? LocalEventManager();

  @override
  void dispose() {
    for (final callback in _removeReceiverCallbacks) {
      callback?.call();
    }
    _removeReceiverCallbacks.clear();
    super.dispose();
  }

  @override
  @mustCallSuper
  void initState() {
    super.initState();

    _removeReceiverCallbacks.add(_getLocalEventManager
        .addLocalEventReceiver<UIRefreshEvent>((localEventReceiver) {
      refreshUI();
    }));
    _removeReceiverCallbacks.add(_getLocalEventManager
        .addLocalEventReceiver<UserInfoUpdatedEvent>((localEventReceiver) {
      refreshUserInfo();
    }));
  }

  /// override this method to reload the api or UI
  /// it will call when language/currency changed
  void refreshUI() {}

  /// override this method to refresh UI related to user info
  /// it will call when user info updated (such as login/logout/edit profile)
  void refreshUserInfo() {}
}
