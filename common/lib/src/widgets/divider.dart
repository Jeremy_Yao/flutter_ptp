import 'package:flutter/material.dart';

/// 水平分割线
class CommonDivider extends StatelessWidget {
  const CommonDivider({
    Key key,
    this.height = 0.5,
    this.indent = 0,
    this.endIndent = 0,
    this.color = const Color.fromRGBO(0, 0, 0, 0.12),
    this.bgColor = Colors.white,
  })  : assert(height != null),
        assert(indent != null),
        assert(endIndent != null),
        assert(color != null),
        assert(bgColor != null),
        super(key: key);

  final double height;
  final double indent;
  final double endIndent;
  final Color color;
  final Color bgColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: bgColor,
      child: Divider(
        height: height,
        thickness: 0.5,
        indent: indent,
        endIndent: endIndent,
        color: color,
      ),
    );
  }
}

/// 垂直分割线
class CommonVerticalDivider extends StatelessWidget {
  const CommonVerticalDivider({
    Key key,
    this.width = 0.5,
    this.indent = 0,
    this.endIndent = 0,
    this.color = const Color.fromRGBO(0, 0, 0, 0.12),
    this.bgColor = Colors.white,
  })  : assert(width != null),
        assert(indent != null),
        assert(endIndent != null),
        assert(color != null),
        assert(bgColor != null),
        super(key: key);

  final double width;
  final double indent;
  final double endIndent;
  final Color color;
  final Color bgColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: bgColor,
      child: VerticalDivider(
        width: width,
        thickness: 0.5,
        indent: indent,
        endIndent: endIndent,
        color: color,
      ),
    );
  }
}
