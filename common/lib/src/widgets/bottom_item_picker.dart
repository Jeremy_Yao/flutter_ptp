import 'package:common/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:common/assets.dart';

class BottomItemPicker extends StatefulWidget {
  const BottomItemPicker(
      {Key key,
      @required String title,
      @required String doneText,
      @required List<String> items,
      @required int selectedIndex,
      SelectedActionCallback onSelected,
      SelectedActionCallback onChanged})
      : assert(
            title != null || items != null || selectedIndex == null,
            'An action sheet must have a non-null value for at least one of the following arguments: '
            'title, items, selectedIndex'),
        _title = title,
        _doneText = doneText,
        _items = items,
        _selectedIndex = selectedIndex,
        _onSelected = onSelected,
        _onChanged = onChanged,
        super(key: key);

  // This is the string of `title`
  final String _title;

  // This is the string of `done action`
  final String _doneText;

  // This is the string list of `actions`
  final List<String> _items;

  // This is the current index of selected item
  final int _selectedIndex;

  // This is the callback of when pressed done button, it will get the index of item
  final SelectedActionCallback _onSelected;

  // This is the callback of when selected item, it will get the index of item
  final SelectedActionCallback _onChanged;

  @override
  State<StatefulWidget> createState() {
    return _BottomItemPickerState();
  }
}

class _BottomItemPickerState extends State<BottomItemPicker> {
  ScrollController _pageController;
  int _currentIndex;

  @override
  void initState() {
    super.initState();

    _currentIndex = widget._selectedIndex;
    _pageController =
        ScrollController(initialScrollOffset: _currentIndex * _itemHeight);
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[
        Container(
          decoration: const BoxDecoration(
            color: ColorUtil.BW10,
            borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
          ),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                _buildHeaderSection(widget._title),
                _fullDivider,
                _buildActionsSection(context),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildHeaderSection(String title) {
    final List<Widget> titleContentGroup = <Widget>[];

    titleContentGroup.add(
      GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: SvgPicture.asset(
          CommonAssets.icon_close_24,
          width: 24,
          height: 24,
          package: CommonAssets.package,
        ),
      ),
    );

    if (title != null) {
      titleContentGroup.add(
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 42, right: 16),
            child: Text(
              title,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: const TextStyle(
                inherit: false,
                fontSize: 16.0,
                fontWeight: FontWeightUtil.semibold,
                color: ColorUtil.BW1,
              ),
            ),
          ),
        ),
      );
    }

    titleContentGroup.add(
      GestureDetector(
        onTap: () {
          if (widget._onSelected != null) {
            widget._onSelected(_currentIndex);
          }
          Navigator.of(context).pop();
        },
        child: Container(
          width: 50,
          height: 28,
          decoration: BoxDecoration(
            color: ColorUtil.BW10,
            borderRadius: const BorderRadius.all(Radius.circular(4)),
            border: Border.all(width: 0.5, color: ColorUtil.BW1),
          ),
          child: Center(
            child: Text(widget._doneText),
          ),
        ),
      ),
    );

    if (titleContentGroup.isEmpty) {
      return Container(
        width: 0.0,
        height: 0.0,
      );
    }

    return Container(
      height: _headerSecionHeight,
      padding: EdgeInsets.symmetric(horizontal: _contentIndent),
      child: Row(children: titleContentGroup),
    );
  }

// Create the actions section of action sheet.
  Widget _buildActionsSection(BuildContext context) {
    final Widget scrollView = NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        if (notification is ScrollEndNotification) {
          _onScrollEnded(notification.metrics);
        }
        return true;
      },
      child: ListView(
        controller: _pageController,
        itemExtent: _itemHeight,
        padding: EdgeInsets.symmetric(vertical: _itemHeight * 3),
        children: _buildActions(),
      ),
    );

    final Widget topMark = Positioned(
      top: 0,
      left: 0,
      right: 0,
      child: IgnorePointer(
        ignoring: true,
        child: Container(
          alignment: Alignment.bottomCenter,
          height: _itemHeight * 3,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [ColorUtil.BW10, ColorUtil.BW10.withOpacity(0.38)],
            ),
          ),
        ),
      ),
    );

    final Widget bottomMark = Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: IgnorePointer(
        ignoring: true,
        child: Container(
          alignment: Alignment.topCenter,
          height: _itemHeight * 3,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [ColorUtil.BW10, ColorUtil.BW10.withOpacity(0.38)],
            ),
          ),
        ),
      ),
    );

    final Widget centerCellBox = Positioned.fill(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _fullDivider,
          SizedBox(
            height: _itemHeight,
          ),
          _fullDivider,
        ],
      ),
    );

    return Container(
      height: 362.0 - _headerSecionHeight,
      padding: EdgeInsets.symmetric(vertical: _contentIndent),
      child: Stack(
        children: <Widget>[
          scrollView,
          topMark,
          bottomMark,
          centerCellBox,
        ],
      ),
    );
  }

  /// Create action widgets from `actionLabels`
  List<Widget> _buildActions() {
    final children = <Widget>[];
    for (int i = 0; i < widget._items.length; i++) {
      // add action item
      children.add(
        GestureDetector(
          onTap: () {
            _currentIndex = i;
            _scrollToIndex(_currentIndex);
          },
          child: Container(
            height: _itemHeight,
            padding: EdgeInsets.symmetric(horizontal: _contentIndent),
            child: Center(
              child: Text(
                widget._items[i],
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  inherit: false,
                  fontSize: 18.0,
                  color: ColorUtil.BW1,
                ),
              ),
            ),
          ),
        ),
      );
    }
    return children;
  }

  void _onScrollEnded(ScrollMetrics metrics) {
    if (!_pageController.hasClients) {
      return;
    }

    if (metrics.pixels > metrics.maxScrollExtent ||
        metrics.pixels < metrics.minScrollExtent) {
      return;
    }

    final double offsetY = metrics.pixels;
    _currentIndex = (offsetY / _itemHeight).round();

    // 直接调用_pageController.animateTo()没有作用
    // 参考flutter issue中的解决方案，使用Future.delayed
    // https://github.com/flutter/flutter/issues/20480#issuecomment-415332934
    Future.delayed(Duration.zero, () {
      _scrollToIndex(_currentIndex);
    });
  }

  void _scrollToIndex(int index) {
    _pageController.animateTo(
      _itemHeight * index,
      duration: const Duration(milliseconds: 100),
      curve: Curves.linear,
    );

    if (widget._onChanged != null) {
      widget._onChanged(_currentIndex);
    }
  }

  final double _headerSecionHeight = 52.0;
  final double _itemHeight = 40.0;
  final double _contentIndent = 16.0;

  final Divider _fullDivider = const Divider(
    height: 0.5,
    thickness: 0.5,
    color: Color(0x1e000000),
  );
}
