import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class TextIcon extends StatelessWidget {
  const TextIcon(this._icon, this._text, {String package}) : _package = package;

  final String _text;
  final String _icon;
  final String _package;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Flexible(
          child: Text(
            _text,
            style: const TextStyle(fontSize: 12, color: ColorUtil.BW1),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8, right: 16),
          child: SvgPicture.asset(
            _icon,
            package: _package,
          ),
        )
      ],
    );
  }
}
