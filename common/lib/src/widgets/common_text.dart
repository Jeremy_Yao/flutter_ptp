import 'package:common/common.dart';
import 'package:flutter/material.dart';

/// 【Class Summary】
/// 这个类封装了[Text]控件4种常用实现
///  * `CommonText()` 普通文本
///  * `CommonText.bold()` 加粗文本
///  * `CommonText.semiBold()` 半粗文本
///  * `CommonText.textStyle()` 指定`textStyle`的文本
///
/// 【Feature】
///  1. 支持快速构造文本，对一般文本控件只需要设置2个参数(`content`, `fontSize`)即可
///  2. 支持 "普通文本" "加粗文本" "半粗文本" 快速切换，只需要修改一个调用的方法即可
///  3. 支持默认截断，支持默认颜色设置
///  4. 默认自动换行
///  5. 默认文本左对齐
///
/// 【参数说明】
/// * 必须参数 `content`, 当content为`null`时,内部会设置文本为"",防止Text控件奔溃
/// * 可选参数 `fontSize`,字体，默认`16`
/// * 可选参数 `height`,行高倍数，设置了会设置内部`Text`控件的`TextStyle`里面的`height`属性，默认 `null`
/// * 可选参数 `overflow`,截断模式，默认`TextOverflow.ellipsis`
/// * 可选参数 `textAlign`,对齐方式，默认`TextAlign.left`
/// * 可选参数 `textColor`,文本颜色，默认`ColorUtil.BW1`
/// * 可选参数 `backgroundColor`,背景颜色，默认`Colors.transparent`
/// * 可选参数 `decorationColor`,最大行数，默认`ColorUtil.BW2`
/// * 可选参数 `decoration`,背景颜色，默认`TextDecoration.none`
/// * 可选参数 `maxLines`,最大行数，默认`99999`
/// * 可选参数 `textStyle`,字体的文本样式，提供这个参数方便对接`design_token`
///
///
/// 【Example】
///  ```
///  // normal text
///  CommonText(
///   content: "some text",
///   fontSize: 14,
///  );
///
///  // semiBold text
///  CommonText.semiBold(
///   content: "some text",
///   fontSize: 14,
///  );
///
///  // bold text
///  CommonText.bold(
///   content: "some text",
///   fontSize: 14,
///  );
///
///  // textStyle text
///  CommonText.textStyle(
///    maxLines: 1,
///    content: "some text",
///    textStyle: _textTheme.textH2
///    .copyWith(color: _colorScheme.colorTextOnDark),
///   );
///  ```
class CommonText extends StatelessWidget {
  /// Normal text
  CommonText({
    @required String content,
    double fontSize,
    double height,
    TextOverflow overflow,
    TextAlign textAlign,
    Color textColor,
    Color backgroundColor,
    Color decorationColor,
    TextDecoration decoration,
    int maxLines,
  }) : this._text(
          content: content,
          overflow: overflow,
          fontWeight: FontWeight.normal,
          textAlign: textAlign,
          fontSize: fontSize,
          height: height,
          color: textColor,
          backgroundColor: backgroundColor,
          maxLines: maxLines,
          decoration: decoration,
          decorationColor: decorationColor,
        );

  /// TextStyle text
  CommonText.textStyle({
    @required String content,
    TextOverflow overflow,
    TextAlign textAlign,
    TextStyle textStyle,
    int maxLines,
  }) : this._text(
          content: content,
          overflow: overflow,
          textAlign: textAlign,
          textStyle: textStyle,
          maxLines: maxLines,
        );

  /// SemiBold text
  CommonText.semiBold({
    @required String content,
    double fontSize,
    double height,
    TextOverflow overflow,
    TextAlign textAlign,
    Color textColor,
    Color backgroundColor,
    Color decorationColor,
    TextDecoration decoration,
    int maxLines,
  }) : this._text(
          content: content,
          overflow: overflow,
          fontWeight: FontWeight.w600,
          textAlign: textAlign,
          fontSize: fontSize,
          height: height,
          color: textColor,
          backgroundColor: backgroundColor,
          maxLines: maxLines,
          decoration: decoration,
          decorationColor: decorationColor,
        );

  /// Bold text
  CommonText.bold({
    @required String content,
    double fontSize,
    double height,
    TextOverflow overflow,
    TextAlign textAlign,
    Color textColor,
    Color backgroundColor,
    Color decorationColor,
    TextDecoration decoration,
    int maxLines,
  }) : this._text(
          content: content,
          overflow: overflow,
          fontWeight: FontWeight.bold,
          textAlign: textAlign,
          fontSize: fontSize,
          height: height,
          color: textColor,
          backgroundColor: backgroundColor,
          maxLines: maxLines,
          decoration: decoration,
          decorationColor: decorationColor,
        );

  CommonText._text({
    @required String content,
    TextOverflow overflow,
    FontWeight fontWeight,
    TextAlign textAlign,
    double fontSize,
    double height,
    Color color,
    Color backgroundColor,
    TextStyle textStyle,
    int maxLines,
    TextDecoration decoration,
    Color decorationColor,
  }) : this._(
          content,
          overflow: overflow ?? TextOverflow.ellipsis,
          maxLines: maxLines ?? 99999,
          textAlign: textAlign ?? TextAlign.left,
          textHeightBehavior: const TextHeightBehavior(
            applyHeightToFirstAscent: false,
            applyHeightToLastDescent: false,
          ),
          style: textStyle ??
              TextStyle(
                decoration: decoration ?? TextDecoration.none,
                decorationColor: decorationColor ?? ColorUtil.BW2,
                fontSize: fontSize ?? 16.0,
                height: height,
                fontWeight: fontWeight ?? FontWeight.normal,
                color: color ?? ColorUtil.BW1,
                backgroundColor: backgroundColor ?? Colors.transparent,
              ),
        );

  const CommonText._(
    this.data, {
    Key key,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
  }) : super(key: key);

  final String data;
  final TextStyle style;
  final StrutStyle strutStyle;
  final TextAlign textAlign;
  final TextDirection textDirection;
  final Locale locale;
  final bool softWrap;
  final TextOverflow overflow;
  final double textScaleFactor;
  final int maxLines;
  final String semanticsLabel;
  final TextWidthBasis textWidthBasis;
  final TextHeightBehavior textHeightBehavior;

  @override
  Widget build(BuildContext context) {
    return Text(
      data ?? '',
      style: style,
      strutStyle: strutStyle,
      textAlign: textAlign,
      textDirection: textDirection,
      locale: locale,
      softWrap: softWrap,
      overflow: overflow,
      textScaleFactor: textScaleFactor,
      maxLines: maxLines,
      semanticsLabel: semanticsLabel,
      textWidthBasis: textWidthBasis,
      textHeightBehavior: textHeightBehavior,
    );
  }
}
