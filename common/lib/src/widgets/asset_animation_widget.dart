import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/widgets.dart';

class AssetAnimationWidget extends StatelessWidget {
  const AssetAnimationWidget(this.assetName, {this.assetAnimationName});

  /// Name of the file to be loaded from the AssetBundle.
  final String assetName;

  /// The name of the animation to play.
  final String assetAnimationName;

  @override
  Widget build(BuildContext context) {
    return FlareActor(assetName, animation: assetAnimationName);
  }
}
