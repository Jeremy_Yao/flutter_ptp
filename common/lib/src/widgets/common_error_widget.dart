import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommonErrorWidget extends StatelessWidget {
  const CommonErrorWidget(
      {Widget child,
      String content,
      String buttonTitle,
      VoidCallback onRefreshTap})
      : _child = child,
        _content = content,
        _buttonTitle = buttonTitle,
        _onRefreshTap = onRefreshTap;

  final Widget _child;
  final String _content;
  final String _buttonTitle;
  final VoidCallback _onRefreshTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        if (_child != null) _child,
        if (_content != null && _content.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Text(
              _content,
              style: const TextStyle(fontSize: 14, color: ColorUtil.BW2),
              textAlign: TextAlign.center,
            ),
          ),
        if (_buttonTitle != null && _buttonTitle.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: OutlinedButton(
              onPressed: _onRefreshTap,
              style: OutlinedButton.styleFrom(
                  side: const BorderSide(color: ColorUtil.BW1, width: 0.5)),
              child: Text(_buttonTitle,
                  style: const TextStyle(fontSize: 14, color: ColorUtil.BW1)),
            ),
          ),
      ],
    );
  }
}
