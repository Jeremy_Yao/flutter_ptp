import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'package:common/assets.dart';
import 'package:common/common_navigator.dart';
import 'package:common/src/utils/widget_util.dart';

typedef OnTapLeadingCallback = void Function();

/// 这个类是[AppBar]的封装类，实现iOS和Android的差异化特征，返回按钮差异化默认关闭，需要差异化
/// 请将[showDifferentPlatformLeadingIcon] 设置为 `true`
///
///【feature1】返回按钮
///  * iOS     < 箭头
///  * Android <- 箭头
///
///【feature2】标题位置
///  * iOS 标题居中
///  * Android 标题居左
///
///【feature3】返回事件自定义
/// 默认是pop到上一个页面，需要自定义事件，请自己实现[leadingCallback]
///
/// @param [title] 导航栏的标题
/// @param [titleWidget] 导航栏的标题,优先级比[title]字段高，为了兼容local_cms
/// @param [actions] 导航栏的右边的动作按钮
/// @param [leading] 导航栏的左边的leading的控件，默认内部实现，可以通过这个接口自定义控件
/// @param [leadingCallback] 导航栏的返回按钮事件，默认实现`Provider.of<CommonNavigator>(context, listen: false).pop();`
/// @param [elevation] 导航栏底部阴影值，默认值是10
/// @param [backgroundColor] 导航栏背景颜色，默认值是[Colors.white]
/// @param [showBottomLine] 是否显示底部分割线，默认为`false`不显示
/// @param [showDifferentPlatformLeadingIcon] 是否显示差异化leading icon，默认为`false`不做差异化
///
/// 【Example】
/// ```
///  Scaffold(
///   appBar: CommonAppBar(
///     title: "title",
///     elevation: 0,
///   ),
///   body: const Center(
///     child: Text("some text"),
///   ),
/// );
/// ```
class CommonAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CommonAppBar({
    Key key,
    this.title,
    this.titleWidget,
    this.actions,
    this.leading,
    this.leadingCallback,
    this.elevation = 10,
    this.backgroundColor = Colors.white,
    this.showBottomLine = false,
    this.showDifferentPlatformLeadingIcon = false,
  })  : assert(showBottomLine != null),
        assert(showDifferentPlatformLeadingIcon != null),
        super(key: key);

  final String title;
  final Widget titleWidget;
  final List<Widget> actions;
  final OnTapLeadingCallback leadingCallback;
  final double elevation;
  final Widget leading;
  final Color backgroundColor;
  final bool showBottomLine;
  final bool showDifferentPlatformLeadingIcon;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: _getTitle(),
      bottom: showBottomLine
          ? PreferredSize(
              preferredSize: const Size.fromHeight(0.5),
              child: Container(
                color: ColorUtil.border,
                height: 0.5,
              ),
            )
          : null,
      backgroundColor: backgroundColor,
      elevation: elevation,
      leading: IconButton(
        icon: _leadingIcon(context),
        onPressed: () {
          if (leadingCallback != null) {
            leadingCallback();
          } else {
            Provider.of<CommonNavigator>(context, listen: false).pop();
          }
        },
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        color: ColorUtil.BW2,
      ),
      actions: actions,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  Widget _leadingIcon(BuildContext context) {
    if (leading != null) {
      return leading;
    }

    Widget w;
    final platform = Theme.of(context).platform;
    if (showDifferentPlatformLeadingIcon) {
      if (platform == TargetPlatform.android) {
        w = SvgPicture.asset(
          CommonAssets.icon_back_android_24,
          width: 24,
          height: 24,
          package: CommonAssets.package,
        );
      } else if (platform == TargetPlatform.iOS) {
        w = SvgPicture.asset(
          CommonAssets.icon_back_black_24,
          color: ColorUtil.BW2,
          width: 24,
          height: 24,
          package: CommonAssets.package,
        );
      } else {
        assert(false, "目前该控件只支持iOS和Android 当前平台 $platform 不支持");
      }
    } else {
      w = SvgPicture.asset(
        CommonAssets.icon_back_black_24,
        width: 24,
        height: 24,
        package: CommonAssets.package,
      );
    }

    return w;
  }

  Widget _getTitle() {
    if (titleWidget != null) {
      return titleWidget;
    }

    return Text(
      title ?? "",
      style: TextStyle(
        color: ColorUtil.BW1,
        fontSize: Platform.isAndroid ? 20 : 17,
        fontWeight: FontWeight.w500,
      ),
    );
  }
}
