import 'package:flutter/material.dart';

Future<DateTime> showCommonDatePicker({
  @required BuildContext context,
  @required DateTime initialDate,
  @required DateTime firstDate,
  @required DateTime lastDate,
}) async {
  return showDatePicker(
    context: context,
    initialDate: initialDate,
    firstDate: firstDate,
    lastDate: lastDate,
    builder: (BuildContext context, Widget child) {
      return Theme(
        data: ThemeData(
          primarySwatch: const MaterialColor(
            0xffff5722,
            <int, Color>{
              50: Color(0xFFFFA386),
              100: Color(0xFFFF9472),
              200: Color(0xFFFF845E),
              300: Color(0xFFFF754A),
              400: Color(0xFFFF6636),
              500: Color(0xffff5722),
              600: Color(0xFFE8501F),
              700: Color(0xFFD1481C),
              800: Color(0xFFBA4019),
              900: Color(0xFFA33816),
            },
          ),
        ),
        child: child,
      );
    },
  );
}
