import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class KlookLoading extends StatelessWidget {
  const KlookLoading({Color backgroundColor})
      : _backgroundColor = backgroundColor;

  final Color _backgroundColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: _backgroundColor != null
          ? BoxDecoration(color: _backgroundColor)
          : null,
      child: const Center(
        child: CupertinoActivityIndicator(animating: true, radius: 15),
      ),
    );
  }
}
