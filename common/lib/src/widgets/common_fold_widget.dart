import 'package:flutter/cupertino.dart';

/// 一个可展开的 widget
///
/// isSelected 表示是否选中，选中为展开状态，否则为收起状态
///
/// 使用方法如下
///class ListOption extends StatefulWidget {
///  @override
///  _ListOptionState createState() => _ListOptionState();
///}
///
///class _ListOptionState extends State<ListOption> {
///  int _character = 0;
///
///  @override
///  Widget build(BuildContext context) {
///    return Column(
///      children: <Widget>[
///        CommonFoldWidget(
///          isSelected: true,
///          onChanged: onChanged,
///          topWidget: Row(
///            children: <Widget>[
///              Text("hello"),
///            ],
///          ),
///          foldWidget: Container(
///            height: 40,
///            color: Colors.blue,
///          ),
///        ),
///        CommonFoldWidget(
///          onChanged: onChanged,
///          topWidget: Text("hello"),
///          foldWidget: Container(
///            height: 40,
///            color: Colors.blue,
///          ),
///        ),
///        CommonFoldWidget(
///          onChanged: onChanged,
///          topWidget: Text("hello"),
///          foldWidget: Container(
///            height: 40,
///            color: Colors.blue,
///          ),
///        ),
///      ],
///    );
///  }
///
///  void onChanged(bool isSelected) {
///
///  }
///}

class CommonFoldWidget<T> extends StatefulWidget {
  const CommonFoldWidget({
    this.isSelected = false,
    this.isEnable = true,
    this.topWidget,
    this.topWidgetBuilder,
    this.foldWidget,
    this.onChanged,
    this.animationDuration = const Duration(milliseconds: 200),
  }) : assert(topWidget != null || topWidgetBuilder != null);

  final Duration animationDuration;
  final bool isSelected;
  final bool isEnable;
  final Widget topWidget;
  final Widget Function(AnimationController controller) topWidgetBuilder;
  final Widget foldWidget;
  final ValueChanged<bool> onChanged;

  @override
  _InnerCommonFoldWidgetState createState() => _InnerCommonFoldWidgetState();
}

class _InnerCommonFoldWidgetState extends State<CommonFoldWidget>
    with SingleTickerProviderStateMixin {
  Animation<double> _heightFactor;
  AnimationController _controller;
  final Animatable<double> _easeInTween = CurveTween(curve: Curves.easeIn);
  bool _isSelected;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(duration: widget.animationDuration, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);
    _isSelected = widget.isSelected;
  }

  @override
  void didUpdateWidget(CommonFoldWidget oldWidget) {
    if (oldWidget.isSelected != widget.isSelected) {
      _isSelected = widget.isSelected;
    }
    super.didUpdateWidget(oldWidget);
  }

  void _handleTap() {
    if (widget.isEnable) {
      setState(() {
        _isSelected = !_isSelected;
      });
      if (widget.onChanged != null) {
        widget.onChanged(_isSelected);
      }
    }
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    final topWidget = widget.topWidget ?? widget.topWidgetBuilder(_controller);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        GestureDetector(
          onTap: _handleTap,
          behavior: HitTestBehavior.opaque,
          child: topWidget,
        ),
        ClipRect(
          child: Align(
            heightFactor: _heightFactor.value,
            child: child,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_isSelected) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: widget.foldWidget,
    );
  }
}
