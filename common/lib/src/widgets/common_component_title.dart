import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/material.dart';

class CommonComponentTitle extends StatelessWidget {
  const CommonComponentTitle(
      {@required String title,
      Color backgroundColor = Colors.transparent,
      String content = "",
      String viewMoreTitle = "",
      bool showViewMore = false,
      VoidCallback viewMoreCallBack})
      : assert(title != null),
        _title = title,
        _backgroundColor = backgroundColor,
        _content = content,
        _viewMoreTitle = viewMoreTitle,
        _showViewMore = showViewMore,
        _viewMoreCallBack = viewMoreCallBack;

  // 标题
  final String _title;
  // 背景色 默认透明
  final Color _backgroundColor;
  // 内容
  final String _content;
  // viewMore的标题
  final String _viewMoreTitle;
  // 是否展示viewMore
  final bool _showViewMore;
  // viewMore点击的回调
  final VoidCallback _viewMoreCallBack;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: _backgroundColor,
      padding: const EdgeInsets.only(left: 16, right: 16, top: 40, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 24,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Text(
                  _title,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Color(0xff212121),
                      height: 1.33,
                      fontWeight: FontWeightUtil.bold),
                )),
                if (_showViewMore)
                  Align(
                      alignment: Alignment.bottomRight,
                      child: GestureDetector(
                        onTap: () {
                          if (_viewMoreCallBack != null) {
                            _viewMoreCallBack();
                          }
                        },
                        child: Text(_viewMoreTitle,
                            style: const TextStyle(
                                fontSize: 12,
                                color: Color(0xff4985E6),
                                height: 1.19)),
                      ))
              ],
            ),
          ),
          if (_content.isNotEmpty) const SizedBox(height: 8),
          if (_content.isNotEmpty)
            Text(_content,
                style: const TextStyle(
                    fontSize: 12, color: Color(0xff757575), height: 1.33)),
        ],
      ),
    );
  }
}
