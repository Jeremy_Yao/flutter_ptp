import 'package:flutter/material.dart';

class WeekdayRow extends StatelessWidget {
  Widget _weekdayContainer(String weekDay) => Expanded(
        child: Center(
          child: DefaultTextStyle(
            style: const TextStyle(
              color: Color(0xffb2b2b2),
              fontSize: 12.0,
            ),
            child: Text(
              weekDay,
            ),
          ),
        ),
      );

  // TODO(terry): 以后使用新日历控件规范
  List<Widget> _renderWeekDays() {
    final List<Widget> list = [];
    // TODO(terry): LANG_MISSING
    list.add(_weekdayContainer("Sun"));
    list.add(_weekdayContainer("Mon"));
    list.add(_weekdayContainer("Tue"));
    list.add(_weekdayContainer("Wed"));
    list.add(_weekdayContainer("Thu"));
    list.add(_weekdayContainer("Fri"));
    list.add(_weekdayContainer("Sat"));
    return list;
  }

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _renderWeekDays(),
      );
}
