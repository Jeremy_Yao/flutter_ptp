import 'package:common/common.dart';
import 'package:flutter/material.dart';

// TODO(terry): 以后改为Decoration构造函数，而不是通过状态去判断Decoration
enum DayState {
  /// 选择日期 起始和结束是同一天
  someDay,

  /// 选择日期范围后，起始日期的样式
  rangeStart,

  /// 选择日期范围后，结束日期的样式
  rangeEnd,

  /// 只选择了起始日期样式
  start,

  /// 选择日期范围内的样式
  inRange,

  /// 选择日期范围外的样式
  outRange,

  /// 不可选日期
  unUsed
}

class DayNumber extends StatefulWidget {
  const DayNumber(
      {@required double size,
      @required int day,
      @required DayState dayState,
      @required ValueChanged<int> onDayItemSelected})
      : _size = size,
        _day = day,
        _dayState = dayState,
        _onDayItemSelected = onDayItemSelected;

  final int _day;
  final double _size;
  final DayState _dayState;
  final ValueChanged<int> _onDayItemSelected;

  @override
  _DayNumberState createState() => _DayNumberState();
}

class _DayNumberState extends State<DayNumber>
    with AutomaticKeepAliveClientMixin {
  /// 需要判读 起始日期 结束日期 范围内日期 范围外日期，起始=结束 初选日期
  Widget _dayItem() {
    switch (widget._dayState) {
      case DayState.rangeEnd:
      case DayState.rangeStart:
        return Container(
          width: widget._size,
          height: widget._size,
          alignment: Alignment.center,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: widget._dayState == DayState.rangeStart
                    ? Alignment.centerRight
                    : Alignment.centerLeft,
                child: Container(
                  width: widget._size / 2,
                  height: widget._size,
                  color: const Color(0xffffeee8),
                ),
              ),
              _createInnerDay(Colors.white, 0)
            ],
          ),
        );
      case DayState.start:
      case DayState.someDay:
        return _createInnerDay(Colors.white);
      case DayState.unUsed:
        return _createInnerDay(ColorUtil.BW5);
      default:
        return _createInnerDay();
    }
  }

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return widget._day > 0 && widget._dayState != DayState.unUsed
        ? InkWell(
            onTap: () => widget._onDayItemSelected(widget._day),
            child: _dayItem())
        : _dayItem();
  }

  // TODO(terry): 将属性颜色 ，圆角暴漏出去
  Decoration _getDecoration(DayState dayState) {
    switch (dayState) {
      case DayState.outRange:
      case DayState.unUsed:
        return null;
      case DayState.inRange:
        return const BoxDecoration(color: Color(0xffffeee8));
      case DayState.someDay:
        return BoxDecoration(
            color: ColorUtil.orange,
            borderRadius: BorderRadius.circular(90),
            border: Border.all(color: const Color(0xffFFE4DB), width: 3));
      default:
        return BoxDecoration(
            color: ColorUtil.orange, borderRadius: BorderRadius.circular(90));
    }
  }

  Widget _createInnerDay(
      [Color textColor = Colors.black87, double margin = 5]) {
    return Container(
      width: widget._size,
      height: widget._size,
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(vertical: margin),
      decoration: _getDecoration(widget._dayState),
      child: Text(
        widget._day < 1 ? '' : widget._day.toString(),
        textAlign: TextAlign.center,
        style: TextStyle(
          color: textColor,
          fontSize: 15.0,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
