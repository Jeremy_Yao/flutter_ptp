import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'month_view.dart';
import 'weekday_row.dart';

/// 日期选择控件
/// 选中日期后，通过onSelectChange函数返回选中的日期，
/// 可以通过firstDate 设置起始日期 ，lastDate 设置终止日期 ，不在这个范围内的日期不可选中
/// selectedStartDate 为默认日期 ，selectedEndDate默认
///
/// e.g.,
/// ```
/// class FullScreenDemo extends StatefulWidget {
///   FullScreenDemo({Key key}) : super(key: key);
///
///   @override
///   _FullScreenDemoState createState() => new _FullScreenDemoState();
/// }
///
/// class _FullScreenDemoState extends State<FullScreenDemo> {
///   DateTime selectStartTime;
///   DateTime selectEndTime;
///   List<DateTime> result = <DateTime>[];
///
///   @override
///   Widget build(BuildContext context) {
///     return Scaffold(
///       body: SafeArea(
///         child: Stack(
///           children: <Widget>[
///             CalendarList(
///               firstDate: DateTime(2020, 4, 10),
///               lastDate: DateTime(2021, 4),
///               selectedStartDate: DateTime(2020, 8, 26),
///               selectedEndDate: DateTime(2020, 8, 26),
///               onSelectChange: (selectStartTime, selectEndTime) {
///                 result.clear();
///                 this.selectEndTime = selectEndTime;
///                 this.selectStartTime = selectStartTime;
///                 result.add(selectStartTime);
///                 if (selectEndTime != null) {
///                   result.add(selectEndTime);
///                 }
///               },
///             ),
///             Positioned(
///               bottom: 0.0,
///               left: 0.0,
///               right: 0.0,
///               height: 100.0,
///               child: Container(
///                 alignment: Alignment.center,
///                 padding: EdgeInsets.only(
///                     left: 15.0, top: 15.0, bottom: 32.0, right: 15.0),
///                 decoration: BoxDecoration(
///                   border: Border.all(width: 3, color: Color(0xffaaaaaa)),
///                   /// 实现阴影效果
///                   color: Colors.white,
///                   boxShadow: [
///                     BoxShadow(
///                         color: Colors.black12,
///                         offset: Offset(0, -4.0),
///                         blurRadius: 4.0)
///                   ],
///                 ),
///                 child: Flex(
///                   direction: Axis.horizontal,
///                   children: <Widget>[
///                     Expanded(
///                       flex: 1,
///                       child: FlatButton(
///                         color: (selectStartTime != null ||
///                                 (selectStartTime != null &&
///                                     selectEndTime != null))
///                             ? Colors.deepOrange
///                             : Colors.grey,
///                         onPressed: () => Navigator.pop(context, result),
///                         padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
///                         textColor: Colors.white,
///                         child: DefaultTextStyle(
///                           style: TextStyle(
///                             color: Colors.white,
///                             fontSize: 16.0,
///                           ),
///                           child: Text(
///                             '确  定',
///                             textAlign: TextAlign.center,
///                           ),
///                         ),
///                       ),
///                     ),
///                   ],
///                 ),
///               ),
///             )
///           ],
///         ),
///       ),
///     );
///   }
/// }
/// ```
class CalendarList extends StatefulWidget {
  CalendarList(
      {@required DateTime firstDate,
      @required DateTime lastDate,
      DateTime selectedStartDate,
      DateTime selectedEndDate,
      int scrollIndex = 0,
      Function onSelectChange,
      bool singleChoose = false})
      : assert(firstDate != null),
        assert(lastDate != null),
        assert(!firstDate.isAfter(lastDate),
            'lastDate must be on or after firstDate'),

        /// selectedEndDate 日期要>=selectedStartDate
        assert(
            selectedEndDate == null ||
                (selectedEndDate != null &&
                    selectedStartDate != null &&
                    !selectedEndDate.isBefore(selectedStartDate)),
            'selectedEndDate must be on or after selectedStartDate'),
        _firstDate = firstDate,
        _lastDate = lastDate,
        _selectedStartDate = selectedStartDate,
        _selectedEndDate = selectedEndDate,
        _scrollIndex = scrollIndex,
        _onSelectChange = onSelectChange,
        _singleChoose = singleChoose;

  final DateTime _firstDate;
  final DateTime _lastDate;
  final DateTime _selectedStartDate;
  final DateTime _selectedEndDate;
  final Function _onSelectChange;
  final int _scrollIndex;
  final bool _singleChoose;

  @override
  _CalendarListState createState() => _CalendarListState();
}

class _CalendarListState extends State<CalendarList> {
  final double horizontalPadding = 25.0;
  DateTime _selectStartTime;
  DateTime _selectEndTime;
  int _yearStart;
  int _monthStart;
  int _yearEnd;
  int _monthEnd;
  int _totalMonthsCount;
  ItemScrollController _controller;

  @override
  void initState() {
    super.initState();

    /// 传入的开始日期
    _selectStartTime = widget._selectedStartDate;

    /// 传入的结束日期
    _selectEndTime = widget._selectedEndDate;
    _yearStart = widget._firstDate.year;
    _monthStart = widget._firstDate.month;
    _yearEnd = widget._lastDate.year;
    _monthEnd = widget._lastDate.month;
    _totalMonthsCount =
        _monthEnd - _monthStart + (_yearEnd - _yearStart) * 12 + 1;
    _controller = ItemScrollController();
  }

  /// 选项处理回调
  void _onSelectDayChanged(dateTime) {
    if (widget._singleChoose) {
      _selectStartTime = dateTime;
      _selectEndTime = dateTime;
    } else {
      if (_selectStartTime == null && _selectEndTime == null) {
        _selectStartTime = dateTime;
      } else if (_selectStartTime != null && _selectEndTime == null) {
        _selectEndTime = dateTime;

        /// 如果用户反选，则交换开始和结束日期
        if (_selectStartTime.isAfter(_selectEndTime)) {
          _selectStartTime = _selectEndTime;
          _selectEndTime = null;
        }
      } else if (_selectStartTime != null && _selectEndTime != null) {
        _selectStartTime = null;
        _selectEndTime = null;
        _selectStartTime = dateTime;
      }
    }
    if (_selectStartTime != null) {
      widget._onSelectChange(_selectStartTime, _selectEndTime);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          height: 55.0,
          child: Container(
            padding: EdgeInsets.only(
                left: horizontalPadding, right: horizontalPadding),
            decoration: const BoxDecoration(
              /// border: Border.all(width: 3, color: Color(0xffaaaaaa)),
              /// 实现阴影效果
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Color(0x1f000000),
                    offset: Offset(0, -0.5),
                    blurRadius: 0,
                    spreadRadius: 0)
              ],
            ),
            child: WeekdayRow(),
          ),
        ),
        Container(
          color: Colors.white,
          margin: const EdgeInsets.only(top: 55.0),
          child: ScrollablePositionedList.builder(
            itemScrollController: _controller,
            initialScrollIndex: widget._scrollIndex,
            itemBuilder: (_, index) {
              final int month = index + _monthStart;
              final DateTime calendarDateTime = DateTime(_yearStart, month);
              return _getMonthView(calendarDateTime);
            },
            itemCount: _totalMonthsCount,
          ),
        ),
      ],
    );
  }

  Widget _getMonthView(DateTime dateTime) {
    final int year = dateTime.year;
    final int month = dateTime.month;
    return MonthView(
      year: year,
      month: month,
      padding: horizontalPadding,
      selectDateTimeStart: _selectStartTime,
      selectDateTimeEnd: _selectEndTime,
      dateTimeStart: widget._firstDate,
      dateTimeEnd: widget._lastDate,
      onSelectDayChanged: (dateTime) => _onSelectDayChanged(dateTime),
    );
  }
}
