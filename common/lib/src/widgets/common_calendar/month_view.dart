import 'package:flutter/material.dart';

import '../../../extensions.dart';
import 'month_title.dart';
import 'day_number.dart';

class MonthView extends StatefulWidget {
  const MonthView({
    @required int year,
    @required int month,
    @required double padding,
    @required DateTime selectDateTimeStart,
    @required DateTime selectDateTimeEnd,
    @required DateTime dateTimeStart,
    @required DateTime dateTimeEnd,
    @required Function onSelectDayChanged,
  })  : _year = year,
        _month = month,
        _padding = padding,
        _selectDateTimeStart = selectDateTimeStart,
        _selectDateTimeEnd = selectDateTimeEnd,
        _dateTimeStart = dateTimeStart,
        _dateTimeEnd = dateTimeEnd,
        _onSelectDayChanged = onSelectDayChanged;
  final int _year;
  final int _month;
  final double _padding;
  final DateTime _selectDateTimeStart;
  final DateTime _selectDateTimeEnd;
  final DateTime _dateTimeStart;
  final DateTime _dateTimeEnd;
  final ValueChanged _onSelectDayChanged;

  @override
  _MonthViewState createState() => _MonthViewState();
}

class _MonthViewState extends State<MonthView> {
  DateTime _selectedDate;

  double _getDayNumberSize(double padding) {
    return (MediaQuery.of(context).size.width - padding * 2) / 7;
  }

  Widget _buildMonthDays(BuildContext context, DateTime selectedDate) {
    final List<Row> dayRows = <Row>[];
    final List<DayNumber> dayRowChildren = <DayNumber>[];

    final int daysInMonth =
        DateTimeExt.getDaysInMonth(widget._year, widget._month);

    // 日 一 二 三 四 五 六
    final int firstWeekdayOfMonth =
        DateTime(widget._year, widget._month, 1).weekday;
    int initDay = 1 - firstWeekdayOfMonth;
    initDay = initDay == -6 ? 1 : initDay;
    for (int day = initDay; day <= daysInMonth; day++) {
      final DateTime moment = DateTime(widget._year, widget._month, day);

      DayState dayState = DayState.outRange;
      if (moment.isAfter(widget._dateTimeEnd) ||
          moment.isBefore(widget._dateTimeStart)) {
        dayState = DayState.unUsed;
      } else if (day > 0 &&
          widget._selectDateTimeStart != null &&
          widget._selectDateTimeEnd != null) {
        if (widget._selectDateTimeEnd.isSameDay(widget._selectDateTimeStart) &&
            widget._selectDateTimeStart.isSameDay(moment))
          dayState = DayState.someDay;
        else if (moment.isSameDay(widget._selectDateTimeStart))
          dayState = DayState.rangeStart;
        else if (moment.isSameDay(widget._selectDateTimeEnd))
          dayState = DayState.rangeEnd;
        else if (!moment.isAfter(widget._selectDateTimeEnd) &&
            !moment.isBefore(widget._selectDateTimeStart)) {
          dayState = DayState.inRange;
        }
      } else if (widget._selectDateTimeStart.isSameDay(selectedDate) &&
          widget._selectDateTimeEnd == null &&
          selectedDate?.day == day) {
        dayState = DayState.start;
      }
      dayRowChildren.add(
        DayNumber(
          size: _getDayNumberSize(widget._padding),
          day: day,
          dayState: dayState,
          onDayItemSelected: (int selectedDay) {
            _selectedDate = DateTime(widget._year, widget._month, selectedDay);
            widget._onSelectDayChanged(_selectedDate);
          },
        ),
      );

      if (dayRowChildren.length == 7 || day == daysInMonth) {
        dayRows.add(
          Row(
            children: List<DayNumber>.from(dayRowChildren),
          ),
        );
        dayRowChildren.clear();
      }
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: dayRows,
    );
  }

  Widget _buildMonthView(BuildContext context) {
    return Container(
      width: 7 * _getDayNumberSize(widget._padding),
      margin: EdgeInsets.all(widget._padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MonthTitle(dateTime: DateTime(widget._year, widget._month)),
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: _buildMonthDays(context, _selectedDate),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildMonthView(context),
    );
  }
}
