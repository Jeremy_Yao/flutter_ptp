import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../date_specification.dart';

class MonthTitle extends StatelessWidget {
  const MonthTitle({DateTime dateTime}) : _dateTime = dateTime;
  final DateTime _dateTime;

  @override
  Widget build(BuildContext context) {
    final _commonDateSpecification =
        Provider.of<CommonDateSpecification>(context, listen: false);
    return Text(
      _commonDateSpecification.formatDate(_dateTime, ignoreDay: true),
      style: const TextStyle(fontSize: 16.0, color: ColorUtil.BW1),
      maxLines: 1,
      overflow: TextOverflow.fade,
      softWrap: false,
    );
  }
}
