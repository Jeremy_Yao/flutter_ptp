import 'package:common/assets.dart';
import 'package:common/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

typedef SelectedActionCallback = void Function(int index);

class SelectableActionSheet extends StatelessWidget {
  const SelectableActionSheet({
    Key key,
    @required String title,
    Widget titleTail,
    @required List<String> actionLabels,
    int selectedIndex,
    SelectedActionCallback onSelectedAtIndex,
    ScrollController scrollController,
  })  : assert(
            title != null || actionLabels != null,
            'An action sheet must have a non-null value for at least one of the following arguments: '
            'title, actionLabels'),
        _title = title,
        _titleTail = titleTail,
        _actionLabels = actionLabels,
        _selectedIndex = selectedIndex,
        _onSelectedAtIndex = onSelectedAtIndex,
        _scrollController = scrollController,
        super(key: key);

  // This is the string fo `title`
  final String _title;

  // This is the widget in the right of `title`
  final Widget _titleTail;

  // This is the string list of `actions`
  final List<String> _actionLabels;

  // This is the current index of selected item
  final int _selectedIndex;

  // This is the callback of selected item, it will get the index of item
  final SelectedActionCallback _onSelectedAtIndex;

  // This is the scroll controller, if item list is over screen, you can scroll it.
  final ScrollController _scrollController;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[
        Container(
          decoration: const BoxDecoration(
            color: ColorUtil.BW10,
            borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
          ),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                _buildHeaderSection(_title, _titleTail),
                _fullDivider,
                _buildActionsSection(context),
                _fullDivider,
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildHeaderSection(String title, Widget titleTail) {
    final List<Widget> titleContentGroup = <Widget>[];
    if (title != null) {
      titleContentGroup.add(
        Row(
          children: <Widget>[
            Expanded(
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  inherit: false,
                  fontSize: 16.0,
                  fontWeight: FontWeightUtil.semibold,
                  color: ColorUtil.BW1,
                ),
              ),
            )
          ],
        ),
      );
    }

    if (titleTail != null) {
      titleContentGroup.add(Positioned(right: 0, child: titleTail));
    }

    if (titleContentGroup.isEmpty) {
      return Container(
        width: 0.0,
        height: 0.0,
      );
    }

    return Container(
      height: _headerSecionHeight,
      padding: const EdgeInsets.symmetric(horizontal: _contentIndent),
      child: Stack(
        alignment: Alignment.center,
        children: titleContentGroup,
      ),
    );
  }

// Create the actions section of action sheet.
  Widget _buildActionsSection(BuildContext context) {
    if (_actionLabels == null || _actionLabels.isEmpty) {
      return Container(
        height: 0.0,
      );
    }

    final double maxHeight = MediaQuery.of(context).size.height * 0.8 -
        MediaQuery.of(context).padding.bottom -
        _headerSecionHeight;
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: maxHeight,
      ),
      child: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: _buildActions(),
        ),
      ),
    );
  }

  /// Create action widgets from `actionLabels`
  List<Widget> _buildActions() {
    if (_actionLabels == null || _actionLabels.isEmpty) {
      return [];
    }

    final children = <Widget>[];
    for (int i = 0; i < _actionLabels.length; i++) {
      // add action item
      children.add(
        GestureDetector(
          onTap: () {
            if (_onSelectedAtIndex != null) {
              _onSelectedAtIndex(i);
            }
          },
          child: Container(
            height: 52,
            padding: const EdgeInsets.symmetric(horizontal: _contentIndent),
            color: ColorUtil.BW10,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    _actionLabels[i],
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      inherit: false,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400,
                      color: _selectedIndex == i
                          ? ColorUtil.orange
                          : ColorUtil.BW1,
                    ),
                  ),
                ),
                if (_selectedIndex == i)
                  SvgPicture.asset(
                    CommonAssets.icon_selected,
                    width: 24,
                    height: 24,
                    package: CommonAssets.package,
                  ),
              ],
            ),
          ),
        ),
      );

      // add divider
      if (i < _actionLabels.length - 1) {
        children.add(_indentedDivider);
      }
    }
    return children;
  }

  static const double _headerSecionHeight = 56.0;
  static const double _contentIndent = 16.0;

  final Divider _fullDivider = const Divider(
    height: 0.5,
    thickness: 0.5,
    color: Color(0x1e000000),
  );

  final Divider _indentedDivider = const Divider(
    height: 0.5,
    thickness: 0.5,
    indent: _contentIndent,
    endIndent: _contentIndent,
    color: Color(0x1e000000),
  );
}
