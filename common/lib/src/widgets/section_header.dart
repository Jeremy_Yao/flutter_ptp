import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/material.dart';

class SectionHeader extends StatelessWidget {
  const SectionHeader(this.title,
      {this.rightButtonTitle, this.onRightButtonClick})
      : assert(title != null),
        assert((rightButtonTitle != null && onRightButtonClick != null) ||
            (rightButtonTitle == null && onRightButtonClick == null));

  final String title;
  final String rightButtonTitle;
  final VoidCallback onRightButtonClick;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 56,
        padding: const EdgeInsets.only(bottom: 16),
        alignment: Alignment.bottomLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Text(
                title,
                style: const TextStyle(fontSize: 14, color: ColorUtil.BW2),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            if (rightButtonTitle != null && rightButtonTitle.isNotEmpty)
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: onRightButtonClick,
                child: Column(
                  children: <Widget>[
                    const Spacer(),
                    Text(rightButtonTitle,
                        style: const TextStyle(
                            fontSize: 14, color: ColorUtil.link)),
                  ],
                ),
              )
          ],
        ));
  }
}
