import 'package:common/common.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';

/// 全局flushBar
Flushbar _flushbar;

/// 全局flushBar的状态
FlushbarStatus _status;

/// 常规展示Flushbar 下拉黄色提示文案的弹窗样式.eg.
///```showCommonFlushbar(
///     context,
///     "hello");
///```
void showCommonFlushbar(BuildContext context,
    {String message,
    int milliseconds = 2000,
    Color backgroundColor = ColorUtil.yellow,
    Widget messageText}) {
  assert(context != null);
  assert(message != null || messageText != null);

  _flushbar = Flushbar(
      padding: EdgeInsets.zero,
      messageText: messageText ??
          Container(
            padding: const EdgeInsets.all(16),
            width: double.infinity,
            color: backgroundColor,
            child: Text(
              message,
              style: const TextStyle(fontSize: 14, color: ColorUtil.BW10),
              textAlign: TextAlign.left,
            ),
          ),
      duration: Duration(milliseconds: milliseconds),
      isDismissible: false,
      flushbarStyle: FlushbarStyle.GROUNDED,
      backgroundColor: backgroundColor,
      flushbarPosition: FlushbarPosition.TOP,
      onStatusChanged: (status) {
        _status = status;
      });

  // 通过判断上一个的状态是否已经是dismissed如果是就可以弹起来下一个，如果不是就不可以弹起来
  if (_status == FlushbarStatus.DISMISSED || _status == null) {
    _flushbar.show(context);
  }
}

/// 隐藏flushbar.eg.
///```dismissCommonFlushbar();
///```
void dismissCommonFlushbar() {
  if (_flushbar != null) {
    _flushbar.dismiss();
  }
}
