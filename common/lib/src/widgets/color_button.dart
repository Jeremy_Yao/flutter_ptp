import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/material.dart';

enum ColorButtonSize {
  /// 14号字体
  small,

  /// 16号字体
  big
}

class ColorButton extends StatelessWidget {
  const ColorButton(this.text,
      {this.backgroundColor = ColorUtil.orange,
      this.textColor = ColorUtil.BW10,
      this.onTap,
      this.size = ColorButtonSize.small})
      : assert(text != null);

  final String text;
  final VoidCallback onTap;
  final ColorButtonSize size;
  final Color textColor;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    double fontSize;
    double height;
    FontWeight fontWeight;
    EdgeInsets paddings;
    switch (size) {
      case ColorButtonSize.big:
        fontSize = 16;
        height = 44;
        fontWeight = FontWeightUtil.semibold;
        paddings = const EdgeInsets.only(left: 8, right: 8);
        break;
      case ColorButtonSize.small:
        fontSize = 14;
        height = 28;
        fontWeight = FontWeight.normal;
        paddings = const EdgeInsets.only(left: 8, right: 8);
        break;
    }
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: paddings,
        height: height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: backgroundColor, borderRadius: BorderRadius.circular(4)),
        child: Text(text,
            style: TextStyle(
                fontSize: fontSize, fontWeight: fontWeight, color: textColor)),
      ),
    );
  }
}
