import 'package:common/src/locale/app_localizations.dart';
import 'package:common/src/utils/widget_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchResultButton extends StatelessWidget {
  const SearchResultButton(
      {SearchResultState status, GestureTapCallback onTap, int num})
      : _num = num,
        _status = status,
        _onTap = onTap;

  const SearchResultButton.init({GestureTapCallback onTap})
      : _onTap = onTap,
        _num = 0,
        _status = SearchResultState.init;

  const SearchResultButton.noResult({GestureTapCallback onTap})
      : _onTap = onTap,
        _num = 0,
        _status = SearchResultState.noResult;

  const SearchResultButton.haveResult({GestureTapCallback onTap, int num})
      : _onTap = onTap,
        _num = num,
        _status = SearchResultState.haveResult;

  final int _num;
  final SearchResultState _status;
  final GestureTapCallback _onTap;

  @override
  Widget build(BuildContext context) {
    final _translations = Provider.of<AppLocalizations>(context, listen: false);

    String textString;
    if (_status == SearchResultState.noResult) {
      textString = _translations.text('jrpass_no_result_button');
    } else if (_status == SearchResultState.init) {
      textString = _translations.text('jrpass_view_result');
    } else if (_status == SearchResultState.haveResult) {
      textString = _translations
          .text('jrpass_show_result_button', placeholder: {"{0}": "$_num"});
    }
    return GestureDetector(
      onTap: _onTap,
      child: Container(
        margin: const EdgeInsets.all(8),
        height: 44,
        decoration: BoxDecoration(
            color: _status != SearchResultState.noResult
                ? ColorUtil.orange
                : const Color(0xffd1d1d1),
            borderRadius: BorderRadius.circular(4)),
        child: Center(
          child: _status != SearchResultState.searching
              ? Text(
                  textString,
                  style: const TextStyle(fontSize: 16, color: Colors.white),
                )
              : const SizedBox(
                  height: 24,
                  width: 24,
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.white,
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}

enum SearchResultState { init, searching, haveResult, noResult }
