import 'package:flutter/material.dart';

/// 未选中star的展示：根据个数和传入的unselectedImage创建对应个数的Widget即可；
/// 选中star的展示：
/// 计算出满star的个数，创建对应的Widget；
/// 计算剩余比例的评分，对最后一个Widget进行裁剪；
class StarRating extends StatelessWidget {
  StarRating({
    @required double rating,
    double maxRating = 5,
    double size = 30,
    Color unselectedColor = const Color(0xffbbbbbb),
    Color selectedColor = const Color(0xffe0aa46),
    Widget unselectedImage,
    Widget selectedImage,
    int count = 5,
  })  : _rating = rating,
        _maxRating = maxRating,
        _size = size,
        _count = count,
        _unselectedImage = unselectedImage ??
            Icon(
              Icons.star,
              size: size,
              color: unselectedColor,
            ),
        _selectedImage =
            selectedImage ?? Icon(Icons.star, size: size, color: selectedColor);

  final double _rating;
  final double _maxRating;
  final Widget _unselectedImage;
  final Widget _selectedImage;
  final int _count;
  final double _size;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.min,
          children: _getUnSelectImage(),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: _getSelectImage(),
        ),
      ],
    );
  }

  // 获取评星
  List<Widget> _getUnSelectImage() {
    return List.generate(_count, (index) => _unselectedImage);
  }

  List<Widget> _getSelectImage() {
    // 1.计算Star个数和剩余比例等
    final double oneValue = _maxRating / _count;
    final int entireCount = (_rating / oneValue).floor();
    final double leftValue = _rating - entireCount * oneValue;
    final double leftRatio = leftValue / oneValue;

    // 2.获取start
    final List<Widget> selectedImages = [];
    for (int i = 0; i < entireCount; i++) {
      selectedImages.add(_selectedImage);
    }

    // 3.计算
    final Widget leftStar = ClipRect(
      clipper: MyRectClipper(width: leftRatio * _size),
      child: _selectedImage,
    );
    selectedImages.add(leftStar);

    return selectedImages;
  }
}

class MyRectClipper extends CustomClipper<Rect> {
  MyRectClipper({this.width});

  final double width;

  @override
  Rect getClip(Size size) {
    return Rect.fromLTRB(0, 0, width, size.height);
  }

  @override
  bool shouldReclip(MyRectClipper oldClipper) {
    return width != oldClipper.width;
  }
}
