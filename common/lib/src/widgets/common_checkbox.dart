import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:common/assets.dart';

class CommonCheckBox extends StatefulWidget {
  const CommonCheckBox(
      {Key key, bool isChecked = false, @required ValueChanged<bool> onChanged})
      : this._(
          key: key,
          isChecked: isChecked,
          onChanged: onChanged,
        );

  const CommonCheckBox._(
      {Key key, bool isChecked = false, @required ValueChanged<bool> onChanged})
      : _isChecked = isChecked,
        _onChanged = onChanged,
        super(key: key);

  const CommonCheckBox.disabled({Key key, bool isChecked = false})
      : this._(
          key: key,
          isChecked: isChecked,
          onChanged: null,
        );

  final ValueChanged<bool> _onChanged;

  final bool _isChecked;

  @override
  State<StatefulWidget> createState() => CommonCheckBoxState();
}

@visibleForTesting
class CommonCheckBoxState extends State<CommonCheckBox> {
  @visibleForTesting
  bool isChecked = false;

  @override
  void initState() {
    isChecked = widget._isChecked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isEnable = widget._onChanged != null;
    return GestureDetector(
      onTap: !isEnable
          ? null
          : () {
              setState(() {
                isChecked = !isChecked;
              });
              widget._onChanged(isChecked);
            },
      behavior: HitTestBehavior.opaque,
      child: SvgPicture.asset(
        !isEnable
            ? CommonAssets.icon_checkbox_disable
            : isChecked
                ? CommonAssets.icon_checkbox_sel
                : CommonAssets.icon_checkbox_nor,
        package: CommonAssets.package,
      ),
    );
  }

  @override
  void didUpdateWidget(CommonCheckBox oldWidget) {
    if (oldWidget._isChecked != widget._isChecked) {
      isChecked = widget._isChecked;
    }
    super.didUpdateWidget(oldWidget);
  }
}
