import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// 自定义视图的水平展示按钮的弹窗样式.eg.
/// ```
///  showHorizontalCommonDialog(
///                           context,
///                           actions: [
///                             CommonDialogAction(
///                                 buttonStyle:
///                                     TextStyle(fontSize: 30, color: Colors.red),
///                                 title: "Cancel",
///                                 onTap: () {
///                                   debugPrint(
///                                       "showHorizontalCommonDialog cancel click");
///                                 }),
///                             CommonDialogAction(
///                                 title: "OK",
///                                 onTap: () {
///                                   debugPrint(
///                                       "showHorizontalCommonDialog OK click");
///                                 })
///                           ],
///                           contentWidget: Padding(
///                             padding: EdgeInsets.all(24),
///                             child: RichText(
///                               text: TextSpan(
///                                 text: "here come ",
///                                 style: TextStyle(
///                                     fontSize: 16, color: Colors.black54),
///                                 children: [
///                                   TextSpan(
///                                       text: "here come ",
///                                       style: TextStyle(
///                                           fontSize: 16, color: Colors.red)),
///                                   TextSpan(
///                                       text: "here come ",
///                                       style: TextStyle(
///                                           fontSize: 16, color: Colors.black54)),
///                                 ],
///                               ),
///                             ),
///                           ),
///                         );
/// ```
/// 常规设置title/content的水平展示按钮的弹窗样式.eg.
/// ```
/// showHorizontalCommonDialog(
///                         context,
///                         title: "hello",
///                         content: "test",
///                       );
/// ```
///
/// 展示竖直方向排列按钮的弹窗
Future<T> showVerticalCommonDialog<T>(BuildContext context,
    {String title,
    String content,
    EdgeInsets titlePadding,
    EdgeInsets contentPadding,
    TextStyle titleStyle,
    TextStyle contentStyle,
    EdgeInsets dialogPadding,
    bool outsideDismiss,
    List<CommonDialogAction> actions,
    Widget contentWidget,
    bool autoDismissDialog = true}) {
  return showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return _CommonDialog(
          title: title,
          content: content,
          actions: actions,
          titlePadding: titlePadding,
          contentPadding: contentPadding,
          titleStyle: titleStyle,
          contentStyle: contentStyle,
          contentWidget: contentWidget,
          dialogPadding: dialogPadding,
          dialogButtonType: _CommonDialogButtonType.vertical,
          outsideDismiss: outsideDismiss,
          autoDismissDialog: autoDismissDialog,
        );
      });
}

/// 展示水平方向排列按钮的弹窗
Future<T> showHorizontalCommonDialog<T>(BuildContext context,
    {String title,
    String content,
    EdgeInsets titlePadding,
    EdgeInsets contentPadding,
    TextStyle titleStyle,
    TextStyle contentStyle,
    EdgeInsets dialogPadding,
    bool outsideDismiss,
    List<CommonDialogAction> actions,
    Widget contentWidget,
    bool autoDismissDialog = true}) {
  return showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return _CommonDialog(
          title: title,
          content: content,
          actions: actions,
          titlePadding: titlePadding,
          contentPadding: contentPadding,
          titleStyle: titleStyle,
          contentStyle: contentStyle,
          contentWidget: contentWidget,
          dialogPadding: dialogPadding,
          dialogButtonType: _CommonDialogButtonType.horizontal,
          outsideDismiss: outsideDismiss,
          autoDismissDialog: autoDismissDialog,
        );
      });
}

/// 自定义通用的DialogAction(即对应的按钮)
class CommonDialogAction {
  CommonDialogAction({
    this.title,
    this.onTap,
    this.buttonStyle = const TextStyle(color: ColorUtil.BW1, fontSize: 16),
  });

  final String title;
  final TextStyle buttonStyle;
  final GestureTapCallback onTap;
}

enum _CommonDialogButtonType {
  /// 竖直排列按钮
  vertical,

  /// 水平排列按钮
  horizontal
}

/// 自定义通用的Dialog
class _CommonDialog extends StatefulWidget {
  _CommonDialog(
      {String title,
      String content,
      _CommonDialogButtonType dialogButtonType,
      EdgeInsets titlePadding,
      EdgeInsets contentPadding,
      TextStyle titleStyle,
      TextStyle contentStyle,
      Widget contentWidget,
      bool outsideDismiss,
      EdgeInsets dialogPadding,
      List<CommonDialogAction> actions,
      bool autoDismissDialog = true})
      : assert(title != null || content != null || contentWidget != null),
        _title = title,
        _content = content,
        _outsideDismiss = outsideDismiss ?? false,
        // 如果不设置actions 默认展示一个ok按钮
        // TODO(zhaohong): OK的多语言...
        _actions = actions ??
            [
              CommonDialogAction(title: "OK"),
            ],
        _contentWidget = contentWidget,
        _dialogButtonType =
            dialogButtonType ?? _CommonDialogButtonType.horizontal,
        _titlePadding = titlePadding ??
            ((content?.isNotEmpty ?? false)
                ? const EdgeInsets.only(left: 24, right: 24, top: 24)
                : const EdgeInsets.all(24)),
        _contentPadding = contentPadding ??
            ((title?.isNotEmpty ?? false)
                ? const EdgeInsets.only(
                    left: 24, right: 24, top: 12, bottom: 23)
                : const EdgeInsets.all(24)),
        _titleStyle = titleStyle ??
            const TextStyle(
                color: Color(0xf8000000),
                fontSize: 18,
                fontWeight: FontWeightUtil.semibold),
        _contentStyle = contentStyle ??
            ((title?.isNotEmpty ?? false)
                ? const TextStyle(
                    color: ColorUtil.BW2,
                    fontSize: 14,
                    height: 1.28,
                    fontWeight: FontWeight.w400)
                : const TextStyle(
                    color: ColorUtil.BW1,
                    fontSize: 14,
                    height: 1.28,
                    fontWeight: FontWeight.w400)),
        _dialogPadding =
            dialogPadding ?? const EdgeInsets.symmetric(horizontal: 40),
        _autoDismissDialog = autoDismissDialog;

  /// 弹窗标题
  final String _title;

  /// 弹窗内容
  final String _content;

  /// 弹窗展示按钮的方式 水平/竖直
  final _CommonDialogButtonType _dialogButtonType;

  /// 弹窗标题的padding
  final EdgeInsets _titlePadding;

  /// 弹窗内容的padding
  final EdgeInsets _contentPadding;

  /// 弹窗内容的padding
  final EdgeInsets _dialogPadding;

  /// 弹窗标题的style
  final TextStyle _titleStyle;

  /// 弹窗内容的style
  final TextStyle _contentStyle;

  /// 点击区域是否隐藏
  final bool _outsideDismiss;

  /// 弹窗按钮
  final List<CommonDialogAction> _actions;

  /// 弹窗内容自定义视图
  final Widget _contentWidget;

  /// Whether the dialog should be dismiss automatically when `CommonDialogAction.onTap` called,
  /// `true` by default.
  final bool _autoDismissDialog;

  @override
  State<StatefulWidget> createState() {
    return _CommonDialogState();
  }
}

class _CommonDialogState extends State<_CommonDialog> {
  final List<Widget> _widgetsList = [];

  CommonNavigator _commonNavigator;

  @override
  void didChangeDependencies() {
    _commonNavigator = Provider.of<CommonNavigator>(context, listen: false);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _widgetsList.clear();

    final size = MediaQuery.of(context).size;
    final width =
        (size.width - widget._dialogPadding.left * 2) / widget._actions.length;

    if (widget._title?.isNotEmpty ?? false) {
      _widgetsList.add(Container(
        padding: widget._titlePadding,
        child: Center(
          child: Text(widget._title ?? "",
              style: widget._titleStyle, textAlign: TextAlign.center),
        ),
      ));
    }

    if ((widget._content?.isNotEmpty ?? false) &&
        (widget._contentWidget == null ?? true)) {
      _widgetsList.add(Container(
        padding: widget._contentPadding,
        child: Center(
          child: Text(widget._content ?? "",
              style: widget._contentStyle, textAlign: TextAlign.center),
        ),
      ));
    }

    if (widget._contentWidget != null) {
      _widgetsList.add(widget._contentWidget);
    }

    _widgetsList
        .add(SizedBox(height: 0.5, child: Container(color: ColorUtil.BW6)));

    if (widget._dialogButtonType == _CommonDialogButtonType.horizontal) {
      final List<Widget> actionWidgets = [];
      for (var i = 0; i < widget._actions.length; i++) {
        final CommonDialogAction action = widget._actions[i];
        actionWidgets.add(GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _dismissDialog();

            action.onTap?.call();
          },
          child: Container(
            decoration: i == 0
                ? null
                : const BoxDecoration(
                    border: Border(
                        left: BorderSide(width: 0.5, color: ColorUtil.BW6))),
            width: width,
            height: 48,
            child: Center(
              child: Text(action.title, style: action.buttonStyle),
            ),
          ),
        ));
      }
      _widgetsList.add(Row(children: actionWidgets));
    } else {
      final List<Widget> actionWidgets = [];
      for (var i = 0; i < widget._actions.length; i++) {
        final CommonDialogAction action = widget._actions[i];
        actionWidgets.add(GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _dismissDialog();

            action.onTap?.call();
          },
          child: Container(
            decoration: (i == widget._actions.length - 1)
                ? null
                : const BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 0.5, color: ColorUtil.BW6))),
            height: 48,
            child: Center(
              child: Text(action.title, style: action.buttonStyle),
            ),
          ),
        ));
      }
      _widgetsList.add(Column(children: actionWidgets));
    }

    return WillPopScope(
      onWillPop: () async {
        return widget._outsideDismiss;
      },
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (widget._outsideDismiss) {
            _dismissDialog();
          }
        },
        child: Padding(
            padding: widget._dialogPadding,
            child: Material(
              type: MaterialType.transparency,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        color: Colors.white),
                    alignment: Alignment.center,
                    child: Column(
                      children: _widgetsList,
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }

  void _dismissDialog() {
    if (widget._autoDismissDialog) {
      _commonNavigator.pop();
    }
  }
}
