import 'package:common/assets.dart';
import 'package:common/src/utils/widget_util.dart';
import 'package:common/src/widgets/common_fold_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CommonTitleFoldWidget extends StatelessWidget {
  const CommonTitleFoldWidget({
    this.title,
    this.titleEdgeInsets = const EdgeInsets.all(16),
    this.titleWidget,
    this.foldWidget,
    this.onChanged,
  }) : assert(title != null || titleWidget != null);

  final String title;
  final EdgeInsets titleEdgeInsets;
  final Widget titleWidget;
  final Widget foldWidget;
  final ValueChanged<bool> onChanged;

  @override
  Widget build(BuildContext context) {
    return CommonFoldWidget(
      topWidgetBuilder: (controller) => _topWidget(controller),
      foldWidget: foldWidget,
      onChanged: onChanged,
    );
  }

  Widget _topWidget(AnimationController controller) {
    final animatable = Tween<double>(begin: 0.0, end: 0.5)
        .chain(CurveTween(curve: Curves.easeIn));
    final leftWidget = titleWidget ??
        Text(
          title ?? "",
          style: const TextStyle(
              fontSize: 16,
              color: ColorUtil.BW1,
              fontWeight: FontWeightUtil.semibold),
        );
    return Padding(
      padding: titleEdgeInsets,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          leftWidget,
          RotationTransition(
            turns: controller.drive(animatable),
            child: SvgPicture.asset(
              CommonAssets.icon_expandmore,
              width: 24,
              height: 24,
              package: CommonAssets.package,
            ),
          ),
        ],
      ),
    );
  }
}
