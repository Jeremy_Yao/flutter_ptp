import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:flutter_svg/svg.dart';

import 'package:common/common.dart';
import 'package:common/assets.dart';

class CommonNoNetworkWidget extends StatelessWidget {
  const CommonNoNetworkWidget(
      {VoidCallback onRefreshTap, double centerYOffset = 40})
      : _onRefreshTap = onRefreshTap,
        _centerYOffset = centerYOffset;
  final VoidCallback _onRefreshTap;
  final double _centerYOffset;

  @override
  Widget build(BuildContext context) {
    final _translations = Provider.of<AppLocalizations>(context, listen: false);
    return Container(
      margin: EdgeInsets.fromLTRB(50, 0, 50, _centerYOffset),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset(CommonAssets.icon_nowifi_72,
              package: CommonAssets.package),
          Container(
              padding: const EdgeInsets.all(16),
              child: Text(
                _translations.text("jrpass_no_internet"),
                style: const TextStyle(fontSize: 16, color: ColorUtil.BW2),
                textAlign: TextAlign.center,
              )),
          RawMaterialButton(
            onPressed: _onRefreshTap,
            splashColor: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2),
              side: const BorderSide(
                width: 1,
                color: ColorUtil.BW2,
                style: BorderStyle.solid,
              ),
            ),
            child: Text(
              _translations.text("jrpass_refresh"),
            ),
          ),
        ],
      ),
    );
  }
}
