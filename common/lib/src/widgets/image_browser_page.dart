import 'package:common/assets.dart';
import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';

class ImageBrowserPage extends StatefulWidget {
  ImageBrowserPage(this._imgUrls, [this._initialIndex])
      : assert(_imgUrls != null &&
            _imgUrls is List<String> &&
            _imgUrls.isNotEmpty);

  final List<String> _imgUrls;
  final int _initialIndex;

  @override
  _ImageBrowserPageState createState() =>
      _ImageBrowserPageState(_initialIndex ?? 0);
}

class _ImageBrowserPageState extends State<ImageBrowserPage> {
  _ImageBrowserPageState(this._currentIndex)
      : _pageController = PageController(initialPage: _currentIndex);

  int _currentIndex;
  final PageController _pageController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
            child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              pageController: _pageController,
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider:
                      CachedNetworkImageProvider(widget._imgUrls[index]),
                );
              },
              itemCount: widget._imgUrls.length,
              loadingBuilder: (context, event) => const Center(
                child: KlookLoading(backgroundColor: Colors.black),
              ),
              onPageChanged: (index) {
                setState(() {
                  _currentIndex = index;
                });
              },
            ),
            Stack(
              alignment: Alignment.centerLeft,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("${_currentIndex + 1}/${widget._imgUrls.length}",
                        style:
                            const TextStyle(fontSize: 18, color: Colors.white)),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Provider.of<CommonNavigator>(context, listen: false).pop();
                  },
                  child: Container(
                      margin: const EdgeInsets.only(left: 16),
                      width: 24,
                      height: 24,
                      child: SvgPicture.asset(CommonAssets.icon_close_24,
                          height: 24,
                          width: 24,
                          color: Colors.white,
                          package: CommonAssets.package)),
                )
              ],
            ),
          ],
        )));
  }
}
