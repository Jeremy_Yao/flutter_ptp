import 'dart:ui';

import 'package:common/src/debug/debug.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

/// Report crash to Crashlytics, the definition is same as `FlutterCrashlytics.reportCrash`.
@Deprecated(
    'Use `ForceReportCrashCallback` from `error_handling` package instead, this will be remove in the near future')
typedef ForceReportCrashCallback = Future<void>
    Function(dynamic error, StackTrace stackTrace, {bool forceCrash});

/// Force report the crash to Crashlytics. This is useful for that caught [Exception],
/// but we want to know what cause this [Exception] and for further fix.
@Deprecated(
    'Use `reportCrashToCrashlytics` from `error_handling` package instead, this will be remove in the near future')
ForceReportCrashCallback forceReportCrash;

bool _isNotUnitTest() {
  return !isFlutterTestEnviroment();
}

/// Try wrap an [Error] to [Exception], which will call [forceReportCrash] to report the
/// crash to Crashlytics if the `errorOrException` is [Error] in the release mode.
@Deprecated(
    'Use `reportCrashToCrashlytics` from `error_handling` package instead, this will be remove in the near future')
Exception errorToException(dynamic errorOrException,
    {ForceReportCrashCallback reportCrashCallback,
    ValueGetter<bool> crashOnDebugMode = _isNotUnitTest}) {
  assert(errorOrException is Error || errorOrException is Exception);

  if (errorOrException is Exception) return errorOrException;

  Exception exception;
  // Although Error should be handle on the development phase, but sometimes
  // we didn't face the Error, which cause it crash in the release version,
  // so we wrap the Error inside the Exception, and report it to the Crashlytics
  // in the release version.
  if (errorOrException is Error) {
    // Force crash on debug mode
    assert(() {
      return !crashOnDebugMode();
    }(), "Pls correct the Error: $errorOrException");

    exception = WrappedErrorException(errorOrException);
    (reportCrashCallback ?? forceReportCrash)?.call(exception, null);
  } else {
    exception = errorOrException;
  }

  return exception;
}

/// Wrap an [Error] to [Exception].
@Deprecated(
    'Use `WrappedErrorException` from `error_handling` package instead, this will be remove in the near future')
class WrappedErrorException implements Exception {
  WrappedErrorException(this._error);
  final Error _error;

  @override
  bool operator ==(other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }

    return other is WrappedErrorException &&
        other._error.toString() == _error.toString();
  }

  @override
  int get hashCode => hashValues(_error.runtimeType, _error.toString());

  @override
  String toString() {
    return "[WrappedErrorException] caused by: $_error ";
  }
}
