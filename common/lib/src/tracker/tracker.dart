import 'package:common/src/channels/track_channnel.dart';

abstract class Tracker {
  Future<T> trackEvent<T>(String category, String action,
      [String label,
      String value,
      bool nonInteraction = false,
      Map<String, dynamic> customDimensions]);

  Future<T> trackScreen<T>(String name, {Map<String, String> customDimensions});

  Future<T> trackMixpanel<T>(String name, Map<String, dynamic> properties,
      {bool isPreTrack = false});
}

class NativeTracker implements Tracker {
  NativeTracker(this._channel);

  final TrackChannel _channel;

  @override
  Future<T> trackEvent<T>(String category, String action,
      [String label,
      String value,
      bool nonInteraction = false,
      Map<String, dynamic> customDimensions]) {
    final Map<String, dynamic> args = {
      "category": category,
      "action": action,
      "label": label,
      "value": value,
      "nonInteraction": nonInteraction,
      "customDimensions": customDimensions
    };
    return _channel.trackEvent<T>(args);
  }

  /// isPreTrack 是否为预埋参数，MixPanel 有些参数是在不同地方拿到的，所以需要预埋
  ///
  /// e.g.
  ///   Vertical page, android端需要确认这个事件需要的参数有没有预埋
  /// ```dart
  ///   trackMixpanel(mixEventVerticalPage, null);
  /// ```
  ///
  ///   Vertical Result Listing 参数比较简单，直接埋
  ///
  /// ```dart
  ///   var properties = {
  ///     mixPropertyVerticalType: "JRpass",
  ///     mixPropertyLandingPage: "False",
  ///     mixPropertyEntrancePath: "JR Pass Vertical"
  ///   };
  ///   trackMixpanel(mixEventVerticalResultListPage, properties);
  /// ```
  ///
  ///   Activity Page 为预埋，实际在活动页发送事件，这里只需要把Entrance Path设置好
  ///   Android端需要确认活动页是怎样提前设置参数的
  ///
  /// ```
  ///   var properties = {mixPropertyEntrancePath: "JR Pass Vertical Result Listing"};
  ///   trackMixpanel(mixEventActivityPage, properties, isPreTrack: true);
  /// ```
  @override
  Future<T> trackMixpanel<T>(String name, Map<String, dynamic> properties,
      {bool isPreTrack = false}) {
    final Map<String, dynamic> args = {
      "name": name,
      "properties": properties,
      "is_pre_track": isPreTrack
    };
    return _channel.trackMixpanel<T>(args);
  }

  @override
  Future<T> trackScreen<T>(String name,
      {Map<String, String> customDimensions}) {
    final Map<String, dynamic> args = {
      "name": name,
      "customDimensions": customDimensions
    };
    return _channel.trackScreen(args);
  }
}
