import 'tracker.dart';

const String _mixPanelEventVerticalPage = "Vertical Page";
const String _mixPanelEventActivityPage = "Activity Page";
const String _mixPanelEventDestinationPage = "Destination Page";

extension CommonMixpanelEventExtension on Tracker {
  /// track `Destination Page`
  Future<T> trackMixpanelEventDestinationPage<T>(
      Map<String, dynamic> properties,
      {bool isPreTrack = false}) {
    return trackMixpanel<T>(_mixPanelEventDestinationPage, properties,
        isPreTrack: isPreTrack);
  }

  /// track `Vertical Page`
  Future<T> trackMixpanelEventVerticalPage<T>(Map<String, dynamic> properties,
      {bool isPreTrack = false}) {
    return trackMixpanel<T>(_mixPanelEventVerticalPage, properties,
        isPreTrack: isPreTrack);
  }

  /// track `Activity Page`
  Future<T> trackMixpanelEventActivityPage<T>(Map<String, dynamic> properties,
      {bool isPreTrack = false}) {
    return trackMixpanel<T>(_mixPanelEventActivityPage, properties,
        isPreTrack: isPreTrack);
  }
}
