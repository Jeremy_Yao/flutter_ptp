import 'package:flutter/material.dart';

class ComponentPlaceHolder extends StatelessWidget {
  const ComponentPlaceHolder();

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
