import 'package:built_value/json_object.dart';
import 'package:common/common.dart';
import 'package:error_handling/error_handling.dart';
import 'package:common/src/bloc/bloc.dart';
import 'package:common/src/network/serialization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'component_simple_bloc.dart';
import 'simple_component_state.dart';
import 'component_state_change_notifier.dart';

typedef AsyncContentBuilder = Widget Function(
    BuildContext context, SimpleComponentState state);
typedef ComponentBlocBuilder = ComponentBloc Function(
    BuildContext, SimpleComponentState, ComponentStateChangeNotifier, int);

/// `initialData`和`tabIndex`只需要透传factory传给widget的content.data和index
class SimpleComponentContentBuilder<T, R extends SimpleBlocRepository<T>>
    extends StatefulWidget {
  const SimpleComponentContentBuilder({
    @required MapJsonObject initialData,
    @required int tabIndex,
    @required bool isAsyncLoad,
    @required AsyncContentBuilder asyncChildBuilder,
    ComponentShouldHidden<T> shouldHidden,
    SimpleBlocRepositoryBuilder<R> blocRepositoryBuilder,
    ComponentBlocBuilder blocBuilder,
  })  : assert((blocRepositoryBuilder != null && blocBuilder == null) ||
            (blocBuilder != null &&
                blocRepositoryBuilder == null &&
                shouldHidden == null)),
        assert(asyncChildBuilder != null),
        assert(tabIndex != null),
        assert(isAsyncLoad != null),
        _initialData = initialData,
        _isAsyncLoad = isAsyncLoad,
        _blocRepositoryBuilder = blocRepositoryBuilder,
        _shouldHidden = shouldHidden,
        _tabIndex = tabIndex,
        _asyncChildBuilder = asyncChildBuilder,
        _blocBuilder = blocBuilder;

  final MapJsonObject _initialData;
  final SimpleBlocRepositoryBuilder<R> _blocRepositoryBuilder;
  final ComponentBlocBuilder _blocBuilder;
  final ComponentShouldHidden<T> _shouldHidden;

  final AsyncContentBuilder _asyncChildBuilder;
  final int _tabIndex;
  final bool _isAsyncLoad;

  @override
  _SimpleComponentContentBuilderState<T, R> createState() =>
      _SimpleComponentContentBuilderState<T, R>();
}

class _SimpleComponentContentBuilderState<T, R extends SimpleBlocRepository<T>>
    extends State<SimpleComponentContentBuilder<T, R>> {
  BlocAsync<T> _initialData;

  @override
  void didChangeDependencies() {
    try {
      if (!widget._isAsyncLoad) {
        if (widget._initialData != null &&
            widget._initialData.asMap.isNotEmpty) {
          _initialData = Success<T>(
              Provider.of<Serialization>(context, listen: false)
                  .deserialize<T>(widget._initialData.asMap));
        } else {
          _initialData = Success<T>(null);
        }
      }
    } catch (e) {
      final Exception exception = errorToException(e);
      debugPrint("[${toString()}] execute Exception: ${exception.toString()}");
      _initialData = Fail(exception);
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ComponentStateChangeNotifier>(
      builder: (context, stateChangeNotifier, _) {
        return BlocProvider<ComponentBloc>(
            create: (context) {
              if (widget._blocBuilder != null) {
                final bloc = widget._blocBuilder(
                    context,
                    _initialData != null
                        ? SimpleComponentState<T>.create(_initialData)
                        : null,
                    stateChangeNotifier,
                    widget._tabIndex);
                assert(bloc != null);
                return bloc;
              } else {
                final repository = widget._blocRepositoryBuilder(context);
                assert(repository != null);
                return ComponentSimpleBloc<T>(
                    repository,
                    _initialData != null
                        ? SimpleComponentState<T>.create(_initialData)
                        : null,
                    widget._shouldHidden,
                    stateChangeNotifier,
                    widget._tabIndex);
              }
            },
            child: _InnerSimpleBlocBuilder<T, R>(widget._asyncChildBuilder,
                stateChangeNotifier.getState<T>(tabIndex: widget._tabIndex)));
      },
    );
  }
}

class _InnerSimpleBlocBuilder<T, R extends SimpleBlocRepository<T>>
    extends StatefulWidget {
  const _InnerSimpleBlocBuilder(this._asyncChildBuilder, this._state);

  final AsyncContentBuilder _asyncChildBuilder;
  final SimpleComponentState<T> _state;

  @override
  State<StatefulWidget> createState() => _InnerSimpleBlocBuilderState<T, R>();
}

class _InnerSimpleBlocBuilderState<T, R extends SimpleBlocRepository<T>>
    extends State<_InnerSimpleBlocBuilder<T, R>> {
  ComponentBloc _simpleBloc;

  @override
  void didChangeDependencies() {
    _simpleBloc = Provider.of<ComponentBloc>(context, listen: false);
    _simpleBloc.getData();

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return widget._asyncChildBuilder(context, widget._state);
  }
}
