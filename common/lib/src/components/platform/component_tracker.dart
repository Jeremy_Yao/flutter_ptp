import 'package:common/src/tracker/tracker.dart';

class ComponentTracker {
  ComponentTracker(this._tracker, this._gaEventCategory, this._pageName,
      this._customDimensions,
      {Map<String, dynamic> commonMixPanelProperties})
      : _commonMixPanelProperties = commonMixPanelProperties;

  final Tracker _tracker;
  final String _gaEventCategory;
  final String _pageName;
  final Map<String, dynamic> _customDimensions;
  final Map<String, dynamic> _commonMixPanelProperties;

  void trackClick(String sectionName, String dataName,
      [String label, String value]) {
    _trackEvent(sectionName, dataName, "Clicked", label, value);
  }

  void trackAppear(String sectionName, String dataName,
      [String label, String value]) {
    _trackEvent(sectionName, dataName, "Appear", label, value, true);
  }

  void trackSlide(String sectionName, String dataName,
      [String label, String value]) {
    _trackEvent(sectionName, dataName, "Slide", label, value);
  }

  void trackClosed(String sectionName, String dataName,
      [String label, String value]) {
    _trackEvent(sectionName, dataName, "Closed", label, value);
  }

  void _trackEvent(String sectionName, String dataName, String action,
      String label, String value,
      [bool nonInteraction = false]) {
    final gaAction = "${_pageName}_${sectionName}_${dataName}_$action";
    _tracker.trackEvent(_gaEventCategory, gaAction, label, value,
        nonInteraction, _customDimensions);
  }

  void preTrackMixpanelEntrancePath(
      String eventName, String sectionName, String dataName) {
    final entrancePath = "${_pageName}_${sectionName}_$dataName";
    _tracker.trackMixpanel(eventName, {"Entrance Path": entrancePath},
        isPreTrack: true);
  }

  void trackMixPanel(String eventName, String sectionName, String dataName,
      [Map<String, dynamic> properties]) {
    final entrancePath = "${_pageName}_${sectionName}_$dataName";
    final Map<String, dynamic> trackProperties = {
      "Entrance Path": entrancePath
    };
    if (_commonMixPanelProperties != null &&
        _commonMixPanelProperties.isNotEmpty) {
      trackProperties.addAll(_commonMixPanelProperties);
    }
    if (properties != null && properties.isNotEmpty) {
      trackProperties.addAll(properties);
    }
    _tracker.trackMixpanel(eventName, trackProperties);
  }
}
