import 'package:flutter/material.dart';

import 'component_builder.dart';
import 'component_content_factory.dart';
import 'component_model.dart';

class ComponentContentSimpleFactory extends ComponentContentFactory {
  ComponentContentSimpleFactory(this._buidlerMap, this._handlerName);

  final Map<String, ComponentContentBuilder> _buidlerMap;
  final String _handlerName;
  @override
  bool canHandle(ComponentSectionContent content) {
    return _buidlerMap.containsKey(content.dataType);
  }

  @override
  String get handlerName => _handlerName;

  @override
  Widget createWidget(
      BuildContext context, ComponentSectionContent content, int index) {
    return _buidlerMap[content.dataType](context, content, index);
  }
}
