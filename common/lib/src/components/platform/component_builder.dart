import 'package:flutter/material.dart';

import 'component_model.dart';

/// 构建content widget
typedef ComponentContentBuilder = Widget Function(
    BuildContext context, ComponentSectionContent content, int index);

/// 切换tab构建content
typedef ComponentTabContentBuilder = Widget Function(
    BuildContext context, ComponentSection section, int index);

/// 构建普通外框
typedef ComponentContainerBuilder = Widget Function(
    ComponentSection section, ComponentContentBuilder childBuilder);

/// 构建tab外框
typedef ComponentTabsContainerBuilder = Widget Function(
    ComponentSection section, ComponentTabContentBuilder tabContentBuilder);
