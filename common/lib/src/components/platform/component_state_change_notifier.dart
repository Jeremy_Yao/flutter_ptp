import 'package:flutter/material.dart';

import 'simple_component_state.dart';

class ComponentStateChangeNotifier with ChangeNotifier {
  SimpleComponentState _childState = SimpleComponentState.initialValue();

  final Map<int, SimpleComponentState> _tabStates = {};
  Map<int, SimpleComponentState> get tabStates => _tabStates;

  /// 如果tabIndex为null，则认为是非tab类型容器
  SimpleComponentState<T> getState<T>({int tabIndex = -1}) {
    assert(tabIndex != null);
    if (tabIndex == -1) {
      return SimpleComponentState<T>.create(_childState.value);
    } else {
      var tabState = _tabStates[tabIndex];
      if (tabState == null) {
        tabState = SimpleComponentState.initialValue();
        _tabStates[tabIndex] = tabState;
      }
      return SimpleComponentState<T>.create(tabState.value);
    }
  }

  /// 如果tabIndex为null，则认为是非tab类型容器
  void updateState(SimpleComponentState state, {int tabIndex = -1}) {
    if (tabIndex == -1) {
      _childState = state;
    } else {
      _tabStates[tabIndex] = state;
    }
    notifyListeners();
  }
}
