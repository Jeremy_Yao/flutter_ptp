import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'simple_component_state.dart';
import 'component_state_change_notifier.dart';

/// common container
typedef AsyncContainerBuilder = Widget Function(
    BuildContext context, SimpleComponentState state);

class SimpleComponentContainerBuilder extends StatefulWidget {
  const SimpleComponentContainerBuilder(
      {@required AsyncContainerBuilder asyncContainerBuilder})
      : assert(asyncContainerBuilder != null),
        _asyncContainerBuilder = asyncContainerBuilder;

  final AsyncContainerBuilder _asyncContainerBuilder;

  @override
  SimpleComponentContainerBuilderState createState() =>
      SimpleComponentContainerBuilderState();
}

class SimpleComponentContainerBuilderState
    extends State<SimpleComponentContainerBuilder> {
  final ComponentStateChangeNotifier _stateChangeNotifier =
      ComponentStateChangeNotifier();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: _stateChangeNotifier,
        child: Consumer<ComponentStateChangeNotifier>(
            builder: (context, notifier, __) {
          return widget._asyncContainerBuilder(context, notifier.getState());
        }));
  }
}

/// tab container
typedef AsyncTabContainerBuilder = Widget Function(
    BuildContext context, SimpleComponentState state);

/// tab类型容器构造器
/// `asyncTabContainerBuilder`会跟着`defaultIndex`对应的tab状态执行回调
/// `defaultIndex`默认为0
class SimpleComponentTabContainerBuilder extends StatefulWidget {
  const SimpleComponentTabContainerBuilder(
      {@required AsyncTabContainerBuilder asyncTabContainerBuilder,
      int defaultIndex})
      : _defaultIndex = defaultIndex ?? 0,
        assert(asyncTabContainerBuilder != null),
        _asyncTabContainerBuilder = asyncTabContainerBuilder;

  final AsyncTabContainerBuilder _asyncTabContainerBuilder;
  final int _defaultIndex;

  @override
  SimpleComponentTabContainerBuilderState createState() =>
      SimpleComponentTabContainerBuilderState();
}

class SimpleComponentTabContainerBuilderState
    extends State<SimpleComponentTabContainerBuilder> {
  final ComponentStateChangeNotifier _stateChangeNotifier =
      ComponentStateChangeNotifier();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _stateChangeNotifier,
      child: Consumer<ComponentStateChangeNotifier>(
          builder: (context, notifier, __) {
        return widget._asyncTabContainerBuilder(
            context, notifier.getState(tabIndex: widget._defaultIndex));
      }),
    );
  }
}
