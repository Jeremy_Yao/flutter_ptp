import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

part 'component_model.g.dart';

abstract class ComponentPage
    implements Built<ComponentPage, ComponentPageBuilder> {
  factory ComponentPage([updates(ComponentPageBuilder b)]) = _$ComponentPage;
  ComponentPage._();

  @BuiltValueField(wireName: 'page_id')
  String get pageId;

  @BuiltValueField(wireName: 'meta')
  ComponentPageMeta get meta;

  @BuiltValueField(wireName: 'body')
  ComponentPageBody get body;

  static Serializer<ComponentPage> get serializer => _$componentPageSerializer;
}

abstract class ComponentPageMeta
    implements Built<ComponentPageMeta, ComponentPageMetaBuilder> {
  factory ComponentPageMeta([updates(ComponentPageMetaBuilder b)]) =
      _$ComponentPageMeta;
  ComponentPageMeta._();

  @BuiltValueField(wireName: 'type')
  String get type;

  @nullable
  @BuiltValueField(wireName: 'user_action')
  ComponentPageUserAction get userAction;

  @nullable
  @BuiltValueField(wireName: 'track')
  ComponentPageUserAction get track;

  static Serializer<ComponentPageMeta> get serializer =>
      _$componentPageMetaSerializer;
}

abstract class ComponentPageUserAction
    implements Built<ComponentPageUserAction, ComponentPageUserActionBuilder> {
  factory ComponentPageUserAction([updates(ComponentPageUserActionBuilder b)]) =
      _$ComponentPageUserAction;

  ComponentPageUserAction._();

  @nullable
  @BuiltValueField(wireName: 'can_refresh')
  bool get canRefresh;

  @nullable
  @BuiltValueField(wireName: 'has_navigation')
  bool get hasNavigation;

  static Serializer<ComponentPageUserAction> get serializer =>
      _$componentPageUserActionSerializer;
}

abstract class ComponentPageTracking
    implements Built<ComponentPageTracking, ComponentPageTrackingBuilder> {
  factory ComponentPageTracking([updates(ComponentPageTrackingBuilder b)]) =
      _$ComponentPageTracking;

  ComponentPageTracking._();

  @nullable
  @BuiltValueField(wireName: 'ga_event_category')
  String get gaEventCategory;

  @nullable
  @BuiltValueField(wireName: 'mixpanel_event_prefix')
  String get mixpanelEventPrefix;

  @nullable
  @BuiltValueField(wireName: 'iht_page_name')
  String get ihtPageName;

  static Serializer<ComponentPageTracking> get serializer =>
      _$componentPageTrackingSerializer;
}

abstract class ComponentPageBody
    implements Built<ComponentPageBody, ComponentPageBodyBuilder> {
  factory ComponentPageBody([updates(ComponentPageBodyBuilder b)]) =
      _$ComponentPageBody;
  ComponentPageBody._();

  @BuiltValueField(wireName: 'sections')
  BuiltList<ComponentSection> get sections;

  @nullable
  @BuiltValueField(wireName: 'second_floor')
  SecondFloor get secondFloor;

  static Serializer<ComponentPageBody> get serializer =>
      _$componentPageBodySerializer;
}

abstract class SecondFloor implements Built<SecondFloor, SecondFloorBuilder> {
  factory SecondFloor([void Function(SecondFloorBuilder) updates]) =
      _$SecondFloor;
  SecondFloor._();

  @BuiltValueField(wireName: 'is_show')
  bool get isShow;

  @BuiltValueField(wireName: 'title')
  String get title;

  @BuiltValueField(wireName: 'deep_link')
  String get deepLink;

  @BuiltValueField(wireName: 'banner_url')
  String get bannerUrl;

  @BuiltValueField(wireName: 'type')
  String get type;

  static Serializer<SecondFloor> get serializer => _$secondFloorSerializer;
}

abstract class ComponentSection
    implements Built<ComponentSection, ComponentSectionBuilder> {
  factory ComponentSection([updates(ComponentSectionBuilder b)]) =
      _$ComponentSection;
  ComponentSection._();

  @BuiltValueField(wireName: 'meta')
  ComponentSectionMeta get meta;
  @nullable
  @BuiltValueField(wireName: 'data')
  JsonObject get data;
  @BuiltValueField(wireName: 'body')
  ComponentSectionBody get body;

  static Serializer<ComponentSection> get serializer =>
      _$componentSectionSerializer;
}

abstract class ComponentSectionMeta
    implements Built<ComponentSectionMeta, ComponentSectionMetaBuilder> {
  factory ComponentSectionMeta([updates(ComponentSectionMetaBuilder b)]) =
      _$ComponentSectionMeta;
  ComponentSectionMeta._();

  @nullable
  @BuiltValueField(wireName: 'placeholder')
  String get placeholder;

  @BuiltValueField(wireName: 'type')
  String get fullType;

  @nullable
  @BuiltValueField(wireName: 'name')
  String get sectionName;

  String get type => fullType?.split(':')?.first;
  String get typeParam => fullType?.replaceFirst(type, '');

  static Serializer<ComponentSectionMeta> get serializer =>
      _$componentSectionMetaSerializer;
}

abstract class ComponentSectionBody
    implements Built<ComponentSectionBody, ComponentSectionBodyBuilder> {
  factory ComponentSectionBody([updates(ComponentSectionBodyBuilder b)]) =
      _$ComponentSectionBody;
  ComponentSectionBody._();

  @nullable
  @BuiltValueField(wireName: 'tabs')
  BuiltList<ComponentSectionContent> get tabs;

  @nullable
  @BuiltValueField(wireName: 'content')
  ComponentSectionContent get content;

  static Serializer<ComponentSectionBody> get serializer =>
      _$componentSectionBodySerializer;
}

abstract class ComponentSectionContent
    implements Built<ComponentSectionContent, ComponentSectionContentBuilder> {
  factory ComponentSectionContent([updates(ComponentSectionContentBuilder b)]) =
      _$ComponentSectionContent;

  ComponentSectionContent._();

  @nullable
  @BuiltValueField(wireName: 'title')
  String get title;
  @BuiltValueField(wireName: 'handler')
  String get handler;
  @BuiltValueField(wireName: 'data_type')
  String get dataType;
  @BuiltValueField(wireName: 'load_type')
  String get loadType;
  @nullable
  @BuiltValueField(wireName: 'placeholder')
  String get placeholder;
  @nullable
  @BuiltValueField(wireName: 'src')
  String get src;
  @nullable
  @BuiltValueField(wireName: 'data')
  JsonObject get data;

  static Serializer<ComponentSectionContent> get serializer =>
      _$componentSectionContentSerializer;
}

extension ComponentSectionContentExt on ComponentSectionContent {
  bool get isAsyncLoad => loadType == "async";
}
