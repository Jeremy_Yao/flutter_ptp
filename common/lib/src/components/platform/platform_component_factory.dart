import 'package:built_collection/built_collection.dart';
import 'package:common/components.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'component_container_factory.dart';
import 'component_content_factory.dart';
import 'component_model.dart';

///
/// 平台模块化列表页面组件工厂类
/// 方案链接：[https://klook.slab.com/posts/%E6%A8%A1%E5%9D%97%E5%8C%96%E5%88%97%E8%A1%A8%E9%A1%B5%E9%9D%A2%E7%BB%84%E4%BB%B6%E6%96%B9%E6%A1%88-mr25mr8m]
/// 第一个参数为内容工厂列表，一个业务提供一个或多个工厂
/// 第二个参数为外框样式工厂，目前由平台提供
///
class PlatformComponentFactory {
  PlatformComponentFactory(
      List<ComponentContentFactory> contentFactories, this._containerFactory)
      : _contentFactoryMap = contentFactories.fold(
            <String, ComponentContentFactory>{}, (previousValue, element) {
          assert(!previousValue.containsKey(element.handlerName),
              "${element.runtimeType} and ${previousValue[element.handlerName].runtimeType} have duplicate handlerName");
          previousValue[element.handlerName] = element;
          return previousValue;
        });
  final ComponentContainerFactory _containerFactory;
  final Map<String, ComponentContentFactory> _contentFactoryMap;
  Widget createWidget(BuildContext context, ComponentSection section) {
    if (section.body.content != null) {
      // 普通类型
      return Provider<ComponentSectionMeta>.value(
          value: section.meta,
          child: _containerFactory.createContainer(section,
              (context, content, index) {
            return _contentFactoryMap[content.handler]
                .createWidget(context, content, index);
          }));
    } else if (section.body.tabs != null) {
      // 多tab类型，过滤不支持的tab
      final tabs = section.body.tabs.where(
          (element) => _contentFactoryMap[element.handler].canHandle(element));
      final newSection = (section.toBuilder()
            ..body = (section.body.toBuilder()..tabs = ListBuilder(tabs)))
          .build();
      return Provider<ComponentSectionMeta>.value(
          value: section.meta,
          child: _containerFactory.createTabsContainer(newSection,
              (context, section, tabIndex) {
            final content = section.body.tabs[tabIndex];
            return _contentFactoryMap[content.handler]
                .createWidget(context, section.body.tabs[tabIndex], tabIndex);
          }));
    }
    throw Exception("unsupported section:$section");
  }

  ///
  /// 是否能处理某个section
  /// 单tab类型section只判断外框和内容是否都支持
  /// 多tab类型section判断外框和至少一个tab内容支持
  ///
  bool canHandle(BuildContext context, ComponentSection section) {
    if (!_containerFactory.canHandle(section)) {
      return false;
    }

    if (section.body.content != null) {
      // single child
      final content = section.body.content;
      if (_contentFactoryMap.containsKey(content.handler)) {
        return _contentFactoryMap[content.handler].canHandle(content);
      }
    } else if (section.body.tabs != null) {
      // mutiple child
      return section.body.tabs.any((element) {
        if (_contentFactoryMap.containsKey(element.handler)) {
          return _contentFactoryMap[element.handler].canHandle(element);
        }
        return false;
      });
    }

    return false;
  }
}
