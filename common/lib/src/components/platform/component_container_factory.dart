import 'package:common/src/components/platform/component_builder.dart';
import 'package:flutter/material.dart';

import 'component_model.dart';

abstract class ComponentContainerFactory {
  /// 创建普通的组件外框
  Widget createContainer(
      ComponentSection section, ComponentContentBuilder childBuilder);

  /// 创建带tab选择器的组件外框
  /// 其他tab 由tabContentBuilder 创建
  Widget createTabsContainer(
      ComponentSection section, ComponentTabContentBuilder tabContentBuilder);

  bool canHandle(ComponentSection section);
}
