import 'package:common/src/bloc/bloc.dart';

class SimpleComponentState<T> implements BlocState {
  SimpleComponentState._(this.value);

  factory SimpleComponentState.create(BlocAsync<T> value) =>
      SimpleComponentState._(value);

  factory SimpleComponentState.initialValue() {
    return SimpleComponentState._(const Uninitialized());
  }

  final BlocAsync<T> value;

  @override
  bool operator ==(other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }

    return other is SimpleComponentState && other.value == value;
  }

  @override
  int get hashCode => identityHashCode(value);

  @override
  String toString() {
    return 'SimpleComponentState{value: $value}';
  }

  SimpleComponentState<T> copy(BlocAsync<T> value) =>
      SimpleComponentState._(value);
}
