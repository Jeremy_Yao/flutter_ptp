import 'package:common/common.dart';

class HiddenSuccess<T> extends Success<T> {
  const HiddenSuccess() : super(null);

  @override
  String toString() => 'HiddenSuccess';
}
