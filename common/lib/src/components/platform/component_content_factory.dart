import 'package:flutter/material.dart';

import 'component_model.dart';

abstract class ComponentContentFactory {
  Widget createWidget(
      BuildContext context, ComponentSectionContent content, int index);
  bool canHandle(ComponentSectionContent content);
  String get handlerName;
}
