import 'package:error_handling/error_handling.dart';
import 'package:common/src/bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'hidden_success.dart';
import 'simple_component_state.dart';
import 'component_state_change_notifier.dart';

typedef ComponentShouldHidden<T> = bool Function(T);

abstract class ComponentBloc<T> implements BaseBloc {
  ComponentBloc(this._stateChangeNotifier, this.initailState, this._tabIndex);

  final int _tabIndex;
  final ComponentStateChangeNotifier _stateChangeNotifier;
  final SimpleComponentState<T> initailState;

  /// 如果需要自定义加载逻辑，参考[ComponentSimpleBloc]
  Future<void> getData();

  SimpleComponentState<T> getCurrentState() {
    return _stateChangeNotifier.getState<T>(tabIndex: _tabIndex);
  }

  /// 如果flutter页面不可见
  /// WidgetsBinding.instance.addPostFrameCallback 要等到flutter页面可见才会执行
  /// 导致调用 getCurrentState 拿到的不是最后的状态
  /// 所以添加isAsync参数，注意不能再didChangeDependency 或 setState 中传false
  void updateState(SimpleComponentState<T> state, {bool isAsync = true}) {
    if (isAsync) {
      // TODO(littlegnal): 优化去掉WidgetsBinding.instance.addPostFrameCallback
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _stateChangeNotifier.updateState(state, tabIndex: _tabIndex);
      });
    } else {
      _stateChangeNotifier.updateState(state, tabIndex: _tabIndex);
    }
  }

  Future<void> execute(
      Future<T> Function(SimpleComponentState<T> Function() preState)
          successValue,
      SimpleComponentState<T> Function(
              SimpleComponentState<T> Function() preState,
              BlocAsync<T> blocAsync)
          reducer) async {
    final preS = getCurrentState;
    try {
      updateState(reducer(preS, const Loading()));

      final sv = Success<T>(await successValue(preS));
      updateState(reducer(preS, sv));
    } catch (e) {
      final Exception exception = errorToException(e);
      debugPrint("[${toString()}] execute Exception: ${exception.toString()}");
      updateState(reducer(preS, Fail<T>(exception)));
    }
  }
}

class ComponentSimpleBloc<T> extends ComponentBloc<T> {
  ComponentSimpleBloc(
      this._simpleBlocRepository,
      SimpleComponentState<T> initailState,
      this._shouldHidden,
      ComponentStateChangeNotifier stateChangeNotifier,
      int tabIndex)
      : super(stateChangeNotifier, initailState, tabIndex);

  final SimpleBlocRepository<T> _simpleBlocRepository;
  final ComponentShouldHidden<T> _shouldHidden;

  @override
  Future<void> getData() async {
    final state = getCurrentState();
    if (state?.value is Uninitialized) {
      if (initailState == null) {
        execute((preState) => _simpleBlocRepository.getData(),
            (preState, blocAsync) {
          if (blocAsync is Success &&
              _shouldHidden != null &&
              _shouldHidden(blocAsync())) {
            return SimpleComponentState<T>.create(HiddenSuccess<T>());
          }
          return SimpleComponentState<T>.create(blocAsync);
        });
      } else {
        if (initailState.value is Success &&
            _shouldHidden != null &&
            _shouldHidden(initailState.value())) {
          updateState(SimpleComponentState<T>.create(HiddenSuccess<T>()));
        } else {
          updateState(initailState);
        }
      }
    }
  }

  @override
  @mustCallSuper
  void dispose() {
    // TODO(zhuozhao): 由于状态发生变化后，bloc会重建，所以这里暂时不能执行
    // _simpleBlocRepository.clear();
  }
}
