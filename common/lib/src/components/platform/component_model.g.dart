// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'component_model.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ComponentPage> _$componentPageSerializer =
    new _$ComponentPageSerializer();
Serializer<ComponentPageMeta> _$componentPageMetaSerializer =
    new _$ComponentPageMetaSerializer();
Serializer<ComponentPageUserAction> _$componentPageUserActionSerializer =
    new _$ComponentPageUserActionSerializer();
Serializer<ComponentPageTracking> _$componentPageTrackingSerializer =
    new _$ComponentPageTrackingSerializer();
Serializer<ComponentPageBody> _$componentPageBodySerializer =
    new _$ComponentPageBodySerializer();
Serializer<SecondFloor> _$secondFloorSerializer = new _$SecondFloorSerializer();
Serializer<ComponentSection> _$componentSectionSerializer =
    new _$ComponentSectionSerializer();
Serializer<ComponentSectionMeta> _$componentSectionMetaSerializer =
    new _$ComponentSectionMetaSerializer();
Serializer<ComponentSectionBody> _$componentSectionBodySerializer =
    new _$ComponentSectionBodySerializer();
Serializer<ComponentSectionContent> _$componentSectionContentSerializer =
    new _$ComponentSectionContentSerializer();

class _$ComponentPageSerializer implements StructuredSerializer<ComponentPage> {
  @override
  final Iterable<Type> types = const [ComponentPage, _$ComponentPage];
  @override
  final String wireName = 'ComponentPage';

  @override
  Iterable<Object> serialize(Serializers serializers, ComponentPage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'page_id',
      serializers.serialize(object.pageId,
          specifiedType: const FullType(String)),
      'meta',
      serializers.serialize(object.meta,
          specifiedType: const FullType(ComponentPageMeta)),
      'body',
      serializers.serialize(object.body,
          specifiedType: const FullType(ComponentPageBody)),
    ];

    return result;
  }

  @override
  ComponentPage deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentPageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'page_id':
          result.pageId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'meta':
          result.meta.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentPageMeta))
              as ComponentPageMeta);
          break;
        case 'body':
          result.body.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentPageBody))
              as ComponentPageBody);
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentPageMetaSerializer
    implements StructuredSerializer<ComponentPageMeta> {
  @override
  final Iterable<Type> types = const [ComponentPageMeta, _$ComponentPageMeta];
  @override
  final String wireName = 'ComponentPageMeta';

  @override
  Iterable<Object> serialize(Serializers serializers, ComponentPageMeta object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
    ];
    if (object.userAction != null) {
      result
        ..add('user_action')
        ..add(serializers.serialize(object.userAction,
            specifiedType: const FullType(ComponentPageUserAction)));
    }
    if (object.track != null) {
      result
        ..add('track')
        ..add(serializers.serialize(object.track,
            specifiedType: const FullType(ComponentPageUserAction)));
    }
    return result;
  }

  @override
  ComponentPageMeta deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentPageMetaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user_action':
          result.userAction.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentPageUserAction))
              as ComponentPageUserAction);
          break;
        case 'track':
          result.track.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentPageUserAction))
              as ComponentPageUserAction);
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentPageUserActionSerializer
    implements StructuredSerializer<ComponentPageUserAction> {
  @override
  final Iterable<Type> types = const [
    ComponentPageUserAction,
    _$ComponentPageUserAction
  ];
  @override
  final String wireName = 'ComponentPageUserAction';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ComponentPageUserAction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.canRefresh != null) {
      result
        ..add('can_refresh')
        ..add(serializers.serialize(object.canRefresh,
            specifiedType: const FullType(bool)));
    }
    if (object.hasNavigation != null) {
      result
        ..add('has_navigation')
        ..add(serializers.serialize(object.hasNavigation,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ComponentPageUserAction deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentPageUserActionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'can_refresh':
          result.canRefresh = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'has_navigation':
          result.hasNavigation = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentPageTrackingSerializer
    implements StructuredSerializer<ComponentPageTracking> {
  @override
  final Iterable<Type> types = const [
    ComponentPageTracking,
    _$ComponentPageTracking
  ];
  @override
  final String wireName = 'ComponentPageTracking';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ComponentPageTracking object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.gaEventCategory != null) {
      result
        ..add('ga_event_category')
        ..add(serializers.serialize(object.gaEventCategory,
            specifiedType: const FullType(String)));
    }
    if (object.mixpanelEventPrefix != null) {
      result
        ..add('mixpanel_event_prefix')
        ..add(serializers.serialize(object.mixpanelEventPrefix,
            specifiedType: const FullType(String)));
    }
    if (object.ihtPageName != null) {
      result
        ..add('iht_page_name')
        ..add(serializers.serialize(object.ihtPageName,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ComponentPageTracking deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentPageTrackingBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ga_event_category':
          result.gaEventCategory = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'mixpanel_event_prefix':
          result.mixpanelEventPrefix = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'iht_page_name':
          result.ihtPageName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentPageBodySerializer
    implements StructuredSerializer<ComponentPageBody> {
  @override
  final Iterable<Type> types = const [ComponentPageBody, _$ComponentPageBody];
  @override
  final String wireName = 'ComponentPageBody';

  @override
  Iterable<Object> serialize(Serializers serializers, ComponentPageBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'sections',
      serializers.serialize(object.sections,
          specifiedType: const FullType(
              BuiltList, const [const FullType(ComponentSection)])),
    ];
    if (object.secondFloor != null) {
      result
        ..add('second_floor')
        ..add(serializers.serialize(object.secondFloor,
            specifiedType: const FullType(SecondFloor)));
    }
    return result;
  }

  @override
  ComponentPageBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentPageBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'sections':
          result.sections.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ComponentSection)]))
              as BuiltList<Object>);
          break;
        case 'second_floor':
          result.secondFloor.replace(serializers.deserialize(value,
              specifiedType: const FullType(SecondFloor)) as SecondFloor);
          break;
      }
    }

    return result.build();
  }
}

class _$SecondFloorSerializer implements StructuredSerializer<SecondFloor> {
  @override
  final Iterable<Type> types = const [SecondFloor, _$SecondFloor];
  @override
  final String wireName = 'SecondFloor';

  @override
  Iterable<Object> serialize(Serializers serializers, SecondFloor object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'is_show',
      serializers.serialize(object.isShow, specifiedType: const FullType(bool)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'deep_link',
      serializers.serialize(object.deepLink,
          specifiedType: const FullType(String)),
      'banner_url',
      serializers.serialize(object.bannerUrl,
          specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SecondFloor deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SecondFloorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'is_show':
          result.isShow = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'deep_link':
          result.deepLink = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'banner_url':
          result.bannerUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentSectionSerializer
    implements StructuredSerializer<ComponentSection> {
  @override
  final Iterable<Type> types = const [ComponentSection, _$ComponentSection];
  @override
  final String wireName = 'ComponentSection';

  @override
  Iterable<Object> serialize(Serializers serializers, ComponentSection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'meta',
      serializers.serialize(object.meta,
          specifiedType: const FullType(ComponentSectionMeta)),
      'body',
      serializers.serialize(object.body,
          specifiedType: const FullType(ComponentSectionBody)),
    ];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(JsonObject)));
    }
    return result;
  }

  @override
  ComponentSection deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentSectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'meta':
          result.meta.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentSectionMeta))
              as ComponentSectionMeta);
          break;
        case 'data':
          result.data = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
        case 'body':
          result.body.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentSectionBody))
              as ComponentSectionBody);
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentSectionMetaSerializer
    implements StructuredSerializer<ComponentSectionMeta> {
  @override
  final Iterable<Type> types = const [
    ComponentSectionMeta,
    _$ComponentSectionMeta
  ];
  @override
  final String wireName = 'ComponentSectionMeta';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ComponentSectionMeta object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'type',
      serializers.serialize(object.fullType,
          specifiedType: const FullType(String)),
    ];
    if (object.placeholder != null) {
      result
        ..add('placeholder')
        ..add(serializers.serialize(object.placeholder,
            specifiedType: const FullType(String)));
    }
    if (object.sectionName != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.sectionName,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ComponentSectionMeta deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentSectionMetaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'placeholder':
          result.placeholder = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.fullType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.sectionName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentSectionBodySerializer
    implements StructuredSerializer<ComponentSectionBody> {
  @override
  final Iterable<Type> types = const [
    ComponentSectionBody,
    _$ComponentSectionBody
  ];
  @override
  final String wireName = 'ComponentSectionBody';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ComponentSectionBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.tabs != null) {
      result
        ..add('tabs')
        ..add(serializers.serialize(object.tabs,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ComponentSectionContent)])));
    }
    if (object.content != null) {
      result
        ..add('content')
        ..add(serializers.serialize(object.content,
            specifiedType: const FullType(ComponentSectionContent)));
    }
    return result;
  }

  @override
  ComponentSectionBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentSectionBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'tabs':
          result.tabs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(ComponentSectionContent)
              ])) as BuiltList<Object>);
          break;
        case 'content':
          result.content.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ComponentSectionContent))
              as ComponentSectionContent);
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentSectionContentSerializer
    implements StructuredSerializer<ComponentSectionContent> {
  @override
  final Iterable<Type> types = const [
    ComponentSectionContent,
    _$ComponentSectionContent
  ];
  @override
  final String wireName = 'ComponentSectionContent';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ComponentSectionContent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'handler',
      serializers.serialize(object.handler,
          specifiedType: const FullType(String)),
      'data_type',
      serializers.serialize(object.dataType,
          specifiedType: const FullType(String)),
      'load_type',
      serializers.serialize(object.loadType,
          specifiedType: const FullType(String)),
    ];
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.placeholder != null) {
      result
        ..add('placeholder')
        ..add(serializers.serialize(object.placeholder,
            specifiedType: const FullType(String)));
    }
    if (object.src != null) {
      result
        ..add('src')
        ..add(serializers.serialize(object.src,
            specifiedType: const FullType(String)));
    }
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(JsonObject)));
    }
    return result;
  }

  @override
  ComponentSectionContent deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ComponentSectionContentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'handler':
          result.handler = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data_type':
          result.dataType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'load_type':
          result.loadType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'placeholder':
          result.placeholder = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'src':
          result.src = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
      }
    }

    return result.build();
  }
}

class _$ComponentPage extends ComponentPage {
  @override
  final String pageId;
  @override
  final ComponentPageMeta meta;
  @override
  final ComponentPageBody body;

  factory _$ComponentPage([void Function(ComponentPageBuilder) updates]) =>
      (new ComponentPageBuilder()..update(updates)).build();

  _$ComponentPage._({this.pageId, this.meta, this.body}) : super._() {
    if (pageId == null) {
      throw new BuiltValueNullFieldError('ComponentPage', 'pageId');
    }
    if (meta == null) {
      throw new BuiltValueNullFieldError('ComponentPage', 'meta');
    }
    if (body == null) {
      throw new BuiltValueNullFieldError('ComponentPage', 'body');
    }
  }

  @override
  ComponentPage rebuild(void Function(ComponentPageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentPageBuilder toBuilder() => new ComponentPageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentPage &&
        pageId == other.pageId &&
        meta == other.meta &&
        body == other.body;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, pageId.hashCode), meta.hashCode), body.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentPage')
          ..add('pageId', pageId)
          ..add('meta', meta)
          ..add('body', body))
        .toString();
  }
}

class ComponentPageBuilder
    implements Builder<ComponentPage, ComponentPageBuilder> {
  _$ComponentPage _$v;

  String _pageId;
  String get pageId => _$this._pageId;
  set pageId(String pageId) => _$this._pageId = pageId;

  ComponentPageMetaBuilder _meta;
  ComponentPageMetaBuilder get meta =>
      _$this._meta ??= new ComponentPageMetaBuilder();
  set meta(ComponentPageMetaBuilder meta) => _$this._meta = meta;

  ComponentPageBodyBuilder _body;
  ComponentPageBodyBuilder get body =>
      _$this._body ??= new ComponentPageBodyBuilder();
  set body(ComponentPageBodyBuilder body) => _$this._body = body;

  ComponentPageBuilder();

  ComponentPageBuilder get _$this {
    if (_$v != null) {
      _pageId = _$v.pageId;
      _meta = _$v.meta?.toBuilder();
      _body = _$v.body?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentPage other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentPage;
  }

  @override
  void update(void Function(ComponentPageBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentPage build() {
    _$ComponentPage _$result;
    try {
      _$result = _$v ??
          new _$ComponentPage._(
              pageId: pageId, meta: meta.build(), body: body.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'body';
        body.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ComponentPage', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ComponentPageMeta extends ComponentPageMeta {
  @override
  final String type;
  @override
  final ComponentPageUserAction userAction;
  @override
  final ComponentPageUserAction track;

  factory _$ComponentPageMeta(
          [void Function(ComponentPageMetaBuilder) updates]) =>
      (new ComponentPageMetaBuilder()..update(updates)).build();

  _$ComponentPageMeta._({this.type, this.userAction, this.track}) : super._() {
    if (type == null) {
      throw new BuiltValueNullFieldError('ComponentPageMeta', 'type');
    }
  }

  @override
  ComponentPageMeta rebuild(void Function(ComponentPageMetaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentPageMetaBuilder toBuilder() =>
      new ComponentPageMetaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentPageMeta &&
        type == other.type &&
        userAction == other.userAction &&
        track == other.track;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, type.hashCode), userAction.hashCode), track.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentPageMeta')
          ..add('type', type)
          ..add('userAction', userAction)
          ..add('track', track))
        .toString();
  }
}

class ComponentPageMetaBuilder
    implements Builder<ComponentPageMeta, ComponentPageMetaBuilder> {
  _$ComponentPageMeta _$v;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  ComponentPageUserActionBuilder _userAction;
  ComponentPageUserActionBuilder get userAction =>
      _$this._userAction ??= new ComponentPageUserActionBuilder();
  set userAction(ComponentPageUserActionBuilder userAction) =>
      _$this._userAction = userAction;

  ComponentPageUserActionBuilder _track;
  ComponentPageUserActionBuilder get track =>
      _$this._track ??= new ComponentPageUserActionBuilder();
  set track(ComponentPageUserActionBuilder track) => _$this._track = track;

  ComponentPageMetaBuilder();

  ComponentPageMetaBuilder get _$this {
    if (_$v != null) {
      _type = _$v.type;
      _userAction = _$v.userAction?.toBuilder();
      _track = _$v.track?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentPageMeta other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentPageMeta;
  }

  @override
  void update(void Function(ComponentPageMetaBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentPageMeta build() {
    _$ComponentPageMeta _$result;
    try {
      _$result = _$v ??
          new _$ComponentPageMeta._(
              type: type,
              userAction: _userAction?.build(),
              track: _track?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'userAction';
        _userAction?.build();
        _$failedField = 'track';
        _track?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ComponentPageMeta', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ComponentPageUserAction extends ComponentPageUserAction {
  @override
  final bool canRefresh;
  @override
  final bool hasNavigation;

  factory _$ComponentPageUserAction(
          [void Function(ComponentPageUserActionBuilder) updates]) =>
      (new ComponentPageUserActionBuilder()..update(updates)).build();

  _$ComponentPageUserAction._({this.canRefresh, this.hasNavigation})
      : super._();

  @override
  ComponentPageUserAction rebuild(
          void Function(ComponentPageUserActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentPageUserActionBuilder toBuilder() =>
      new ComponentPageUserActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentPageUserAction &&
        canRefresh == other.canRefresh &&
        hasNavigation == other.hasNavigation;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, canRefresh.hashCode), hasNavigation.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentPageUserAction')
          ..add('canRefresh', canRefresh)
          ..add('hasNavigation', hasNavigation))
        .toString();
  }
}

class ComponentPageUserActionBuilder
    implements
        Builder<ComponentPageUserAction, ComponentPageUserActionBuilder> {
  _$ComponentPageUserAction _$v;

  bool _canRefresh;
  bool get canRefresh => _$this._canRefresh;
  set canRefresh(bool canRefresh) => _$this._canRefresh = canRefresh;

  bool _hasNavigation;
  bool get hasNavigation => _$this._hasNavigation;
  set hasNavigation(bool hasNavigation) =>
      _$this._hasNavigation = hasNavigation;

  ComponentPageUserActionBuilder();

  ComponentPageUserActionBuilder get _$this {
    if (_$v != null) {
      _canRefresh = _$v.canRefresh;
      _hasNavigation = _$v.hasNavigation;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentPageUserAction other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentPageUserAction;
  }

  @override
  void update(void Function(ComponentPageUserActionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentPageUserAction build() {
    final _$result = _$v ??
        new _$ComponentPageUserAction._(
            canRefresh: canRefresh, hasNavigation: hasNavigation);
    replace(_$result);
    return _$result;
  }
}

class _$ComponentPageTracking extends ComponentPageTracking {
  @override
  final String gaEventCategory;
  @override
  final String mixpanelEventPrefix;
  @override
  final String ihtPageName;

  factory _$ComponentPageTracking(
          [void Function(ComponentPageTrackingBuilder) updates]) =>
      (new ComponentPageTrackingBuilder()..update(updates)).build();

  _$ComponentPageTracking._(
      {this.gaEventCategory, this.mixpanelEventPrefix, this.ihtPageName})
      : super._();

  @override
  ComponentPageTracking rebuild(
          void Function(ComponentPageTrackingBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentPageTrackingBuilder toBuilder() =>
      new ComponentPageTrackingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentPageTracking &&
        gaEventCategory == other.gaEventCategory &&
        mixpanelEventPrefix == other.mixpanelEventPrefix &&
        ihtPageName == other.ihtPageName;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, gaEventCategory.hashCode), mixpanelEventPrefix.hashCode),
        ihtPageName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentPageTracking')
          ..add('gaEventCategory', gaEventCategory)
          ..add('mixpanelEventPrefix', mixpanelEventPrefix)
          ..add('ihtPageName', ihtPageName))
        .toString();
  }
}

class ComponentPageTrackingBuilder
    implements Builder<ComponentPageTracking, ComponentPageTrackingBuilder> {
  _$ComponentPageTracking _$v;

  String _gaEventCategory;
  String get gaEventCategory => _$this._gaEventCategory;
  set gaEventCategory(String gaEventCategory) =>
      _$this._gaEventCategory = gaEventCategory;

  String _mixpanelEventPrefix;
  String get mixpanelEventPrefix => _$this._mixpanelEventPrefix;
  set mixpanelEventPrefix(String mixpanelEventPrefix) =>
      _$this._mixpanelEventPrefix = mixpanelEventPrefix;

  String _ihtPageName;
  String get ihtPageName => _$this._ihtPageName;
  set ihtPageName(String ihtPageName) => _$this._ihtPageName = ihtPageName;

  ComponentPageTrackingBuilder();

  ComponentPageTrackingBuilder get _$this {
    if (_$v != null) {
      _gaEventCategory = _$v.gaEventCategory;
      _mixpanelEventPrefix = _$v.mixpanelEventPrefix;
      _ihtPageName = _$v.ihtPageName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentPageTracking other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentPageTracking;
  }

  @override
  void update(void Function(ComponentPageTrackingBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentPageTracking build() {
    final _$result = _$v ??
        new _$ComponentPageTracking._(
            gaEventCategory: gaEventCategory,
            mixpanelEventPrefix: mixpanelEventPrefix,
            ihtPageName: ihtPageName);
    replace(_$result);
    return _$result;
  }
}

class _$ComponentPageBody extends ComponentPageBody {
  @override
  final BuiltList<ComponentSection> sections;
  @override
  final SecondFloor secondFloor;

  factory _$ComponentPageBody(
          [void Function(ComponentPageBodyBuilder) updates]) =>
      (new ComponentPageBodyBuilder()..update(updates)).build();

  _$ComponentPageBody._({this.sections, this.secondFloor}) : super._() {
    if (sections == null) {
      throw new BuiltValueNullFieldError('ComponentPageBody', 'sections');
    }
  }

  @override
  ComponentPageBody rebuild(void Function(ComponentPageBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentPageBodyBuilder toBuilder() =>
      new ComponentPageBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentPageBody &&
        sections == other.sections &&
        secondFloor == other.secondFloor;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, sections.hashCode), secondFloor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentPageBody')
          ..add('sections', sections)
          ..add('secondFloor', secondFloor))
        .toString();
  }
}

class ComponentPageBodyBuilder
    implements Builder<ComponentPageBody, ComponentPageBodyBuilder> {
  _$ComponentPageBody _$v;

  ListBuilder<ComponentSection> _sections;
  ListBuilder<ComponentSection> get sections =>
      _$this._sections ??= new ListBuilder<ComponentSection>();
  set sections(ListBuilder<ComponentSection> sections) =>
      _$this._sections = sections;

  SecondFloorBuilder _secondFloor;
  SecondFloorBuilder get secondFloor =>
      _$this._secondFloor ??= new SecondFloorBuilder();
  set secondFloor(SecondFloorBuilder secondFloor) =>
      _$this._secondFloor = secondFloor;

  ComponentPageBodyBuilder();

  ComponentPageBodyBuilder get _$this {
    if (_$v != null) {
      _sections = _$v.sections?.toBuilder();
      _secondFloor = _$v.secondFloor?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentPageBody other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentPageBody;
  }

  @override
  void update(void Function(ComponentPageBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentPageBody build() {
    _$ComponentPageBody _$result;
    try {
      _$result = _$v ??
          new _$ComponentPageBody._(
              sections: sections.build(), secondFloor: _secondFloor?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'sections';
        sections.build();
        _$failedField = 'secondFloor';
        _secondFloor?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ComponentPageBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$SecondFloor extends SecondFloor {
  @override
  final bool isShow;
  @override
  final String title;
  @override
  final String deepLink;
  @override
  final String bannerUrl;
  @override
  final String type;

  factory _$SecondFloor([void Function(SecondFloorBuilder) updates]) =>
      (new SecondFloorBuilder()..update(updates)).build();

  _$SecondFloor._(
      {this.isShow, this.title, this.deepLink, this.bannerUrl, this.type})
      : super._() {
    if (isShow == null) {
      throw new BuiltValueNullFieldError('SecondFloor', 'isShow');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('SecondFloor', 'title');
    }
    if (deepLink == null) {
      throw new BuiltValueNullFieldError('SecondFloor', 'deepLink');
    }
    if (bannerUrl == null) {
      throw new BuiltValueNullFieldError('SecondFloor', 'bannerUrl');
    }
    if (type == null) {
      throw new BuiltValueNullFieldError('SecondFloor', 'type');
    }
  }

  @override
  SecondFloor rebuild(void Function(SecondFloorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SecondFloorBuilder toBuilder() => new SecondFloorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SecondFloor &&
        isShow == other.isShow &&
        title == other.title &&
        deepLink == other.deepLink &&
        bannerUrl == other.bannerUrl &&
        type == other.type;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, isShow.hashCode), title.hashCode),
                deepLink.hashCode),
            bannerUrl.hashCode),
        type.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SecondFloor')
          ..add('isShow', isShow)
          ..add('title', title)
          ..add('deepLink', deepLink)
          ..add('bannerUrl', bannerUrl)
          ..add('type', type))
        .toString();
  }
}

class SecondFloorBuilder implements Builder<SecondFloor, SecondFloorBuilder> {
  _$SecondFloor _$v;

  bool _isShow;
  bool get isShow => _$this._isShow;
  set isShow(bool isShow) => _$this._isShow = isShow;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _deepLink;
  String get deepLink => _$this._deepLink;
  set deepLink(String deepLink) => _$this._deepLink = deepLink;

  String _bannerUrl;
  String get bannerUrl => _$this._bannerUrl;
  set bannerUrl(String bannerUrl) => _$this._bannerUrl = bannerUrl;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  SecondFloorBuilder();

  SecondFloorBuilder get _$this {
    if (_$v != null) {
      _isShow = _$v.isShow;
      _title = _$v.title;
      _deepLink = _$v.deepLink;
      _bannerUrl = _$v.bannerUrl;
      _type = _$v.type;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SecondFloor other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SecondFloor;
  }

  @override
  void update(void Function(SecondFloorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SecondFloor build() {
    final _$result = _$v ??
        new _$SecondFloor._(
            isShow: isShow,
            title: title,
            deepLink: deepLink,
            bannerUrl: bannerUrl,
            type: type);
    replace(_$result);
    return _$result;
  }
}

class _$ComponentSection extends ComponentSection {
  @override
  final ComponentSectionMeta meta;
  @override
  final JsonObject data;
  @override
  final ComponentSectionBody body;

  factory _$ComponentSection(
          [void Function(ComponentSectionBuilder) updates]) =>
      (new ComponentSectionBuilder()..update(updates)).build();

  _$ComponentSection._({this.meta, this.data, this.body}) : super._() {
    if (meta == null) {
      throw new BuiltValueNullFieldError('ComponentSection', 'meta');
    }
    if (body == null) {
      throw new BuiltValueNullFieldError('ComponentSection', 'body');
    }
  }

  @override
  ComponentSection rebuild(void Function(ComponentSectionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentSectionBuilder toBuilder() =>
      new ComponentSectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentSection &&
        meta == other.meta &&
        data == other.data &&
        body == other.body;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, meta.hashCode), data.hashCode), body.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentSection')
          ..add('meta', meta)
          ..add('data', data)
          ..add('body', body))
        .toString();
  }
}

class ComponentSectionBuilder
    implements Builder<ComponentSection, ComponentSectionBuilder> {
  _$ComponentSection _$v;

  ComponentSectionMetaBuilder _meta;
  ComponentSectionMetaBuilder get meta =>
      _$this._meta ??= new ComponentSectionMetaBuilder();
  set meta(ComponentSectionMetaBuilder meta) => _$this._meta = meta;

  JsonObject _data;
  JsonObject get data => _$this._data;
  set data(JsonObject data) => _$this._data = data;

  ComponentSectionBodyBuilder _body;
  ComponentSectionBodyBuilder get body =>
      _$this._body ??= new ComponentSectionBodyBuilder();
  set body(ComponentSectionBodyBuilder body) => _$this._body = body;

  ComponentSectionBuilder();

  ComponentSectionBuilder get _$this {
    if (_$v != null) {
      _meta = _$v.meta?.toBuilder();
      _data = _$v.data;
      _body = _$v.body?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentSection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentSection;
  }

  @override
  void update(void Function(ComponentSectionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentSection build() {
    _$ComponentSection _$result;
    try {
      _$result = _$v ??
          new _$ComponentSection._(
              meta: meta.build(), data: data, body: body.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'meta';
        meta.build();

        _$failedField = 'body';
        body.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ComponentSection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ComponentSectionMeta extends ComponentSectionMeta {
  @override
  final String placeholder;
  @override
  final String fullType;
  @override
  final String sectionName;

  factory _$ComponentSectionMeta(
          [void Function(ComponentSectionMetaBuilder) updates]) =>
      (new ComponentSectionMetaBuilder()..update(updates)).build();

  _$ComponentSectionMeta._({this.placeholder, this.fullType, this.sectionName})
      : super._() {
    if (fullType == null) {
      throw new BuiltValueNullFieldError('ComponentSectionMeta', 'fullType');
    }
  }

  @override
  ComponentSectionMeta rebuild(
          void Function(ComponentSectionMetaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentSectionMetaBuilder toBuilder() =>
      new ComponentSectionMetaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentSectionMeta &&
        placeholder == other.placeholder &&
        fullType == other.fullType &&
        sectionName == other.sectionName;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, placeholder.hashCode), fullType.hashCode),
        sectionName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentSectionMeta')
          ..add('placeholder', placeholder)
          ..add('fullType', fullType)
          ..add('sectionName', sectionName))
        .toString();
  }
}

class ComponentSectionMetaBuilder
    implements Builder<ComponentSectionMeta, ComponentSectionMetaBuilder> {
  _$ComponentSectionMeta _$v;

  String _placeholder;
  String get placeholder => _$this._placeholder;
  set placeholder(String placeholder) => _$this._placeholder = placeholder;

  String _fullType;
  String get fullType => _$this._fullType;
  set fullType(String fullType) => _$this._fullType = fullType;

  String _sectionName;
  String get sectionName => _$this._sectionName;
  set sectionName(String sectionName) => _$this._sectionName = sectionName;

  ComponentSectionMetaBuilder();

  ComponentSectionMetaBuilder get _$this {
    if (_$v != null) {
      _placeholder = _$v.placeholder;
      _fullType = _$v.fullType;
      _sectionName = _$v.sectionName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentSectionMeta other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentSectionMeta;
  }

  @override
  void update(void Function(ComponentSectionMetaBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentSectionMeta build() {
    final _$result = _$v ??
        new _$ComponentSectionMeta._(
            placeholder: placeholder,
            fullType: fullType,
            sectionName: sectionName);
    replace(_$result);
    return _$result;
  }
}

class _$ComponentSectionBody extends ComponentSectionBody {
  @override
  final BuiltList<ComponentSectionContent> tabs;
  @override
  final ComponentSectionContent content;

  factory _$ComponentSectionBody(
          [void Function(ComponentSectionBodyBuilder) updates]) =>
      (new ComponentSectionBodyBuilder()..update(updates)).build();

  _$ComponentSectionBody._({this.tabs, this.content}) : super._();

  @override
  ComponentSectionBody rebuild(
          void Function(ComponentSectionBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentSectionBodyBuilder toBuilder() =>
      new ComponentSectionBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentSectionBody &&
        tabs == other.tabs &&
        content == other.content;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, tabs.hashCode), content.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentSectionBody')
          ..add('tabs', tabs)
          ..add('content', content))
        .toString();
  }
}

class ComponentSectionBodyBuilder
    implements Builder<ComponentSectionBody, ComponentSectionBodyBuilder> {
  _$ComponentSectionBody _$v;

  ListBuilder<ComponentSectionContent> _tabs;
  ListBuilder<ComponentSectionContent> get tabs =>
      _$this._tabs ??= new ListBuilder<ComponentSectionContent>();
  set tabs(ListBuilder<ComponentSectionContent> tabs) => _$this._tabs = tabs;

  ComponentSectionContentBuilder _content;
  ComponentSectionContentBuilder get content =>
      _$this._content ??= new ComponentSectionContentBuilder();
  set content(ComponentSectionContentBuilder content) =>
      _$this._content = content;

  ComponentSectionBodyBuilder();

  ComponentSectionBodyBuilder get _$this {
    if (_$v != null) {
      _tabs = _$v.tabs?.toBuilder();
      _content = _$v.content?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentSectionBody other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentSectionBody;
  }

  @override
  void update(void Function(ComponentSectionBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentSectionBody build() {
    _$ComponentSectionBody _$result;
    try {
      _$result = _$v ??
          new _$ComponentSectionBody._(
              tabs: _tabs?.build(), content: _content?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'tabs';
        _tabs?.build();
        _$failedField = 'content';
        _content?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ComponentSectionBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ComponentSectionContent extends ComponentSectionContent {
  @override
  final String title;
  @override
  final String handler;
  @override
  final String dataType;
  @override
  final String loadType;
  @override
  final String placeholder;
  @override
  final String src;
  @override
  final JsonObject data;

  factory _$ComponentSectionContent(
          [void Function(ComponentSectionContentBuilder) updates]) =>
      (new ComponentSectionContentBuilder()..update(updates)).build();

  _$ComponentSectionContent._(
      {this.title,
      this.handler,
      this.dataType,
      this.loadType,
      this.placeholder,
      this.src,
      this.data})
      : super._() {
    if (handler == null) {
      throw new BuiltValueNullFieldError('ComponentSectionContent', 'handler');
    }
    if (dataType == null) {
      throw new BuiltValueNullFieldError('ComponentSectionContent', 'dataType');
    }
    if (loadType == null) {
      throw new BuiltValueNullFieldError('ComponentSectionContent', 'loadType');
    }
  }

  @override
  ComponentSectionContent rebuild(
          void Function(ComponentSectionContentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ComponentSectionContentBuilder toBuilder() =>
      new ComponentSectionContentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ComponentSectionContent &&
        title == other.title &&
        handler == other.handler &&
        dataType == other.dataType &&
        loadType == other.loadType &&
        placeholder == other.placeholder &&
        src == other.src &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, title.hashCode), handler.hashCode),
                        dataType.hashCode),
                    loadType.hashCode),
                placeholder.hashCode),
            src.hashCode),
        data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ComponentSectionContent')
          ..add('title', title)
          ..add('handler', handler)
          ..add('dataType', dataType)
          ..add('loadType', loadType)
          ..add('placeholder', placeholder)
          ..add('src', src)
          ..add('data', data))
        .toString();
  }
}

class ComponentSectionContentBuilder
    implements
        Builder<ComponentSectionContent, ComponentSectionContentBuilder> {
  _$ComponentSectionContent _$v;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _handler;
  String get handler => _$this._handler;
  set handler(String handler) => _$this._handler = handler;

  String _dataType;
  String get dataType => _$this._dataType;
  set dataType(String dataType) => _$this._dataType = dataType;

  String _loadType;
  String get loadType => _$this._loadType;
  set loadType(String loadType) => _$this._loadType = loadType;

  String _placeholder;
  String get placeholder => _$this._placeholder;
  set placeholder(String placeholder) => _$this._placeholder = placeholder;

  String _src;
  String get src => _$this._src;
  set src(String src) => _$this._src = src;

  JsonObject _data;
  JsonObject get data => _$this._data;
  set data(JsonObject data) => _$this._data = data;

  ComponentSectionContentBuilder();

  ComponentSectionContentBuilder get _$this {
    if (_$v != null) {
      _title = _$v.title;
      _handler = _$v.handler;
      _dataType = _$v.dataType;
      _loadType = _$v.loadType;
      _placeholder = _$v.placeholder;
      _src = _$v.src;
      _data = _$v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ComponentSectionContent other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ComponentSectionContent;
  }

  @override
  void update(void Function(ComponentSectionContentBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ComponentSectionContent build() {
    final _$result = _$v ??
        new _$ComponentSectionContent._(
            title: title,
            handler: handler,
            dataType: dataType,
            loadType: loadType,
            placeholder: placeholder,
            src: src,
            data: data);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
