import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

///
/// 用于组件为页面提供验证、数据等接口
///
mixin ComponentValidatorMixin<T extends StatefulWidget> on State<T> {
  ///
  /// Return true/false if the content is valid/invalid
  /// If the return false, you should return error message in [errorMessage] function
  ///
  @required
  bool validate();

  ///
  /// Return the result of this component, it will be combine in a Map
  /// Typical usage is as follows:
  ///
  /// ```dart
  /// Map<String, dynamic> result() {
  ///   return {"component_name":{}};
  /// }
  ///
  /// ```
  ///
  @required
  Map<String, dynamic> result();

  ///
  /// Return the error message if content is invalid
  ///
  String errorMessage() {
    return null;
  }

  /// Reset the component's state
  void reset() {}

  @mustCallSuper
  @override
  void deactivate() {
    ComponentValidator.of(context)?._unregister(this);
    super.deactivate();
  }

  @mustCallSuper
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    ComponentValidator.of(context)?._register(this);
  }
}

///
/// Component结构的页面需要对组件进行校验和获取组件数据
/// [ComponentValidator]内的组件可以在[ComponentValidator]调用[ComponentValidatorMixin]的方法时
/// 遍历调用组件的方法。
/// Typical usage is as follows:
///
///```dart
/// final globalKey = GlobalKey<ComponentValidatorState>()
///
/// ComponentValidator(
///   key:globalKey,
///   child: Column(
///     children:<Widget>[
///       // some component implement ComponentValidatorMixin
///     ]
///   )
/// )
///
/// globalKey.currentState.validate();
///
///```
///
class ComponentValidator extends StatefulWidget {
  const ComponentValidator({Key key, @required Widget child})
      : assert(child != null),
        _child = child,
        super(key: key);
  final Widget _child;
  @override
  State<StatefulWidget> createState() {
    return ComponentValidatorState();
  }

  /// Returns the closest [ComponentValidatorState] which encloses the given context.
  ///
  /// Typical usage is as follows:
  ///
  /// ```dart
  /// ComponentValidatorState state = ComponentValidator.of(context);
  /// state.validate();
  /// ```
  static ComponentValidatorState of(BuildContext context) {
    return Provider.of<ComponentValidatorState>(context);
  }
}

class ComponentValidatorState extends State<ComponentValidator> {
  /// 这里改为protected是为了方便外面继承ComponentValidatorState拓展
  @protected
  final Set<ComponentValidatorMixin> fields = <ComponentValidatorMixin>{};
  @override
  Widget build(BuildContext context) {
    return Provider.value(
        value: this,
        updateShouldNotify: (_, __) => false,
        child: widget._child);
  }

  void _register(ComponentValidatorMixin field) {
    fields.add(field);
  }

  void _unregister(ComponentValidatorMixin field) {
    fields.remove(field);
  }

  /// Validates every [ComponentValidatorMixin] that is a descendant of this [ComponentValidator], and
  /// returns true if there are no errors.
  bool validate() {
    return fields.fold<bool>(true, (pre, field) => field.validate() && pre);
  }

  ///
  /// Get the `first` error message if one of the [ComponentValidatorMixin]
  /// that is a descendant of this [ComponentValidator] is invalid.
  ///
  String errorMessage() {
    String errorMessage;
    for (final field in fields)
      if (!field.validate()) {
        errorMessage = field.errorMessage();
        break;
      }
    return errorMessage;
  }

  ///
  /// Get all results of [ComponentValidatorMixin] that is a descendant of this [ComponentValidator],
  /// Will throw exception is anyone [ComponentValidatorMixin] is invalid,
  /// You must call validate() before call this.
  ///
  Map<String, dynamic> result() {
    final Map<String, dynamic> result = {};
    for (final field in fields) {
      if (field.validate()) {
        result.addAll(field.result());
      } else {
        throw Exception("validate failed in ${field.widget}");
      }
    }
    return result;
  }

  /// Reset the components' state
  void reset() {
    for (final field in fields) field.reset();
  }
}
