import 'package:common/common.dart';
import 'package:flutter/widgets.dart';

/// An implementation of [SimpleBlocRepository] for the component widgets which
/// url need to be passed outside.
abstract class ComponentSimpleBlocRepository<T>
    extends RemoteSimpleBlocRepository<T> {
  ComponentSimpleBlocRepository(CommonRequest commonRequest, this.url)
      : super(commonRequest);

  @protected
  final String url;
}

/// An implementation of [SimpleBlocRepository] that override the
/// [SimpleBlocRepository.getData] with get request.
///
/// **NOTE**: This class only override the [getData] with the [CommonRequest.get],
/// if your API is not `get`, this class is not for you, pls implement the
/// [SimpleBlocRepository] yourself.
class ComponentGetRequestSimpleBlocRepository<T>
    extends ComponentSimpleBlocRepository<T> {
  ComponentGetRequestSimpleBlocRepository(
      CommonRequest commonRequest, String url,
      {Object queryParameters})
      : _queryParameters = queryParameters,
        super(commonRequest, url);

  final Object _queryParameters;

  @override
  Future<T> getData() {
    return commonRequest
        .get<T>(url, _queryParameters)
        .addToCompositeCancellable(compositeCancellable)
        .asFuture();
  }
}
