import 'dart:ui';

import 'package:flutter/foundation.dart';

class ComponentConfig {
  const ComponentConfig(this.name,
      {this.apiPath, this.params, this.commonTrackingParams});

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is ComponentConfig &&
        other.name == name &&
        other.apiPath == apiPath &&
        mapEquals(other.params, params) &&
        mapEquals(other.commonTrackingParams, commonTrackingParams);
  }

  @override
  int get hashCode => hashValues(name, params);

  final String name;
  final String apiPath;
  final Map<String, dynamic> params;

  ///
  /// include all tracking channel params. eg.
  ///```dart
  ///{
  ///     "ga":{
  ///         "event_category": "JR PTP Vertical Page"
  ///     },
  ///     "mixpanel_properties":{
  ///         "Vertical Type": "JR PTP",
  ///         "Page Name": "Vertical Page"
  ///     }
  /// }
  ///```
  ///
  // TODO(zhuozhao): 后序将map换为buildMap
  final Map<String, dynamic> commonTrackingParams;
}

extension ComponentConfigExt on ComponentConfig {
  Map<String, dynamic> toMixpanelProperties(Map<String, dynamic> extra) {
    assert(extra != null);
    final Map<String, dynamic> extraProperties =
        Map<String, dynamic>.from(extra);
    extraProperties.addAll(Map<String, dynamic>.from(
        (commonTrackingParams ?? {})['mixpanel_properties'] ?? {}));
    return extraProperties;
  }

  String toGAEventCategory() {
    final Map<String, dynamic> gaProperties =
        Map<String, dynamic>.from((commonTrackingParams ?? {})['ga'] ?? {});
    return gaProperties['event_category'] ?? "";
  }

  String toMixpanelPageName() {
    final Map<String, dynamic> mixpanelProperties = toMixpanelProperties({});
    return mixpanelProperties['Page Name'] ?? "";
  }

  String toMixpanelVerticalType() {
    final Map<String, dynamic> mixpanelProperties = toMixpanelProperties({});
    return mixpanelProperties['Vertical Type'] ?? "";
  }
}
