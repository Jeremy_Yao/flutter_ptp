import 'package:flutter/material.dart';

import 'component_builder.dart';
import 'component_config.dart';

// TODO(zhuozhao): move to rails_ptp module
class ComponentFactory {
  ComponentFactory(this._builderMap);

  final Map<String, ComponentBuilder> _builderMap;

  Widget createWidget(BuildContext context, ComponentConfig config) {
    return _builderMap[config.name](context, config);
  }

  bool canCreateWidget(BuildContext context, ComponentConfig config) {
    return _builderMap.containsKey(config.name);
  }
}
