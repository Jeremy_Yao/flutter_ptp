import 'package:flutter/material.dart';

import 'component_config.dart';

typedef ComponentBuilder = Widget Function(
    BuildContext context, ComponentConfig config);
