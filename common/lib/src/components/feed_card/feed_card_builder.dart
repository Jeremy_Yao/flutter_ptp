import 'package:flutter/material.dart';

import 'feed_card_model.dart';

/// 构建 Feed Card Content Widget
typedef FeedCardContentBuilder = Widget Function(
    BuildContext context, FeedContentCard feedContentCard, int index);

/// 构建generic Card item  Widget
typedef GenericFeedCardItemBuilder = Widget Function(
    BuildContext context, FeedContentCardItem item, int index);
