import 'package:flutter/material.dart';

import 'feed_card_builder.dart';
import 'feed_card_item_factory.dart';
import 'feed_card_model.dart';

class GenericFeedCardItemImplFactory extends GenericFeedCardItemFactory {
  GenericFeedCardItemImplFactory(this._builderMap);

  final Map<String, GenericFeedCardItemBuilder> _builderMap;

  @override
  bool canHandle(FeedContentCardItem cardItem) {
    return _builderMap.containsKey(cardItem.type);
  }

  @override
  Widget createWidget(
      BuildContext context, FeedContentCardItem cardItem, int index) {
    return _builderMap[cardItem.type](context, cardItem, index);
  }
}
