import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

part 'feed_card_model.g.dart';

abstract class FeedContentCard
    implements Built<FeedContentCard, FeedContentCardBuilder> {
  factory FeedContentCard([void Function(FeedContentCardBuilder) updates]) =
      _$FeedContentCard;
  FeedContentCard._();

  @BuiltValueField(wireName: 'card_type')
  String get cardType;

  @nullable
  @BuiltValueField(wireName: 'data_type')
  String get dataType;

  @nullable
  @BuiltValueField(wireName: 'tracing')
  Tracing get tracing;

  @nullable
  @BuiltValueField(wireName: 'deep_link')
  String get deepLink;

  @nullable
  @BuiltValueField(wireName: 'items')
  BuiltList<FeedContentCardItem> get items;

  static Serializer<FeedContentCard> get serializer =>
      _$feedContentCardSerializer;
}

abstract class FeedContentCardItem
    implements Built<FeedContentCardItem, FeedContentCardItemBuilder> {
  factory FeedContentCardItem(
          [void Function(FeedContentCardItemBuilder) updates]) =
      _$FeedContentCardItem;
  FeedContentCardItem._();

  @BuiltValueField(wireName: 'type')
  String get type;

  @BuiltValueField(wireName: 'data')
  JsonObject get data;

  static Serializer<FeedContentCardItem> get serializer =>
      _$feedContentCardItemSerializer;
}

abstract class Tracing implements Built<Tracing, TracingBuilder> {
  factory Tracing([void Function(TracingBuilder) updates]) = _$Tracing;
  Tracing._();

  @nullable
  @BuiltValueField(wireName: 'tracing_type')
  String get tracingType;

  @nullable
  @BuiltValueField(wireName: 'tracing_data')
  String get tracingData;

  static Serializer<Tracing> get serializer => _$tracingSerializer;
}
