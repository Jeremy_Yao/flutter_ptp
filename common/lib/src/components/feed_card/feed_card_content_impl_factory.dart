import 'package:flutter/material.dart';

import 'feed_card_builder.dart';
import 'feed_card_content_factory.dart';
import 'feed_card_model.dart';

class FeedCardContentImplFactory extends FeedCardContentFactory {
  FeedCardContentImplFactory(this._builderMap);

  final Map<String, FeedCardContentBuilder> _builderMap;

  @override
  bool canHandle(FeedContentCard feedContentCard) {
    return _builderMap.containsKey(feedContentCard.cardType);
  }

  @override
  Widget createWidget(
      BuildContext context, FeedContentCard feedContentCard, int index) {
    return _builderMap[feedContentCard.cardType](
        context, feedContentCard, index);
  }
}
