import 'package:flutter/material.dart';

import 'feed_card_model.dart';

abstract class GenericFeedCardItemFactory {
  Widget createWidget(
      BuildContext context, FeedContentCardItem cardItem, int index);

  bool canHandle(FeedContentCardItem cardItem);
}
