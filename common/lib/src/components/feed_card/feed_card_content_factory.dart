import 'package:flutter/material.dart';

import 'feed_card_model.dart';

abstract class FeedCardContentFactory {
  Widget createWidget(
      BuildContext context, FeedContentCard feedContentCard, int index);

  bool canHandle(FeedContentCard feedContentCard);
}
