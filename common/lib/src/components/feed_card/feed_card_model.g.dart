// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_card_model.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<FeedContentCard> _$feedContentCardSerializer =
    new _$FeedContentCardSerializer();
Serializer<FeedContentCardItem> _$feedContentCardItemSerializer =
    new _$FeedContentCardItemSerializer();
Serializer<Tracing> _$tracingSerializer = new _$TracingSerializer();

class _$FeedContentCardSerializer
    implements StructuredSerializer<FeedContentCard> {
  @override
  final Iterable<Type> types = const [FeedContentCard, _$FeedContentCard];
  @override
  final String wireName = 'FeedContentCard';

  @override
  Iterable<Object> serialize(Serializers serializers, FeedContentCard object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'card_type',
      serializers.serialize(object.cardType,
          specifiedType: const FullType(String)),
    ];
    if (object.dataType != null) {
      result
        ..add('data_type')
        ..add(serializers.serialize(object.dataType,
            specifiedType: const FullType(String)));
    }
    if (object.tracing != null) {
      result
        ..add('tracing')
        ..add(serializers.serialize(object.tracing,
            specifiedType: const FullType(Tracing)));
    }
    if (object.deepLink != null) {
      result
        ..add('deep_link')
        ..add(serializers.serialize(object.deepLink,
            specifiedType: const FullType(String)));
    }
    if (object.items != null) {
      result
        ..add('items')
        ..add(serializers.serialize(object.items,
            specifiedType: const FullType(
                BuiltList, const [const FullType(FeedContentCardItem)])));
    }
    return result;
  }

  @override
  FeedContentCard deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FeedContentCardBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'card_type':
          result.cardType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data_type':
          result.dataType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tracing':
          result.tracing.replace(serializers.deserialize(value,
              specifiedType: const FullType(Tracing)) as Tracing);
          break;
        case 'deep_link':
          result.deepLink = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'items':
          result.items.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(FeedContentCardItem)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$FeedContentCardItemSerializer
    implements StructuredSerializer<FeedContentCardItem> {
  @override
  final Iterable<Type> types = const [
    FeedContentCardItem,
    _$FeedContentCardItem
  ];
  @override
  final String wireName = 'FeedContentCardItem';

  @override
  Iterable<Object> serialize(
      Serializers serializers, FeedContentCardItem object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'data',
      serializers.serialize(object.data,
          specifiedType: const FullType(JsonObject)),
    ];

    return result;
  }

  @override
  FeedContentCardItem deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FeedContentCardItemBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
      }
    }

    return result.build();
  }
}

class _$TracingSerializer implements StructuredSerializer<Tracing> {
  @override
  final Iterable<Type> types = const [Tracing, _$Tracing];
  @override
  final String wireName = 'Tracing';

  @override
  Iterable<Object> serialize(Serializers serializers, Tracing object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.tracingType != null) {
      result
        ..add('tracing_type')
        ..add(serializers.serialize(object.tracingType,
            specifiedType: const FullType(String)));
    }
    if (object.tracingData != null) {
      result
        ..add('tracing_data')
        ..add(serializers.serialize(object.tracingData,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Tracing deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TracingBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'tracing_type':
          result.tracingType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tracing_data':
          result.tracingData = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$FeedContentCard extends FeedContentCard {
  @override
  final String cardType;
  @override
  final String dataType;
  @override
  final Tracing tracing;
  @override
  final String deepLink;
  @override
  final BuiltList<FeedContentCardItem> items;

  factory _$FeedContentCard([void Function(FeedContentCardBuilder) updates]) =>
      (new FeedContentCardBuilder()..update(updates)).build();

  _$FeedContentCard._(
      {this.cardType, this.dataType, this.tracing, this.deepLink, this.items})
      : super._() {
    if (cardType == null) {
      throw new BuiltValueNullFieldError('FeedContentCard', 'cardType');
    }
  }

  @override
  FeedContentCard rebuild(void Function(FeedContentCardBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FeedContentCardBuilder toBuilder() =>
      new FeedContentCardBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FeedContentCard &&
        cardType == other.cardType &&
        dataType == other.dataType &&
        tracing == other.tracing &&
        deepLink == other.deepLink &&
        items == other.items;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, cardType.hashCode), dataType.hashCode),
                tracing.hashCode),
            deepLink.hashCode),
        items.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FeedContentCard')
          ..add('cardType', cardType)
          ..add('dataType', dataType)
          ..add('tracing', tracing)
          ..add('deepLink', deepLink)
          ..add('items', items))
        .toString();
  }
}

class FeedContentCardBuilder
    implements Builder<FeedContentCard, FeedContentCardBuilder> {
  _$FeedContentCard _$v;

  String _cardType;
  String get cardType => _$this._cardType;
  set cardType(String cardType) => _$this._cardType = cardType;

  String _dataType;
  String get dataType => _$this._dataType;
  set dataType(String dataType) => _$this._dataType = dataType;

  TracingBuilder _tracing;
  TracingBuilder get tracing => _$this._tracing ??= new TracingBuilder();
  set tracing(TracingBuilder tracing) => _$this._tracing = tracing;

  String _deepLink;
  String get deepLink => _$this._deepLink;
  set deepLink(String deepLink) => _$this._deepLink = deepLink;

  ListBuilder<FeedContentCardItem> _items;
  ListBuilder<FeedContentCardItem> get items =>
      _$this._items ??= new ListBuilder<FeedContentCardItem>();
  set items(ListBuilder<FeedContentCardItem> items) => _$this._items = items;

  FeedContentCardBuilder();

  FeedContentCardBuilder get _$this {
    if (_$v != null) {
      _cardType = _$v.cardType;
      _dataType = _$v.dataType;
      _tracing = _$v.tracing?.toBuilder();
      _deepLink = _$v.deepLink;
      _items = _$v.items?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FeedContentCard other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$FeedContentCard;
  }

  @override
  void update(void Function(FeedContentCardBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FeedContentCard build() {
    _$FeedContentCard _$result;
    try {
      _$result = _$v ??
          new _$FeedContentCard._(
              cardType: cardType,
              dataType: dataType,
              tracing: _tracing?.build(),
              deepLink: deepLink,
              items: _items?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'tracing';
        _tracing?.build();

        _$failedField = 'items';
        _items?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'FeedContentCard', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$FeedContentCardItem extends FeedContentCardItem {
  @override
  final String type;
  @override
  final JsonObject data;

  factory _$FeedContentCardItem(
          [void Function(FeedContentCardItemBuilder) updates]) =>
      (new FeedContentCardItemBuilder()..update(updates)).build();

  _$FeedContentCardItem._({this.type, this.data}) : super._() {
    if (type == null) {
      throw new BuiltValueNullFieldError('FeedContentCardItem', 'type');
    }
    if (data == null) {
      throw new BuiltValueNullFieldError('FeedContentCardItem', 'data');
    }
  }

  @override
  FeedContentCardItem rebuild(
          void Function(FeedContentCardItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FeedContentCardItemBuilder toBuilder() =>
      new FeedContentCardItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FeedContentCardItem &&
        type == other.type &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, type.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FeedContentCardItem')
          ..add('type', type)
          ..add('data', data))
        .toString();
  }
}

class FeedContentCardItemBuilder
    implements Builder<FeedContentCardItem, FeedContentCardItemBuilder> {
  _$FeedContentCardItem _$v;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  JsonObject _data;
  JsonObject get data => _$this._data;
  set data(JsonObject data) => _$this._data = data;

  FeedContentCardItemBuilder();

  FeedContentCardItemBuilder get _$this {
    if (_$v != null) {
      _type = _$v.type;
      _data = _$v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FeedContentCardItem other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$FeedContentCardItem;
  }

  @override
  void update(void Function(FeedContentCardItemBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FeedContentCardItem build() {
    final _$result = _$v ?? new _$FeedContentCardItem._(type: type, data: data);
    replace(_$result);
    return _$result;
  }
}

class _$Tracing extends Tracing {
  @override
  final String tracingType;
  @override
  final String tracingData;

  factory _$Tracing([void Function(TracingBuilder) updates]) =>
      (new TracingBuilder()..update(updates)).build();

  _$Tracing._({this.tracingType, this.tracingData}) : super._();

  @override
  Tracing rebuild(void Function(TracingBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TracingBuilder toBuilder() => new TracingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Tracing &&
        tracingType == other.tracingType &&
        tracingData == other.tracingData;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, tracingType.hashCode), tracingData.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Tracing')
          ..add('tracingType', tracingType)
          ..add('tracingData', tracingData))
        .toString();
  }
}

class TracingBuilder implements Builder<Tracing, TracingBuilder> {
  _$Tracing _$v;

  String _tracingType;
  String get tracingType => _$this._tracingType;
  set tracingType(String tracingType) => _$this._tracingType = tracingType;

  String _tracingData;
  String get tracingData => _$this._tracingData;
  set tracingData(String tracingData) => _$this._tracingData = tracingData;

  TracingBuilder();

  TracingBuilder get _$this {
    if (_$v != null) {
      _tracingType = _$v.tracingType;
      _tracingData = _$v.tracingData;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Tracing other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Tracing;
  }

  @override
  void update(void Function(TracingBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Tracing build() {
    final _$result = _$v ??
        new _$Tracing._(tracingType: tracingType, tracingData: tracingData);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
