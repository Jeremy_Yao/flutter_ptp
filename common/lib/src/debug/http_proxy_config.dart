import 'package:shared_preferences/shared_preferences.dart';

class HttpProxyConfig {
  static final _key = "enable_proxy";
  static Future<void> setEnable(bool value) async {
    final preference = await SharedPreferences.getInstance();
    return preference.setBool(_key, value);
  }

  static Future<bool> getEnable() async {
    final preference = await SharedPreferences.getInstance();
    return preference.getBool(_key) ?? false;
  }
}
