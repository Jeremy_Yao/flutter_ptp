import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ci_build_info/ci_build_info.dart';

class CIBuildInfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('CI Build Info')),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: CIBuildInfoWidget(),
        ),
      ),
    );
  }
}
