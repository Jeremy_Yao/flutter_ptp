import 'package:common/src/debug/ci_build_info/ci_build_info_page.dart';
import 'package:common/src/debug/debug_home_page.dart';
import 'package:common/src/debug/debug_render_component_page.dart';
import 'package:common/src/route/page_builder.dart';

final Map<String, PageBuilder> debugRoutes = {
  '/debug/render_component': (_, __, ___) => DebugRenderComponentPage(),
  '/debug/ci_build_info': (_, __, ___) => CIBuildInfoPage(),
  '/debug/home': (_, __, ___) => DebugHomePage(),
};
