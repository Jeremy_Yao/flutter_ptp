import 'package:common/src/debug/debug.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void overrideErrorWidgetForDebug() {
  // This will only execute in debug mode.
  assert(() {
    if (!isFlutterTestEnviroment()) {
      ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
        return _CustomErrorWidget(errorDetails);
      };
    }

    return true;
  }());
  // hide widget in release mode
  if (kReleaseMode) {
    ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
      return Container();
    };
  }
}

/// A custom [ErrorWidget] that show all the exception log to the screen.
class _CustomErrorWidget extends StatelessWidget {
  const _CustomErrorWidget(this._errorDetails);

  final FlutterErrorDetails _errorDetails;
  @override
  Widget build(BuildContext context) {
    final errorMessgae = _errorDetails.toString();
    final exception = _errorDetails.exception;
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: ErrorWidget.withDetails(
            message: errorMessgae,
            error: exception is FlutterError ? exception : null,
          ),
        ),
        Positioned(
          top: 50,
          child: ElevatedButton(
            onPressed: () {
              Clipboard.setData(ClipboardData(text: errorMessgae)).then((_) {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Error log copied to clipboard")));
              });
            },
            child: const Text('Copy to Clipboard'),
          ),
        ),
      ],
    );
  }
}
