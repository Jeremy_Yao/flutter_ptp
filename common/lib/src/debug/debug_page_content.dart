import 'package:common/common_navigator.dart';
import 'package:common/src/debug/http_proxy_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DebugPageContent extends StatefulWidget {
  const DebugPageContent({this.isDrawer});
  final bool isDrawer;
  @override
  _DebugPageContentState createState() => _DebugPageContentState();
}

class _DebugPageContentState extends State<DebugPageContent> {
  bool _enableProxy = false;
  CommonNavigator _commonNavigator;
  @override
  void initState() {
    super.initState();
    _loadConfig();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _commonNavigator = Provider.of<CommonNavigator>(context, listen: false);
  }

  Future<void> _loadConfig() async {
    _enableProxy = await HttpProxyConfig.getEnable();
    setState(() {});
  }

  void _pop() {
    if (widget.isDrawer) {
      _commonNavigator.pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: const Text("Render Component by key"),
          onTap: () {
            _pop();
            _commonNavigator.pushNamed("/debug/render_component");
          },
        ),
        ListTile(
          title: const Text("Clear SharePreference"),
          onTap: () async {
            final preferennce = await SharedPreferences.getInstance();
            preferennce.clear();
            _pop();
          },
        ),
        ListTile(
          title: const Text("CI Build Info"),
          onTap: () async {
            _pop();
            _commonNavigator.pushNamed("/debug/ci_build_info");
          },
        ),
        ListTile(
          title: const Text("L10N cms debug page"),
          onTap: () async {
            _pop();
            _commonNavigator.pushNamed("/debug/l10n_cms_debug_page");
          },
        ),
        ListTile(
          title: const Text("Enable Proxy"),
          subtitle: const Text("重启后生效"),
          trailing: Switch(
              value: _enableProxy,
              onChanged: (value) {
                HttpProxyConfig.setEnable(value);
                _enableProxy = value;
                setState(() {});
              }),
        ),
      ],
    );
  }
}
