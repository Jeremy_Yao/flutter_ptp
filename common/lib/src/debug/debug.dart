import 'dart:io';

import 'package:flutter/foundation.dart';

/// Indicate is running flutter test or not.
bool isFlutterTestEnviroment() {
  if (kReleaseMode) return false;
  bool isFlutterTest = false;
  assert(() {
    // Only run this on debug mode.
    isFlutterTest = Platform.environment.containsKey('FLUTTER_TEST');

    return true;
  }());
  return isFlutterTest;
}
