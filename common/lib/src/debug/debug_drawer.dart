import 'package:common/src/debug/debug_page_content.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DebugDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: SafeArea(
      child: Column(children: const [
        Text(
          'Debug Drawer',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
        Expanded(child: DebugPageContent(isDrawer: true))
      ]),
    ));
  }
}
