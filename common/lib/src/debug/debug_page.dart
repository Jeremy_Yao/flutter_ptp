import 'package:common/src/debug/debug_drawer.dart';
import 'package:flutter/material.dart';

class DebugPage extends StatelessWidget {
  const DebugPage({@required Widget child}) : _child = child;

  final Widget _child;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: DebugDrawer(),
      body: _child,
    );
  }
}
