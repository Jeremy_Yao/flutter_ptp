import 'package:common/src/debug/debug_page_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DebugHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Debug Page"),
      ),
      body: const DebugPageContent(isDrawer: false),
    );
  }
}
