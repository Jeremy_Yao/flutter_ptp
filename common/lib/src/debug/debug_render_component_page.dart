import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DebugRenderComponentPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DebugRenderComponentPageState();
}

class _DebugRenderComponentPageState extends State<DebugRenderComponentPage> {
  final TextEditingController _textEditingController = TextEditingController();

  List<String> _componentKeys = [];

  ComponentFactory _componentFactory;

  @override
  void didChangeDependencies() {
    _componentFactory = Provider.of<ComponentFactory>(context, listen: false);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: const Text(
              "Render Component",
              style: TextStyle(color: Colors.black),
            ),
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: const Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Provider.of<CommonNavigator>(context, listen: false).pop();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                      "Input the component key, you can input multiple keys sperate by \",\". \ne.g. jrpass_shinkansen_list,jrpass_article_list,jrpass_region_list"),
                  TextField(
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Input the component key'),
                    controller: _textEditingController,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            setState(() {
                              final String text =
                                  _textEditingController.value.text;
                              _componentKeys = text.split(",");
                              debugPrint("_componentKeys: $_componentKeys");
                            });
                          },
                          child: const Text("Render"),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _componentKeys.clear();
                            });
                          },
                          child: const Text("Clear"),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 1,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
          ),
          for (var key in _componentKeys)
            _componentFactory.createWidget(context, ComponentConfig(key))
        ],
      ),
    );
  }
}
