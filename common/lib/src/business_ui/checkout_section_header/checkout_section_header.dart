import 'package:common/common.dart';
import 'package:flutter/material.dart';

class CheckoutSectinHeader extends StatelessWidget {
  const CheckoutSectinHeader(String title)
      : assert(title != null),
        _title = title;
  final String _title;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        border: Border(bottom: BorderSide(color: ColorUtil.border, width: 0.5)),
      ),
      child: Text(_title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeightUtil.semibold,
              color: ColorUtil.BW1)),
    );
  }
}
