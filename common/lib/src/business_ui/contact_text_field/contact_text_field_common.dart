import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ContactTextField extends StatefulWidget {
  ContactTextField(String text,
      {String hintText,
      String labelText,
      String rightText,
      TextInputType keyboardType = TextInputType.text,
      ValueChanged<String> onChanged,
      FocusNode focusNode,
      bool warning = false,
      bool enable = true,
      List<TextInputFormatter> inputFormatters,
      TextEditingController controller})
      : _rightText = rightText,
        _keyboardType = keyboardType,
        _hintText = hintText,
        _onChanged = onChanged,
        _focusNode = focusNode,
        _warning = warning,
        _enable = enable,
        _controller = controller ?? TextEditingController(text: text),
        _inputFormatters = inputFormatters,
        _labelText = labelText;

  final String _hintText;
  final String _labelText;
  final String _rightText;
  final TextInputType _keyboardType;
  final ValueChanged<String> _onChanged;
  final FocusNode _focusNode;
  final bool _warning;
  final bool _enable;
  final TextEditingController _controller;

  //用来限制某些输入框只能输入特定的字符，如预定人姓名只能为中文
  final List<TextInputFormatter> _inputFormatters;

  @override
  State<StatefulWidget> createState() {
    return _ContactTextFieldState();
  }
}

class _ContactTextFieldState extends State<ContactTextField> {
  @override
  void dispose() {
    widget._controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 16, right: 16),
      child: Row(children: [
        Expanded(
            child: TextField(
          enabled: widget._enable,
          cursorColor: ColorUtil.orange,
          onChanged: widget._onChanged,
          style: const TextStyle(fontSize: 14, color: ColorUtil.BW1),
          controller: widget._controller,
          textInputAction: TextInputAction.done,
          keyboardType: widget._keyboardType,
          inputFormatters: widget._inputFormatters,
          focusNode: widget._focusNode,
          decoration: InputDecoration(
            hintMaxLines: 1,
            labelText: widget._labelText,
            labelStyle: const TextStyle(color: ColorUtil.BW1, fontSize: 12),
            hintText: widget._hintText,
            hintStyle: TextStyle(
                color: widget._warning ? ColorUtil.red : ColorUtil.BW3),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
          ),
        )),
        const SizedBox(width: 16),
        Text(
          widget._rightText ?? "",
          style: const TextStyle(fontSize: 12, color: Color(0xff25B885)),
        ),
      ]),
    );
  }
}
