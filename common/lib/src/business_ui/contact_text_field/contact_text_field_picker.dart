import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:common/assets.dart';

class ContactTextFieldPicker extends StatelessWidget {
  ContactTextFieldPicker(String text,
      {String hintText,
      bool warning = false,
      VoidCallback onPickerTap,
      String labelText})
      : _onPickerTap = onPickerTap,
        _hintText = hintText,
        _labelText = labelText,
        _warning = warning,
        _textEditingController = TextEditingController(text: text);

  final String _hintText;
  final String _labelText;
  final VoidCallback _onPickerTap;
  final bool _warning;
  final TextEditingController _textEditingController;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: _onPickerTap,
        behavior: HitTestBehavior.opaque,
        child: Container(
          margin: const EdgeInsets.only(left: 16, right: 16),
          child: Row(children: [
            Expanded(
                child: TextField(
              style: const TextStyle(fontSize: 14, color: ColorUtil.BW1),
              enabled: false,
              controller: _textEditingController,
              decoration: InputDecoration(
                hintMaxLines: 1,
                hintText: _hintText,
                labelText: _labelText,
                labelStyle: const TextStyle(color: ColorUtil.BW1, fontSize: 12),
                hintStyle:
                    TextStyle(color: _warning ? ColorUtil.red : ColorUtil.BW3),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
              ),
            )),
            const SizedBox(width: 16),
            Align(
              alignment: Alignment.centerRight,
              child: SvgPicture.asset(
                CommonAssets.icon_expandmore,
                width: 24,
                height: 24,
                package: CommonAssets.package,
              ),
            ),
          ]),
        ));
  }
}
