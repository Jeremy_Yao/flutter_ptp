import 'package:common/assets.dart';
import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'contact_text_field_common.dart';

class ContactTextFieldTips extends StatefulWidget {
  const ContactTextFieldTips(this._text,
      {String hintText,
      TextInputType keyboardType = TextInputType.text,
      TextEditingController controller,
      String tips,
      bool enable = true,
      bool warning = false,
      ValueChanged<String> onChanged})
      : _keyboardType = keyboardType,
        _hintText = hintText,
        _onChanged = onChanged,
        _warning = warning,
        _enable = enable,
        _tips = tips,
        _controller = controller;
  final String _text;
  final String _hintText;
  final TextInputType _keyboardType;
  final String _tips;
  final bool _warning;
  final bool _enable;
  final ValueChanged<String> _onChanged;
  final TextEditingController _controller;

  @override
  State<StatefulWidget> createState() {
    return _ContactTextFieldTipsState();
  }
}

class _ContactTextFieldTipsState extends State<ContactTextFieldTips> {
  final FocusNode _focusNode = FocusNode();
  OverlayEntry _overlayEntry;

  final LayerLink _layerLink = LayerLink();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(_onFocusChange);
  }

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
        link: _layerLink,
        child: ContactTextField(
          widget._text,
          controller: widget._controller,
          enable: widget._enable,
          hintText: widget._hintText,
          keyboardType: widget._keyboardType,
          onChanged: widget._onChanged,
          warning: widget._warning,
          focusNode: _focusNode,
        ));
  }

  OverlayEntry _createOverlayEntry() {
    final RenderBox renderBox = context.findRenderObject();
    final size = renderBox.size;
    return OverlayEntry(
        builder: (context) => Positioned(
            width: size.width - 8,
            child: CompositedTransformFollower(
              link: _layerLink,
              showWhenUnlinked: false,
              offset: const Offset(4, -100),
              child: Material(
                  elevation: 0,
                  color: Colors.transparent,
                  child: Container(
                      height: 100,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                decoration: const BoxDecoration(
                                    color: ColorUtil.BW1,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(6))),
                                padding: const EdgeInsets.only(
                                    left: 16, top: 4, bottom: 4),
                                child: Row(children: <Widget>[
                                  // TODO(zhuozhao): LANG_FIX
                                  Expanded(
                                      child: Text(
                                    widget._tips,
                                    style: const TextStyle(color: Colors.white),
                                  )),
                                  IconButton(
                                      icon: SvgPicture.asset(
                                        CommonAssets.icon_close,
                                        width: 24,
                                        height: 24,
                                        color: Colors.white,
                                        package: CommonAssets.package,
                                      ),
                                      onPressed: () {
                                        _removeOverlay();
                                      })
                                ])),
                            Container(
                              padding: const EdgeInsets.only(left: 20),
                              child: CustomPaint(
                                  size: const Size(20, 10),
                                  painter: _DrawTriangle()),
                            ),
                          ]))),
            )));
  }

  void _onFocusChange() {
    if (_focusNode.hasFocus) {
      _showOverlay();
    } else {
      _removeOverlay();
    }
  }

  void _showOverlay() {
    _overlayEntry ??= _createOverlayEntry();
    Overlay.of(context).insert(_overlayEntry);
  }

  void _removeOverlay() {
    if (_overlayEntry != null) {
      _overlayEntry.remove();
      _overlayEntry = null;
    }
  }

  @override
  void dispose() {
    _removeOverlay();
    _focusNode.removeListener(_onFocusChange);
    super.dispose();
  }
}

class _DrawTriangle extends CustomPainter {
  _DrawTriangle() {
    _paint = Paint()
      ..color = ColorUtil.BW1
      ..style = PaintingStyle.fill;
  }

  Paint _paint;

  @override
  void paint(Canvas canvas, Size size) {
    final path = Path();
    path.moveTo(0, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width / 2, size.height);
    path.close();
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
