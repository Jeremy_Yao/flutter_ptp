import '../base/channel.dart';

class SignatureChannel extends Channel {
  @override
  // TODO(littlegnal): Will not implement Channel in the future
  // ignore: overridden_fields
  String name = 'com.klook.platform/signature';

  /// 获取签名
  Future<String> getSignature(String raw) async {
    return "";
    return invokeMethod('get_signature', raw);
  }
}
