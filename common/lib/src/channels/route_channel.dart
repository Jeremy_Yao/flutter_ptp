import 'package:common/src/route/navigation_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

@Deprecated("This will be removed in the futre.")
class RouteChannel {
  RouteChannel() {
    _channel.setMethodCallHandler((call) {
      switch (call.method) {
        case "pushReplacementNamed":
          debugPrint("pushReplacementNamed from native: ${call.toString()}");
          // The arguments format should be {"path": "your/path", "params": dynamic}
          final map = call.arguments;
          final path = map["path"];
          final params = map["params"];
          // 如果flutter页面没有通过pop关闭，route会保留在navigator中，新开页面继续push的话，pop会混乱
          // 简单处理，下一次push之前要把route 移除，共享引擎插件会解决这个问题
          if (navigationService.canPop()) {
            navigationService.popUntil((route) => route.isFirst);
          }
          navigationService.pushReplacementNamed(path, arguments: params);
          return Future.value(true);
          break;
        case "pop":
          return Future.value(navigationService.pop());
          break;
        case "popUntilNamed":
          final url = call.arguments;
          navigationService.popUntilNamed(url);
          break;
      }

      return Future.value(false);
    });
  }

  final MethodChannel _channel =
      const MethodChannel('com.klook.platform/router');

  Future<bool> route(String path, Object params) async {
    final Map<String, dynamic> args = {"path": path, "params": params};
    return _channel.invokeMethod<bool>('route', args);
  }

  Future pop() async {
    return _channel.invokeMethod('pop');
  }

  /// 这里是因为iOS原生端业务需要支持 pop到iOS原生的根目录
  Future popToRoot() async {
    return _channel.invokeMethod('pop_to_root');
  }
}
