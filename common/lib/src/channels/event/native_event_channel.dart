import 'package:common/src/channels/event/native_event_dispatcher.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

typedef NativeEventChangedListener = void Function(
    String eventName, dynamic arguments);

/// The event channel
///
/// The events just be forwarded so all the events, and the events will be handled
/// by [NativeEventDispatcher]
///
/// see also:
/// [NativeEventDispatcher]
class NativeEventChannel {
  NativeEventChannel() {
    channel.setMethodCallHandler(handler);
  }

  @visibleForTesting
  Future<dynamic> handler(MethodCall call) {
    checkEventName(call.method);
    debugPrint("NativeEventChannel: Received event:[$call.method]");
    _eventChangedListeners.forEach((listener) {
      listener(call.method, call.arguments);
    });

    return Future.value(true);
  }

  @visibleForTesting
  final MethodChannel channel = const MethodChannel('com.klook.platform/event');

  final List<NativeEventChangedListener> _eventChangedListeners = [];

  void addEventChangedListener(NativeEventChangedListener listener) {
    _eventChangedListeners.add(listener);
  }

  void sendEvent(String event, {dynamic arguments}) {
    checkEventName(event);

    // TODO(RUIJIN): channel 暂时不处理
    //channel.invokeMethod(event, arguments);
  }

  static void checkEventName(String event) {
    assert(event.startsWith("event"),
        "The event name should be start with \"event\", e.g. \"event_update\"");
  }
}
