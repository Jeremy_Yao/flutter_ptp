import 'dart:ui';

import 'package:common/local_event.dart';
import 'package:common/src/channels/event/native_event_channel.dart';
import 'package:flutter/foundation.dart';

class GlobalGenericNativeEvent implements LocalEvent {
  GlobalGenericNativeEvent(this.eventName, this.arguments);

  final String eventName;
  final Map<dynamic, dynamic> arguments;

  @override
  bool operator ==(other) {
    if (identical(other, this)) {
      return true;
    }
    return other is GlobalGenericNativeEvent &&
        other.eventName == eventName &&
        mapEquals(other.arguments, arguments);
  }

  @override
  int get hashCode => hashValues(eventName, arguments);

  @override
  String toString() {
    return 'GlobalGenericNativeEvent: eventName = $eventName, arguments = $arguments';
  }
}

class UIRefreshEvent implements LocalEvent {
  @override
  String toString() {
    return 'UIRefreshEvent';
  }
}

class UserInfoUpdatedEvent implements LocalEvent {
  @override
  String toString() {
    return 'UserInfoUpdatedEvent';
  }
}

typedef UIRefreshEventListener = void Function(String event);
typedef UserInfoUpdateEventListener = void Function(String event);

/// Handle the events received from the [NativeEventChannel]
class NativeEventDispatcher {
  NativeEventDispatcher(
      {@required NativeEventChannel nativeEventChannel,
      @required LocalEventManager localEventManager}) {
    _nativeEventChannel = nativeEventChannel;
    _localEventManager = localEventManager;
    _nativeEventChannel.addEventChangedListener((event, arguments) {
      switch (event) {
        case "event_currency_changed":
        case "event_language_changed":
        case "event_force_update_ui":
          notifyUIRefreshEventListener(event);
          break;
        case "event_update_user_info":
          notifyUserInfoUpdateEventListener(event);
          break;
        default:
          // TODO(littlegnal): Directly send the event by using `LocalEventManager.send` at this time,
          // will refactor the `NativeEventHandler`, `NativeEventDispatcher` in the future.
          localEventManager
              .sendEvent(GlobalGenericNativeEvent(event, arguments));
          break;
      }
    });
  }

  NativeEventChannel _nativeEventChannel;
  LocalEventManager _localEventManager;

  @visibleForTesting
  void notifyUIRefreshEventListener(String event) {
    _localEventManager.sendEvent(UIRefreshEvent());
  }

  @visibleForTesting
  void notifyUserInfoUpdateEventListener(String event) {
    _localEventManager.sendEvent(UserInfoUpdatedEvent());
  }
}

// TODO(littlegnal): Move to individual file later.
extension NativeEventDispatcherExt on NativeEventDispatcher {
  void sendAppUpgradeEvent() {
    _nativeEventChannel.sendEvent('event_update_app');
  }

  void sendEventByName(String eventName, {dynamic arguments}) {
    _nativeEventChannel.sendEvent(eventName, arguments: arguments);
  }
}
