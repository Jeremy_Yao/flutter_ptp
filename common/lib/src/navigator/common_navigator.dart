import 'package:astronomia/astronomia.dart';
import 'package:flutter/material.dart';

class CommonNavigator implements CommonNavigatorDelegate {
  CommonNavigator(this._commonNavigatorDelegate);

  final CommonNavigatorDelegate _commonNavigatorDelegate;

  @override
  GlobalKey<AstronomiaNavigatorState> get astronomiaNavigatorKey =>
      _commonNavigatorDelegate.astronomiaNavigatorKey;

  @override
  Future<T> pushNamed<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    return _commonNavigatorDelegate.pushNamed<T>(routeName,
        arguments: arguments);
  }

  @override
  Future<T> pushNamedNative<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    return _commonNavigatorDelegate.pushNamedNative<T>(routeName,
        arguments: arguments);
  }

  @override
  void pop<T extends Object>([T result]) {
    _commonNavigatorDelegate.pop<T>(result);
  }

  @override
  Future<bool> maybePop<T extends Object>([T result]) {
    return _commonNavigatorDelegate.maybePop<T>(result);
  }

  @override
  void popUntilNamed(String routeName) {
    _commonNavigatorDelegate.popUntilNamed(routeName);
  }
}

abstract class CommonNavigatorDelegate {
  // This class is intended to be used as an interface, and should not be
  // extended directly; this constructor prevents instantiation and extension.
  factory CommonNavigatorDelegate._() => null;

  GlobalKey<AstronomiaNavigatorState> get astronomiaNavigatorKey;

  Future<T> pushNamed<T extends Object>(
    String routeName, {
    Object arguments,
  });

  Future<T> pushNamedNative<T extends Object>(
    String routeName, {
    Object arguments,
  });

  void pop<T extends Object>([T result]);

  Future<bool> maybePop<T extends Object>([T result]);

  void popUntilNamed(String routeName);
}

extension CommonNavigatorExt on CommonNavigatorDelegate {
  NavigatorState getFlutterNavigatorState() {
    final flutterNavigatorKey =
        astronomiaNavigatorKey.currentState.widget.child.key;

    assert(flutterNavigatorKey is GlobalKey<NavigatorState>);
    return (flutterNavigatorKey as GlobalKey<NavigatorState>).currentState;
  }
}

class DefaultCommonNavigatorDelegate implements CommonNavigatorDelegate {
  final GlobalKey<AstronomiaNavigatorState> _astronomiaNavigatorKey =
      GlobalKey<AstronomiaNavigatorState>();
  @override
  GlobalKey<AstronomiaNavigatorState> get astronomiaNavigatorKey =>
      _astronomiaNavigatorKey;

  @override
  Future<T> pushNamed<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    return _astronomiaNavigatorKey.currentState
        .pushNamed<T>(routeName, arguments: arguments);
  }

  @override
  Future<T> pushNamedNative<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    final parameters = {"isPushNativeRoute": true, "arguments": arguments};

    return pushNamed<T>(routeName, arguments: parameters);
  }

  @override
  void pop<T extends Object>([T result]) {
    _astronomiaNavigatorKey.currentState.pop<T>(result);
  }

  @override
  Future<bool> maybePop<T extends Object>([T result]) {
    return _astronomiaNavigatorKey.currentState.maybePop<T>(result);
  }

  @override
  void popUntilNamed(String routeName) {
    _astronomiaNavigatorKey.currentState.popUntilNamed(routeName);
  }
}
