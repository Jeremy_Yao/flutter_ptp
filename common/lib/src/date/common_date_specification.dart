import 'package:common/common.dart';
import 'package:intl/intl.dart';

/// Implementation of Common Date Specification,
/// https://docs.google.com/spreadsheets/d/12ADmhEDKjLONxkzdvqwfx-74LVpiAC1MUsvtQQU8XEM/edit#gid=0
///
/// Usage:
/// ```dart
/// final CommonDateSpecification commonDateSpecification =
///   Provider.of<CommonDateSpecification>(context);
///
/// DateTime date = DateTime(2020, 4, 16, 16, 36);
/// commonDateSpecification.formatDate(date);
///
/// commonDateSpecification.formatFullDateTime(date);
///
/// commonDateSpecification.formatTime(date);
/// ```
class CommonDateSpecification {
  const CommonDateSpecification(this._appLocalizations);
  final AppLocalizations _appLocalizations;

  String _arrangeDatePattern(
      Map<int, String> datePatternMap, String seperator) {
    final keys = datePatternMap.keys.toList();
    keys.sort();
    final StringBuffer sb = StringBuffer();
    for (final key in keys) {
      final value = datePatternMap[key];
      if (value.isEmpty) continue;
      if (sb.isNotEmpty) {
        sb.write(seperator);
      }
      sb.write(value);
    }

    return sb.toString();
  }

  String _datePattern(
    DateTime dateTime, {
    bool ignoreYear = false,
    bool ignoreMonth = false,
    bool ignoreDay = false,
  }) {
    final currentLanguage = _appLocalizations.currentLanguage;

    String datePattern;

    if (currentLanguage.contains('zh')) {
      // zh: yyyy年M月d日
      datePattern = _arrangeDatePattern({
        1: !ignoreYear ? 'yyyy年' : '',
        2: !ignoreMonth ? 'M月' : '',
        3: !ignoreDay ? 'd日' : ''
      }, '');
    } else if (currentLanguage == 'vi_VN') {
      // vi_VN：d/M/yyyy
      datePattern = _arrangeDatePattern({
        1: !ignoreDay ? 'd' : '',
        2: !ignoreMonth ? 'M' : '',
        3: !ignoreYear ? 'yyyy' : '',
      }, '/');
    } else if (currentLanguage == 'ko_KR') {
      // ko_KR：yyyy년 M월 d일
      datePattern = _arrangeDatePattern({
        1: !ignoreYear ? 'yyyy년' : '',
        2: !ignoreMonth ? 'M월' : '',
        3: !ignoreDay ? 'd일' : ''
      }, ' ');
    } else {
      // Default: dd MMM yyyy
      String monthFormat;
      if (dateTime.month == 5 || dateTime.month == 6 || dateTime.month == 7) {
        monthFormat = 'MMMM';
      } else {
        monthFormat = 'MMM';
      }
      datePattern = 'dd $monthFormat yyyy';

      datePattern = _arrangeDatePattern({
        1: !ignoreDay ? 'dd' : '',
        2: !ignoreMonth ? monthFormat : '',
        3: !ignoreYear ? 'yyyy' : '',
      }, ' ');
    }

    return datePattern;
  }

  String _timePattern() {
    final currentLanguage = _appLocalizations.currentLanguage;
    String timeFormat;
    if (currentLanguage.contains('zh')) {
      timeFormat = 'h:mm a';
    } else if (currentLanguage == 'vi_VN' || currentLanguage == 'th_TH') {
      // vi_VN, th_TH
      timeFormat = 'H:mm';
    } else if (currentLanguage == 'ko_KR') {
      // ko_KR
      timeFormat = 'h:mm a';
    } else {
      timeFormat = 'h:mma';
    }

    return timeFormat;
  }

  /// Format date implementation of:
  /// > Date 中文（简、繁）的写法，使用公历日期标准格式，中间没有空格；    如: 2016年8月8日
  /// > - 日期应突出和醒目的表达，使用阿拉伯数字,
  /// > - 不得以小数点或顿号代替年月日
  /// > - 月和日是一位数字时，不在数字前补零；
  /// > - 年、月、日必须写全，不能简写成 16年10月1日
  /// >
  /// > Date 英文的写法，月份用英文，日和年用阿拉伯数字，中间各有1个空格； 如:  21 Oct 2016
  /// > - 年份应完全写出，不能简写。
  /// > - 月份要用英文名称，不要用数字代替，否则不同用户习惯用法不同，会引起误解
  /// > - 月份名称用缩写式。但 May, June, July, 因为较短，不可缩写
  /// > - 日期用基数词1，2，3，4，5，……28，29，30，31简单明了 (不用序数词 4th, 5th...）
  /// >
  /// > - Date 韩语的写法，年用년，月用월，日用일，年份与月份与日之间有空格；如： 2016년 12월 9일
  /// > （Lena增加，2017年4月1日）
  /// > - Date 泰语的写法：同英文：21 Oct 2017
  /// > - Date 越南语的写法：21/10/2017   日/月/年
  ///
  /// This function allow you ignore the year, month, day if you like, such as set
  /// the `ignoreYear = true` in Chinese, the output will become `8月8日`, etc.
  String formatDate(
    DateTime dateTime, {
    bool ignoreYear = false,
    bool ignoreMonth = false,
    bool ignoreDay = false,
  }) {
    return DateFormat(_datePattern(dateTime,
            ignoreYear: ignoreYear,
            ignoreMonth: ignoreMonth,
            ignoreDay: ignoreDay))
        .format(dateTime);
  }

  /// The full date time format, e.g., 2016年8月8日 4:36 PM
  String formatFullDateTime(
    DateTime dateTime, {
    bool ignoreYear = false,
    bool ignoreMonth = false,
    bool ignoreDay = false,
  }) {
    final String output = DateFormat(
            '${_datePattern(dateTime, ignoreYear: ignoreYear, ignoreMonth: ignoreMonth, ignoreDay: ignoreDay)} ${_timePattern()}')
        .format(dateTime);
    return _adjustAMPMMarkerIfNeed(output);
  }

  /// Format time implementation of:
  /// > 按style guide文档上面来统一；https://docs.google.com/document/d/1PBxdyIaczJ5Ssi1IgjSOXtAn2bFTBdy9H2NOjCXBeA0/edit
  /// > Time 采用12小时制，中文（简、繁）""AM/PM"" 大写； 英文“am/pm"" 小写
  ///
  /// > 如：中文（简、繁）格式  9:30 AM
  /// > 区间格式 7:00 AM-11:50 PM
  ///
  /// > 英文格式 11:30am
  /// > 区间格式 7:00am-9:30pm"
  ///
  /// > 韩语：上午：오전    下午：오후
  ///
  /// > 越南语格式 24小时制：06:00    19:00
  ///
  /// > 泰语格式 24小时制：6:00   19:00
  ///
  /// See also:
  /// [formatTimeRange]
  String formatTime(DateTime dateTime) {
    final String output = DateFormat(_timePattern()).format(dateTime);

    return _adjustAMPMMarkerIfNeed(output);
  }

  String _adjustAMPMMarkerIfNeed(String original) {
    final currentLanguage = _appLocalizations.currentLanguage;
    if (currentLanguage.startsWith('zh') ||
        currentLanguage == 'vi_VN' ||
        currentLanguage == 'th_TH') {
      return original;
    }
    if (currentLanguage == 'ko_KR') {
      if (original.endsWith('AM')) {
        return original.replaceAll('AM', '오전');
      } else {
        return original.replaceAll('PM', '오후');
      }
    }

    return original.toLowerCase();
  }

  /// The range format of time, which add a `-` between [formatTime], e.g., 7:00am-9:30pm.
  ///
  /// **NOTE** that it just add a `-` between [formatTime], and do not check the whetcher
  /// the `startTime` is before the `endTime` or not.
  String formatTimeRange(DateTime startTime, DateTime endTime) {
    return '${formatTime(startTime)}-${formatTime(endTime)}';
  }
}
