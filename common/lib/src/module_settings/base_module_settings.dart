import 'package:built_value/serializer.dart';
import 'package:common/common.dart';
import 'package:flutter/widgets.dart';
import 'package:l10n_cms/l10n_cms.dart';

/// Implementation of https://bitbucket.org/klook/klook-app-flutter/issues/6/modulesettings-module.
abstract class BaseModuleSettings {
  const BaseModuleSettings();

  Serializers serializers();

  Map<String, PageBuilder> pageRoutes();

  // TODO(littlegnal): Add this only for campable the `startUpApp`, try remove it later.
  Map<String, ComponentBuilder> railsComponentBuilderMap() =>
      const <String, ComponentBuilder>{};

  LocalizationsDelegate l10ncmsLocalizations(
    GlobalL10NCMSLocalizationsLookup globalL10NCMSLocalizationsLookup,
  ) {
    return null;
  }
}
