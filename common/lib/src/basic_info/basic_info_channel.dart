import 'package:common/src/basic_info/model/location.dart';
import 'package:common/src/channels/base/channel.dart';
import 'package:flutter/foundation.dart';

import 'model/app_info.dart';
import 'model/country_info.dart';
import 'model/device_info.dart';
import 'model/prompt_update_info.dart';
import 'model/protocol_info.dart';
import 'model/user_info.dart';

// TODO(RUIJIN): channel 暂时使用死数据
class BasicInfoChannel extends Channel {
  BasicInfoChannel();

  @override
  // ignore: overridden_fields
  String name = 'com.klook.platform/basicInfo';

  /// 获取设备信息
  Future<DeviceInfo> getDeviceInfo() async {
    return DeviceInfo.fromJson(Map<String, dynamic>.from({
      "manufacturer": "",
      "platform": "platform",
      "device_name": "device_name",
      "advertising_id": "advertising_id",
      "system_version": "system_version",
      "scale": 1.0,
      "device_id": 'device_id',
      "language": 'zh_CN',
      "language_display_name": 'zh_CN'
    }));

    final Map info = await invokeMethod('get_device_info');
    return DeviceInfo.fromJson(Map<String, dynamic>.from(info));
  }

  /// 获取用户信息
  Future<UserInfo> getUserInfo() async {
    return UserInfo.fromJson(Map<String, dynamic>.from({
      "email": 'email@qq.com',
      "user_id": 'user_id',
      "global_id": 'global_id',
      "username": 'username',
      "familyName": 'familyName',
      "first_name": 'first_name',
      "title": 'title',
      "country_code": 'country_code',
      "mobile": 'mobile',
      "token": 'token',
      "avatar": 'avatar',
      "credits": 'credits',
      "valid_coupons": 'valid_coupons',
      "latest_unreview_booking_ref_no": 'latest_unreview_booking_ref_no',
      "unread_message_count": "4"
    }));

    Map info = await invokeMethod('get_user_info');
    info = info ?? {};
    return UserInfo.fromJson(Map<String, dynamic>.from(info));
  }

  /// 是否需要显示切换region, 用于控制显示本地本地的region切换通知
  Future<bool> isShowRegionSwitchIndication() async {
    return false;

    Map info = await invokeMethod('is_preferred_site_same_with_current');
    info = info ?? {"same": true};
    // 一样的话，就不显示
    return !(info["same"] ?? true);
  }

  /// 获取APP信息
  Future<AppInfo> getAppInfo() async {
    return AppInfo.fromJson(Map<String, dynamic>.from({
      "version": "5.54.0",
      "api_host": "https://t43.test.klook.io",
      "api_version": "8.4",
      "language": "zh_CN",
      "bundle_id": "",
      "local": "",
      "klook_host": "",
      "klook_tint": "",
      "x_country_code": "",
      "currency": {"name": "", "symbol": ""},
      "develop_setting": {"isPreviewMode": true}
    }));

    final Map info = await invokeMethod('get_app_info');
    return AppInfo.fromJson(Map<String, dynamic>.from(info));
  }

  /// 获取用户协议信息
  Future<ProtocolInfo> getProtocolInfo() async {
    return ProtocolInfo.fromJson([]);

    final List list = await invokeMethod('get_protocol_info');
    final List<Map<String, dynamic>> protocolInfoList =
        list.map((map) => Map<String, dynamic>.from(map)).toList();
    return ProtocolInfo.fromJson(protocolInfoList);
  }

  /// 提交用户协议信息
  Future<void> submitSelectProtocolInfo(String termId) async {
    //invokeMethod('submit_select_protocol_info', termId);
  }

  /// 提交JSession 用于风控
  Future<void> submitJSession() async {
    //invokeMethod('submit_jsession');
  }

  /// 同步 jsession 到 Natvive
  Future<void> syncJSession(String sid) async {
    //invokeMethod('sync_jsession', sid);
  }

  /// 获取用户支付渠道信息
  Future<List<String>> getPaymentGatewayInfo() async {
    return [];

    final List list = await invokeMethod('get_payment_gateway_info');
    return List<String>.from(list);
  }

  /// 根据 relativeUrl 获取当前语言环境下完整的 webUrl (webUrl 中语言标示与个人信息中的 language 不同)
  /// 例如：jrpass/seat => http://t51.test.klook.io/zh-CN/jrpass/seat
  Future<String> getWebLink(String relativeUrl) async {
    final String webLink =
        await invokeMethod<String>('get_web_link', relativeUrl);
    return webLink;
  }

  /// 获取国家/地区代码列表
  Future<List<CountryInfo>> getCountryInfoList() async {
    return [];

    final List list = await invokeMethod('get_country_info_list');
    final List<Map<String, dynamic>> countryInfoListMap =
        list.map((map) => Map<String, dynamic>.from(map)).toList();

    return countryInfoListMap
            ?.map((map) => CountryInfo.fromJson(map))
            ?.toList() ??
        [];
  }

  /// 修改APP语言
  void changeLanguage(String language) {

    //invokeMethod('change_language', language);
  }

  /// 获取缓存定位
  Future<Location> getLocation() async {
    return Location.fromJson(Map<String, dynamic>.from({}));

    final Map info = await invokeMethod('get_location');
    if (info == null) {
      return null;
    }
    return Location.fromJson(Map<String, dynamic>.from(info));
  }

  /// 请求定位
  Future<Location> requestLocation() async {
    return Location.fromJson(Map<String, dynamic>.from({}));

    final Map info = await invokeMethod('request_location');
    if (info == null) {
      return null;
    }
    return Location.fromJson(Map<String, dynamic>.from(info));
  }

  ///获取App提示更新信息
  Future<PromptUpdateInfo> getPromptUpdateInfo() async {
    return PromptUpdateInfo.fromJson(Map<String, dynamic>.from({}));

    final Map info = await invokeMethod('get_prompt_update_info');
    if (info == null) {
      return null;
    }
    return PromptUpdateInfo.fromJson(Map<String, dynamic>.from(info));
  }

  /// 获取aff的信息，这个信息一般在生成订单时作为请求的参数一起提交
  Future<Map<String, dynamic>> getAffiliateFields() async {
    return Map<String, dynamic>.from({});

    final Map info = await invokeMethod('get_affiliate_fields');
    if (info == null) {
      return null;
    }
    return Map<String, dynamic>.from(info);
  }

  /// 根据实验名称查询实验分组
  Future<String> abtestGroupName({@required String experimentName}) {
    return invokeMethod(
        'abtest_group_name_for_experiment_name', experimentName);
  }

  /// 根据实验名称查询是否控制组
  Future<bool> abtestIsControlGroup({@required String experimentName}) {
    return invokeMethod(
        'abtest_is_control_group_for_experiment_name', experimentName);
  }

  /// 根据实验名称查询是否实验组
  Future<bool> abtestIsExperimentGroup({@required String experimentName}) {
    return invokeMethod(
        'abtest_is_experiment_group_for_experiment_name', experimentName);
  }
}
