class PromptUpdateInfo {
  PromptUpdateInfo(this.promptUpdate, this.title, this.msg);

  factory PromptUpdateInfo.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return null;
    }
    final promptUpdate = map["promptUpdate"];
    bool boolPromptUpdate = false;
    if (promptUpdate is bool) {
      boolPromptUpdate = promptUpdate;
    } else if (promptUpdate is int) {
      boolPromptUpdate = promptUpdate > 0;
    }
    return PromptUpdateInfo(boolPromptUpdate, map["title"], map["msg"]);
  }

  final String title;
  final String msg;
  final bool promptUpdate;
}
