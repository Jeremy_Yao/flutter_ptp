import 'dart:ui';

class CountryInfo {
  const CountryInfo(
      this.countryCode, this.countryRegion, this.countryName, this.isSelected);

  CountryInfo.fromJson(Map<String, dynamic> map)
      : countryCode = map["countryCode"],
        countryRegion = map["countryRegion"],
        countryName = map["countryName"],
        isSelected = map["isSelected"];

  final String countryCode;
  final String countryRegion;
  final String countryName;
  final bool isSelected;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CountryInfo &&
          runtimeType == other.runtimeType &&
          countryCode == other.countryCode &&
          countryRegion == other.countryRegion &&
          countryName == other.countryName &&
          isSelected == other.isSelected;

  @override
  int get hashCode =>
      hashValues(countryCode, countryRegion, countryName, isSelected);
}
