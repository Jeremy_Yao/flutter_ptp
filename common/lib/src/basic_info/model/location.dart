class Location {
  Location(this.longitude, this.latitude);
  factory Location.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return null;
    }
    return Location(map["longitude"], map["latitude"]);
  }
  final String longitude;
  final String latitude;
}
