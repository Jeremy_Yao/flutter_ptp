class DevelopSetting {
  DevelopSetting.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    isPreviewMode = map['preview_mode'];
  }
  bool isPreviewMode;
}
