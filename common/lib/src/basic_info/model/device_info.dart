class DeviceInfo {
  DeviceInfo.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    manufacturer = map["manufacturer"];
    platform = map["platform"];
    deviceName = map["device_name"];
    advertisingId = map["advertising_id"];
    systemVersion = map["system_version"];
    scale = map["scale"];
    deviceId = map['device_id'];
    language = map['language'];
    languageDispalyName = map['language_display_name'];
  }

  String manufacturer;
  String platform;
  String deviceName;
  String systemVersion;
  double scale;
  String deviceId;
  String advertisingId;

  /// 设备语言
  String language;

  /// 设备语言对应的名称
  String languageDispalyName;
}
