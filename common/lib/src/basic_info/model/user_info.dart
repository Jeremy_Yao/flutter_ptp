class UserInfo {
  UserInfo.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    email = map['email'];
    userId = map['user_id'];
    globalId = map['global_id'];
    username = map['username'];
    familyName = map['familyName'];
    firstName = map['first_name'];
    title = map['title'];
    countryCode = map['country_code'];
    mobile = map['mobile'];
    token = map['token'];
    avatar = map['avatar'];
    credits = map['credits'];
    validCoupons = map['valid_coupons'];
    latestUnreviewBookingRefNo = map['latest_unreview_booking_ref_no'];
    unreadMessageCount = map["unread_message_count"];
  }

  String email;
  String userId;
  String globalId;
  String username;
  String familyName;
  String firstName;
  String title;
  String countryCode;
  String mobile;
  String token;
  String avatar;
  String credits;
  String validCoupons;
  String latestUnreviewBookingRefNo;
  String unreadMessageCount;
}
