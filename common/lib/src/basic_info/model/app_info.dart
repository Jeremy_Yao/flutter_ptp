import 'develop_setting.dart';

class AppInfo {
  AppInfo.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    version = map["version"];
    apiHost = map["api_host"];
    apiVersion = map["api_version"];
    language = map["language"];
    bundleId = map["bundle_id"];
    local = map["local"];
    klookHost = map["klook_host"];
    klookTint = map["klook_tint"];
    xCountryCode = map["x_country_code"];
    currency =
        Currency.fromJson(Map<String, dynamic>.from(map["currency"] ?? {}));
    developSetting = DevelopSetting.fromJson(
        Map<String, dynamic>.from(map['develop_setting'] ?? {}));
  }

  String version;
  String apiHost;
  String apiVersion;
  String language;
  String bundleId;
  String local;
  Currency currency;
  String klookHost;
  String klookTint;
  String xCountryCode;
  DevelopSetting developSetting;
}

class Currency {
  Currency.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    name = map["name"];
    symbol = map["symbol"];
  }
  String name;
  String symbol;
}
