import 'package:flutter/cupertino.dart';

class ProtocolInfo {
  ProtocolInfo.fromJson(List<Map<String, dynamic>> list) {
    if (list == null || list.isEmpty) {
      return;
    }
    protocolItem = list.map((dict) => ProtocolItem.fromJson(dict)).toList();
  }
  List<ProtocolItem> protocolItem;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProtocolInfo &&
          runtimeType == other.runtimeType &&
          protocolItem == other.protocolItem;

  @override
  int get hashCode => protocolItem.hashCode;
}

class ProtocolItem {
  ProtocolItem.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    content = map["content"];
    termId = map["termId"];
    contentLink = map["contentLink"];
    isChecked = map["checked"];
    isRequired = map["required"];
  }
  String content;
  bool isChecked;
  bool isRequired;
  int termId;
  String contentLink;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProtocolItem &&
          runtimeType == other.runtimeType &&
          content == other.content &&
          isChecked == other.isChecked &&
          isRequired == other.isRequired &&
          termId == other.termId &&
          contentLink == other.contentLink;

  @override
  int get hashCode =>
      hashValues(content, isChecked, isRequired, termId, contentLink);
}
