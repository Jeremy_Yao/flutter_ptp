import 'package:common/common.dart';
import 'package:common/src/basic_info/model/user_info.dart';

class LoginManager {
  LoginManager(this._nativeInfoManager);

  final NativeInfoManager _nativeInfoManager;

  /// Return `true` if currently login
  Future<bool> isLogin() async {
    final UserInfo userInfo = await _nativeInfoManager.provideUserInfo();

    // android userInfo token default value is ""
    return userInfo.token?.isNotEmpty ?? false;
  }
}
