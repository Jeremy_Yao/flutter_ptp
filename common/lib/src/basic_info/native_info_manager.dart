import 'package:common/src/basic_info/basic_info_channel.dart';
import 'package:common/src/basic_info/model/app_info.dart';
import 'package:common/src/basic_info/model/country_info.dart';
import 'package:common/src/basic_info/model/device_info.dart';
import 'package:common/src/basic_info/model/prompt_update_info.dart';
import 'package:common/src/basic_info/model/protocol_info.dart';
import 'package:common/src/basic_info/model/user_info.dart';
import 'package:common/src/channels/signature/signature_channel.dart';
import 'package:flutter/foundation.dart';

import 'model/location.dart';

class AppInfoProvider {
  Future<AppInfo> provideAppInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class UserInfoProvider {
  Future<UserInfo> provideUserInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class SignatureInfoProvider {
  Future<String> provideSignature(String raw) {
    throw UnsupportedError("Not Supported!");
  }
}

class DeviceInfoProvider {
  Future<DeviceInfo> provideDeviceInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class RegionSwitchIndicationProvider {
  Future<bool> provideRegionSwitchIndicationInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class ProtocolInfoProvider {
  Future<ProtocolInfo> provideProtocolInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class PaymentGatewayProvider {
  Future<List> providePaymentGatewayInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class WebUrlProvider {
  Future<String> provideWebUrlProvider(String relativeUrl) {
    throw UnsupportedError("Not Supported!");
  }
}

class CountryInfoProvider {
  Future<List<CountryInfo>> provideCountryInfo() {
    throw UnsupportedError("Not Supported!");
  }
}

class AffiliateFieldsProvider {
  Future<Map<String, dynamic>> provideAffiliateFields() {
    throw UnsupportedError("Not Supported!");
  }
}

class ABTestProvider {
  /// 根据实验名称查询实验分组
  Future<String> abtestGroupName({@required String experimentName}) {
    throw UnsupportedError("Not Supported!");
  }

  /// 根据实验名称查询是否控制组
  Future<bool> abtestIsControlGroup({@required String experimentName}) {
    throw UnsupportedError("Not Supported!");
  }

  /// 根据实验名称查询是否实验组
  Future<bool> abtestIsExperimentGroup({@required String experimentName}) {
    throw UnsupportedError("Not Supported!");
  }
}

/// Class that handle the [AppInfo], [UserInfo], [DeviceInfo], etc...
class NativeInfoManager
    implements
        AppInfoProvider,
        UserInfoProvider,
        SignatureInfoProvider,
        DeviceInfoProvider,
        ProtocolInfoProvider,
        PaymentGatewayProvider,
        WebUrlProvider,
        CountryInfoProvider,
        AffiliateFieldsProvider,
        RegionSwitchIndicationProvider,
        ABTestProvider {
  NativeInfoManager(
      [BasicInfoChannel basicInfoChannel, SignatureChannel signatureChannel])
      : _basicInfoChannel = basicInfoChannel ?? BasicInfoChannel(),
        _signatureChannel = signatureChannel ?? SignatureChannel();

  final BasicInfoChannel _basicInfoChannel;
  final SignatureChannel _signatureChannel;
  DeviceInfo _deviceInfo;

  @override
  Future<AppInfo> provideAppInfo() {
    return _basicInfoChannel.getAppInfo();
  }

  @override
  Future<String> provideSignature(String raw) =>
      _signatureChannel.getSignature(raw);

  // TODO(littlegnal): Maybe AppInfo, UserInfo can combine into a single object in the future,
  // maybe named NativeInfo, to reduce the `channel.invokeMethod` times.
  @override
  Future<UserInfo> provideUserInfo() {
    return _basicInfoChannel.getUserInfo();
  }

  @override
  Future<bool> provideRegionSwitchIndicationInfo() {
    return _basicInfoChannel.isShowRegionSwitchIndication();
  }

  @override
  Future<DeviceInfo> provideDeviceInfo() async {
    if (_deviceInfo != null) {
      return _deviceInfo;
    }
    _deviceInfo = await _basicInfoChannel.getDeviceInfo();
    return _deviceInfo;
  }

  @override
  Future<ProtocolInfo> provideProtocolInfo() {
    return _basicInfoChannel.getProtocolInfo();
  }

  @override
  Future<List<CountryInfo>> provideCountryInfo() {
    return _basicInfoChannel.getCountryInfoList();
  }

  /// 通信到原生界面
  /// 调用原生的提交协议接口，将flutter端选择的协议上传到服务器
  ///
  /// ```dart
  /// String termId = "23,24";
  /// _nativeInfoManager.submitSelectProtocolInfo(termId);
  /// ```
  void submitSelectProtocolInfo(String termId) {
    _basicInfoChannel.submitSelectProtocolInfo(termId);
  }

  /// 通信到原生界面
  /// 获取支持的支付方式
  /// 返回数据的格式为
  /// android:["googlepay"]
  /// iOS:["applepay"]
  @override
  Future<List<String>> providePaymentGatewayInfo() {
    return _basicInfoChannel.getPaymentGatewayInfo();
  }

  /// 根据 relativeUrl 获取当前语言环境下完整的 webUrl (webUrl 中语言标识与个人信息中的 language 不同)
  /// 例如：jrpass/seat => http://t51.test.klook.io/zh-CN/jrpass/seat
  @override
  Future<String> provideWebUrlProvider(String relativeUrl) {
    return _basicInfoChannel.getWebLink(relativeUrl);
  }

  ///
  /// 通信到原生界面
  /// 调用原生的提交session模块，防止风控
  ///
  /// ```dart
  /// _nativeInfoManager.submitJSession();
  /// ```
  ///
  void submitJSession() {
    _basicInfoChannel.submitJSession();
  }

  ///
  /// 通信到原生
  /// 同步jsession 到 natvie
  void syncJSessionId(String sid) {
    _basicInfoChannel.syncJSession(sid);
  }

  /// 修改APP语言
  /// language 格式：en_US
  void changeLanguage(String language) {
    _basicInfoChannel.changeLanguage(language);
  }

  /// 获取缓存定位，立即返回
  Future<Location> getLocation() {
    return _basicInfoChannel.getLocation();
  }

  /// 请求定位，可能会有一定延迟返回
  Future<Location> requestLocation() {
    return _basicInfoChannel.requestLocation();
  }

  /// 获取提示更新的内容
  Future<PromptUpdateInfo> getPromptUpdateInfo() {
    return _basicInfoChannel.getPromptUpdateInfo();
  }

  /// 获取aff的参数，这个参数一般在生成订单时作为参数一起提交
  @override
  Future<Map<String, dynamic>> provideAffiliateFields() {
    return _basicInfoChannel.getAffiliateFields();
  }

  @override
  Future<String> abtestGroupName({@required String experimentName}) {
    return _basicInfoChannel.abtestGroupName(experimentName: experimentName);
  }

  @override
  Future<bool> abtestIsControlGroup({@required String experimentName}) {
    return _basicInfoChannel.abtestIsControlGroup(
        experimentName: experimentName);
  }

  @override
  Future<bool> abtestIsExperimentGroup({@required String experimentName}) {
    return _basicInfoChannel.abtestIsExperimentGroup(
        experimentName: experimentName);
  }
}
