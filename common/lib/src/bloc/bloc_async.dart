import 'package:common/common.dart';
import 'package:error_handling/error_handling.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/subjects.dart';

/// Abstract class that indicate the async state:
///
/// * [Uninitialized] : initial state, which is useful when you need handle the initial state
/// * [Loading] : loading state
/// * [Success] : success state
/// * [Fail] : fail state
///
/// The `call()` method has been overridden, so that you can invoke your [BlocAsync] instance like a function, e.g.,
///
/// ```dart
/// var success = Success(10);
/// var successValue = success(); // successValue = 10
/// ```
///
/// Ref: [https://dart.dev/guides/language/language-tour#callable-classes](https://dart.dev/guides/language/language-tour#callable-classes)
abstract class BlocAsync<T> {
  const BlocAsync();

  /// Returns the [Success] value or null.
  ///
  /// Can be invoked as an operator like: `yourProp()`
  T call() => null;
}

class Uninitialized<T> extends BlocAsync<T> {
  const Uninitialized();

  @override
  bool operator ==(dynamic other) => other is Uninitialized<T>;

  @override
  int get hashCode => "Uninitialized".hashCode;
}

class Loading<T> extends BlocAsync<T> {
  const Loading();

  @override
  bool operator ==(dynamic other) => other is Loading;

  @override
  int get hashCode => "Loading".hashCode;
}

class Success<T> extends BlocAsync<T> {
  const Success(this._value);

  final T _value;

  @override
  T call() => _value;

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) {
      return true;
    }
    return other is Success && _value == other._value;
  }

  @override
  int get hashCode => hashValues(runtimeType, identityHashCode(_value));

  @override
  String toString() => 'Success{_value: $_value}';
}

class Fail<T> extends BlocAsync<T> {
  const Fail(this.error);

  final Exception error;

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) {
      return true;
    }
    return other is Fail &&
        error.runtimeType == other.error.runtimeType &&
        error.toString() == other.error.toString();
  }

  @override
  int get hashCode =>
      hashValues(error.runtimeType, identityHashCode(error.toString()));

  @override
  String toString() {
    return 'Fail{error: $error}';
  }
}

extension FailEx<T> on Fail<T> {
  /// `true` if the [Fail.error] is [CancelHttpException], you can use this flag
  /// to handle you business logic if need.
  bool get isHttpRequestCancelled => error is CancelHttpException;

  /// Show to users
  String get errorMessage {
    if (error is BusinessLogicError) {
      return (error as BusinessLogicError).message;
    }
    return error.toString();
  }
}

extension BehaviorSubjectBlocAsyncEx<T> on BehaviorSubject<T> {
  /// An extension function that handle the [BlocAsync] for the [BehaviorSubject] instance.
  ///
  /// We handle the [Loading] and [Fail] state for you, but you need to define the
  /// success [Function] to return the success value, and you also need to implement your
  /// reducer function to emit the new state. e.g.,
  ///
  /// ```dart
  ///
  ///   class State {
  ///     const State(this._isLoading, this._successValue, this._isFail);
  ///
  ///     final bool _isLoading;
  ///     final int _successValue;
  ///     final bool _isFail;
  ///   }
  ///
  ///   void handleExecute() {
  ///     var successValue = Future.value(10);
  ///
  ///      _subject.execute(() => successValue, (preState, blocAsync) {
  ///         if (blocAsync is Loading) return const State(true, 0, false);
  ///         if (blocAsync is Success) return const State(false, blocAsync(), false);
  ///         if (blocAsync is Fail) return const State(false, 0, true);
  ///
  ///         return const State(false, 0, false);
  ///      });
  ///   }
  ///
  /// ```
  Future<void> execute<V>(
      Future<V> Function(T Function() preState) successValue,
      T Function(T Function() preState, BlocAsync<V> blocAsync) reducer) async {
    final preS = () => value;
    try {
      if (!isClosed) {
        sink.add(reducer(preS, const Loading()));
      }

      final sv = Success<V>(await successValue(preS));
      if (!isClosed) {
        sink.add(reducer(preS, sv));
      }
    } catch (e) {
      if (isClosed) {
        return;
      }
      final Exception exception = errorToException(e);
      debugPrint("[${toString()}] execute Exception: ${exception.toString()}");
      sink.add(reducer(preS, Fail<V>(exception)));
    }
  }
}
