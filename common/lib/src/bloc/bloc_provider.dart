import 'package:common/src/bloc/base_bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

/// A [StatelessWidget] that help you reduce boilerplate code of instantiating and disposing a BLoC.
///
/// The [BlocProvider] underly base on the [Provider], so you can simplified get the BLoC by using the
/// [Provider]'s syntax inside the child widget: `Provider.of<YourBLoC>(context)`
class BlocProvider<T extends BaseBloc> extends StatelessWidget {
  const BlocProvider({@required Create<T> create, @required Widget child})
      : _create = create,
        _child = child;

  final Create<T> _create;
  final Widget _child;

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: _create,
      dispose: (context, bloc) {
        bloc.dispose();
      },
      child: _child,
    );
  }
}
