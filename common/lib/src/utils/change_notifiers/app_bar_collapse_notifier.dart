import 'package:flutter/foundation.dart';

class AppBarCollapseNotifier with ChangeNotifier {
  bool _isCollapseAppBar = false;

  bool get isCollapseAppBar => _isCollapseAppBar;

  set appBarCollapsePercent(double percent) {
    bool isCollapseAppBar = false;
    if (percent >= 0.75) {
      if (_isCollapseAppBar) {
        return;
      }
      isCollapseAppBar = true;
    } else {
      if (!_isCollapseAppBar) {
        return;
      }
      isCollapseAppBar = false;
    }

    if (isCollapseAppBar != _isCollapseAppBar) {
      _isCollapseAppBar = isCollapseAppBar;
      notifyListeners();
    }
  }
}
