/// We force use SVG files in the project, use this function can be simplified
/// you get the SVG file path.
String svgPath(String assetImageName, String packageName) {
  assert(assetImageName != null);
  assert(packageName != null);
  return "assets/svgs/$packageName/$assetImageName.svg";
}

/// Get SVG file path for `jrpass` package, the path schema is:
/// `assets/svgs/jrpass/$assetImageName.svg`
@Deprecated("This function will be removed in the future, use r.dart instead")
String jrpassSVGPath(String assetImageName) =>
    svgPath(assetImageName, "jrpass");
@Deprecated(
    "This function will be removed in the future, import common/asssets.dart and get the asset path instead")
String commonSVGPath(String assetImageName) =>
    svgPath(assetImageName, "common");
@Deprecated("This function will be removed in the future, use r.dart instead")
String railPtpSVGPath(String assetImageName) =>
    svgPath(assetImageName, "rail_ptp");
