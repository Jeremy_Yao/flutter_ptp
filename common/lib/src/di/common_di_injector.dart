import 'package:astronomia/astronomia.dart';
import 'package:built_value/serializer.dart';
import 'package:common/basic_info.dart';
import 'package:common/common.dart';
import 'package:common/components.dart';
import 'package:common/local_event.dart';
import 'package:common/src/components/feed_card/feed_card_builder.dart';
import 'package:common/src/components/feed_card/feed_card_content_factory.dart';
import 'package:common/src/components/feed_card/feed_card_content_impl_factory.dart';
import 'package:common/src/components/feed_card/feed_card_item_factory.dart';
import 'package:common/src/components/feed_card/feed_card_item_impl_factory.dart';
import 'package:common/src/date/common_date_specification.dart';
import 'package:common/src/navigator/common_navigator.dart';
import 'package:common/src/network/interceptors/app_force_update_interceptor.dart';
import 'package:common/src/network/interceptors/charles_400_interceptor.dart';
import 'package:common/src/network/interceptors/header_interceptor.dart';
import 'package:common/src/network/interceptors/host_interceptor.dart';
import 'package:common/src/network/interceptors/kount_sessionid_interceptor.dart';
import 'package:common/src/network/interceptors/optimus_message_manager.dart';
import 'package:common/src/network/interceptors/optimus_monitor_interceptor.dart';
import 'package:common/src/network/interceptors/signature_interceptor.dart';
import 'package:common/src/network/transfer_error_handler.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:l10n_cms/l10n_cms.dart';
import 'package:provider/provider.dart';
import 'package:kltracker/kltracker.dart' show InHouseTrackingManager;

/// Class that initial and provide the global used instance.
class DIProvider {
  DIProvider({
    @required Serializers serializers,
    @required Map<String, ComponentBuilder> railsComponentBuilderMap,
    List<ComponentContentFactory> componentContentFactories,
    ComponentContainerFactory componentWrapperFactory,
    @required Map<String, FeedCardContentBuilder> feedContentCardBuilderMap,
    @required
        Map<String, GenericFeedCardItemBuilder> genericFeedCardItemBuilderMap,
    @required GlobalL10NCMSLocalizationsLookup globalL10NCMSLocalizationsLookup,
  }) {
    _init(
      serializers,
      railsComponentBuilderMap,
      componentContentFactories,
      componentWrapperFactory,
      feedContentCardBuilderMap,
      genericFeedCardItemBuilderMap,
      globalL10NCMSLocalizationsLookup,
    );
  }

  CommonNavigator _commonNavigator;

  CommonNavigator get commonNavigator => _commonNavigator;

  GlobalKey<AstronomiaNavigatorState> _astronomiaNavigatorKey;

  GlobalKey<AstronomiaNavigatorState> get astronomiaNavigatorKey =>
      _astronomiaNavigatorKey;

  NativeInfoManager _nativeInfoManager;

  NativeInfoManager get nativeInfoManager => _nativeInfoManager;

  AppLocalizations _appLocalizations;

  AppLocalizations get appLocalizations => _appLocalizations;

  NativeEventDispatcher _nativeEventDispatcher;

  NativeEventDispatcher get nativeEventDispatcher => _nativeEventDispatcher;

  Serialization _serialization;

  Serialization get serialization => _serialization;

  CommonRequest _commonRequest;

  CommonRequest get commonRequest => _commonRequest;

  Tracker _tracker;

  Tracker get tracker => _tracker;

  ComponentFactory _railsComponentFactory;

  ComponentFactory get railsComponentFactory => _railsComponentFactory;

  PlatformComponentFactory _componentFactory;

  PlatformComponentFactory get componentFactory => _componentFactory;

  FeedCardContentImplFactory _feedCardContentImplFactory;

  FeedCardContentImplFactory get feedCardContentImplFactory =>
      _feedCardContentImplFactory;

  GenericFeedCardItemImplFactory _genericFeedCardItemImplFactory;

  GenericFeedCardItemImplFactory get genericFeedCardItemImplFactory =>
      _genericFeedCardItemImplFactory;

  LocalEventManager _localEventManager;

  LocalEventManager get localEventManager => _localEventManager;
  LoginManager _loginManager;

  LoginManager get loginManager => _loginManager;

  CommonDateSpecification _commonDateSpecification;

  CommonDateSpecification get commonDateSpecification =>
      _commonDateSpecification;

  AstronomiaNavigatorObserverDelegate get astronomiaNavigatorObserverDelegate =>
      _astronomiaNavigatorObserverDelegate;

  AstronomiaNavigatorObserverDelegate _astronomiaNavigatorObserverDelegate;

  OptimusMessageManager get optimusMessageManager => _optimusMessageManager;

  OptimusMessageManager _optimusMessageManager;

  void _init(
    Serializers serializers,
    Map<String, ComponentBuilder> railsComponentBuilderMap,
    List<ComponentContentFactory> componentContentFactories,
    ComponentContainerFactory componentWrapperFactory,
    Map<String, FeedCardContentBuilder> feedContentCardBuilderMap,
    Map<String, GenericFeedCardItemBuilder> genericFeedCardItemBuilderMap,
    GlobalL10NCMSLocalizationsLookup globalL10NCMSLocalizationsLookup,
  ) {
    final CommonNavigatorDelegate commonNavigatorDelegate =
        _createCommonNavigatorDelegate();
    _commonNavigator = CommonNavigator(commonNavigatorDelegate);
    _astronomiaNavigatorKey = _commonNavigator.astronomiaNavigatorKey;

    _astronomiaNavigatorObserverDelegate =
        AstronomiaNavigatorObserverDelegate();

    _optimusMessageManager = OptimusMessageManager();

    _nativeInfoManager =
        NativeInfoManager(BasicInfoChannel(), SignatureChannel());
    _appLocalizations =
        AppLocalizations(_nativeInfoManager, globalL10NCMSLocalizationsLookup);

    _localEventManager = LocalEventManager();
    _astronomiaNavigatorObserverDelegate
        .addNavigatorObserver(InHouseTrackingManager.instance.routerObserver);

    final nativeEventChannel = NativeEventChannel();
    _nativeEventDispatcher = NativeEventDispatcher(
        nativeEventChannel: nativeEventChannel,
        localEventManager: _localEventManager);

    _serialization = Serialization(serializers);

    final List<Interceptor> interceptors = [
      HostInterceptor(_nativeInfoManager),
      HeaderInterceptor(_nativeInfoManager, _nativeInfoManager,
          _nativeInfoManager, _nativeInfoManager),
      SignatureInterceptor(_nativeInfoManager),
      AppForceUpdateInterceptor(
        _commonNavigator,
        nativeEventChannel,
        _appLocalizations,
      ),
      if (kDebugMode) Charles400Interceptor(),
      KountSessionIdInterceptor(_nativeInfoManager),
      OptimusMonitorInterceptor(_optimusMessageManager),
    ];
    _commonRequest = CommonRequest(HTTP.instance, interceptors, _serialization,
        httpRequestErrorHandler: HttpRequestErrorHandlers(
            [LoginErrorHandler(_commonNavigator), TransferErrorHandler()]));

    _tracker = NativeTracker(TrackChannel());
    _railsComponentFactory = ComponentFactory(railsComponentBuilderMap);
    _componentFactory = PlatformComponentFactory(
        componentContentFactories, componentWrapperFactory);
    _feedCardContentImplFactory =
        FeedCardContentImplFactory(feedContentCardBuilderMap);
    _genericFeedCardItemImplFactory =
        GenericFeedCardItemImplFactory(genericFeedCardItemBuilderMap);
    _loginManager = LoginManager(_nativeInfoManager);

    _commonDateSpecification = CommonDateSpecification(_appLocalizations);
  }

  CommonNavigatorDelegate _createCommonNavigatorDelegate() {
    final DefaultCommonNavigatorDelegate defaultCommonNavigatorDelegate =
        DefaultCommonNavigatorDelegate();

    return defaultCommonNavigatorDelegate;
  }
}

class CommonDIInjector extends StatelessWidget {
  const CommonDIInjector({
    Key key,
    @required DIProvider diProvider,
    @required Widget child,
  })  : assert(diProvider != null),
        _child = child,
        _diProvider = diProvider,
        super(key: key);

  final DIProvider _diProvider;

  final Widget _child;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<CommonNavigator>.value(value: _diProvider.commonNavigator),
        Provider<NativeEventDispatcher>.value(
            value: _diProvider.nativeEventDispatcher),
        Provider<Serialization>.value(value: _diProvider.serialization),
        Provider<AppLocalizations>.value(value: _diProvider.appLocalizations),
        Provider<CommonRequest>.value(value: _diProvider.commonRequest),
        Provider<Tracker>.value(value: _diProvider.tracker),
        Provider<ComponentFactory>.value(
            value: _diProvider.railsComponentFactory),
        Provider<PlatformComponentFactory>.value(
            value: _diProvider.componentFactory),
        Provider<FeedCardContentFactory>.value(
            value: _diProvider.feedCardContentImplFactory),
        Provider<GenericFeedCardItemFactory>.value(
            value: _diProvider.genericFeedCardItemImplFactory),
        Provider<LocalEventManager>.value(
            value: _diProvider._localEventManager),
        Provider<LoginManager>.value(value: _diProvider.loginManager),
        Provider<CommonDateSpecification>.value(
            value: _diProvider.commonDateSpecification),
        Provider<NativeInfoManager>.value(value: _diProvider.nativeInfoManager),
      ],
      child: _child,
    );
  }
}
