import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:common/local_event.dart';
import 'package:common/src/channels/event/native_event_dispatcher.dart';
import 'package:provider/provider.dart';
import 'common_share.dart';

mixin CommonShareMixin<T extends StatefulWidget> on State<T> {
  final List<VoidCallback> _removeReceiverCallbacks = [];

  LocalEventManager _mockLocalEventManager() => null;

  LocalEventManager get _getLocalEventManager =>
      _mockLocalEventManager() ?? LocalEventManager();

  SharedClickedListener _sharedClickedListener;

  @override
  void initState() {
    super.initState();
    _removeReceiverCallbacks.add(_getLocalEventManager
        .addLocalEventReceiver<GlobalGenericNativeEvent>((event) {
      if (event.eventName == "event_share_app_click") {
        /// 分享弹窗里分享app被点击后的回调处理
        /// parameter: trackerEventLabel（app标签）, shareType（分享类型）
        final String trackerEventLabel = event.arguments["trackerEventLabel"];
        final String shareType = event.arguments["shareType"];
        if (_sharedClickedListener != null) {
          _sharedClickedListener(trackerEventLabel, shareType);
          _sharedClickedListener = null;
        }
      } else if (event.eventName == "event_share_dismiss") {
        if (_sharedClickedListener != null) {
          _sharedClickedListener = null;
        }
      }
    }));
  }

  @override
  void dispose() {
    for (final callback in _removeReceiverCallbacks) {
      callback?.call();
    }
    _removeReceiverCallbacks.clear();
    super.dispose();
  }

  /// 打开native的分享面板
  /// [shareContent] 分享内容信息
  /// [panelTitle] 分享面板的标题
  /// [pageName] 页面名称，主要用于埋点
  /// [utmParams] 分享落地链接utm参数，主要用于数据跟踪。点击对应平台时，会将预配置的utm参数拼接在link后面
  void openShare(BuildContext buildContext, ShareContent shareContent,
      {String panelTitle,
      String pageName,
      List<ShareUtmParameter> utmParams,
      SharedClickedListener sharedClickedListener}) {
    _sharedClickedListener = sharedClickedListener;

    final eventDispatcher =
        Provider.of<NativeEventDispatcher>(buildContext, listen: false);
    // 将分享相关信息封装成Map
    final Map<String, String> shareMap = {
      'title': shareContent.title ?? "",
      'sub_title': shareContent.subTitle ?? "",
      'image_url': shareContent.image ?? "",
      'link_url': shareContent.link ?? "",
    };

    if (panelTitle != null) {
      shareMap['panel_title'] = panelTitle;
    }

    if (pageName != null) {
      shareMap['page_name'] = pageName;
    }

    // 分享链接UTM参数
    if (utmParams != null) {
      final utmParamsMap = utmParams.map((e) {
        final Map paramMap = {};
        paramMap['type'] = e.type.value();
        paramMap['params'] = {
          'utm_source': e.source ?? "",
          'utm_medium': e.medium ?? "",
          'utm_campaign': e.campaign ?? "",
        };
        return paramMap;
      }).toList();
      if (utmParamsMap.isNotEmpty) {
        shareMap['utm_params'] = jsonEncode(utmParamsMap);
      }
    }
    // Android使用的分享面板是对话框，不方便使用route，这里统一使用发送事件处理
    eventDispatcher.sendEventByName(SharePanelEventName, arguments: shareMap);
  }
}
