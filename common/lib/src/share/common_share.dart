import 'dart:convert';
import 'package:common/common.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

/// Android打开分享面板的事件名称
const SharePanelEventName = 'event_share';

/// 分享面板控制器，用于打开native的分享面板
/// Demo👇
/// context.openShare(
///    ShareContent('title', 'sub title', 'http://cdn.klook.com/image.jpg', 'http://klook.com/activity/123'),
///    panelTitle: '活动分享',
///    pageName: 'fnb_activity_package_page',
///    utmParams: [ShareUtmParameter(ShareType.wechat, 'wechat', 'social', 'ww_social_ps_fnb-package-page_ao')]
/// );
///
extension CommonShareContext on BuildContext {
  /// 打开native的分享面板
  /// [shareContent] 分享内容信息
  /// [panelTitle] 分享面板的标题
  /// [pageName] 页面名称，主要用于埋点
  /// [utmParams] 分享落地链接utm参数，主要用于数据跟踪。点击对应平台时，会将预配置的utm参数拼接在link后面
  @Deprecated("此方法已标记过期，请使用common_share_mixin中 openShare")
  void openShare(
    ShareContent shareContent, {
    String panelTitle,
    String pageName,
    List<ShareUtmParameter> utmParams,
  }) {
    final eventDispatcher =
        Provider.of<NativeEventDispatcher>(this, listen: false);

    // 将分享相关信息封装成Map
    final Map<String, String> shareMap = {
      'title': shareContent.title ?? "",
      'sub_title': shareContent.subTitle ?? "",
      'image_url': shareContent.image ?? "",
      'link_url': shareContent.link ?? "",
    };

    if (panelTitle != null) {
      shareMap['panel_title'] = panelTitle;
    }

    if (pageName != null) {
      shareMap['page_name'] = pageName;
    }

    // 分享链接UTM参数
    if (utmParams != null) {
      final utmParamsMap = utmParams.map((e) {
        final Map paramMap = {};
        paramMap['type'] = e.type.value();
        paramMap['params'] = {
          'utm_source': e.source ?? "",
          'utm_medium': e.medium ?? "",
          'utm_campaign': e.campaign ?? "",
        };
        return paramMap;
      }).toList();
      if (utmParamsMap.isNotEmpty) {
        shareMap['utm_params'] = jsonEncode(utmParamsMap);
      }
    }
    // Android使用的分享面板是对话框，不方便使用route，这里统一使用发送事件处理
    eventDispatcher.sendEventByName(SharePanelEventName, arguments: shareMap);
  }
}

/// 分享点击Listener
typedef SharedClickedListener = void Function(
    String trackerEventLabel, String shareType);

/// 分享内容
class ShareContent {
  ShareContent(
    this.title,
    this.subTitle,
    this.image,
    this.link,
  );

  final String title;
  final String subTitle;
  final String image;
  final String link;
}

/// 分享链接的utm参数类
class ShareUtmParameter {
  ShareUtmParameter(
    this.type,
    this.source,
    this.medium,
    this.campaign,
  );

  final ShareType type;
  final String source;
  final String medium;
  final String campaign;
}

/// 分享平台类型，
/// Android数目比iOS多，这里是并集
/// 有变动时需要同事更新iOS和Android Native的代码
enum ShareType {
  unknown, // Unknown    ,0,   "unknown"
  facebook, // Facebook   ,1,   "com.facebook.katana"
  twitter, // Twitter    ,3,   "com.twitter.android"
  email, // Email      ,4,   "email"
  message, // Message    ,5,   "sms"
  whatsApp, // WhatsApp   ,6,   "com.whatsapp"
  line, // Line       ,7,   "jp.naver.line.android"
  wechat, // Wechat     ,8,   "com.tencent.mm"
  wechatMoments, // Moments ,9,   "com.tencent.mm_moment"
  sinaWeibo, // SinaWeibo  ,10,  "com.sina.weibo"
  pinterest, // Pinterest  ,11,  "com.pinterest"
  kakao, // Kakao      ,12,  "com.kakao.talk"
  messenger, // Messenger  ,13,  "com.facebook.orca"
  copy, // Copy       ,14,  "copy_to_clipboard"
  others, // Others     ,100, "others"
}

extension ShareTypeExt on ShareType {
  /// 在native代码中iOS的枚举值是数字，Android枚举值是包名，
  /// 为了保持一致的匹配规则，统一使用Android的包名，iOS端需要调整解析方法。
  String value() {
    switch (this) {
      case ShareType.unknown:
        return "unknown";
      case ShareType.facebook:
        return "com.facebook.katana";
      case ShareType.twitter:
        return "com.twitter.android";
      case ShareType.email:
        return "email";
      case ShareType.message:
        return "sms";
      case ShareType.whatsApp:
        return "com.whatsapp";
      case ShareType.line:
        return "jp.naver.line.android";
      case ShareType.wechat:
        return "com.tencent.mm";
      case ShareType.wechatMoments:
        return "com.tencent.mm_moment";
      case ShareType.sinaWeibo:
        return "com.sina.weibo";
      case ShareType.pinterest:
        return "com.pinterest";
      case ShareType.kakao:
        return "com.kakao.talk";
      case ShareType.messenger:
        return "com.facebook.orca";
      case ShareType.copy:
        return "copy_to_clipboard";
      case ShareType.others:
        return "others";
      default:
        assert(false, "Undefined type: $this");
    }
    return "unknown";
  }
}
