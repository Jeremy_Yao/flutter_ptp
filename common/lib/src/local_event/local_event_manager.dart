import 'package:flutter/widgets.dart';
import 'package:common/local_event.dart';
import 'package:rxdart/subjects.dart';

/// [LocalEvent] is the base interface for the local event type.
///
/// It's recommend that implement the subtype of [LocalEvent] for your module, which
/// indicate the event type should be use inside the your module. e.g,:
/// ```dart
/// abstract class PTPDetailEvent implement LocalEvent { }
///
/// class PTPDetailSelectTicketEvent implement PTPDetailEvent { }
///
/// class PTPDetailClickNextEvent implement PTPDetailEvent { }
/// ```
abstract class LocalEvent {}

/// [LocalEventSender] is the base interface of the local event sender, which only
/// define the [sendEvent] function.
abstract class LocalEventSender {
  /// Send event to the [LocalEventReceiver], which type should be subclass of
  /// the [LocalEvent].
  void sendEvent<T extends LocalEvent>(T event);
}

/// A function type that for receive the [LocalEvent].
typedef LocalEventReceiver<T extends LocalEvent> = void Function(T);

/// An implementation of [LocalEventSender], which also implement the [addLocalEventReceiver]
/// for the [LocalEventReceiver].
///
/// All the [LocalEventReceiver] added by using [addLocalEventReceiver] will receive the
/// [LocalEvent] when the [sendEvent] has been invoked.
///
/// **NOTE:**
///
/// Use [LocalEventSenderMixin] to send or receive [LocalEvent] by using [LocalEventReceiverMixin]
/// instead use this class directly. But if you really want to use it, you should get it by using
/// `Provider.of<LocalEventManager>(context, listen: false)`, but not construct
/// it directly.
///
/// See also:
/// * [LocalEventSenderMixin]
/// * [LocalEventReceiverMixin]
class LocalEventManager implements LocalEventSender {
  factory LocalEventManager() {
    return _localEventManager;
  }

  LocalEventManager._();

  /// Use this constructor for testing purpose, that avoid LocalEventManager as singleton
  /// on test.
  @visibleForTesting
  LocalEventManager.forTesting();

  static final LocalEventManager _localEventManager = LocalEventManager._();

  @visibleForTesting
  final PublishSubject eventSubject = PublishSubject();

  @override
  void sendEvent<T extends LocalEvent>(T event) {
    debugPrint('[LocalEventManager] sendEvent: $event');
    eventSubject.sink.add(event);
  }

  VoidCallback addLocalEventReceiver<T extends LocalEvent>(
      LocalEventReceiver<T> localEventReceiver) {
    final subscription =
        eventSubject.stream.whereType<T>().listen(localEventReceiver);
    return () {
      subscription.cancel();
    };
  }
}
