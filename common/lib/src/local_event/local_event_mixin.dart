import 'package:common/src/local_event/local_event_manager.dart';
import 'package:flutter/widgets.dart';

/// [LocalEventReceiverMixin] allow you receive the [LocalEvent] in the [StatefulWidget].
///
/// To receive the [LocalEvent] you should register your [LocalEventReceiver] by using [onEventReceived]
///
/// ```dart
/// abstract class MyEvent implement LocalEvent { }
///
/// class MyEventSelectEvent implement MyEvent { }
///
/// class MyEventClickEvent implement MyEvent { }
///
/// class MyWidget extends StatefulWidget {
///   @override
///   State<StatefulWidget> createState() => MyWidgetState();
/// }
///
/// class MyWidgetState extends State<MyWidget> with LocalEventReceiverMixin {
///
///   @override
///   void initState() {
///     super.initState();
///
///     onEventReceived<MyEventSelectEvent>((event) {
///       // do something...
///     });
///
///     onEventReceived<MyEventClickEvent>((event) {
///       // do something...
///     });
///   }
/// }
/// ```
///
/// See also:
/// * [LocalEventSenderMixin]
mixin LocalEventReceiverMixin<T extends StatefulWidget> on State<T> {
  final List<VoidCallback> _removeReceiverCallbacks = [];

  @Deprecated(
      'This method will be removed in the future, pls use onEventReceived instead!')
  void onEvent(LocalEvent event) {}

  /// Allow set a mock LocalEventManager to the LocalEventReceiverMixin for testing purpose.
  @visibleForTesting
  LocalEventManager mockLocalEventManager() => null;

  LocalEventManager get _getLocalEventManager =>
      mockLocalEventManager() ?? LocalEventManager();

  void onEventReceived<E extends LocalEvent>(
      LocalEventReceiver<E> localEventReceiver) {
    _removeReceiverCallbacks.add(
        _getLocalEventManager.addLocalEventReceiver<E>(localEventReceiver));
  }

  @mustCallSuper
  @override
  void initState() {
    // Compatible with `onEvent`, and will be removed in the future
    // ignore: deprecated_member_use_from_same_package
    onEventReceived(onEvent);
    super.initState();
  }

  @override
  void dispose() {
    for (final callback in _removeReceiverCallbacks) {
      callback?.call();
    }
    _removeReceiverCallbacks.clear();
    super.dispose();
  }
}

/// [LocalEventSenderMixin] allow you send the [LocalEvent] in the [StatefulWidget].
///
/// To send event you can use `localEventSender.sendEvent`. e.g.,
/// ```dart
/// abstract class MyEvent implement LocalEvent { }
///
/// class MyEventClickEvent implement MyEvent { }
///
/// class MyWidget extends StatefulWidget {
///   @override
///   State<StatefulWidget> createState() => MyWidgetState();
/// }
///
/// class MyWidgetState extends State<MyWidget> with LocalEventSenderMixin {
///
///   @override
///   Widget build(BuildContext context) {
///     return GestureDetector(
///       onTap: () {
///         localEventSender.sendEvent(MyEventClickEvent())
///       },
///       child: Text("Click to send event"),
///     );
///   }
/// }
/// ```
///
/// See also:
/// * [LocalEventReceiverMixin]
mixin LocalEventSenderMixin on Widget {
  /// Allow set a mock LocalEventManager to the LocalEventReceiverMixin for testing purpose.
  @visibleForTesting
  LocalEventManager mockLocalEventManager() => null;

  LocalEventManager get _getLocalEventManager =>
      mockLocalEventManager() ?? LocalEventManager();
  LocalEventSender get localEventSender => _getLocalEventManager;
}
