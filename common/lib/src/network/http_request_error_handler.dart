import 'package:common/common.dart';
import 'package:flutter/widgets.dart';

/// [HttpRequestErrorHandler] allow you to handle and re-throw the [Exception]
/// from [CommonRequest]. You need to return `null` if you don't want to re-throw
/// the [Exception].
///
/// See also:
/// * [LoginErrorHandler]
abstract class HttpRequestErrorHandler {
  Exception onError(Exception exception, {Map<dynamic, dynamic> errorData});
}

@immutable
class HttpRequestErrorHandlers implements HttpRequestErrorHandler {
  const HttpRequestErrorHandlers(this._httpRequestErrorHandlers);

  final List<HttpRequestErrorHandler> _httpRequestErrorHandlers;

  @override
  Exception onError(Exception exception, {Map<dynamic, dynamic> errorData}) {
    for (final handler in _httpRequestErrorHandlers) {
      final e = handler.onError(exception, errorData: errorData);
      if (e != null) {
        return e;
      }
    }
    return null;
  }
}
