import 'dart:convert';

import 'package:common/src/network/interceptors/optimus_message_manager.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http_parser/http_parser.dart';
import 'package:uuid/uuid.dart';

const _requestID = 'RequestID';
const _errorRequestFailed = '680100';

class OptimusMonitorInterceptor extends Interceptor {
  OptimusMonitorInterceptor(OptimusMessageManager optimusMessageManager)
      : assert(optimusMessageManager != null),
        _optimusMessageManager = optimusMessageManager;

  final OptimusMessageManager _optimusMessageManager;

  @override
  Future onRequest(RequestOptions options) async {
    String requestId = options.headers[_requestID];
    if (requestId == null) {
      requestId = Uuid().v4().substring(0, 8);
      options.headers[_requestID] = requestId;

      debugPrint(
          "no valid RequestID in request header, generate one $requestId for it");
    }

    try {
      final requestInfo = generateRequestInfo(options);
      _optimusMessageManager.enqueue(requestId, requestInfo);
    } catch (e) {
      debugPrint("error occur when enqueue optimus message: $e");
    }

    return options;
  }

  @override
  Future onResponse(Response response) async {
    // dio会统一将header的key转为小写
    final requestId = response.request.headers[_requestID.toLowerCase()];

    try {
      final resp = generateResponseInfo(response);
      _optimusMessageManager.requestFinished(requestId, resp);
    } catch (e) {
      debugPrint(
          "error occur when handle request finished optimus message in onResponse: $e");
    }

    return response;
  }

  @override
  Future onError(DioError err) async {
    final requestId = err.request.headers[_requestID.toLowerCase()];
    final response = ResponseInfo.error(
      _errorRequestFailed,
      err.message,
      DateTime.now().millisecondsSinceEpoch.toString(),
    );

    try {
      _optimusMessageManager.requestFinished(requestId, response);
    } catch (e) {
      debugPrint(
          "error occur when handle request finished optimus message in onError: $e");
    }

    return err;
  }

  @visibleForTesting
  RequestInfo generateRequestInfo(RequestOptions options) => RequestInfo(
        contentLength: options.headers[Headers.contentLengthHeader] ?? "0",
        path: options.path,
        queryParameters: options.queryParameters?.entries
            ?.map((e) => "${e.key}=${e.value.toString()}")
            ?.join('&'),
        timeStamp: options.headers['X-TimeStamp'] ??
            DateTime.now().millisecondsSinceEpoch.toString(),
      );

  @visibleForTesting
  ResponseInfo generateResponseInfo(Response response) {
    final data = response.data;

    String contentLength = response.headers[Headers.contentLengthHeader]?.first;

    bool isSucceed = true;
    Error error;

    if (data != null) {
      final responseType = response.request.responseType;

      if (responseType == ResponseType.json &&
          _isJsonMime(response.headers[Headers.contentTypeHeader]?.first)) {
        isSucceed = (data['success'] ?? false) as bool;

        if (!isSucceed) {
          final err = data['error'];
          error = err == null
              ? null
              : Error(code: err['code'], message: err['message']);
        }

        contentLength ??= utf8.encode(json.encode(data)).length.toString();
      } else if (responseType == ResponseType.plain) {
        contentLength ??= utf8.encode(data).length.toString();
      } else if (responseType == ResponseType.bytes) {
        contentLength ??= (data as List<int>).length.toString();
      }
    }

    return ResponseInfo.success(
      Result(
        contentLength: contentLength ?? "0",
        statusCode: response.statusCode,
        isSucceed: isSucceed,
        error: error,
      ),
      DateTime.now().millisecondsSinceEpoch.toString(),
    );
  }

  bool _isJsonMime(String contentType) {
    if (contentType == null) return false;
    return MediaType.parse(contentType).mimeType.toLowerCase() ==
        Headers.jsonMimeType.mimeType;
  }
}
