import 'dart:async';
import 'dart:convert';

import 'package:common/src/basic_info/native_info_manager.dart';
import 'package:dio/dio.dart';

// 添加请求的签名
class SignatureInterceptor extends Interceptor {
  SignatureInterceptor(this._signatureInfoProvider);

  final SignatureInfoProvider _signatureInfoProvider;

  @override
  Future onRequest(RequestOptions options) async {
    final String raw = _getRawString(options);
    final String result = await _signatureInfoProvider.provideSignature(raw);
    options.headers['X-Signature'] = result;
    return options;
  }

  String _getRawString(RequestOptions options) {
    final Map headers = options.headers ?? {};
    final String timeStamp = headers['X-TimeStamp'];
    final String platform = headers['X-Platform'];
    final String deviceId = headers['X-DeviceID'];
    final String pt = headers['_pt'];
    final List<int> data = [];
    String url = options.path;
    if (options.queryParameters != null) {
      final String query = Transformer.urlEncodeMap(options.queryParameters);
      if (query.isNotEmpty) {
        url += (url.contains("?") ? "&" : "?") + query;
      }
    }
    String raw = '$timeStamp$platform$deviceId$pt$url';
    data.addAll(utf8.encode(raw));
    if (options.method == "POST" && options.data != null) {
      List<int> bodyData = [];
      if (options.contentType == Headers.formUrlEncodedContentType) {
        final String form = Transformer.urlEncodeMap(options.data);
        bodyData = utf8.encode(form);
      } else if (options.contentType == Headers.jsonContentType) {
        bodyData = utf8.encode(json.encode(options.data));
      }
      if (bodyData.length >= 1024) {
        bodyData = bodyData.sublist(0, 1024);
      }
      data.addAll(bodyData);
    }

    raw = base64.encode(data);
    return raw;
  }
}
