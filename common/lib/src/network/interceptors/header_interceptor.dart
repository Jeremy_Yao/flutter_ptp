import 'dart:async';

import 'package:common/src/basic_info/model/app_info.dart';
import 'package:common/src/basic_info/model/develop_setting.dart';
import 'package:common/src/basic_info/native_info_manager.dart';
import 'package:common/src/network/interceptors/common_request_headers.dart';
import 'package:dio/dio.dart';

// 添加请求的header
class HeaderInterceptor extends Interceptor {
  HeaderInterceptor(this._userInfoProvider, this._deviceInfoProvider,
      this._appInfoProvider, this._affiliateFieldsInfoProvider);

  final UserInfoProvider _userInfoProvider;
  final AppInfoProvider _appInfoProvider;
  final DeviceInfoProvider _deviceInfoProvider;
  final AffiliateFieldsProvider _affiliateFieldsInfoProvider;

  @override
  Future onRequest(RequestOptions options) async {
    final String timeStamp = DateTime.now().millisecondsSinceEpoch.toString();

    final commonHeaders = await createCommonHeaders(
      _userInfoProvider,
      _appInfoProvider,
      _deviceInfoProvider,
      _affiliateFieldsInfoProvider,
      timeStamp: timeStamp,
    );

    options.queryParameters = options.queryParameters ?? {};
    final AppInfo appInfo = await _appInfoProvider.provideAppInfo();
    final DevelopSetting developSetting = appInfo.developSetting;
    if (developSetting.isPreviewMode ?? false) {
      options.queryParameters['admin_preview_mode'] = 1;
    }
    options.queryParameters['_t'] = timeStamp;

    final Map headers = options.headers ?? {};
    headers.addAll(commonHeaders);

    options.headers = headers;
    return options;
  }
}
