import 'package:dio/dio.dart';

///
/// Charles 打断点修改响应后会添加 Content-Length 和 Transfer-Encoding，
/// 根据 [RFC 2616](https://greenbytes.de/tech/webdav/rfc2616.html#rfc.section.4.4), 这是不合法的
/// Flutter底层http实现中遇到这种情况会把statusCode设为400，Dio会抛出错误
///
class Charles400Interceptor extends Interceptor {
  Charles400Interceptor();

  @override
  Future onError(DioError err) {
    if (err.type == DioErrorType.RESPONSE) {
      final response = err.response;
      // header中的Content-Length已经被移除
      // 所以无法判断 Content-Length 和 Transfer-Encoding 是否同时存在
      if (response.statusCode == 400 &&
          response.data is Map &&
          response.data["success"] == true) {
        response.statusCode = 200;
        return Future.value(response);
      }
    }
    return Future.value(err);
  }
}
