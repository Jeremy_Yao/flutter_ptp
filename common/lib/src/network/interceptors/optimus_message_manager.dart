import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OptimusMonitorChannel {
  OptimusMonitorChannel();

  final MethodChannel _chanel =
      const MethodChannel('com.klook.platform/optimusMonitor');

  void sendMonitorMessage(
      String requestId, RequestInfo request, ResponseInfo response) {
    assert(request != null && response != null);

    Map<String, String> mapError(Error error) =>
        error == null ? null : {'code': error.code, 'msg': error.message};

    _chanel.invokeMethod('uploadMessage', {
      'request': {
        'id': requestId,
        'content_length': request.contentLength,
        'path': request.path,
        'query_parameters': request.queryParameters,
        'time_stamp': request.timeStamp,
      },
      'response': {
        'result': response.result == null
            ? null
            : {
                'content_length': response.result.contentLength,
                'status_code': response.result.statusCode,
                'is_succeed': response.result.isSucceed,
                'error': mapError(response.result.error),
              },
        'error': mapError(response.error),
        'time_stamp': response.timeStamp,
      },
    });
  }
}

class OptimusMessageManager {
  OptimusMessageManager({
    OptimusMonitorChannel channel,
    Map<String, RequestInfo> pendingRequests,
  })  : _channel = channel ?? OptimusMonitorChannel(),
        pendingRequests = pendingRequests ?? {};

  @visibleForTesting
  final Map<String, RequestInfo> pendingRequests;
  final OptimusMonitorChannel _channel;

  void enqueue(String requestId, RequestInfo request) {
    assert(pendingRequests[requestId] == null);

    pendingRequests[requestId] = request;
  }

  void requestFinished(String requestId, ResponseInfo response) {
    final request = pendingRequests[requestId];

    //TODO(RUIJIN): channel 通信暂时注释
    debugPrint("request: $request");
    debugPrint("response: $response");

    //assert(request != null);
    //_channel.sendMonitorMessage(requestId, request, response);
    pendingRequests.removeWhere((key, value) => key == requestId);
  }
}

class RequestInfo {
  const RequestInfo({
    this.contentLength,
    this.path,
    this.queryParameters,
    this.timeStamp,
  });

  final String contentLength;
  final String path;
  final String queryParameters;
  final String timeStamp;
}

class ResponseInfo {
  const ResponseInfo._({
    this.result,
    this.error,
    this.timeStamp,
  }) : assert(result != null || error != null);

  factory ResponseInfo.success(
    Result result,
    String timeStamp,
  ) =>
      ResponseInfo._(
        result: result,
        timeStamp: timeStamp,
      );

  factory ResponseInfo.error(
    String code,
    String message,
    String timeStamp,
  ) =>
      ResponseInfo._(
        error: Error(code: code, message: message),
        timeStamp: timeStamp,
      );

  final Result result;
  final Error error;
  final String timeStamp;
}

class Result {
  const Result({
    this.contentLength,
    this.statusCode,
    this.isSucceed,
    this.error,
  });

  final String contentLength;
  final int statusCode;
  final bool isSucceed;
  final Error error;
}

class Error {
  const Error({this.code, this.message});

  final String code;
  final String message;
}
