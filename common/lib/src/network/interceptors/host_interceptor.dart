import 'dart:async';

import 'package:common/src/basic_info/native_info_manager.dart';
import 'package:dio/dio.dart';

// 添加请求的host(baseUrl)
class HostInterceptor extends Interceptor {
  HostInterceptor(this._appInfoProvider);

  final AppInfoProvider _appInfoProvider;

  @override
  Future onRequest(RequestOptions options) async {
    final appInfo = await _appInfoProvider.provideAppInfo();
    options.baseUrl = appInfo.apiHost;
    return options;
  }
}
