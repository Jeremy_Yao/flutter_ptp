import 'dart:convert';

import 'package:common/src/basic_info/model/app_info.dart';
import 'package:common/src/basic_info/model/device_info.dart';
import 'package:common/src/basic_info/model/user_info.dart';
import 'package:common/src/basic_info/native_info_manager.dart';
import 'package:uuid/uuid.dart';

// TODO(littlegnal): Add unit test
/// Create common request headers.
Future<Map<String, dynamic>> createCommonHeaders(
    UserInfoProvider userInfoProvider,
    AppInfoProvider appInfoProvider,
    DeviceInfoProvider deviceInfoProvider,
    AffiliateFieldsProvider affiliateFieldsInfoProvider,
    {String timeStamp}) async {
  final UserInfo userInfo = await userInfoProvider.provideUserInfo();
  final AppInfo appInfo = await appInfoProvider.provideAppInfo();
  final DeviceInfo deviceInfo = await deviceInfoProvider.provideDeviceInfo();
  final Map<String, dynamic> affiliateFields =
      await affiliateFieldsInfoProvider.provideAffiliateFields();

  final String tempTimeStamp =
      timeStamp ?? DateTime.now().millisecondsSinceEpoch.toString();

  final Map<String, dynamic> headers = {};
  headers['X-TimeStamp'] = tempTimeStamp;
  headers['version'] = appInfo.apiVersion;
  headers['Klook-App-Version'] = appInfo.version;
  headers['Access-Type'] = "3";
  headers['Accept-Language'] = appInfo.language;
  headers['Currency'] = appInfo.currency.name;
  headers['X-Klook-Host'] = appInfo.klookHost;
  headers['X-Klook-Tint'] = appInfo.klookTint;

  headers['_pt'] = deviceInfo.advertisingId;
  headers['X-DeviceID'] = deviceInfo.deviceId;
  headers['X-Klook-Kepler-Id'] = deviceInfo.deviceId;
  headers['X-Platform'] = deviceInfo.platform;
  headers['Gateway'] = deviceInfo.platform.toLowerCase();
  headers['User-Agent'] =
      'klook/${appInfo.version} (${deviceInfo.deviceName}; ${deviceInfo.platform} ${deviceInfo.systemVersion}; Scale/${deviceInfo.scale.toStringAsFixed(2)})';
  if (appInfo.xCountryCode?.isNotEmpty ?? false) {
    headers['X-Country-Code'] = appInfo.xCountryCode;
  }
  var widString = '';
  if (affiliateFields['wid'] is String) {
    widString = affiliateFields['wid'];
  }
  headers['X-Klook-Affiliate-Aid'] = widString;
  final aidExtraString = affiliateFields['aid_extra'];
  var pidString = '';
  if (aidExtraString is String) {
    try {
      final Map<String, dynamic> aidExtra = json.decode(aidExtraString);
      if (aidExtra['aff_pid'] is String) {
        pidString = aidExtra['aff_pid'];
      }
    } catch (e) {
      assert(false, 'aid extra string is not a valid json string');
    }
  }
  headers['X-Klook-Affiliate-Pid'] = pidString;

  headers['token'] = userInfo.token ?? "";

  headers['RequestID'] = Uuid().v4().substring(0, 8);

  return headers;
}
