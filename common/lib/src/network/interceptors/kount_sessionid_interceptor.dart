import 'dart:async';
import 'package:common/common.dart';
import 'package:dio/dio.dart';

// 获取cookie sessiond id
class KountSessionIdInterceptor extends Interceptor {
  KountSessionIdInterceptor(this._infoManager);

  final NativeInfoManager _infoManager;

  @override
  Future onResponse(Response response) async {
    final List<String> cookie = response.headers.map['set-cookie'];
    if (cookie == null) return response;
    final result = cookie
        .expand((e) => e.split(','))
        .where((e) => e.contains('=') && !e.endsWith('='))
        .map((e) {
          final keyValue = e.split('=');
          return {keyValue.first: keyValue.last ?? ''};
        })
        .where((e) => e.containsKey('JSESSIONID'))
        .toList();
    if (result.isEmpty) return response;
    final sessionId = result.first['JSESSIONID'];
    if (sessionId == null) return response;
    _infoManager.syncJSessionId(sessionId);
    return response;
  }
}
