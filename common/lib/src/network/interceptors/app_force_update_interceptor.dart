import 'dart:convert';

import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:common/widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _forceUpdateKey = 'force_update';
const _recommendUpdateKey = 'recommend_update';
@visibleForTesting
const skipRecommandUpdateTimestampPreferencesKey =
    'skip_recommand_update_timestamp_preferences_key';

/// Handle app upgrade logic from [Response]
///
/// See more detail: [https://klook.slab.com/posts/app%E5%8D%87%E7%BA%A7%E6%8E%A7%E5%88%B6-90idns58](https://klook.slab.com/posts/app%E5%8D%87%E7%BA%A7%E6%8E%A7%E5%88%B6-90idns58)
class AppForceUpdateInterceptor extends Interceptor {
  AppForceUpdateInterceptor(
      this._commonNavigator, this._nativeEventChannel, this._appLocalizations);

  final CommonNavigator _commonNavigator;

  final NativeEventChannel _nativeEventChannel;

  final AppLocalizations _appLocalizations;

  BuildContext get _navigatorContext =>
      _commonNavigator.getFlutterNavigatorState().overlay.context;

  // Use for mocking in unit test.
  @visibleForTesting
  ValueGetter<DateTime> now = () => DateTime.now();

  @override
  Future onResponse(Response response) async {
    final headers = response.headers;
    final isForceUpdate = headers?.value(_forceUpdateKey) == 'true';
    final isRecommendUpdate = headers?.value(_recommendUpdateKey) == 'true';
    if (isForceUpdate) {
      await _showForceUpdateDialog();
    } else if (isRecommendUpdate) {
      await _showRecommandUpdateDialog(
          (response?.request?.baseUrl ?? '') + (response?.request?.path ?? ''));
    }

    return super.onResponse(response);
  }

  Future _showForceUpdateDialog() {
    return showHorizontalCommonDialog(_navigatorContext,
        title: _appLocalizations.text('time_to_upgrade'),
        content: _appLocalizations.text('upgrade_latest_version_to_proceed'),
        outsideDismiss: false,
        autoDismissDialog: false,
        actions: [
          CommonDialogAction(
              title: _appLocalizations.text('app_upgrade_later'),
              onTap: () {
                // Pop the dialog
                _commonNavigator.pop();
                // Pop the topmost route
                _commonNavigator.pop();
              }),
          CommonDialogAction(
              title: _appLocalizations.text('app_upgrade_update_now'),
              onTap: () {
                // Notify native to handle the upgrade logic
                _notifyNativeAppUpdate();
              }),
        ]);
  }

  Future<void> _showRecommandUpdateDialog(String key) async {
    if (key.isEmpty) return;

    if (!await _shouldShowRecommandUpdateDialog(key)) return;

    return showHorizontalCommonDialog(_navigatorContext,
        title: _appLocalizations.text('time_to_upgrade'),
        content: _appLocalizations.text('update_to_keep_exploring'),
        outsideDismiss: true,
        autoDismissDialog: false,
        actions: [
          CommonDialogAction(
              title: _appLocalizations.text('app_upgrade_later'),
              onTap: () async {
                // Pop the dialog
                _commonNavigator.pop();
                // Save click timestamp
                _saveSkipRecommandUpdateTimestamp(key);
              }),
          CommonDialogAction(
              title: _appLocalizations.text('app_upgrade_update_now'),
              onTap: () {
                // Notify native to handle the upgrade logic
                _notifyNativeAppUpdate();
              }),
        ]);
  }

  Future<void> _saveSkipRecommandUpdateTimestamp(String key) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    if (!sharedPreferences
        .containsKey(skipRecommandUpdateTimestampPreferencesKey)) {
      sharedPreferences.setString(skipRecommandUpdateTimestampPreferencesKey,
          jsonEncode({key: now().millisecondsSinceEpoch}));
      return;
    }
    final urlSkipTimestamps =
        sharedPreferences.getString(skipRecommandUpdateTimestampPreferencesKey);
    final urlSkipTimestampsMap = Map.from(jsonDecode(urlSkipTimestamps));

    if (!urlSkipTimestampsMap.containsKey(key) ||
        await _shouldShowRecommandUpdateDialog(key)) {
      urlSkipTimestampsMap[key] = now().millisecondsSinceEpoch;
      sharedPreferences.setString(skipRecommandUpdateTimestampPreferencesKey,
          jsonEncode(urlSkipTimestampsMap));
    }
  }

  Future<bool> _shouldShowRecommandUpdateDialog(String path) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    if (!sharedPreferences
        .containsKey(skipRecommandUpdateTimestampPreferencesKey)) {
      return true;
    }

    final urlSkipTimestamps =
        sharedPreferences.getString(skipRecommandUpdateTimestampPreferencesKey);
    final urlSkipTimestampsMap = Map.from(jsonDecode(urlSkipTimestamps));

    if (!urlSkipTimestampsMap.containsKey(path)) {
      return true;
    }

    final preSkipTimeStamp =
        DateTime.fromMillisecondsSinceEpoch(urlSkipTimestampsMap[path]);

    final differenceDays = now().difference(preSkipTimeStamp).inDays;
    return differenceDays > 7;
  }

  Future<void> _notifyNativeAppUpdate() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
    _nativeEventChannel.sendEvent('event_update_app');
  }
}
