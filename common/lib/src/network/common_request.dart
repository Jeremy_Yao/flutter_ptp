import 'dart:collection';

import 'package:common/src/network/cancellable_request.dart';
import 'package:common/src/network/common_request_exception.dart';
import 'package:common/src/network/http.dart';
import 'package:common/src/network/http_request_error_handler.dart';
import 'package:common/src/network/serialization.dart';
import 'package:dio/dio.dart';

import 'abstract_request.dart';
import 'enums.dart';

class CommonRequest extends AbstractRequest {
  CommonRequest(HTTP http, this._interceptorList, this._serialization,
      {HttpRequestErrorHandler httpRequestErrorHandler})
      : _httpRequestErrorHandler = httpRequestErrorHandler,
        super(http);

  final List<Interceptor> _interceptorList;
  final Serialization _serialization;
  final Map<String, CancelToken> _cancelTokenMap = HashMap();
  final HttpRequestErrorHandler _httpRequestErrorHandler;

  @override
  Interceptors interceptors() {
    final interceptors = Interceptors();
    interceptors.addAll(_interceptorList);
    return interceptors;
  }

  void cancelRequest(dynamic cancelFor, [String reason]) {
    if (cancelFor != null) {
      final key = cancelFor.toString();
      _cancelTokenMap[cancelFor]?.cancel("Cancel $key: ${reason ?? ""}");
      _cancelTokenMap.remove(key);
    }
  }

  // TODO(littlegnal): Remove this in the future
  CancelToken handleCancelToken(String cancelFor) {
    CancelToken cancelToken;
    if (cancelFor != null) {
      final key = cancelFor.toString();
      if (_cancelTokenMap.containsKey(key)) {
        cancelToken = _cancelTokenMap[key];
        cancelToken.cancel();
      }
      cancelToken = CancelToken();
      _cancelTokenMap[key] = cancelToken;
    }
    return cancelToken;
  }

  CancellableRequest<T> get<T>(String path,
      [Object queryParameters, Options extraOptions]) {
    return CancellableRequestImpl.fromRequest(
        commonRequest<T>(RequestMethod.GET, path,
            queryParameters: queryParameters, extraOptions: extraOptions),
        path: path);
  }

  @Deprecated("Use `get` instead. Will be removed in the future")
  Future<T> commonGet<T>(String path, [Object queryParameters]) {
    return commonRequest(RequestMethod.GET, path,
        queryParameters: queryParameters);
  }

  CancellableRequest<T> post<T>(String path,
      {dynamic data,
      SerializerType requestSerializerType = SerializerType.Form,
      SerializerType responseSerializerType = SerializerType.JSON,
      Options extraOptions}) {
    return CancellableRequestImpl.fromRequest(
      commonRequest(RequestMethod.POST, path,
          data: data,
          requestSerializerType: requestSerializerType,
          responseSerializerType: responseSerializerType,
          extraOptions: extraOptions),
      path: path,
    );
  }

  @Deprecated("Use `post` instead. Will be removed in the future")
  Future<T> commonPost<T>(
    String path, {
    dynamic data,
    SerializerType requestSerializerType = SerializerType.Form,
    SerializerType responseSerializerType = SerializerType.JSON,
  }) {
    return commonRequest(
      RequestMethod.POST,
      path,
      data: data,
      requestSerializerType: requestSerializerType,
      responseSerializerType: responseSerializerType,
    );
  }

  Future<T> commonRequest<T>(RequestMethod method, String path,
      {Object queryParameters,
      dynamic data,
      SerializerType requestSerializerType = SerializerType.Form,
      SerializerType responseSerializerType = SerializerType.JSON,
      dynamic cancelFor,
      Options extraOptions}) {
    assert(method != null);
    assert(path != null);

    // TODO(littlegnal): Remove this in the future
    final CancelToken cancelToken = handleCancelToken(cancelFor);

    return request(method, path, requestSerializerType, queryParameters, data,
            responseSerializerType, cancelToken,
            interceptors: interceptors(), extraOptions: extraOptions)
        .then((r) {
      // TODO(littlegnal): Will replace this implementation to `deserialize<BaseResponse<T>>(jsonDecode(r.data))` when this issue be solved  https://github.com/google/built_value.dart/issues/368
      // The result should be deserialize by using `deserialize<BaseResponse<T>>(jsonDecode(r.data))`,
      // but the `built_value` lack of support for the generic type, so we decode the json
      // to the `Map<String, dynamic>` directly at this time.
      final m = r.data;
      final isSuccess = m["success"] as bool;
      if (!isSuccess) {
        final error = m["error"];
        final exception = BusinessLogicError(
            error["code"] as String, error["message"] as String);
        if (_httpRequestErrorHandler != null) {
          final handledException =
              _httpRequestErrorHandler.onError(exception, errorData: error);
          if (handledException != null) {
            throw handledException;
          }
        }
        throw exception;
      }

      final result = m["result"];
      return _serialization.deserialize<T>(result);
    }).catchError((e) {
      final dioError = e as DioError;
      if (dioError.type == DioErrorType.CONNECT_TIMEOUT ||
          dioError.type == DioErrorType.RECEIVE_TIMEOUT ||
          dioError.type == DioErrorType.SEND_TIMEOUT) {
        throw TimeoutException(dioError.message);
      } else {
        final exception = HttpError(e.message, e);
        if (_httpRequestErrorHandler != null) {
          final handledException = _httpRequestErrorHandler.onError(exception);
          if (handledException != null) {
            throw handledException;
          }
        }
        throw exception;
      }
    }, test: (e) => e is DioError);
  }
}
