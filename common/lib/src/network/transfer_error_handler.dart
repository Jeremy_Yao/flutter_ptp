import 'package:common/common.dart';
import 'package:common/src/network/http_request_error_handler.dart';
import 'package:flutter/widgets.dart';

class TransferBusinessLogicException extends BusinessLogicError {
  TransferBusinessLogicException(String code, String message, this.type,
      this.hiddenCode, this.hiddenMessage)
      : super(code, message);

  final String type;
  final String hiddenCode;
  final String hiddenMessage;

  @override
  bool operator ==(other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is TransferBusinessLogicException &&
        other.code == code &&
        other.message == message &&
        other.type == type &&
        other.hiddenCode == hiddenCode &&
        other.hiddenMessage == hiddenMessage;
  }

  @override
  int get hashCode =>
      hashValues(code, message, type, hiddenCode, hiddenMessage);

  @override
  String toString() {
    return "[TransferBusinessLogicException] message:$message, code: $code, type: $type, hiddenCode: $hiddenCode, hiddenMessage: $hiddenMessage";
  }
}

// TODO(littlegnal): The TransferErrorHandler should move out the common package and move
//  to the individual business module(car_rental) in the future.
class TransferErrorHandler implements HttpRequestErrorHandler {
  @override
  Exception onError(Exception exception, {Map<dynamic, dynamic> errorData}) {
    if (errorData != null &&
        errorData.containsKey("type") &&
        errorData.containsKey("hiddenCode") &&
        errorData.containsKey("hiddenMessage")) {
      return TransferBusinessLogicException(
          errorData["code"],
          errorData["message"],
          errorData["type"].toString(),
          errorData["hiddenCode"],
          errorData["hiddenMessage"]);
    }
    return null;
  }
}
