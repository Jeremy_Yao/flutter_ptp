import 'dart:io';

import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:common/src/network/http_request_error_handler.dart';

class LoginErrorHandler implements HttpRequestErrorHandler {
  LoginErrorHandler(this._commonNavigator);

  static const unLogin = "4001";
  static const tokenExpired = "4004";

  final CommonNavigator _commonNavigator;

  @override
  Exception onError(Exception exception, {Map<dynamic, dynamic> errorData}) {
    if (exception is BusinessLogicError) {
      if (exception.code == unLogin || exception.code == tokenExpired) {
        // TODO(littlegnal): Get `navigationService` from constructor after `_NavigationService` public,
        //  it's not testable at this time
        if (!Platform.environment.containsKey('FLUTTER_TEST')) {
          _commonNavigator.pushNamedNative("klook://login",
              arguments: {"is_token_expired": true});
        }

        return UnauthorizedException(exception.code, exception.message);
      }
    }

    return null;
  }
}
