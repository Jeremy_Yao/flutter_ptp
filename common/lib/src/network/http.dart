import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'enums.dart';

String methodName(RequestMethod method) {
  switch (method) {
    case RequestMethod.GET:
      return "GET";
    case RequestMethod.POST:
      return "POST";
    default:
      return "GET";
  }
}

class HTTP {
  HTTP({BaseOptions baseOptions}) {
    // The default timeout configuration that same as natvie.
    _dio = Dio(baseOptions ??
        BaseOptions(
          connectTimeout: 60000,
          // TODO(littlegnal): It seems the dio don't handle the receiveTimeout, so there's no unit test for this timeout.
          receiveTimeout: 30000,
          sendTimeout: 30000,
        ));

    _dio.interceptors.add(LogInterceptor(
        responseBody: kDebugMode,
        requestBody: kDebugMode,
        logPrint: (log) {
          debugPrint(log);
        }));
  }
  // TODO(littlegnal): Fix this later
  // ignore: prefer_typing_uninitialized_variables
  var _dio;
  static final _instance = HTTP();

  static HTTP get instance => _instance;

  Future<Response<T>> request<T>(
      RequestMethod method,
      String path,
      SerializerType requestSerializerType,
      Object queryParameters,
      dynamic data,
      SerializerType responseSerializerType,
      CancelToken cancelToken,
      Interceptors interceptors,
      {Options extraOptions}) async {
    final Options options = Options().merge(
        method: extraOptions?.method,
        sendTimeout: extraOptions?.sendTimeout,
        receiveTimeout: extraOptions?.receiveTimeout,
        extra: extraOptions?.extra,
        headers: extraOptions?.headers,
        // responseType: extraOptions?.responseType,
        // contentType: extraOptions?.contentType,
        validateStatus: extraOptions?.validateStatus,
        receiveDataWhenStatusError: extraOptions?.receiveDataWhenStatusError,
        followRedirects: extraOptions?.followRedirects,
        maxRedirects: extraOptions?.maxRedirects,
        requestEncoder: extraOptions?.requestEncoder,
        responseDecoder: extraOptions?.responseDecoder);
    options.method = methodName(method);
    options.responseType = ResponseType.json;
    if (method == RequestMethod.POST) {
      switch (requestSerializerType) {
        case SerializerType.Form:
          options.contentType = Headers.formUrlEncodedContentType;
          break;
        case SerializerType.JSON:
          options.contentType = Headers.jsonContentType;
          break;
        default:
      }
    }

    final RequestOptions requestOptions =
        _dio.mergeOptions(options, path, data, queryParameters);
    requestOptions.cancelToken = cancelToken;

    // Convert the request/response interceptor to a functional callback in which
    // we can handle the return value of interceptor callback.
    // ignore: always_declare_return_types
    _interceptorWrapper(interceptor, bool request) {
      return (data) async {
        final bool type =
            request ? (data is RequestOptions) : (data is Response);
        if (_isErrorOrException(data) || type) {
          if (type) {
            if (!request) {
              data.request = data.request ?? requestOptions;
            }
            return interceptor(data).then((e) => e ?? data);
          } else {
            throw assureDioError(data, requestOptions);
          }
        } else {
          return assureResponse(data, requestOptions);
        }
      };
    }

    // Convert the error interceptor to a functional callback in which
    // we can handle the return value of interceptor callback.
    // ignore: always_declare_return_types
    _errorInterceptorWrapper(errInterceptor) {
      return (err) async {
        if (err is! Response) {
          final _e = await errInterceptor(assureDioError(err, requestOptions));
          if (_e is! Response) {
            throw assureDioError(_e ?? err, requestOptions);
          }
          err = _e;
        }
        // err is a Response instance
        return err;
      };
    }

    Future<Response<T>> _dispatchRequest(RequestOptions options) {
      return _dio.request(options.path, options: options);
    }

    // Build a request flow in which the processors(interceptors)
    // execute in FIFO order.

    // Start the request flow
    Future future = Future.value(requestOptions);

    // Add request interceptors to request flow
    interceptors.forEach((Interceptor interceptor) {
      future = future.then(_interceptorWrapper(interceptor.onRequest, true));
    });

    // Add dispatching callback to request flow
    future = future.then(_interceptorWrapper(_dispatchRequest, true));

    // Add response interceptors to request flow
    interceptors.forEach((Interceptor interceptor) {
      future = future.then(_interceptorWrapper(interceptor.onResponse, false));
    });

    // // Add error handlers to request flow
    interceptors.forEach((Interceptor interceptor) {
      future = future.catchError(_errorInterceptorWrapper(interceptor.onError));
    });

    return future.then<Response<T>>((data) {
      return assureResponse<T>(data);
    }).catchError((err) {
      if (err == null || _isErrorOrException(err)) {
        throw assureDioError(err, requestOptions);
      }
      return assureResponse<T>(err, requestOptions);
    });
  }

  static Response<T> assureResponse<T>(response,
      [RequestOptions requestOptions]) {
    if (response is Response<T>) {
      response.request = response.request ?? requestOptions;
    } else if (response is! Response) {
      // ignore: parameter_assignments
      response = Response<T>(data: response, request: requestOptions);
    } else {
      final T data = response.data;
      // ignore: parameter_assignments
      response = Response<T>(
        data: data,
        headers: response.headers,
        request: response.request,
        statusCode: response.statusCode,
        isRedirect: response.isRedirect,
        redirects: response.redirects,
        statusMessage: response.statusMessage,
      );
    }
    return response;
  }

  static bool _isErrorOrException(t) => t is Exception || t is Error;
  static DioError assureDioError(err, [RequestOptions requestOptions]) {
    DioError dioError;
    if (err is DioError) {
      dioError = err;
    } else if (_isErrorOrException(err)) {
      // 抛出非网络错误
      throw err;
    } else {
      dioError = DioError(error: err);
    }
    dioError.request = dioError.request ?? requestOptions;
    return dioError;
  }

  // interceptor
  static Future<Response<T>> resolve<T>(response) {
    if (response is! Future) {
      // ignore: parameter_assignments
      response = Future.value(response);
    }
    return response.then<Response<T>>((data) {
      return HTTP.assureResponse<T>(data);
    }, onError: (err) {
      // transform "error" to "success"
      return HTTP.assureResponse<T>(err);
    });
  }

  /// Assure the final future state is failed!
  static Future<Response<T>> reject<T>(err) {
    if (err is! Future) {
      // ignore: parameter_assignments
      err = Future.error(err);
    }
    return err.then<Response<T>>((v) {
      // transform "success" to "error"
      throw HTTP.assureDioError(v);
    }, onError: (e) {
      throw HTTP.assureDioError(e);
    });
  }
}
