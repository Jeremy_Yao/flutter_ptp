extension DateTimeExt on DateTime {
  ///
  /// Returns a new `DateTime` representing the start of the day (00:00:00).
  ///
  DateTime get beginningOfDay => DateTime(year, month, day);

  bool isToday() {
    final DateTime now = DateTime.now();
    return day == now.day && month == now.month && year == now.year;
  }

  static int getDaysInMonth(int year, int month) {
    return month < DateTime.monthsPerYear
        ? DateTime(year, month + 1, 0).day
        : DateTime(year + 1, 1, 0).day;
  }

  bool isSameDay(DateTime dateTime) {
    if (dateTime == null) return false;
    return dateTime.year == year &&
        dateTime.month == month &&
        dateTime.day == day;
  }
}
