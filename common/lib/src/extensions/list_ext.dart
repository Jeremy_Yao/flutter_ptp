extension ListExt<T> on List<T> {
  /// Replace the old element in the index which first matched by [where] to the [newElement]
  void replaceFirst(bool Function(T) where, T Function(T) newElement) {
    final preIndex = indexWhere(where);
    if (preIndex != -1) {
      final preElement = this[preIndex];
      this[preIndex] = newElement(preElement);
    }
  }
}
