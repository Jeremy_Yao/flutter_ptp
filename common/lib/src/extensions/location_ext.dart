import 'dart:math';

import 'package:common/basic_info.dart';

extension LocationExt on Location {
  /// 根据两点经纬度算出之间距离
  /// 单位：米
  double distance(Location location) {
    if (location == null ||
        location.latitude == null ||
        location.longitude == null ||
        latitude == null ||
        longitude == null) {
      return 0;
    }
    final double def = 6378137.0; // def ：地球半径
    final double radLat1 = _rad(latitude);
    final double radLat2 = _rad(location.latitude);
    final double a = radLat1 - radLat2;
    final double b = _rad(longitude) - _rad(location.longitude);
    final double s = 2 *
        asin(sqrt(pow(sin(a / 2), 2) +
            cos(radLat1) * cos(radLat2) * pow(sin(b / 2), 2)));
    return (s * def).roundToDouble();
  }

  double _rad(String d) {
    return double.tryParse(d) * pi / 180.0;
  }
}
