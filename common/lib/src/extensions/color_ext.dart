import 'dart:ui';

extension StringToColorExt on String {
  /// Construct a color from a hex code string, of the format #RRGGBB. If the length
  /// of the hex code string is less than 8, it will insert `FF` on the beginning.
  /// e.g., `"#FFB6C1".hexToColor() == const Color(0xFFFFB6C1)`
  Color hexToColor() {
    if (startsWith("#")) {
      final hexString = replaceFirst('#', '');
      if (hexString.length >= 6) {
        final buffer = StringBuffer();
        if (hexString.length <= 8) {
          buffer.write('ff');
        }
        buffer.write(hexString);
        return Color(int.parse(buffer.toString(), radix: 16));
      }
    }
    return null;
  }
}
