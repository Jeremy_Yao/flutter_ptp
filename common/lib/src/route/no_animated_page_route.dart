import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 这个是因为为了让原生跳转进入flutter页面,flutter内部没有动画效果
/// 因此新增的一个PageRouteBuilder,把内部动画效果全部取消,因此视图效果就是瞬间从原生切换到flutter
class NoAnimatedPageRoute<T> extends PageRouteBuilder {
  NoAnimatedPageRoute({@required this.widget, @required this.settings})
      : super(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return widget;
          },
        );
  @override
  final RouteSettings settings;

  final Widget widget;
}
