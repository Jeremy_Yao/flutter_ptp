import 'dart:io';

import 'package:common/src/bloc/base_bloc.dart';
import 'package:common/src/channels/route_channel.dart';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

/// A global navigator that allow you do navigation without [BuildContext],
/// but there's some limitation, you can only access it from [StatelessWidget],
/// [StatefulWidget], [State], [BaseBloc]
///
/// see also:
/// [NavigationServiceMyAppExtension]
/// [NavigationServiceStatelessWidgetExtension]
/// [NavigationServiceStatefulWidgetExtension]
/// [NavigationServiceStateExtension]
/// [NavigationServiceBlocExtension]
// TODO(littlegnal): Make _NavigationService public and get it by using Provider.of()
@Deprecated(
    "This class will no longer work correctly, and will be remove in the futre, use CommonNavigator instead")
class _NavigationService {
  factory _NavigationService() {
    return _navigationService;
  }
  _NavigationService._() {
    _navigatorObservers = <NavigatorObserver>[_navigatorObserverDelegate];
  }

  static final _NavigationService _navigationService = _NavigationService._();

  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

  final RouteChannel _nativeRouteChannel = RouteChannel();

  List<NavigatorObserver> _navigatorObservers;

  final _NavigatorObserverDelegate _navigatorObserverDelegate =
      _NavigatorObserverDelegate();

  /// Get topmost route
  String get topRoute => _navigatorObserverDelegate.topRoute;

  /// Add [NavigatorObserver]
  VoidCallback addNavigatorObserver(NavigatorObserver navigatorObserver) =>
      _navigatorObserverDelegate.addNavigatorObserver(navigatorObserver);

  /// see [NavigatorState.pushNamed]
  Future<T> pushNamed<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    return _navigatorKey.currentState
        .pushNamed<T>(routeName, arguments: arguments);
  }

  /// see [NavigatorState.pushNamed]
  Future<bool> pushNamedNative<T extends Object>(
    String routeName, {
    Object arguments,
  }) {
    return _nativeRouteChannel.route(routeName, arguments);
  }

  bool canPop() {
    return _navigatorKey.currentState.canPop();
  }

  /// Pop current page or pop the native page (Activity/Fragment on Android, ViewController in iOS)
  /// if on the root page.
  /// see [NavigatorState.pop], [SystemNavigator.pop]
  Future<bool> pop<T extends Object>([T result]) async {
    if (await _navigatorKey.currentState.maybePop(result)) {
      return true;
    } else {
      if (Platform.isAndroid) {
        SystemNavigator.pop();
      } else {
        _nativeRouteChannel.pop();
      }
      // Reset the root route '/' to ensure the cache page gone when the page open again
      _navigatorKey.currentState.pushReplacementNamed("/");

      return false;
    }
  }

  /// Force pop current page or pop the native page (Activity/Fragment on Android, ViewController in iOS)
  /// if on the root page.
  /// see [NavigatorState.pop], [SystemNavigator.pop]
  ///
  /// Declaration: This function can be eliminate by adding a `isForcePop` parameter
  /// to the [pop] function, like `Future<bool> pop<T extends Object>([T result, bool isForcePop])`
  /// but it will cause the invocation to be `navigationService.pop(null, true)`
  /// if there is no `result` passed due to the bad design of the [pop] function. So
  /// we add this individual `forcePop` function at this time, and will refactor
  /// the [_NavigationService] in the future.
  Future<bool> forcePop<T extends Object>([T result]) async {
    if (_navigatorKey.currentState.canPop()) {
      _navigatorKey.currentState.pop(result);
      return true;
    }

    if (Platform.isAndroid) {
      SystemNavigator.pop();
    } else {
      _nativeRouteChannel.pop();
    }
    // Reset the root route '/' to ensure the cache page gone when the page open again
    _navigatorKey.currentState.pushReplacementNamed("/");

    return false;
  }

  /// see [NavigatorState.popUntil]
  void popUntilNamed(String name) {
    _navigatorKey.currentState.popUntil(ModalRoute.withName(name));
  }

  /// see [Navigator.pushReplacementNamed]
  Future<T> pushReplacementNamed<T extends Object, TO extends Object>(
      String routeName,
      {TO result,
      Object arguments}) {
    return _navigatorKey.currentState
        .pushReplacementNamed(routeName, result: result, arguments: arguments);
  }

  /// see [Navigator.pushNamedAndRemoveUntil]
  void popUntil(RoutePredicate predicate) {
    _navigatorKey.currentState.popUntil(predicate);
  }
}

@Deprecated(
    "This class will be remove in the futre, use CommonNavigator instead")
class _NavigatorObserverDelegate extends NavigatorObserver {
  final List<NavigatorObserver> _navigatorObservers = [];

  // NOTE: Using `ModalRoute.of(context).settings.name` to get the top route is
  // easiest way to get the top route, but it will cause unexpected widget rebuild
  // at sometime, so we track the top route manually, will do some investigation
  // and try optimize it in the future.
  String _topRoute;

  String get topRoute => _topRoute;

  VoidCallback addNavigatorObserver(NavigatorObserver navigatorObserver) {
    _navigatorObservers.add(navigatorObserver);

    return () {
      _navigatorObservers.remove(navigatorObserver);
    };
  }

  @override
  void didPop(Route route, Route previousRoute) {
    _topRoute = previousRoute.settings.name;
    _navigatorObservers.forEach((e) {
      e.didPop(route, previousRoute);
    });
  }

  @override
  void didPush(Route route, Route previousRoute) {
    _topRoute = route.settings.name;
    _navigatorObservers.forEach((e) {
      e.didPush(route, previousRoute);
    });
  }

  @override
  void didRemove(Route route, Route previousRoute) {
    _topRoute = previousRoute.settings.name;
    _navigatorObservers.forEach((e) {
      e.didRemove(route, previousRoute);
    });
  }

  @override
  void didReplace({Route newRoute, Route oldRoute}) {
    _topRoute = newRoute.settings.name;
    _navigatorObservers.forEach((e) {
      e.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    });
  }

  @override
  void didStartUserGesture(Route route, Route previousRoute) {
    _topRoute = previousRoute.settings.name;
    _navigatorObservers.forEach((e) {
      e.didStartUserGesture(route, previousRoute);
    });
  }

  @override
  void didStopUserGesture() {
    _navigatorObservers.forEach((e) {
      e.didStopUserGesture();
    });
  }
}

@Deprecated(
    "This class will be remove in the futre, use CommonNavigator instead")
abstract class NavigatorServiceProvider {}

// ignore: deprecated_member_use_from_same_package
extension NavigationServiceMyAppExtension on NavigatorServiceProvider {
  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  GlobalKey<NavigatorState> get navigatorKey =>
      _NavigationService()._navigatorKey;

  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  List<NavigatorObserver> get navigatorObservers =>
      _NavigationService()._navigatorObservers;

  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  _NavigationService get navigationService => _NavigationService();
}

// ignore: deprecated_member_use_from_same_package
extension NavigationServiceRouteChannelExtension on RouteChannel {
  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  _NavigationService get navigationService => _NavigationService();
}

extension NavigationServiceStatelessWidgetExtension on StatelessWidget {
  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  _NavigationService get navigationService => _NavigationService();
}

extension NavigationServiceStatefulWidgetExtension on StatefulWidget {
  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  _NavigationService get navigationService => _NavigationService();
}

extension NavigationServiceStateExtension on State {
  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  _NavigationService get navigationService => _NavigationService();
}

extension NavigationServiceBlocExtension on BaseBloc {
  @Deprecated(
      "This class will be remove in the futre, use CommonNavigator instead")
  _NavigationService get navigationService => _NavigationService();
}
