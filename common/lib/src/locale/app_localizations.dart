import 'dart:convert';

import 'package:common/src/basic_info/native_info_manager.dart';
import 'package:error_handling/error_handling.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:l10n_cms/l10n_cms.dart';

typedef _LoadAssetString = Future<String> Function(String key);

class AppLocalizations {
  factory AppLocalizations(NativeInfoManager nativeInfoManager,
      GlobalL10NCMSLocalizationsLookup globalL10NCMSLocalizationsLookup) {
    _appLocalizations ??=
        AppLocalizations._(nativeInfoManager, globalL10NCMSLocalizationsLookup);

    return _appLocalizations;
  }

  AppLocalizations._(
      this._nativeInfoManager, this._globalL10NCMSLocalizationsLookup);

  /// Only be used on unit test
  @visibleForTesting
  AppLocalizations.mock(this._nativeInfoManager,
      this._globalL10NCMSLocalizationsLookup, this._loadAssetString);

  static AppLocalizations _appLocalizations;

  final NativeInfoManager _nativeInfoManager;

  final GlobalL10NCMSLocalizationsLookup _globalL10NCMSLocalizationsLookup;

  /// Allow set a mock load string function to return mock data on unit test. Only be set on unit test.
  _LoadAssetString _loadAssetString;

  GlobalL10NCMSLocalizations _globalL10NCMSLocalizations;

  Map<String, dynamic> _localizedValues;
  Map<String, dynamic> _enLocalizedValues;

  String _currentLanguage;

  bool _isInitialized = false;

  void _debugCheckInitialization() {
    assert(() {
      if (!_isInitialized)
        throw StateError(
            'Make sure the "ensureInit" function has been called.');
      return true;
    }());
  }

  /// This should be called before the [runApp] to ensure the language resources haved been loaded.
  Future<void> ensureInit() async {
    final appInfo = await _nativeInfoManager.provideAppInfo();
    await load(appInfo.language);

    assert(() {
      _isInitialized = true;
      return true;
    }());
  }

  Future<String> _loadString(String key) {
    if (_loadAssetString != null) {
      return _loadAssetString(key);
    }

    return rootBundle.loadString(key);
  }

  // TODO(littlegnal): Add unit test
  Future<void> load(String language) async {
    if (_currentLanguage != null && language == _currentLanguage) {
      return;
    }

    final tempLanguage = language.split('_');
    assert(tempLanguage.isNotEmpty);
    assert(tempLanguage.length == 2);
    _globalL10NCMSLocalizations =
        _globalL10NCMSLocalizationsLookup.lookFor(Locale.fromSubtags(
      languageCode: tempLanguage[0],
      countryCode: tempLanguage[1],
    ));
    // The `appLocalizationsTextGetDebugModeCompat` need to be called explicitly to read the debug flag to memory.
    final debugMode = await _globalL10NCMSLocalizations
        .appLocalizationsTextGetDebugModeCompat();
    debugPrint(
        'AppLocalizations: l10n_cms [$_globalL10NCMSLocalizations] debug mode: $debugMode');

    _enLocalizedValues ??=
        json.decode(await _loadString("locale/i18n_en.json"));
    debugPrint("AppLocalizations: Load language resources for [$language]");
    try {
      _localizedValues =
          json.decode(await _loadString("locale/i18n_$language.json"));
      _currentLanguage = language;
    } catch (e) {
      debugPrint(
          "AppLocalizations: The language: [$language] doesn't exist, fallback to [en]");
      _localizedValues = _enLocalizedValues;
      _currentLanguage = "en";
    }
  }

  ///这个方法为多语言文案的展示方法
  /// key 为多语言的locale文件中 flutter_key,placeholder为占位符号，替换多语言文案中的占位符
  /// eg.
  ///   多语言文案为"jrpass_show_result_button" : "View {0} Result"
  ///   则key为"jrpass_show_result_button"，占位符为{0},
  ///   使用方法：_translations.text('jrpass_show_result_button', placeholder: {"{0}": "$_num"})
  String text(String key, {Map<String, String> placeholder}) {
    _debugCheckInitialization();
    try {
      return _globalL10NCMSLocalizations.appLocalizationsTextCompat(
          key, placeholder ?? {});
    } catch (e, stacktrace) {
      debugPrint(
          'AppLocalizations: get text from l10n_cms [$_globalL10NCMSLocalizations] error, key: $key, placeholder: $placeholder\n$e');

      reportCrashToCrashlytics(
          _L10NTextNotFoundException(key, placeholder, e), stacktrace);
    }

    String tempString;
    if (_localizedValues.containsKey(key)) {
      tempString = _localizedValues[key];
    } else {
      tempString = _enLocalizedValues[key];
    }
    if (placeholder != null && placeholder.isNotEmpty) {
      placeholder.forEach((k, v) => tempString = tempString.replaceFirst(k, v));
    }
    return tempString ?? '';
  }

  String get currentLanguage => _currentLanguage;
}

class _L10NTextNotFoundException implements Exception {
  _L10NTextNotFoundException(this.key, this.placeholder, this.originException);

  final String key;
  final Map<String, String> placeholder;
  final dynamic originException;

  @override
  String toString() {
    return '[_L10NTextNotFoundException] Can\'t get text from l10n_cms, key: $key, placeholder: $placeholder\n${originException.toString()}';
  }
}
