// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(ComponentPage.serializer)
      ..add(ComponentPageBody.serializer)
      ..add(ComponentPageMeta.serializer)
      ..add(ComponentPageUserAction.serializer)
      ..add(ComponentSection.serializer)
      ..add(ComponentSectionBody.serializer)
      ..add(ComponentSectionContent.serializer)
      ..add(ComponentSectionMeta.serializer)
      ..add(SecondFloor.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ComponentSection)]),
          () => new ListBuilder<ComponentSection>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ComponentSectionContent)]),
          () => new ListBuilder<ComponentSectionContent>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
