import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'src/components/platform/component_model.dart';

part 'serializers.g.dart';

@SerializersFor([
  ComponentPage,
])
final Serializers serializers = _$serializers;
