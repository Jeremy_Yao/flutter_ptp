import 'package:common/src/error_handling/wrapped_error_exception.dart';
import 'package:flutter_test/flutter_test.dart';

@Deprecated(
    'This file have been moved to `error_handling` package, will be removed it the near future')
void main() {
  group('errorToException', () {
    test('pass an Error', () {
      expect(errorToException(StateError("StateError")),
          WrappedErrorException(StateError("StateError")));
    });

    test('pass an Exception', () {
      expect(errorToException(Exception("Error")), isA<Exception>());
    });

    test('call reportCrashCallback', () {
      final expectedException = WrappedErrorException(StateError("StateError"));
      final exception = errorToException(StateError("StateError"),
          reportCrashCallback: (error, stackTrace, {bool forceCrash}) async {
        expect(error, expectedException);
      });

      expect(exception, expectedException);
    });

    test('crashOnDebugMode = true', () {
      expect(
          () => errorToException(StateError("StateError"),
              crashOnDebugMode: () => true),
          throwsAssertionError);
    });
  });
}
