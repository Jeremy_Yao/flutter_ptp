import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:common/common.dart';
import 'package:error_handling/error_handling.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:rxdart/rxdart.dart';

class _TestBlocState extends BlocState {
  const _TestBlocState(this.blocAsync);

  final BlocAsync blocAsync;

  @override
  bool operator ==(other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is _TestBlocState && other.blocAsync == blocAsync;
  }

  @override
  int get hashCode => blocAsync.hashCode;
}

class _TestBlocStateWith2Values extends BlocState {
  const _TestBlocStateWith2Values(this.blocAsync1, this.blocAsync2);

  final BlocAsync blocAsync1;
  final BlocAsync blocAsync2;

  _TestBlocStateWith2Values copy1(BlocAsync blocAsync1) {
    return _TestBlocStateWith2Values(blocAsync1, blocAsync2);
  }

  _TestBlocStateWith2Values copy2(BlocAsync blocAsync2) {
    return _TestBlocStateWith2Values(blocAsync1, blocAsync2);
  }

  @override
  bool operator ==(other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is _TestBlocStateWith2Values &&
        other.blocAsync1 == blocAsync1 &&
        other.blocAsync2 == blocAsync2;
  }

  @override
  int get hashCode => hashValues(blocAsync1, blocAsync2);

  @override
  String toString() {
    return '_TestBlocStateWith2Values{blocAsync1: $blocAsync1, blocAsync2: $blocAsync2}';
  }
}

void main() {
  group("BlocAsync equal", () {
    test("Success equal", () {
      expect(const Success(10) == const Success(10), true);
      expect(
          Success(BuiltList<int>([1, 10])) == Success(BuiltList<int>([1, 10])),
          true);
    });

    test("Fail equal", () {
      final error = const HttpError("111");
      expect(Fail(error) == Fail(error), true);
    });

    test("Loading equal", () {
      expect(const Loading() == const Loading(), true);
    });

    test("Uninitialized equal", () {
      expect(const Uninitialized() == const Uninitialized(), true);
    });
  });

  group("FailEx", () {
    test("isHttpRequestCancelled", () {
      expect(Fail(CancelHttpException()).isHttpRequestCancelled, true);
    });
  });

  group("BehaviorSubjectBlocAsyncEx execute()", () {
    test("execute() success", () async {
      final subject = BehaviorSubject<_TestBlocState>();
      subject.execute<int>((_) => Future.value(10), (preState, blocAsync) {
        return _TestBlocState(blocAsync);
      });

      await expectLater(
          subject.stream,
          emitsInOrder([
            const _TestBlocState(Loading()),
            const _TestBlocState(Success(10))
          ]));

      subject.close();
    });

    test("execute() success lazy get value", () async {
      final subject = BehaviorSubject<_TestBlocStateWith2Values>.seeded(
          const _TestBlocStateWith2Values(Uninitialized(), Uninitialized()));
      subject.execute<int>(
          (_) => Future.delayed(const Duration(milliseconds: 10), () => 10),
          (preState, blocAsync) {
        return preState().copy1(blocAsync);
      });

      subject.execute<int>(
          (_) => Future.delayed(const Duration(milliseconds: 5), () => 5),
          (preState, blocAsync) {
        return preState().copy2(blocAsync);
      });

      await expectLater(
          subject.stream,
          emitsInOrder([
            const _TestBlocStateWith2Values(Loading(), Loading()),
            const _TestBlocStateWith2Values(Loading(), Success(5)),
            const _TestBlocStateWith2Values(Success(10), Success(5))
          ]));

      subject.close();
    });

    test("execute() when subject close() in the beginning", () async {
      final subject = BehaviorSubject<_TestBlocState>();
      await subject.close();
      await subject.execute<int>((_) => Future.value(10),
          (preState, blocAsync) {
        return _TestBlocState(blocAsync);
      });

      await expectLater(await subject.stream.length, 0);
    });

    test("execute() when subject close() when Future in progress", () async {
      final subject = BehaviorSubject<_TestBlocState>();

      final delayValue = Future.wait([
        Future.delayed(const Duration(milliseconds: 100)),
        Future.delayed(const Duration(milliseconds: 50), () {
          subject.close();
        })
      ]).then((_) => 10);

      subject.execute<int>((_) => delayValue, (preState, blocAsync) {
        return _TestBlocState(blocAsync);
      });

      await expectLater(subject.stream,
          emitsInOrder([const _TestBlocState(Loading()), emitsDone]));
    });

    test("execute() fail with Exception", () async {
      final subject = BehaviorSubject<_TestBlocState>();
      subject.execute<int>((_) => Future.error(const HttpError("HttpError")),
          (preState, blocAsync) {
        return _TestBlocState(blocAsync);
      });

      await expectLater(
          subject.stream,
          emitsInOrder([
            const _TestBlocState(Loading()),
            const _TestBlocState(Fail(HttpError("HttpError")))
          ]));

      subject.close();
    });

    test("execute() fail with Error", () async {
      final subject = BehaviorSubject<_TestBlocState>();

      subject.execute<int>((_) => Future.error(StateError("StateError")),
          (preState, blocAsync) {
        return _TestBlocState(blocAsync);
      });

      await expectLater(
          subject.stream,
          emitsInOrder([
            const _TestBlocState(Loading()),
            _TestBlocState(
                Fail(WrappedErrorException(StateError("StateError"))))
          ]));

      subject.close();
    });
  });
}
