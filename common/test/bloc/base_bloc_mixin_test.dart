import 'package:common/common.dart';
import 'package:common/src/bloc/base_bloc_mixin.dart';
import 'package:common/src/bloc/bloc_state.dart';
import 'package:common/src/network/cancellable_request.dart';
import 'package:test/test.dart';

class _TestBlocState implements BlocState {
  _TestBlocState(this.value);

  final BlocAsync<int> value;

  @override
  bool operator ==(other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is _TestBlocState && other.value == value;
  }

  @override
  int get hashCode => identityHashCode(value);
}

class _TestSingleStateBaseBloc extends SingleStateBaseBloc<_TestBlocState> {
  _TestSingleStateBaseBloc(_TestBlocState initialValue) : super(initialValue);
}

class _TestCancellableRequestBlocMixin
    extends SingleStateBaseBloc<_TestBlocState>
    with CancellableRequestSingleStateBaseBlocMixin<_TestBlocState> {
  _TestCancellableRequestBlocMixin(_TestBlocState initialValue)
      : super(initialValue);
}

void main() {
  group("SingleStateBaseBloc", () {
    test("initial value", () {
      final _testSingleValueBaseBloc =
          _TestSingleStateBaseBloc(_TestBlocState(const Success(10)));
      expect(_testSingleValueBaseBloc.subject.value,
          _TestBlocState(const Success(10)));
    });
    test("add value", () {
      final _testSingleValueBaseBloc =
          _TestSingleStateBaseBloc(_TestBlocState(const Success(10)));

      _testSingleValueBaseBloc.subject.sink
          .add(_TestBlocState(const Success(10)));
      expect(_testSingleValueBaseBloc.stream,
          emits(_TestBlocState(const Success(10))));
    });

    test("dispose", () {
      final _testSingleValueBaseBloc =
          _TestSingleStateBaseBloc(_TestBlocState(const Success(10)));
      _testSingleValueBaseBloc.dispose();
      expect(_testSingleValueBaseBloc.subject.isClosed, true);
    });

    test("execute success", () async {
      final _testSingleValueBaseBloc =
          _TestSingleStateBaseBloc(_TestBlocState(const Uninitialized()));
      _testSingleValueBaseBloc.execute((_) => Future.value(10),
          (preState, blocAsync) => _TestBlocState(blocAsync));
      expect(
          _testSingleValueBaseBloc.stream,
          emitsInOrder([
            _TestBlocState(const Loading()),
            _TestBlocState(const Success(10))
          ]));
    });

    test("execute fail", () {
      final _testSingleValueBaseBloc =
          _TestSingleStateBaseBloc(_TestBlocState(const Uninitialized()));
      _testSingleValueBaseBloc.execute<int>(
          (_) => Future.error(Exception("error")),
          (preState, blocAsync) => _TestBlocState(blocAsync));
      expect(
          _testSingleValueBaseBloc.stream,
          emitsInOrder([
            _TestBlocState(const Loading()),
            _TestBlocState(Fail(Exception("error")))
          ]));
    });
  });

  group("CancellableRequestSingleStateBaseBlocMixin", () {
    test("executeRequest success", () {
      final _testCancellableRequestBlocMixin = _TestCancellableRequestBlocMixin(
          _TestBlocState(const Uninitialized()));
      _testCancellableRequestBlocMixin.executeRequest(
          (_) => CancellableRequestImpl.fromRequest(Future.value(10)),
          (preState, blocAsync) => _TestBlocState(blocAsync));
      expect(
          _testCancellableRequestBlocMixin.stream,
          emitsInOrder([
            _TestBlocState(const Loading()),
            _TestBlocState(const Success(10))
          ]));
    });

    test("executeRequest fail", () {
      final _testCancellableRequestBlocMixin = _TestCancellableRequestBlocMixin(
          _TestBlocState(const Uninitialized()));
      _testCancellableRequestBlocMixin.executeRequest<int>(
          (_) => CancellableRequestImpl.fromRequest(
              Future.error(Exception("error"))),
          (preState, blocAsync) => _TestBlocState(blocAsync));
      expect(
          _testCancellableRequestBlocMixin.stream,
          emitsInOrder([
            _TestBlocState(const Loading()),
            _TestBlocState(Fail(Exception("error")))
          ]));
    });

    test("dispose", () {
      final _testCancellableRequestBlocMixin = _TestCancellableRequestBlocMixin(
          _TestBlocState(const Uninitialized()));
      _testCancellableRequestBlocMixin.executeRequest(
          (_) => CancellableRequestImpl.fromRequest(Future.value(10)),
          (preState, blocAsync) => _TestBlocState(blocAsync));
      expect(
          _testCancellableRequestBlocMixin
              .compositeCancellable.cancellableList.length,
          1);

      _testCancellableRequestBlocMixin.dispose();

      expect(
          _testCancellableRequestBlocMixin
              .compositeCancellable.cancellableList.length,
          0);
    });
  });
}
