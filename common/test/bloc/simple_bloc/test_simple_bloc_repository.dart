import 'package:common/src/bloc/simple_bloc/simple_bloc_repository.dart';

class TestSimpleBlocRepository<T> implements SimpleBlocRepository<T> {
  TestSimpleBlocRepository(this._value);

  final Future<T> _value;

  @override
  void clear() {}

  @override
  Future<T> getData() => _value;
}
