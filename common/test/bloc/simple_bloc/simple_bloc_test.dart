import 'package:common/common.dart';
import 'package:test/test.dart';

import 'test_simple_bloc_repository.dart';

void main() {
  group("SimpleBloc", () {
    test("getData success", () {
      final simpleBloc = SimpleBloc<int, TestSimpleBlocRepository<int>>(
          TestSimpleBlocRepository<int>(Future.value(10)));
      simpleBloc.getData();
      expect(
          simpleBloc.stream,
          emitsInOrder([
            SimpleBlocState<int>.create(const Loading()),
            SimpleBlocState<int>.create(const Success(10))
          ]));
    });

    test("getData fail", () {
      final simpleBloc = SimpleBloc<int, TestSimpleBlocRepository<int>>(
          TestSimpleBlocRepository<int>(Future.error(Exception("error"))));
      simpleBloc.getData();
      expect(
          simpleBloc.stream,
          emitsInOrder([
            SimpleBlocState<int>.create(const Loading()),
            SimpleBlocState<int>.create(Fail(Exception("error")))
          ]));
    });
  });
}
