import 'package:common/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

import 'test_simple_bloc_repository.dart';

void main() {
  final asyncChildBuilder = (context, snapshot) {
    if (snapshot.data?.value is Success) {
      return Text("success: ${snapshot.data.value()}",
          textDirection: TextDirection.ltr);
    } else if (snapshot.data?.value is Loading) {
      return const Text("loading", textDirection: TextDirection.ltr);
    } else if (snapshot.data?.value is Fail) {
      return const Text("fail", textDirection: TextDirection.ltr);
    } else {
      return const Text("placeholder", textDirection: TextDirection.ltr);
    }
  };

  group("SimpleBlocBuilder", () {
    testWidgets("success", (WidgetTester tester) async {
      await tester
          .pumpWidget(SimpleBlocBuilder<int, TestSimpleBlocRepository<int>>(
        blocRepositoryBuilder: (_) => TestSimpleBlocRepository(
            Future.delayed(const Duration(milliseconds: 10), () => 10)),
        asyncChildBuilder: asyncChildBuilder,
      ));

      // Initial state
      expect(find.text("placeholder"), findsOneWidget);
      // First emit the loading state
      await tester.pump();
      expect(find.text("loading"), findsOneWidget);
      // Then emit the success state
      await tester.pump(const Duration(milliseconds: 10));
      expect(find.text("success: 10"), findsOneWidget);
      expect(find.text("fail"), findsNothing);
    });

    testWidgets("fail", (WidgetTester tester) async {
      await tester
          .pumpWidget(SimpleBlocBuilder<int, TestSimpleBlocRepository<int>>(
        blocRepositoryBuilder: (_) => TestSimpleBlocRepository(Future.delayed(
            const Duration(milliseconds: 10),
            () => Future.error(Exception("error")))),
        asyncChildBuilder: asyncChildBuilder,
      ));

      // Initial state
      expect(find.text("placeholder"), findsOneWidget);
      // First emit the loading state
      await tester.pump();
      expect(find.text("loading"), findsOneWidget);
      // Then emit the fail state
      await tester.pump(const Duration(milliseconds: 10));
      expect(find.text("fail"), findsOneWidget);
      expect(find.text("success: 10"), findsNothing);
    });
  });
}
