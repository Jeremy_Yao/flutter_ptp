import 'package:common/src/date/common_date_specification.dart';
import 'package:common/src/locale/app_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockAppLocalizations extends Mock implements AppLocalizations {}

// ignore: non_constant_identifier_names
final DateTime _2020Apr16Thu1636 = DateTime(2020, 4, 16, 16, 36);
// ignore: non_constant_identifier_names
final DateTime _2020Apr16Thu436 = DateTime(2020, 4, 16, 4, 36);

void main() {
  MockAppLocalizations appLocalizations;
  CommonDateSpecification commonDateSpecification;

  setUp(() {
    appLocalizations = MockAppLocalizations();
    commonDateSpecification = CommonDateSpecification(appLocalizations);
  });

  group('formatDate', () {
    test('zh_CN', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636), '2020年4月16日');
    });

    test('zh_CN ignoreYear', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreYear: true),
          '4月16日');
    });

    test('zh_CN ignoreMonth', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreMonth: true),
          '2020年16日');
    });

    test('zh_CN ignoreDay', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreDay: true),
          '2020年4月');
    });

    test('zh_HK', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_HK');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636), '2020年4月16日');
    });

    test('zh_TW', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_TW');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636), '2020年4月16日');
    });

    test('vi_VN', () {
      when(appLocalizations.currentLanguage).thenReturn('vi_VN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636), '16/4/2020');
    });

    test('vi_VN ignoreYear', () {
      when(appLocalizations.currentLanguage).thenReturn('vi_VN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreYear: true),
          '16/4');
    });

    test('vi_VN ignoreMonth', () {
      when(appLocalizations.currentLanguage).thenReturn('vi_VN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreMonth: true),
          '16/2020');
    });

    test('vi_VN ignoreDay', () {
      when(appLocalizations.currentLanguage).thenReturn('vi_VN');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreDay: true),
          '4/2020');
    });

    test('ko_KR', () {
      when(appLocalizations.currentLanguage).thenReturn('ko_KR');

      expect(commonDateSpecification.formatDate(_2020Apr16Thu1636),
          '2020년 4월 16일');
    });

    test('ko_KR ignoreYear', () {
      when(appLocalizations.currentLanguage).thenReturn('ko_KR');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreYear: true),
          '4월 16일');
    });

    test('ko_KR ignoreMonth', () {
      when(appLocalizations.currentLanguage).thenReturn('ko_KR');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreMonth: true),
          '2020년 16일');
    });

    test('ko_KR ignoreDay', () {
      when(appLocalizations.currentLanguage).thenReturn('ko_KR');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreDay: true),
          '2020년 4월');
    });

    test('en', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636), '16 Apr 2020');
    });

    test('en May', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      // ignore: non_constant_identifier_names
      final DateTime _2020May16Thu1636 = DateTime(2020, 5, 16, 16, 36);
      expect(
          commonDateSpecification.formatDate(_2020May16Thu1636), '16 May 2020');
    });

    test('en June', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      // ignore: non_constant_identifier_names
      final DateTime _2020June16Thu1636 = DateTime(2020, 6, 16, 16, 36);
      expect(commonDateSpecification.formatDate(_2020June16Thu1636),
          '16 June 2020');
    });

    test('en July', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      // ignore: non_constant_identifier_names
      final DateTime _2020July16Thu1636 = DateTime(2020, 7, 16, 16, 36);
      expect(commonDateSpecification.formatDate(_2020July16Thu1636),
          '16 July 2020');
    });

    test('en ignoreYear', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreYear: true),
          '16 Apr');
    });

    test('en ignoreMonth', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreMonth: true),
          '16 2020');
    });

    test('en ignoreDay', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636,
              ignoreDay: true),
          'Apr 2020');
    });

    test('ja_JP', () {
      when(appLocalizations.currentLanguage).thenReturn('ja_JP');

      expect(
          commonDateSpecification.formatDate(_2020Apr16Thu1636), '16 Apr 2020');
    });
  });

  group('formatTime', () {
    test('zh_CN AM', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36 AM');
    });
    test('zh_CN PM', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '4:36 PM');
    });

    test('zh_HK AM', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_HK');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36 AM');
    });

    test('zh_HK PM', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_HK');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '4:36 PM');
    });

    test('zh_TW AM', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_TW');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36 AM');
    });

    test('zh_TW PM', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_TW');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '4:36 PM');
    });

    test('vi_VN AM', () {
      when(appLocalizations.currentLanguage).thenReturn('vi_VN');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36');
    });

    test('vi_VN PM', () {
      when(appLocalizations.currentLanguage).thenReturn('vi_VN');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '16:36');
    });

    test('th_TH AM', () {
      when(appLocalizations.currentLanguage).thenReturn('th_TH');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36');
    });

    test('th_TH PM', () {
      when(appLocalizations.currentLanguage).thenReturn('th_TH');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '16:36');
    });

    test('ko_KR AM', () {
      when(appLocalizations.currentLanguage).thenReturn('ko_KR');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36 오전');
    });

    test('ko_KR PM', () {
      when(appLocalizations.currentLanguage).thenReturn('ko_KR');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '4:36 오후');
    });

    test('en AM', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36am');
    });
    test('en PM', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '4:36pm');
    });

    test('ja_JP AM', () {
      when(appLocalizations.currentLanguage).thenReturn('ja_JP');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu436), '4:36am');
    });
    test('ja_JP PM', () {
      when(appLocalizations.currentLanguage).thenReturn('ja_JP');

      expect(commonDateSpecification.formatTime(_2020Apr16Thu1636), '4:36pm');
    });
  });

  group('formatTimeRange', () {
    test('en', () {
      when(appLocalizations.currentLanguage).thenReturn('en');

      expect(
          commonDateSpecification.formatTimeRange(
              _2020Apr16Thu436, _2020Apr16Thu1636),
          '4:36am-4:36pm');
    });
  });

  group('formatFullDateTime', () {
    test('zh_CN', () {
      when(appLocalizations.currentLanguage).thenReturn('zh_CN');

      expect(commonDateSpecification.formatFullDateTime(_2020Apr16Thu1636),
          '2020年4月16日 4:36 PM');
    });
  });
}
