import 'package:flutter_test/flutter_test.dart';
import 'package:common/extensions.dart';

void main() {
  group("ListExt", () {
    test("replace where the old element exist", () {
      final List<int> intList = [1, 2, 3, 4];
      intList.replaceFirst((e) => e == 3, (e) => 10);
      expect(intList, [1, 2, 10, 4]);
    });

    test("replace where the old element not exist", () {
      final List<int> intList = [1, 2, 3, 4];
      intList.replaceFirst((e) => e == 10, (e) => 20);
      expect(intList, [1, 2, 3, 4]);
    });
  });
}
