import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:common/src/extensions/color_ext.dart';

void main() {
  group("StringToColorExt", () {
    test("hexToColor #FFB6C1", () {
      expect("#FFB6C1".hexToColor(), const Color(0xFFFFB6C1));
    });

    test("hexToColor #FFFB6C1", () {
      expect("#FFFB6C1".hexToColor(), const Color(0xFFFFB6C1));
    });

    test("hexToColor hex color string less than 7", () {
      expect("#FFF".hexToColor(), null);
    });

    test("hexToColor hex color string not start with #", () {
      expect("FFB6C1".hexToColor(), null);
    });
  });
}
