import 'package:built_value/serializer.dart';
import 'package:common/basic_info.dart';
import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:common/date_specification.dart';
import 'package:common/local_event.dart';
import 'package:common/src/di/common_di_injector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

import '../mock/mock_global_l10n_cms_localizations_lookup.dart';

class _TestWidget extends StatelessWidget {
  const _TestWidget(this._widgetBuilder);

  final WidgetBuilder _widgetBuilder;

  @override
  Widget build(BuildContext context) {
    return _widgetBuilder(context);
  }
}

Widget _testWidget(WidgetBuilder widgetBuilder) {
  return CommonDIInjector(
    diProvider: DIProvider(
      serializers: Serializers(),
      railsComponentBuilderMap: const {},
      componentContentFactories: const [],
      componentWrapperFactory: null,
      feedContentCardBuilderMap: const {},
      genericFeedCardItemBuilderMap: const {},
      globalL10NCMSLocalizationsLookup: MockGlobalL10NCMSLocalizationsLookup(),
    ),
    child: _TestWidget(widgetBuilder),
  );
}

void main() {
  group('CommonDIInjector', () {
    testWidgets("get CommonNavigator", (WidgetTester tester) async {
      CommonNavigator commonNavigator;
      await tester.pumpWidget(_testWidget((context) {
        commonNavigator = Provider.of<CommonNavigator>(context);
        return Container();
      }));

      expect(commonNavigator, isNotNull);
    });

    testWidgets("get NativeEventDispatcher", (WidgetTester tester) async {
      NativeEventDispatcher nativeEventDispatcher;
      await tester.pumpWidget(_testWidget((context) {
        nativeEventDispatcher = Provider.of<NativeEventDispatcher>(context);
        return Container();
      }));

      expect(nativeEventDispatcher, isNotNull);
    });

    testWidgets("get Serialization", (WidgetTester tester) async {
      Serialization serialization;
      await tester.pumpWidget(_testWidget((context) {
        serialization = Provider.of<Serialization>(context);
        return Container();
      }));

      expect(serialization, isNotNull);
    });

    testWidgets("get AppLocalizations", (WidgetTester tester) async {
      AppLocalizations appLocalizations;
      await tester.pumpWidget(_testWidget((context) {
        appLocalizations = Provider.of<AppLocalizations>(context);
        return Container();
      }));

      expect(appLocalizations, isNotNull);
    });

    testWidgets("get CommonRequest", (WidgetTester tester) async {
      CommonRequest commonRequest;
      await tester.pumpWidget(_testWidget((context) {
        commonRequest = Provider.of<CommonRequest>(context);
        return Container();
      }));

      expect(commonRequest, isNotNull);
    });

    testWidgets("get Tracker", (WidgetTester tester) async {
      Tracker tracker;
      await tester.pumpWidget(_testWidget((context) {
        tracker = Provider.of<Tracker>(context);
        return Container();
      }));

      expect(tracker, isNotNull);
    });

    testWidgets("get RailsComponentFactory", (WidgetTester tester) async {
      ComponentFactory componentFactory;
      await tester.pumpWidget(_testWidget((context) {
        componentFactory = Provider.of<ComponentFactory>(context);
        return Container();
      }));

      expect(componentFactory, isNotNull);
    });

    testWidgets("get ComponentFactory", (WidgetTester tester) async {
      ComponentFactory componentFactory;
      await tester.pumpWidget(_testWidget((context) {
        componentFactory = Provider.of<ComponentFactory>(context);
        return Container();
      }));

      expect(componentFactory, isNotNull);
    });

    testWidgets("get LocalEventManager", (WidgetTester tester) async {
      LocalEventManager localEventManager;
      await tester.pumpWidget(_testWidget((context) {
        localEventManager = Provider.of<LocalEventManager>(context);
        return Container();
      }));

      expect(localEventManager, isNotNull);
    });

    testWidgets("get LoginManager", (WidgetTester tester) async {
      LoginManager loginManager;
      await tester.pumpWidget(_testWidget((context) {
        loginManager = Provider.of<LoginManager>(context);
        return Container();
      }));

      expect(loginManager, isNotNull);
    });

    testWidgets("get CommonDateSpecification", (WidgetTester tester) async {
      CommonDateSpecification commonDateSpecification;
      await tester.pumpWidget(_testWidget((context) {
        commonDateSpecification = Provider.of<CommonDateSpecification>(context);
        return Container();
      }));

      expect(commonDateSpecification, isNotNull);
    });
  });
}
