import 'package:common/src/components/component_validator.dart';
import 'package:flutter/material.dart' hide TypeMatcher;
import 'package:flutter_test/flutter_test.dart';
import 'package:test/test.dart' show TypeMatcher;

class _ComponentWidget extends StatefulWidget {
  const _ComponentWidget(this.flag, this.errorMessage, this.result);
  final bool flag;
  final String errorMessage;
  final Map<String, dynamic> result;
  @override
  State<StatefulWidget> createState() {
    return _ComponentWidgetState();
  }
}

class _ComponentWidgetState extends State<_ComponentWidget>
    with ComponentValidatorMixin {
  bool resetFlag = true;
  @override
  Widget build(BuildContext context) {
    return const Text("text", textDirection: TextDirection.ltr);
  }

  @override
  Map<String, dynamic> result() {
    return widget.result;
  }

  @override
  bool validate() {
    return widget.flag && resetFlag;
  }

  @override
  String errorMessage() {
    return widget.errorMessage;
  }

  @override
  void reset() {
    resetFlag = false;
  }
}

void main() {
  testWidgets("ComponentValidator validate()", (WidgetTester tester) async {
    final globalKey = GlobalKey<ComponentValidatorState>();
    await tester.pumpWidget(ComponentValidator(
      key: globalKey,
      child: Column(children: const <Widget>[
        _ComponentWidget(true, null, null),
        _ComponentWidget(true, null, null),
      ]),
    ));
    expect(globalKey.currentState.validate(), true);
  });

  group("ComponentValidator errorMessage()", () {
    final errorMsg1 = "msg1";
    final errorMsg2 = "msg2";
    testWidgets("ComponentValidator errorMessage() validate true",
        (WidgetTester tester) async {
      final globalKey = GlobalKey<ComponentValidatorState>();
      await tester.pumpWidget(ComponentValidator(
        key: globalKey,
        child: Column(children: <Widget>[
          _ComponentWidget(true, errorMsg1, null),
          _ComponentWidget(true, errorMsg2, null)
        ]),
      ));
      expect(globalKey.currentState.errorMessage(), null);
    });
    testWidgets("ComponentValidator errorMessage() validate false",
        (WidgetTester tester) async {
      final globalKey = GlobalKey<ComponentValidatorState>();
      await tester.pumpWidget(ComponentValidator(
        key: globalKey,
        child: Column(children: <Widget>[
          _ComponentWidget(true, errorMsg1, null),
          _ComponentWidget(false, errorMsg2, null)
        ]),
      ));
      expect(globalKey.currentState.errorMessage(), errorMsg2);
    });
  });
  group("ComponentValidator result()", () {
    testWidgets("ComponentValidator result() validate true",
        (WidgetTester tester) async {
      final globalKey = GlobalKey<ComponentValidatorState>();
      final result1 = {"result1": "result1"};
      final result2 = {"result2": "result2"};
      final result = {}..addAll(result1)..addAll(result2);
      await tester.pumpWidget(ComponentValidator(
        key: globalKey,
        child: Column(children: <Widget>[
          _ComponentWidget(true, null, result1),
          _ComponentWidget(true, null, result2)
        ]),
      ));
      expect(globalKey.currentState.result(), result);
    });
    testWidgets("ComponentValidator result() validate false",
        (WidgetTester tester) async {
      final globalKey = GlobalKey<ComponentValidatorState>();
      final result1 = {"result1": "result1"};
      final result2 = {"result2": "result2"};
      await tester.pumpWidget(ComponentValidator(
        key: globalKey,
        child: Column(children: <Widget>[
          _ComponentWidget(false, null, result1),
          _ComponentWidget(true, null, result2)
        ]),
      ));
      try {
        globalKey.currentState.result();
      } catch (e) {
        expect(e, const TypeMatcher<Exception>());
      }
    });
  });

  testWidgets("ComponentValidator reset()", (WidgetTester tester) async {
    final globalKey = GlobalKey<ComponentValidatorState>();
    await tester.pumpWidget(ComponentValidator(
      key: globalKey,
      child: Column(children: const <Widget>[
        _ComponentWidget(true, null, null),
        _ComponentWidget(true, null, null)
      ]),
    ));
    expect(globalKey.currentState.validate(), true);
    globalKey.currentState.reset();
    expect(globalKey.currentState.validate(), false);
  });
}
