import 'dart:convert';

import 'package:common/common.dart';
import 'package:common/src/components/component_simple_bloc_repository.dart';
import 'package:dio/dio.dart';
import 'package:test/test.dart';

import '../network/test_action.dart';
import '../mock/test_common_request.dart';
import '../network/test_serializers.dart';

void main() {
  TestCommonRequest commonRequest;
  ComponentGetRequestSimpleBlocRepository
      componentGetRequestSimpleBlocRepository;

  setUp(() {
    commonRequest = TestCommonRequest(
        Serialization(serializers.toStandardJsonSerializers()));
    componentGetRequestSimpleBlocRepository =
        ComponentGetRequestSimpleBlocRepository<TestAction>(
            commonRequest, "/fake");
  });

  tearDown(() {
    commonRequest = null;
  });

  group("ComponentGetRequestSimpleBlocRepository", () {
    test("getData() success", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      final expectAction = TestAction((b) => b
        ..path = "/path"
        ..type = "deeplink"
        ..extra = "args");

      expect(await componentGetRequestSimpleBlocRepository.getData(),
          expectAction);
    });

    test("getData() fail", () async {
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        await componentGetRequestSimpleBlocRepository.getData();
      } catch (e) {
        expect(e, const TypeMatcher<HttpError>());
      }
    });

    test("clear()", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest.stubGet("/fake").thenAnswer((_) => Future.delayed(
          const Duration(milliseconds: 100),
          () => Response(data: jsonDecode(rs))));

      Exception error;
      try {
        await Future.wait([
          componentGetRequestSimpleBlocRepository.getData(),
          Future.delayed(const Duration(milliseconds: 10), () {
            componentGetRequestSimpleBlocRepository.clear();
          })
        ]);
      } catch (e) {
        error = e;
      }

      expect(error, CancelHttpException(path: "/fake"));
    });
  });
}
