import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class _ComponentWidget extends StatelessWidget {
  const _ComponentWidget(this._config);
  final ComponentConfig _config;

  @override
  Widget build(BuildContext context) =>
      Text(_config.name, textDirection: TextDirection.ltr);
}

class _TestWidget extends StatelessWidget {
  const _TestWidget(this._componentName);
  final String _componentName;

  @override
  Widget build(BuildContext context) {
    final componentConfig = const ComponentConfig("component1", params: null);
    final componentsMap = {
      _componentName: (_, config) => _ComponentWidget(config),
    };
    final ComponentFactory componentFactory = ComponentFactory(componentsMap);
    if (componentFactory.canCreateWidget(context, componentConfig)) {
      return componentFactory.createWidget(context, componentConfig);
    } else {
      return const Text("not supported", textDirection: TextDirection.ltr);
    }
  }
}

void main() {
  testWidgets("ComponentFactory canCreateWidget()",
      (WidgetTester tester) async {
    final name = "component";
    await tester.pumpWidget(_TestWidget(name));
    expect(find.byType(_ComponentWidget), findsNothing);
    expect(find.text("not supported"), findsOneWidget);
  });

  testWidgets("ComponentFactory createWidget()", (WidgetTester tester) async {
    final name = "component1";
    await tester.pumpWidget(_TestWidget(name));
    expect(find.byType(_ComponentWidget), findsOneWidget);
    expect(find.text(name), findsOneWidget);
  });
}
