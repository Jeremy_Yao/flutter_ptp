import 'dart:convert';

import 'package:common/src/network/interceptors/optimus_message_manager.dart';
import 'package:common/src/network/interceptors/optimus_monitor_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class DioAdapterMock extends Mock implements HttpClientAdapter {}

class OptimusMessageManagerMock extends Mock implements OptimusMessageManager {}

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  RequestOptions options;
  OptimusMessageManager optimusMessageManager;
  OptimusMonitorInterceptor interceptor;

  group('test interceptor called manager functions', () {
    setUp(() {
      options = RequestOptions();
      optimusMessageManager = OptimusMessageManagerMock();
      interceptor = OptimusMonitorInterceptor(optimusMessageManager);

      when(optimusMessageManager.enqueue(any, any)).thenAnswer((_) => null);
      when(optimusMessageManager.requestFinished(any, any))
          .thenAnswer((_) => null);
    });

    test('test enqueue', () async {
      await interceptor.onRequest(options);

      verify(optimusMessageManager.enqueue(any, any)).called(1);
    });
    group('test requestFinished', () {
      test('test in on response', () async {
        await interceptor.onResponse(Response()
          ..request = (options..headers = {})
          ..headers = Headers());

        verify(optimusMessageManager.requestFinished(any, any)).called(1);
      });
      test('test in on error', () async {
        await interceptor
            .onError(DioError()..request = (options..headers = {}));

        verify(optimusMessageManager.requestFinished(any, any)).called(1);
      });
    });
  });

  group('test request', () {
    setUp(() async {
      options = RequestOptions();
      optimusMessageManager = OptimusMessageManager();
      interceptor = OptimusMonitorInterceptor(optimusMessageManager);

      final RequestOptions newRequest = await interceptor.onRequest(options);
      expect(newRequest.headers['RequestID'], isNotEmpty);
    });

    tearDown(() {
      options.headers.remove('RequestID');
    });

    group("test GET request", () {
      setUp(() {
        options.path = '/v1/home';
      });
      test("test GET", () {
        final requestInfo = interceptor.generateRequestInfo(options);
        expect(requestInfo.contentLength, '0');
        expect(requestInfo.path, '/v1/home');
        expect(requestInfo.queryParameters, null);
        expect(requestInfo.timeStamp, isNotEmpty);
      });
      test("test GET with query params", () {
        options.queryParameters = {
          'k1': 'v1',
          'k2': 2,
          'k3': false,
          'k4': 1.2,
        };

        final requestInfo = interceptor.generateRequestInfo(options);
        expect(requestInfo.queryParameters, 'k1=v1&k2=2&k3=false&k4=1.2');
      });
      test("test GET json content", () {
        options.responseType = ResponseType.json;

        final content =
            '{"success":true,"error":null,"result":{"k1":"v","k2":0}}';

        final response = Response(
          data: json.decode(content),
          request: options,
          statusCode: 200,
          headers: Headers()
            ..map[Headers.contentTypeHeader] = [Headers.jsonContentType],
        );
        final responseInfo = interceptor.generateResponseInfo(response);
        expect(responseInfo.result, isNotNull);
        expect(responseInfo.result.contentLength, content.length.toString());
        expect(responseInfo.result.isSucceed, true);
        expect(responseInfo.result.error, null);
        expect(responseInfo.result.statusCode, 200);
      });
      test("test GET plain content", () {
        options.responseType = ResponseType.plain;

        final content = 'test';

        final response = Response(
          data: content,
          request: options,
          headers: Headers(),
        );
        final responseInfo = interceptor.generateResponseInfo(response);
        expect(responseInfo.result, isNotNull);
        expect(responseInfo.result.contentLength, content.length.toString());
        expect(responseInfo.result.isSucceed, true);
        expect(responseInfo.result.error, null);
      });
    });

    group("test POST request", () {
      setUp(() {
        options.path = '/v1/home';
        options.method = 'POST';
        options.contentType = Headers.formUrlEncodedContentType;
      });
      test("test POST", () {
        final requestInfo = interceptor.generateRequestInfo(options);
        expect(requestInfo.contentLength, '0');
        expect(requestInfo.path, '/v1/home');
        expect(requestInfo.queryParameters, null);
        expect(requestInfo.timeStamp, isNotEmpty);
      });
      test("test POST with body", () async {
        options.data = {
          'k1': 'v1',
          'k2': 2,
          'k3': false,
          'k4': 1.2,
        };

        final dioAdapterMock = DioAdapterMock();
        final dio = Dio()..httpClientAdapter = dioAdapterMock;

        when(dioAdapterMock.fetch(any, any, any))
            .thenAnswer((_) async => ResponseBody.fromString('', 200));

        final result = await dio.request('', options: options);
        final request = result.request;

        final requestInfo = interceptor.generateRequestInfo(request);
        expect(requestInfo.contentLength,
            'k1=v1&k2=2&k3=false&k4=1.2'.length.toString());
        // request的 query parameters 在dio中会被处理为{}，最后得到空串
        expect(
            requestInfo.queryParameters, (String v) => v == null || v.isEmpty);
      });
    });
  });
}
