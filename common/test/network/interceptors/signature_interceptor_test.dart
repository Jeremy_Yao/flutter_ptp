import 'package:common/common.dart';
import 'package:common/src/network/interceptors/signature_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class MockSignatureInfoProvider extends Mock implements SignatureInfoProvider {}

///
/// 签名需要调用MethodChannel获取加密结果，这里只对签名前的源数据进行测试
///
void main() {
  final timeStamp = "1579491340130";
  final platform = "iOS";
  final deviceId = "CE069007-6881-475E-AB8B-C77A4A19AACF-1571802129";
  final pt = "9f89c84a559f573636a47ff8daed0d33";
  RequestOptions options;
  SignatureInterceptor interceptor;
  MockSignatureInfoProvider provider;

  setUp(() {
    provider = MockSignatureInfoProvider();
    interceptor = SignatureInterceptor(provider);
    options = RequestOptions()
      ..headers = {
        "X-TimeStamp": timeStamp,
        "X-Platform": platform,
        "X-DeviceID": deviceId,
        "_pt": pt
      };
  });

  tearDown(() {
    options.headers.remove("X-Signature");
    reset(provider);
  });

  group("test GET request", () {
    setUp(() {
      options.method = "GET";
    });
    tearDown(() {
      reset(provider);
    });
    test("test GET without query", () async {
      options.path = "/v1/reappserv/jrpass/search/basic_info";
      when(provider.provideSignature(
              "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvYmFzaWNfaW5mbw=="))
          .thenAnswer((_) => Future.value("fake result"));
      final RequestOptions newOptions = await interceptor.onRequest(options);
      expect(newOptions.headers["X-Signature"], "fake result");
    });
    test("test GET with query", () async {
      options.queryParameters = {"word": "w"};
      options.queryParameters['_t'] = timeStamp;
      options.path = "/v1/reappserv/jrpass/search/suggest";
      when(provider.provideSignature(
              "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD93b3JkPXcmX3Q9MTU3OTQ5MTM0MDEzMA=="))
          .thenAnswer((_) => Future.value("fake result"));
      final RequestOptions newOptions = await interceptor.onRequest(options);
      expect(newOptions.headers["X-Signature"], "fake result");
    });
  });

  group("test POST request", () {
    setUp(() {
      options.method = "POST";
    });
    tearDown(() {
      reset(provider);
    });
    test("test POST without body", () async {
      options
        ..queryParameters = {'_t': timeStamp}
        ..path = "/v1/reappserv/jrpass/home";
      when(provider.provideSignature(
              "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9ob21lP190PTE1Nzk0OTEzNDAxMzA="))
          .thenAnswer((_) => Future.value("fake result"));
      final RequestOptions newOptions = await interceptor.onRequest(options);
      expect(newOptions.headers["X-Signature"], "fake result");
    });
    test("test POST with form body", () async {
      options.queryParameters = {'_t': timeStamp};
      options.path = "/v1/reappserv/jrpass/search/suggest";
      options.data = {"word": "w"};
      options.contentType = Headers.formUrlEncodedContentType;
      when(provider.provideSignature(
              "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMwd29yZD13"))
          .thenAnswer((_) => Future.value("fake result"));
      final RequestOptions newOptions = await interceptor.onRequest(options);
      expect(newOptions.headers["X-Signature"], "fake result");
    });
    test("test POST with json body", () async {
      options.path = "/v1/reappserv/jrpass/home/category";
      options.queryParameters = {'_t': timeStamp};
      options.data = {"category_id": "2"};
      options.contentType = Headers.jsonContentType;
      when(provider.provideSignature(
              "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9ob21lL2NhdGVnb3J5P190PTE1Nzk0OTEzNDAxMzB7ImNhdGVnb3J5X2lkIjoiMiJ9"))
          .thenAnswer((_) => Future.value("fake result"));
      final RequestOptions newOptions = await interceptor.onRequest(options);
      expect(newOptions.headers["X-Signature"], "fake result");
    });
    test("test POST with large body", () async {
      options.path = "/v1/reappserv/jrpass/search/suggest";
      options.queryParameters = {'_t': timeStamp};
      options.data = {
        "category_id": "2",
        "dump":
            "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJ3b3JkIjoidyJ9"
      };
      options.contentType = Headers.jsonContentType;
      when(provider.provideSignature(
              "MTU3OTQ5MTM0MDEzMGlPU0NFMDY5MDA3LTY4ODEtNDc1RS1BQjhCLUM3N0E0QTE5QUFDRi0xNTcxODAyMTI5OWY4OWM4NGE1NTlmNTczNjM2YTQ3ZmY4ZGFlZDBkMzMvdjEvcmVhcHBzZXJ2L2pycGFzcy9zZWFyY2gvc3VnZ2VzdD9fdD0xNTc5NDkxMzQwMTMweyJjYXRlZ29yeV9pZCI6IjIiLCJkdW1wIjoiTVRVM09UUTVNVE0wTURFek1HbFBVME5GTURZNU1EQTNMVFk0T0RFdE5EYzFSUzFCUWpoQ0xVTTNOMEUwUVRFNVFVRkRSaTB4TlRjeE9EQXlNVEk1T1dZNE9XTTROR0UxTlRsbU5UY3pOak0yWVRRM1ptWTRaR0ZsWkRCa016TXZkakV2Y21WaGNIQnpaWEoyTDJweWNHRnpjeTl6WldGeVkyZ3ZjM1ZuWjJWemREOWZkRDB4TlRjNU5Ea3hNelF3TVRNd2V5SjNiM0prSWpvaWR5SjlNVFUzT1RRNU1UTTBNREV6TUdsUFUwTkZNRFk1TURBM0xUWTRPREV0TkRjMVJTMUJRamhDTFVNM04wRTBRVEU1UVVGRFJpMHhOVGN4T0RBeU1USTVPV1k0T1dNNE5HRTFOVGxtTlRjek5qTTJZVFEzWm1ZNFpHRmxaREJrTXpNdmRqRXZjbVZoY0hCelpYSjJMMnB5Y0dGemN5OXpaV0Z5WTJndmMzVm5aMlZ6ZEQ5ZmREMHhOVGM1TkRreE16UXdNVE13ZXlKM2IzSmtJam9pZHlKOU1UVTNPVFE1TVRNME1ERXpNR2xQVTBORk1EWTVNREEzTFRZNE9ERXRORGMxUlMxQlFqaENMVU0zTjBFMFFURTVRVUZEUmkweE5UY3hPREF5TVRJNU9XWTRPV000TkdFMU5UbG1OVGN6TmpNMllUUTNabVk0WkdGbFpEQmtNek12ZGpFdmNtVmhjSEJ6WlhKMkwycHljR0Z6Y3k5elpXRnlZMmd2YzNWbloyVnpkRDlmZEQweE5UYzVORGt4TXpRd01UTXdleUozYjNKa0lqb2lkeUo5TVRVM09UUTVNVE0wTURFek1HbFBVME5GTURZNU1EQTNMVFk0T0RFdE5EYzFSUzFCUWpoQ0xVTTNOMEUwUVRFNVFVRkRSaTB4TlRjeE9EQXlNVEk1T1dZNE9XTTROR0UxTlRsbU5UY3pOak0yWVRRM1ptWTRaR0ZsWkRCa016TXZkakV2Y21WaGNIQnpaWEoyTDJweWNHRnpjeTl6WldGeVkyZ3ZjM1ZuWjJWemREOWZkRDB4TlRjNU5Ea3hNelF3TVRNd2V5SjNiM0prSWpvaWR5SjlNVFUzT1RRNU1UTTBNREV6TUdsUFUwTkZNRFk1TURBM0xUWTRPREV0TkRjMVJTMUJRamhDTFVNM04wRTBRVEU1UVVGRFJpMHhOVGN4T0RBeU1USTVPV1k0T1dNNE5HRTFOVGxtTlRjek5qTTJZVFEzWm1ZNFpHRmxaREJrTXpNdmRqRXZjbVZoY0hCelpYSjJMMnB5Yw=="))
          .thenAnswer((_) => Future.value("fake result"));
      final RequestOptions newOptions = await interceptor.onRequest(options);
      expect(newOptions.headers["X-Signature"], "fake result");
    });
  });
}
