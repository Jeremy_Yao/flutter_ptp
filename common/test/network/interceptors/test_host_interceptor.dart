// TODO(littlegnal): Make this more generic, and move to common test kit in the future.
import 'package:dio/dio.dart';

class TestHostInterceptor extends Interceptor {
  TestHostInterceptor(this._baseUrl);
  final String _baseUrl;
  @override
  Future onRequest(RequestOptions options) async {
    options.baseUrl = _baseUrl;
    return options;
  }
}
