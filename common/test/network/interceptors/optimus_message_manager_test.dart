import 'package:common/src/network/interceptors/optimus_message_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:uuid/uuid.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  final optimusMessageManager = OptimusMessageManager();

  test(
    'test manager',
    () {
      final requestId = Uuid().v4().substring(0, 8);

      optimusMessageManager.enqueue(requestId, const RequestInfo());
      expect(optimusMessageManager.pendingRequests,
          (Map<String, RequestInfo> map) => map.containsKey(requestId));

      expect(() {
        optimusMessageManager.enqueue(requestId, const RequestInfo());
      }, throwsAssertionError);

      optimusMessageManager.requestFinished(
          requestId, ResponseInfo.error(null, null, null));
      expect(optimusMessageManager.pendingRequests,
          (Map<String, RequestInfo> map) => !map.containsKey(requestId));

      expect(() {
        optimusMessageManager.requestFinished(
            requestId, ResponseInfo.error(null, null, null));
      }, throwsAssertionError);
    },
    // TODO(zhiwei): Fix it later
    skip: true,
  );
}
