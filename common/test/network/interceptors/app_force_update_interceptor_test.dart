import 'dart:convert';

import 'package:astronomia/astronomia.dart';
import 'package:common/common.dart';
import 'package:common/common_navigator.dart';

import 'package:common/src/network/interceptors/app_force_update_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class _MockNativeEventChannel extends Mock implements NativeEventChannel {}

class _MockAppLocalications extends Mock implements AppLocalizations {}

void main() {
  CommonNavigator commonNavigator;
  _MockNativeEventChannel nativeEventChannel;
  _MockAppLocalications appLocalications;

  setUp(() async {
    commonNavigator = CommonNavigator(DefaultCommonNavigatorDelegate());
    appLocalications = _MockAppLocalications();
    nativeEventChannel = _MockNativeEventChannel();

    when(appLocalications.text('time_to_upgrade'))
        .thenReturn('The New Version');
    when(appLocalications.text('app_upgrade_later')).thenReturn('SKIP');
    when(appLocalications.text('app_upgrade_update_now')).thenReturn('UPDATE');
  });

  tearDown(() async {
    commonNavigator = null;
    appLocalications = null;
    nativeEventChannel = null;
  });

  Future<void> _pumpEntryPointApp(WidgetTester tester) {
    final navigatorObserverDelegate = AstronomiaNavigatorObserverDelegate();
    return tester.pumpWidget(MultiProvider(
      providers: [
        Provider<CommonNavigator>.value(
          value: commonNavigator,
        )
      ],
      child: MaterialApp(
        // TODO(littlegnal): Remove this when this issuse fixed:
        // https://bitbucket.org/klook/klook-app-flutter/issues/7/astronomia-unsupported-operation-cannot
        // ignore: prefer_const_literals_to_create_immutables
        navigatorObservers: [],
        builder: AstronomiaNavigator.builder(
            astronomiaNavigatorKey: commonNavigator.astronomiaNavigatorKey,
            navigatorObserverDelegate: navigatorObserverDelegate),
        onGenerateRoute: (settings) {
          return MaterialPageRoute(builder: (context) {
            if (settings.name == '/show_force_update') {
              return const Text('Show force update dialog');
            }
            if (settings.name == '/show_recommand_update') {
              return const Text('Show recommand update dialog page');
            }
            return _HomePage();
          });
        },
      ),
    ));
  }

  group('AppForceUpdateInterceptor show nothing', () {
    testWidgets("should not show any dialog", (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      // TODO(littlegnal): Make it more easy to mock in the future.
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show force update'));
      await tester.pumpAndSettle();

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      appForceUpdateInterceptor.onResponse(
        Response(),
      );

      await tester.pumpAndSettle();

      expect(find.text('The New Version'), findsNothing);
      expect(find.text('Show force update dialog'), findsOneWidget);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });
  });

  group('AppForceUpdateInterceptor show force update dialog', () {
    testWidgets("click skip", (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      // TODO(littlegnal): Make it more easy to mock in the future.
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show force update'));
      await tester.pumpAndSettle();

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      appForceUpdateInterceptor.onResponse(
        Response(
          headers: Headers.fromMap({
            'force_update': ['true'],
          }),
        ),
      );

      await tester.pumpAndSettle();

      expect(find.text('The New Version'), findsOneWidget);

      await tester.tap(find.text('SKIP'));
      await tester.pumpAndSettle();

      expect(find.byType(_HomePage), findsOneWidget);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });

    testWidgets("click update", (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show force update'));
      await tester.pumpAndSettle();

      SharedPreferences.setMockInitialValues({});

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      appForceUpdateInterceptor.onResponse(
        Response(
          headers: Headers.fromMap({
            'force_update': ['true'],
          }),
        ),
      );

      await tester.pumpAndSettle();

      // Verify force update dialog is showed.
      expect(find.text('The New Version'), findsOneWidget);

      expect(find.text('UPDATE'), findsOneWidget);
      await tester.tap(find.text('UPDATE'));

      await tester.pumpAndSettle();

      // Verify sent the event to native.
      verify(nativeEventChannel.sendEvent('event_update_app')).called(1);

      // Expect dialog not be closed.
      expect(find.text('The New Version'), findsOneWidget);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });
  });

  group('AppForceUpdateInterceptor show recommand update dialog', () {
    testWidgets("click skip", (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show recommand update'));
      await tester.pumpAndSettle();

      SharedPreferences.setMockInitialValues({});

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      final mockNow = DateTime(2020, 9, 3, 17, 3);
      appForceUpdateInterceptor.now = () => mockNow;

      appForceUpdateInterceptor.onResponse(
        Response(
            headers: Headers.fromMap({
              'recommend_update': ['true'],
            }),
            request: RequestOptions(
              baseUrl: 'https://bbb.com',
              path: '/show_recommend_update',
            )),
      );

      await tester.pumpAndSettle();

      // Verify recommend update dialog is showed.
      expect(find.text('The New Version'), findsOneWidget);

      await tester.tap(find.text('SKIP'));

      await tester.pumpAndSettle();

      final sharedPreferences = await SharedPreferences.getInstance();
      final expectedKey = 'https://bbb.com/show_recommend_update';
      expect(
          sharedPreferences
              .containsKey(skipRecommandUpdateTimestampPreferencesKey),
          true);
      final timeStampMap = Map.from(jsonDecode(
          sharedPreferences.get(skipRecommandUpdateTimestampPreferencesKey)));
      expect(timeStampMap.length, 1);
      expect(timeStampMap[expectedKey], mockNow.millisecondsSinceEpoch);

      expect(find.text('Show recommand update dialog page'), findsOneWidget);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });

    testWidgets("should show dialog after 7 days", (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show recommand update'));
      await tester.pumpAndSettle();

      final mockNow = DateTime(2020, 9, 3, 17, 3);

      SharedPreferences.setMockInitialValues({
        skipRecommandUpdateTimestampPreferencesKey: jsonEncode({
          'https://bbb.com/show_recommend_update':
              DateTime(2020, 8, 3, 12, 3).millisecondsSinceEpoch,
        }),
      });

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      appForceUpdateInterceptor.now = () => mockNow;

      appForceUpdateInterceptor.onResponse(
        Response(
            headers: Headers.fromMap({
              'recommend_update': ['true'],
            }),
            request: RequestOptions(
              baseUrl: 'https://bbb.com',
              path: '/show_recommend_update',
            )),
      );

      await tester.pumpAndSettle();

      // Verify recommend update dialog is showed.
      expect(find.text('The New Version'), findsOneWidget);

      await tester.tap(find.text('SKIP'));

      await tester.pumpAndSettle();

      final sharedPreferences = await SharedPreferences.getInstance();
      final expectedKey = 'https://bbb.com/show_recommend_update';
      expect(
          sharedPreferences
              .containsKey(skipRecommandUpdateTimestampPreferencesKey),
          true);
      final timeStampMap = Map.from(jsonDecode(
          sharedPreferences.get(skipRecommandUpdateTimestampPreferencesKey)));
      expect(timeStampMap.length, 1);
      expect(timeStampMap[expectedKey], mockNow.millisecondsSinceEpoch);

      expect(find.text('Show recommand update dialog page'), findsOneWidget);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });

    testWidgets("should not show dialog", (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show recommand update'));
      await tester.pumpAndSettle();

      final mockNow = DateTime(2020, 9, 3, 17, 3);

      SharedPreferences.setMockInitialValues({
        skipRecommandUpdateTimestampPreferencesKey: jsonEncode({
          'https://bbb.com/show_recommend_update':
              DateTime(2020, 9, 3, 12, 3).millisecondsSinceEpoch,
        }),
      });

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      appForceUpdateInterceptor.now = () => mockNow;

      appForceUpdateInterceptor.onResponse(
        Response(
            headers: Headers.fromMap({
              'recommend_update': ['true'],
            }),
            request: RequestOptions(
              baseUrl: 'https://bbb.com',
              path: '/show_recommend_update',
            )),
      );

      await tester.pumpAndSettle();

      // Verify recommend update dialog is showed.
      expect(find.text('The New Version'), findsNothing);

      expect(find.text('Show recommand update dialog page'), findsOneWidget);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });

    testWidgets("click update when the dialog show again after 7 days",
        (WidgetTester tester) async {
      await _pumpEntryPointApp(tester);
      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler((call) => null);

      await tester.tap(find.text('Show recommand update'));
      await tester.pumpAndSettle();

      final mockNow = DateTime(2020, 9, 3, 17, 3);

      SharedPreferences.setMockInitialValues({
        skipRecommandUpdateTimestampPreferencesKey: jsonEncode({
          'https://bbb.com/show_recommend_update':
              DateTime(2020, 8, 3, 12, 3).millisecondsSinceEpoch,
        }),
      });

      final appForceUpdateInterceptor = AppForceUpdateInterceptor(
        commonNavigator,
        nativeEventChannel,
        appLocalications,
      );

      appForceUpdateInterceptor.now = () => mockNow;

      appForceUpdateInterceptor.onResponse(
        Response(
          headers: Headers.fromMap({
            'recommend_update': ['true'],
          }),
          request: RequestOptions(
            baseUrl: 'https://bbb.com',
            path: '/show_recommend_update',
          ),
        ),
      );

      await tester.pumpAndSettle();

      // Verify force update dialog is showed.
      expect(find.text('The New Version'), findsOneWidget);

      expect(find.text('UPDATE'), findsOneWidget);
      await tester.tap(find.text('UPDATE'));

      await tester.pumpAndSettle();

      verify(nativeEventChannel.sendEvent('event_update_app')).called(1);

      // Expect dialog not be closed.
      expect(find.text('The New Version'), findsOneWidget);

      expect((await SharedPreferences.getInstance()).getKeys(), []);

      commonNavigator
          .astronomiaNavigatorKey.currentState.routeChannel.astronomiaChannel
          .setMockMethodCallHandler(null);
    });
  });
}

class _HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {
            context.read<CommonNavigator>().pushNamed('/show_force_update');
          },
          child: const Text('Show force update'),
        ),
        ElevatedButton(
          onPressed: () {
            context.read<CommonNavigator>().pushNamed('/show_recommand_update');
          },
          child: const Text('Show recommand update'),
        ),
      ],
    );
  }
}
