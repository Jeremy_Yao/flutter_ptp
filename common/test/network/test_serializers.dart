import 'package:built_value/serializer.dart';

import 'test_action.dart';

part 'test_serializers.g.dart';

@SerializersFor([TestAction])
final Serializers serializers = _$serializers;
