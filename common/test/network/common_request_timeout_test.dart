import 'dart:convert';

import 'package:common/common.dart';
import 'package:dio/dio.dart';
import 'package:mock_web_server/mock_web_server.dart';
import 'package:test/test.dart';

import 'interceptors/test_host_interceptor.dart';
import 'test_action.dart';
import 'test_serializers.dart';

void main() {
  CommonRequest commonRequest;

  MockWebServer server = MockWebServer();

  setUp(() async {
    // TODO(littlegnal): Make this more generic, and move to common test kit in the future.
    await server.start();
    final url = Uri.parse('http://localhost:${server.port}').toString();
    commonRequest = CommonRequest(
        HTTP(
            baseOptions: BaseOptions(
          connectTimeout: 1000,
        )),
        [TestHostInterceptor(url)],
        Serialization(serializers.toStandardJsonSerializers()));
  });

  tearDown(() async {
    await server.shutdown();
    server = null;
  });

  test('timeout exception', () async {
    final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
    final response = MockResponse()
      ..httpCode = 200
      ..headers = const {"Content-Type": "application/json"}
      ..body = Response(data: jsonDecode(rs))
      ..delay = const Duration(seconds: 2);

    server.enqueueResponse(response);

    try {
      await commonRequest.get<TestAction>('/timeout').asFuture();
    } catch (e) {
      expect(e, const TypeMatcher<TimeoutException>());
    }
  });
}
