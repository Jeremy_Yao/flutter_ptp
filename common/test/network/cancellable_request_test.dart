import 'package:common/common.dart';
import 'package:common/src/network/cancellable_request.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("CancellableRequestImpl", () {
    test("fromRequest()", () async {
      final cancellableRequest =
          CancellableRequestImpl.fromRequest(Future.value(10));
      expect(await cancellableRequest.asFuture(), 10);
    });

    test("then()", () async {
      final cancellableRequest =
          CancellableRequestImpl.fromRequest(Future.value(10))
              .then((_) => Future.value("10"));
      expect(await cancellableRequest.asFuture(), "10");
    });

    test("addToCompositeCancellable()", () async {
      final CompositeCancellable compositeCancellable = CompositeCancellable();
      final cancellableRequest =
          CancellableRequestImpl.fromRequest(Future.value(10))
              .addToCompositeCancellable(compositeCancellable);
      compositeCancellable.cancel();
      try {
        await cancellableRequest.asFuture();
      } catch (e) {
        expect(e, CancelHttpException());
      }
    });

    // TODO(littlegnal): This is not a valid use case, which had comment on the addToCompositeCancellable,
    // so we temporarily comment out this test case.
    // test("invoke addToCompositeCancellable() before then()", () async {
    //   final CompositeCancellable compositeCancellable = CompositeCancellable();
    //   final cancellableRequest =
    //       CancellableRequestImpl.fromRequest(Future.value(10))
    //           .addToCompositeCancellable(compositeCancellable)
    //           .then((_) => Future.value("10"));
    //   compositeCancellable.cancel();
    //   Exception error;
    //   try {
    //     await cancellableRequest.asFuture();
    //   } catch (e) {
    //     error = e;
    //   }

    //   expect(error, null);
    // });

    test("invoke addToCompositeCancellable() after then()", () async {
      final CompositeCancellable compositeCancellable = CompositeCancellable();
      final cancellableRequest =
          CancellableRequestImpl.fromRequest(Future.value(10))
              .then((_) => Future.value("10"))
              .addToCompositeCancellable(compositeCancellable);
      compositeCancellable.cancel();
      Exception error;
      try {
        await cancellableRequest.asFuture();
      } catch (e) {
        error = e;
      }

      expect(error, CancelHttpException());
    });
  });
}
