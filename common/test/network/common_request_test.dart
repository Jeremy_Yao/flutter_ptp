import 'dart:convert';

import 'package:common/common.dart';
import 'package:dio/dio.dart';
import 'package:test/test.dart';

import 'test_action.dart';
import '../mock/test_common_request.dart';
import 'test_serializers.dart';

class _TestCommonRequestException implements Exception {}

class _TestCommonRequestExceptionWithErrorData implements Exception {}

class _TestErrorHandler implements HttpRequestErrorHandler {
  @override
  Exception onError(Exception exception, {Map<dynamic, dynamic> errorData}) {
    if (errorData != null) {
      return _TestCommonRequestExceptionWithErrorData();
    }
    return _TestCommonRequestException();
  }
}

class _TestEmptyErrorHandler implements HttpRequestErrorHandler {
  @override
  Exception onError(Exception exception, {Map<dynamic, dynamic> errorData}) =>
      null;
}

void main() {
  TestCommonRequest commonRequest;

  setUp(() {
    commonRequest = TestCommonRequest(
        Serialization(serializers.toStandardJsonSerializers()));
  });

  tearDown(() {
    commonRequest = null;
  });

  group("CommonRequest serialization", () {
    test("commonGet serialize Action object", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      final expectAction = TestAction((b) => b
        ..path = "/path"
        ..type = "deeplink"
        ..extra = "args");

      // TODO(littlegnal): Replace by CancellableRequest.
      // ignore: deprecated_member_use_from_same_package
      expect(await commonRequest.commonGet<TestAction>("/fake"), expectAction);
    });

    test("commonPost serialize Action object", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      final expectAction = TestAction((b) => b
        ..path = "/path"
        ..type = "deeplink"
        ..extra = "args");

      // TODO(littlegnal): Replace by CancellableRequest
      // ignore: deprecated_member_use_from_same_package
      expect(await commonRequest.commonPost<TestAction>("/fake"), expectAction);
    });
  });

  group(" CommonRequest error handling", () {
    test("The response should throw HttpError", () async {
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        // TODO(littlegnal): Replace by CancellableRequest
        // ignore: deprecated_member_use_from_same_package
        await commonRequest.commonGet<TestAction>("/fake");
      } catch (e) {
        expect(e, const TypeMatcher<HttpError>());
      }
    });

    test("The response should throw BusinessLogicError", () async {
      final rs = """
    {
        "error": {
          "code": "9999",
          "message": "error of 9999"
        },
        "result": {
          "path": "/path",
          "type": "deeplink",
          "extra": "args"
        },
        "success": false
    }
    """;
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));
      try {
        // TODO(littlegnal): Replace by CancellableRequest
        // ignore: deprecated_member_use_from_same_package
        await commonRequest.commonGet<TestAction>("/fake");
      } catch (e) {
        expect(e, const TypeMatcher<BusinessLogicError>());
      }
    });
  });

  group("CommonRequest with CancellableRequest", () {
    test("call get() function", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      final expectAction = TestAction((b) => b
        ..path = "/path"
        ..type = "deeplink"
        ..extra = "args");

      expect(await commonRequest.get<TestAction>("/fake").asFuture(),
          expectAction);
    });

    test("cancel get() function", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest.stubGet("/fake").thenAnswer((_) => Future.delayed(
          const Duration(milliseconds: 10),
          () => Response(data: jsonDecode(rs))));

      final cancellableRequest = commonRequest.get<TestAction>("/fake");
      cancellableRequest.cancel();
      try {
        await cancellableRequest.asFuture();
      } catch (e) {
        expect(e, CancelHttpException(path: "/fake"));
      }
    });

    test("call post() function", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      final expectAction = TestAction((b) => b
        ..path = "/path"
        ..type = "deeplink"
        ..extra = "args");

      expect(await commonRequest.post<TestAction>("/fake").asFuture(),
          expectAction);
    });

    test("cancel post() function", () async {
      final rs = """
      {
          "error": {
            "code": "",
            "message": ""
          },
          "result": {
            "path": "/path",
            "type": "deeplink",
            "extra": "args"
          },
          "success": true
      }
      """;
      commonRequest.stubPost("/fake").thenAnswer((_) => Future.delayed(
          const Duration(milliseconds: 10),
          () => Response(data: jsonDecode(rs))));

      final cancellableRequest = commonRequest.post<TestAction>("/fake");
      cancellableRequest.cancel();
      try {
        await cancellableRequest.asFuture();
      } catch (e) {
        expect(e, CancelHttpException(path: "/fake"));
      }
    });
  });

  group("CommonRequest with CancellableRequest error handling", () {
    test("call get() throw HttpError", () async {
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        await commonRequest.get<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<HttpError>());
      }
    });

    test("call get() throw BusinessLogicError", () async {
      final rs = """
    {
        "error": {
          "code": "9999",
          "message": "error of 9999"
        },
        "result": {
          "path": "/path",
          "type": "deeplink",
          "extra": "args"
        },
        "success": false
    }
    """;
      commonRequest
          .stubGet("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));
      try {
        await commonRequest.get<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<BusinessLogicError>());
      }
    });

    test("call post() throw HttpError", () async {
      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<HttpError>());
      }
    });

    test("call post() throw BusinessLogicError", () async {
      final rs = """
    {
        "error": {
          "code": "9999",
          "message": "error of 9999"
        },
        "result": {
          "path": "/path",
          "type": "deeplink",
          "extra": "args"
        },
        "success": false
    }
    """;
      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));
      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<BusinessLogicError>());
      }
    });
  });

  group("CommonRequest with HttpRequestErrorHandler", () {
    test("re-throw exception", () async {
      commonRequest = TestCommonRequest(
          Serialization(serializers.toStandardJsonSerializers()),
          httpRequestErrorHandler: _TestErrorHandler());

      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<_TestCommonRequestException>());
      }
    });

    test("re-throw exception with errorData", () async {
      commonRequest = TestCommonRequest(
          Serialization(serializers.toStandardJsonSerializers()),
          httpRequestErrorHandler: _TestErrorHandler());

      final rs = """
    {
        "error": {
          "code": "9999",
          "message": "error of 9999"
        },
        "result": {
          "path": "/path",
          "type": "deeplink",
          "extra": "args"
        },
        "success": false
    }
    """;
      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(
            e, const TypeMatcher<_TestCommonRequestExceptionWithErrorData>());
      }
    });
  });

  group("CommonRequest with HttpRequestErrorHandlers", () {
    test("re-throw by first HttpRequestErrorHandler", () async {
      commonRequest = TestCommonRequest(
          Serialization(serializers.toStandardJsonSerializers()),
          httpRequestErrorHandler: HttpRequestErrorHandlers(
              [_TestErrorHandler(), _TestEmptyErrorHandler()]));

      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<_TestCommonRequestException>());
      }
    });

    test("re-throw by second HttpRequestErrorHandler", () async {
      commonRequest = TestCommonRequest(
          Serialization(serializers.toStandardJsonSerializers()),
          httpRequestErrorHandler: HttpRequestErrorHandlers(
              [_TestEmptyErrorHandler(), _TestErrorHandler()]));

      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.error(DioError()));
      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(e, const TypeMatcher<_TestCommonRequestException>());
      }
    });

    test("re-throw by second HttpRequestErrorHandler with errorData", () async {
      commonRequest = TestCommonRequest(
          Serialization(serializers.toStandardJsonSerializers()),
          httpRequestErrorHandler: HttpRequestErrorHandlers(
              [_TestEmptyErrorHandler(), _TestErrorHandler()]));

      final rs = """
    {
        "error": {
          "code": "9999",
          "message": "error of 9999"
        },
        "result": {
          "path": "/path",
          "type": "deeplink",
          "extra": "args"
        },
        "success": false
    }
    """;
      commonRequest
          .stubPost("/fake")
          .thenAnswer((_) => Future.value(Response(data: jsonDecode(rs))));

      try {
        await commonRequest.post<TestAction>("/fake").asFuture();
      } catch (e) {
        expect(
            e, const TypeMatcher<_TestCommonRequestExceptionWithErrorData>());
      }
    });
  });
}
