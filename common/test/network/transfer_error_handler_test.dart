import 'dart:convert';

import 'package:common/src/network/common_request_exception.dart';
import 'package:common/src/network/transfer_error_handler.dart';
import 'package:test/test.dart';

void main() {
  TransferErrorHandler transferErrorHandler;

  setUp(() {
    transferErrorHandler = TransferErrorHandler();
  });

  group("TransferErrorHandler", () {
    test("TransferBusinessLogicException", () {
      final rs = '''
      {
        "type": 2,
        "message": "载入失败，请稍候重试 (Error ID: 29113002)",
        "hiddenCode": "29113002",
        "hiddenMessage": "载入失败，请稍候重试 (Error ID: 29113002)",
        "code": "29113002"
      }
      ''';
      final expectedException = TransferBusinessLogicException(
          "29113002",
          "载入失败，请稍候重试 (Error ID: 29113002)",
          "2",
          "29113002",
          "载入失败，请稍候重试 (Error ID: 29113002)");
      expect(
          transferErrorHandler.onError(const BusinessLogicError("0", ""),
              errorData: jsonDecode(rs)),
          expectedException);
    });
  });
}
