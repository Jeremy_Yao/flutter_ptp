import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class _MockCommonNavigator extends Mock implements CommonNavigator {}

void main() {
  LoginErrorHandler loginErrorHandler;

  setUp(() {
    loginErrorHandler = LoginErrorHandler(_MockCommonNavigator());
  });

  group("LoginErrorHandler", () {
    test("unLogin", () {
      expect(
          loginErrorHandler
              .onError(const BusinessLogicError(LoginErrorHandler.unLogin, "")),
          const TypeMatcher<UnauthorizedException>());
    });

    test("tokenExpired", () {
      expect(
          loginErrorHandler.onError(
              const BusinessLogicError(LoginErrorHandler.tokenExpired, "")),
          const TypeMatcher<UnauthorizedException>());
    });
  });
}
