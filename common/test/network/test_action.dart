import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'test_action.g.dart';

abstract class TestAction implements Built<TestAction, TestActionBuilder> {
  factory TestAction([updates(TestActionBuilder b)]) = _$TestAction;
  TestAction._();

  @BuiltValueField(wireName: 'path')
  String get path;
  @BuiltValueField(wireName: 'type')
  String get type;
  @BuiltValueField(wireName: 'extra')
  String get extra;

  static Serializer<TestAction> get serializer => _$testActionSerializer;
}
