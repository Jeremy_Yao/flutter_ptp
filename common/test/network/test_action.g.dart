// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_action.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TestAction> _$testActionSerializer = new _$TestActionSerializer();

class _$TestActionSerializer implements StructuredSerializer<TestAction> {
  @override
  final Iterable<Type> types = const [TestAction, _$TestAction];
  @override
  final String wireName = 'TestAction';

  @override
  Iterable<Object> serialize(Serializers serializers, TestAction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'path',
      serializers.serialize(object.path, specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'extra',
      serializers.serialize(object.extra,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TestAction deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TestActionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'path':
          result.path = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'extra':
          result.extra = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TestAction extends TestAction {
  @override
  final String path;
  @override
  final String type;
  @override
  final String extra;

  factory _$TestAction([void Function(TestActionBuilder) updates]) =>
      (new TestActionBuilder()..update(updates)).build();

  _$TestAction._({this.path, this.type, this.extra}) : super._() {
    if (path == null) {
      throw new BuiltValueNullFieldError('TestAction', 'path');
    }
    if (type == null) {
      throw new BuiltValueNullFieldError('TestAction', 'type');
    }
    if (extra == null) {
      throw new BuiltValueNullFieldError('TestAction', 'extra');
    }
  }

  @override
  TestAction rebuild(void Function(TestActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TestActionBuilder toBuilder() => new TestActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TestAction &&
        path == other.path &&
        type == other.type &&
        extra == other.extra;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, path.hashCode), type.hashCode), extra.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TestAction')
          ..add('path', path)
          ..add('type', type)
          ..add('extra', extra))
        .toString();
  }
}

class TestActionBuilder implements Builder<TestAction, TestActionBuilder> {
  _$TestAction _$v;

  String _path;
  String get path => _$this._path;
  set path(String path) => _$this._path = path;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _extra;
  String get extra => _$this._extra;
  set extra(String extra) => _$this._extra = extra;

  TestActionBuilder();

  TestActionBuilder get _$this {
    if (_$v != null) {
      _path = _$v.path;
      _type = _$v.type;
      _extra = _$v.extra;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TestAction other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TestAction;
  }

  @override
  void update(void Function(TestActionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TestAction build() {
    final _$result =
        _$v ?? new _$TestAction._(path: path, type: type, extra: extra);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
