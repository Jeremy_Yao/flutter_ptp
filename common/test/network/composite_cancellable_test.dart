import 'package:common/src/network/composite_cancellable.dart';
import 'package:common/src/protocol/cancellable.dart';
import 'package:flutter_test/flutter_test.dart';

class _TestCancellable implements Cancellable {
  bool isCancelled = false;

  @override
  void cancel() {
    isCancelled = true;
  }
}

void main() {
  group("CompositeCancellable", () {
    test("add a Cancellable", () {
      final compositeCancellable = CompositeCancellable();
      final testCancellable1 = _TestCancellable();
      final testCancellable2 = _TestCancellable();
      expect(testCancellable1.isCancelled, false);
      expect(testCancellable2.isCancelled, false);

      compositeCancellable.add(testCancellable1);
      compositeCancellable.add(testCancellable2);
      expect(compositeCancellable.cancellableList.length, 2);

      compositeCancellable.cancel();
      expect(testCancellable1.isCancelled, true);
      expect(testCancellable2.isCancelled, true);
      expect(compositeCancellable.cancellableList.length, 0);
    });

    test("add a Cancellable by using operator +", () {
      var compositeCancellable = CompositeCancellable();
      final testCancellable1 = _TestCancellable();
      final testCancellable2 = _TestCancellable();
      expect(testCancellable1.isCancelled, false);
      expect(testCancellable2.isCancelled, false);

      compositeCancellable = compositeCancellable + testCancellable1;
      compositeCancellable = compositeCancellable + testCancellable2;
      expect(compositeCancellable.cancellableList.length, 2);

      compositeCancellable.cancel();
      expect(testCancellable1.isCancelled, true);
      expect(testCancellable2.isCancelled, true);
      expect(compositeCancellable.cancellableList.length, 0);
    });

    test("add a Cancellable by using operator +=", () {
      var compositeCancellable = CompositeCancellable();
      final testCancellable1 = _TestCancellable();
      final testCancellable2 = _TestCancellable();
      expect(testCancellable1.isCancelled, false);
      expect(testCancellable2.isCancelled, false);

      compositeCancellable += testCancellable1;
      compositeCancellable += testCancellable2;
      expect(compositeCancellable.cancellableList.length, 2);

      compositeCancellable.cancel();
      expect(testCancellable1.isCancelled, true);
      expect(testCancellable2.isCancelled, true);
      expect(compositeCancellable.cancellableList.length, 0);
    });
  });
}
