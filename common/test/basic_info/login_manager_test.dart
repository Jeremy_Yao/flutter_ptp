import 'package:common/src/basic_info/login_manager.dart';
import 'package:common/src/basic_info/model/user_info.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import '../mock/mock_native_info_manager.dart';

void main() {
  MockNativeInfoManager mockNativeInfoManager;
  LoginManager loginManager;

  setUp(() {
    mockNativeInfoManager = MockNativeInfoManager();
    loginManager = LoginManager(mockNativeInfoManager);
  });

  group("LoginManager", () {
    test("isLogin() true", () async {
      // Return "token" in map to simulate there's token returned
      when(mockNativeInfoManager.provideUserInfo()).thenAnswer(
          (_) => Future.value(UserInfo.fromJson({"token": "1234"})));
      expect(await loginManager.isLogin(), true);

      verify(mockNativeInfoManager.provideUserInfo()).called(1);
    });

    test("isLogin() false", () async {
      // Return empty map to simulate there's no token returned
      when(mockNativeInfoManager.provideUserInfo())
          .thenAnswer((_) => Future.value(UserInfo.fromJson({})));
      expect(await loginManager.isLogin(), false);

      verify(mockNativeInfoManager.provideUserInfo()).called(1);
    });

    test("isLogin() false with empty token string", () async {
      // Return "token" with empty value in map to simulate token is empty returned
      when(mockNativeInfoManager.provideUserInfo())
          .thenAnswer((_) => Future.value(UserInfo.fromJson({"token": ""})));
      expect(await loginManager.isLogin(), false);

      verify(mockNativeInfoManager.provideUserInfo()).called(1);
    });
  });
}
