import 'package:common/common.dart';
import 'package:common/src/basic_info/model/app_info.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:l10n_cms/l10n_cms.dart';
import 'package:mockito/mockito.dart';

class _MockNativeInfoManager extends Mock implements NativeInfoManager {}

class _MockGlobalL10NCMSLocalizationsLookup extends Mock
    implements GlobalL10NCMSLocalizationsLookup {}

class _MockGlobalL10NCMSLocalizations extends Mock
    implements GlobalL10NCMSLocalizations {}

void main() {
  NativeInfoManager nativeInfoManager;
  GlobalL10NCMSLocalizationsLookup globalL10NCMSLocalizationsLookup;
  GlobalL10NCMSLocalizations globalL10NCMSLocalizations;
  AppLocalizations appLocalizations;

  setUpAll(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  setUp(() {
    nativeInfoManager = _MockNativeInfoManager();
    globalL10NCMSLocalizationsLookup = _MockGlobalL10NCMSLocalizationsLookup();
    globalL10NCMSLocalizations = _MockGlobalL10NCMSLocalizations();
    appLocalizations = AppLocalizations.mock(
        nativeInfoManager, globalL10NCMSLocalizationsLookup, (key) async {
      return '''
{
  "jrpass_popular" : "热门",
  "jrpass_number_of_days" : "天数",
  "jrpass_select_places" : "选择目的地",
  "jrpass_view_on_map" : "在地图中查看搜索结果",
  "jrpass_select_all" : "选择全部",
  "jrpass" : "JR Pass铁路通票"
}
      ''';
    });
  });

  test(
      'Call GlobalL10NCMSLocalizations.appLocalizationsTextGetDebugModeCompat in load',
      () async {
    when(nativeInfoManager.provideAppInfo()).thenAnswer(
        (_) => Future.value(AppInfo.fromJson({'language': 'zh_CN'})));
    when(globalL10NCMSLocalizationsLookup.lookFor(const Locale('zh', 'CN')))
        .thenReturn(globalL10NCMSLocalizations);
    await appLocalizations.ensureInit();
    verify(globalL10NCMSLocalizations.appLocalizationsTextGetDebugModeCompat())
        .called(1);
  });

  test(
      'Can get value from GlobalL10NCMSLocalizations.appLocalizationsTextCompat text',
      () async {
    when(nativeInfoManager.provideAppInfo()).thenAnswer(
        (_) => Future.value(AppInfo.fromJson({'language': 'zh_CN'})));
    when(globalL10NCMSLocalizationsLookup.lookFor(const Locale('zh', 'CN')))
        .thenReturn(globalL10NCMSLocalizations);
    when(globalL10NCMSLocalizations.appLocalizationsTextCompat('jrpass', {}))
        .thenReturn('JR PASS');
    await appLocalizations.ensureInit();

    expect(appLocalizations.text('jrpass'), 'JR PASS');
  });

  test(
      'Can get default value when GlobalL10NCMSLocalizations.appLocalizationsTextCompat with exception in text',
      () async {
    when(nativeInfoManager.provideAppInfo()).thenAnswer(
        (_) => Future.value(AppInfo.fromJson({'language': 'zh_CN'})));
    when(globalL10NCMSLocalizationsLookup.lookFor(const Locale('zh', 'CN')))
        .thenReturn(globalL10NCMSLocalizations);
    when(globalL10NCMSLocalizations.appLocalizationsTextCompat('jrpass', {}))
        .thenThrow(Exception('No text found'));
    await appLocalizations.ensureInit();

    expect(appLocalizations.text('jrpass'), 'JR Pass铁路通票');
  });
}
