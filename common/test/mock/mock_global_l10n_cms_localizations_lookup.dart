import 'package:l10n_cms/l10n_cms.dart';
import 'package:mockito/mockito.dart';

class MockGlobalL10NCMSLocalizationsLookup extends Mock
    implements GlobalL10NCMSLocalizationsLookup {}
