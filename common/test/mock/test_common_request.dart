import 'package:common/common.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';

class _TestCommonRequestDelegate extends CommonRequest {
  _TestCommonRequestDelegate(HTTP http, Serialization serialization,
      {HttpRequestErrorHandler httpRequestErrorHandler})
      : _http = http,
        super(http, const [], serialization,
            httpRequestErrorHandler: httpRequestErrorHandler);

  final HTTP _http;

  // Use empty interceptors for unit test.
  @override
  Interceptors interceptors() => Interceptors();
}

/// Because of we can't retrieve the `AppInfo`, `UserInfo` and other resource in
/// the test environment, and it's not necessary because we can mock the [CommonRequest] to
/// return the mock data directly. e.g.,
///
/// ```dart
/// var commonRequest = TestCommonRequest(Serialization(serializers.toStandardJsonSerializers()));
///
/// var rs = """
/// {
///     "error": {
///       "code": "",
///       "message": ""
///     },
///     "result": {
///       "path": "/path",
///       "type": "deeplink",
///       "extra": "args"
///     },
///     "success": true
/// }
/// """;
///
/// commonRequest.stubGet("/fake")
///     .thenAnswer((_) => Future.value(Response(data: rs)));
///
/// var expectAction = TestAction((b) => b
///   ..path = "/path"
///   ..type = "deeplink"
///   ..extra = "args");
///
/// expect(await commonRequest.commonGet<TestAction>("/fake"), expectAction);
/// ```

class TestCommonRequest extends _TestCommonRequestDelegate {
  TestCommonRequest(Serialization serialization,
      {HttpRequestErrorHandler httpRequestErrorHandler,
      CancelToken mockCancelToken})
      : _mockCancelToken = mockCancelToken,
        super(_MockHttp(), serialization,
            httpRequestErrorHandler: httpRequestErrorHandler);

  final CancelToken _mockCancelToken;

  @override
  CancelToken handleCancelToken(String cancelFor) => _mockCancelToken;

  ///
  /// stub commonPost to return mock data, the namedArgument's default value is the same as commonPost
  ///
  PostExpectation<Future<Response>> stubPost(
    String path, {
    SerializerType requestSerializerType = SerializerType.Form,
    Object queryParameters,
    dynamic data,
    SerializerType responseSerializerType = SerializerType.JSON,
  }) =>
      stubRequest(RequestMethod.POST, path, requestSerializerType,
          queryParameters, data, responseSerializerType);

  ///
  /// stub commonGet to return mock data, the namedArgument's default value is the same as commonGet
  ///
  PostExpectation<Future<Response>> stubGet(String path,
          {SerializerType requestSerializerType = SerializerType.Form,
          Object queryParameters,
          dynamic data,
          SerializerType responseSerializerType = SerializerType.JSON}) =>
      stubRequest(RequestMethod.GET, path, requestSerializerType,
          queryParameters, data, responseSerializerType);

  ///
  /// stub request to return mock data
  ///
  PostExpectation<Future<Response>> stubRequest(
          RequestMethod method,
          String path,
          SerializerType requestSerializerType,
          Object queryParameters,
          dynamic data,
          SerializerType responseSerializerType) =>
      when(_http.request(method, path, requestSerializerType, queryParameters,
          data, responseSerializerType, _mockCancelToken, Interceptors()));
}

/// A mocked [HTTP] use for test only, which is mainly work with  [TestCommonRequest]
class _MockHttp extends Mock implements HTTP {}
