import 'package:common/local_event.dart';
import 'package:test/test.dart';

import 'test_local_event.dart';

void main() {
  LocalEventManager localEventManager;

  setUp(() {
    localEventManager = LocalEventManager.forTesting();
  });

  tearDown(() {
    localEventManager.eventSubject.close();
    localEventManager = null;
  });

  group("LocalEventManager", () {
    test("addLocalEventReceiver()", () {
      localEventManager.addLocalEventReceiver(expectAsync1((e) {
        expect(e, const TypeMatcher<TestLocalEvent>());
      }, count: 1));

      localEventManager.eventSubject.add(const TestLocalEvent());
    });

    test("remove receiver by VoidCallback", () {
      final removeCallback =
          localEventManager.addLocalEventReceiver(expectAsync1((e) {
        expect(e, const TypeMatcher<TestLocalEventSub1>());
      }, count: 0));
      localEventManager.eventSubject.add(const TestLocalEventSub1());
      removeCallback();
    });
  });
}
