import 'package:common/local_event.dart';

class TestLocalEvent implements LocalEvent {
  const TestLocalEvent();

  @override
  String toString() {
    return 'TestLocalEvent';
  }
}

class TestLocalEventSub1 extends TestLocalEvent {
  const TestLocalEventSub1();

  @override
  String toString() {
    return 'TestLocalEventSub1';
  }
}

class TestLocalEventSub2 extends TestLocalEvent {
  const TestLocalEventSub2();

  @override
  String toString() {
    return 'TestLocalEventSub2';
  }
}
