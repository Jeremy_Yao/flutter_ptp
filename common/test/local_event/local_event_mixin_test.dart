import 'package:common/local_event.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import 'test_local_event.dart';

class _TestLocalEventReceiverWidget extends StatefulWidget {
  const _TestLocalEventReceiverWidget(this._localEventManager);
  final LocalEventManager _localEventManager;
  @override
  State<StatefulWidget> createState() => _TestLocalEventReceiverWidgetState();
}

class _TestLocalEventReceiverWidgetState
    extends State<_TestLocalEventReceiverWidget> with LocalEventReceiverMixin {
  String _text = "No event";

  @override
  LocalEventManager mockLocalEventManager() => widget._localEventManager;

  @override
  void initState() {
    super.initState();

    onEventReceived<TestLocalEventSub2>((event) {
      setState(() {
        _text = event.toString();
      });
    });
  }

  @override
  void onEvent(LocalEvent event) {
    if (event is! TestLocalEventSub1) return;
    setState(() {
      _text = event.toString();
    });
  }

  @override
  Widget build(BuildContext context) => Text(
        _text,
        textDirection: TextDirection.ltr,
      );
}

class _TestLocalEventSenderWidget extends StatefulWidget
    with LocalEventSenderMixin {
  _TestLocalEventSenderWidget({Key key, LocalEventManager localEventManager})
      : _localEventManager = localEventManager,
        super(key: key);
  final LocalEventManager _localEventManager;

  @override
  State<StatefulWidget> createState() => _TestLocalEventSenderWidgetState();

  @override
  LocalEventManager mockLocalEventManager() => _localEventManager;
}

class _TestLocalEventSenderWidgetState
    extends State<_TestLocalEventSenderWidget> {
  void sendEvent() {
    widget.localEventSender.sendEvent(const TestLocalEventSub1());
  }

  void sendEvent2() {
    widget.localEventSender.sendEvent(const TestLocalEventSub2());
  }

  @override
  Widget build(BuildContext context) => const Text(
        "No event",
        textDirection: TextDirection.ltr,
      );
}

void main() {
  LocalEventManager localEventManager;

  setUp(() {
    localEventManager = LocalEventManager.forTesting();
  });

  tearDown(() {
    localEventManager.eventSubject.close();
    localEventManager = null;
  });

  group("LocalEventMixin", () {
    testWidgets("receive event", (WidgetTester tester) async {
      await tester.pumpWidget(_TestLocalEventReceiverWidget(localEventManager));
      expect(find.text("No event"), findsOneWidget);

      localEventManager.sendEvent(const TestLocalEventSub1());

      await tester.pumpAndSettle();
      expect(find.text("TestLocalEventSub1"), findsOneWidget);
    });

    testWidgets("receive event by using onEventReceived",
        (WidgetTester tester) async {
      await tester.pumpWidget(_TestLocalEventReceiverWidget(localEventManager));
      expect(find.text("No event"), findsOneWidget);

      localEventManager.sendEvent(const TestLocalEventSub2());

      await tester.pumpAndSettle();
      expect(find.text("TestLocalEventSub2"), findsOneWidget);
    });

    testWidgets("send event TestLocalEventSub1", (WidgetTester tester) async {
      final GlobalKey key = GlobalKey();
      await tester.pumpWidget(Column(
        children: <Widget>[
          _TestLocalEventReceiverWidget(localEventManager),
          _TestLocalEventSenderWidget(
              key: key, localEventManager: localEventManager)
        ],
      ));
      (key.currentState as _TestLocalEventSenderWidgetState).sendEvent();

      await tester.pumpAndSettle();
      expect(find.text("TestLocalEventSub1"), findsOneWidget);
    });

    testWidgets("send event TestLocalEventSub2", (WidgetTester tester) async {
      final GlobalKey key = GlobalKey();
      await tester.pumpWidget(Column(
        children: <Widget>[
          _TestLocalEventReceiverWidget(localEventManager),
          _TestLocalEventSenderWidget(
              key: key, localEventManager: localEventManager)
        ],
      ));
      (key.currentState as _TestLocalEventSenderWidgetState).sendEvent2();

      await tester.pumpAndSettle();
      expect(find.text("TestLocalEventSub2"), findsOneWidget);
    });

    testWidgets("widget dispose", (WidgetTester tester) async {
      await tester.pumpWidget(
        _TestLocalEventReceiverWidget(localEventManager),
      );

      await tester.pumpWidget(Container());

      localEventManager.sendEvent(const TestLocalEventSub1());

      await tester.pumpAndSettle();
      expect(find.text("TestLocalEventSub1"), findsNothing);
    });
  });
}
