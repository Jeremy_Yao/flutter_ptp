import 'package:common/common.dart';
import 'package:common/local_event.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

// class _MockNativeEventChannel extends Mock implements NativeEventChannel {}

class _MockLocalEventManager extends Mock implements LocalEventManager {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  NativeEventChannel nativeEventChannel;
  _MockLocalEventManager mockLocalEventManager;

  setUp(() {
    nativeEventChannel = NativeEventChannel();
    nativeEventChannel.channel
        .setMockMethodCallHandler(nativeEventChannel.handler);
    mockLocalEventManager = _MockLocalEventManager();
    NativeEventDispatcher(
        nativeEventChannel: nativeEventChannel,
        localEventManager: mockLocalEventManager);
  });

  tearDown(() {
    nativeEventChannel.channel.setMockMethodCallHandler(null);
    nativeEventChannel = null;
    mockLocalEventManager = null;
  });

  group('NativeEventDispatcher', () {
    test('receive generic event event_test2', () async {
      await nativeEventChannel.channel
          .invokeMethod('event_test1', const {'key': 'value'});
      verify(mockLocalEventManager.sendEvent(
              GlobalGenericNativeEvent('event_test1', const {'key': 'value'})))
          .called(1);
    });

    test('receive generic event event_test2', () async {
      await nativeEventChannel.channel
          .invokeMethod('event_test2', const {'key2': 'value2'});
      verify(mockLocalEventManager.sendEvent(GlobalGenericNativeEvent(
          'event_test2', const {'key2': 'value2'}))).called(1);
    });
  });
}
