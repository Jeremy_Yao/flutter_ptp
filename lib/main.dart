import 'package:app_entry_point/app_entry_point.dart';
import 'package:common/common.dart';
import 'package:consume_platform/consume_platform_components_factory.dart';
import 'package:flutter_ptp/modules.configuration.dart';
import 'package:flutter_ptp/src/l10n/global_l10n_cms_localizations_lookup.dart';
import 'package:provider/provider.dart';
import 'package:ptp/src/home/ui/ptp_home_page.dart';
import 'package:ptp/src/home/bloc/ptp_home_bloc.dart';
import 'package:ptp/src/home/data/ptp_home_repository.dart';

void main() {
  startUpApp(
      componentContentFactories: [
        PlatformComponentContentFactory(),
      ],
      componentWrapperFactory: PlatformComponentWrapperFactory(),
      feedCardContentBuilderMap: platformFeeCardContentBuilderMap,
      genericFeedCardItemBuilderMap: platformGenericFeedCardItemBuilderMap,
      moduleSettings: const ModuleSettings(),
      globalL10NCMSLocalizationsLookup: GlobalL10NCMSLocalizationsLookupImpl(),
      home: BlocProvider<PtpHomeBloc>(
          create: (context) => PtpHomeBloc(PtpHomeRepository(
              Provider.of<CommonRequest>(context, listen: false),
              productType: "",
              productId: "",
              productTypeId: "5")),
          child: PtpHomePage(productSubType: "")));
}
