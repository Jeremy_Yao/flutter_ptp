import 'dart:ui';

import 'package:l10n_cms/l10n_cms.dart';
import 'global_l10n_cms_localizations_de_de.dart';
import 'global_l10n_cms_localizations_en_us.dart';
import 'global_l10n_cms_localizations_en_au.dart';
import 'global_l10n_cms_localizations_en_bs.dart';
import 'global_l10n_cms_localizations_en_ca.dart';
import 'global_l10n_cms_localizations_en_gb.dart';
import 'global_l10n_cms_localizations_en_hk.dart';
import 'global_l10n_cms_localizations_en_in.dart';
import 'global_l10n_cms_localizations_en_my.dart';
import 'global_l10n_cms_localizations_en_nz.dart';
import 'global_l10n_cms_localizations_en_ph.dart';
import 'global_l10n_cms_localizations_en_sg.dart';
import 'global_l10n_cms_localizations_es_es.dart';
import 'global_l10n_cms_localizations_fr_fr.dart';
import 'global_l10n_cms_localizations_id_id.dart';
import 'global_l10n_cms_localizations_it_it.dart';
import 'global_l10n_cms_localizations_ja_jp.dart';
import 'global_l10n_cms_localizations_ko_kr.dart';
import 'global_l10n_cms_localizations_ru_ru.dart';
import 'global_l10n_cms_localizations_th_th.dart';
import 'global_l10n_cms_localizations_vi_vn.dart';
import 'global_l10n_cms_localizations_zh_cn.dart';
import 'global_l10n_cms_localizations_zh_hk.dart';
import 'global_l10n_cms_localizations_zh_tw.dart';

/// GENERATED BY l10n_cms. DO NOT MODIFY BY HAND.
/// See more detail in https://bitbucket.org/klook/klook-flutter-common/src/master/l10n_cms/
class GlobalL10NCMSLocalizationsLookupImpl
    extends IncrementalGlobalL10NCMSLocalizationsLookup {
  GlobalL10NCMSLocalizationsLookupImpl(
      {bool fetchedIncrementalTextsFromServer = false})
      : super(fetchedIncrementalTextsFromServer);

  @override
  Set<String> get supportedLanguages => const {
        'de',
        'en',
        'es',
        'fr',
        'id',
        'it',
        'ja',
        'ko',
        'ru',
        'th',
        'vi',
        'zh'
      };

  @override
  Iterable<Locale> get supportedLocales => const [
        Locale.fromSubtags(languageCode: 'de', countryCode: 'DE'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'AU'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'BS'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'CA'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'GB'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'HK'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'IN'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'MY'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'NZ'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'PH'),
        Locale.fromSubtags(languageCode: 'en', countryCode: 'SG'),
        Locale.fromSubtags(languageCode: 'es', countryCode: 'ES'),
        Locale.fromSubtags(languageCode: 'fr', countryCode: 'FR'),
        Locale.fromSubtags(languageCode: 'id', countryCode: 'ID'),
        Locale.fromSubtags(languageCode: 'it', countryCode: 'IT'),
        Locale.fromSubtags(languageCode: 'ja', countryCode: 'JP'),
        Locale.fromSubtags(languageCode: 'ko', countryCode: 'KR'),
        Locale.fromSubtags(languageCode: 'ru', countryCode: 'RU'),
        Locale.fromSubtags(languageCode: 'th', countryCode: 'TH'),
        Locale.fromSubtags(languageCode: 'vi', countryCode: 'VN'),
        Locale.fromSubtags(languageCode: 'zh', countryCode: 'CN'),
        Locale.fromSubtags(languageCode: 'zh', countryCode: 'HK'),
        Locale.fromSubtags(languageCode: 'zh', countryCode: 'TW')
      ];

  @override
  GlobalL10NCMSLocalizations lookFor(Locale locale) {
    final localeName = '${locale.languageCode}_${locale.countryCode}';
    switch (localeName) {
      case 'de_DE':
        return GlobalL10NCMSLocalizationsDeDe();
      case 'en_US':
        return GlobalL10NCMSLocalizationsEnUs();
      case 'en_AU':
        return GlobalL10NCMSLocalizationsEnAu();
      case 'en_BS':
        return GlobalL10NCMSLocalizationsEnBs();
      case 'en_CA':
        return GlobalL10NCMSLocalizationsEnCa();
      case 'en_GB':
        return GlobalL10NCMSLocalizationsEnGb();
      case 'en_HK':
        return GlobalL10NCMSLocalizationsEnHk();
      case 'en_IN':
        return GlobalL10NCMSLocalizationsEnIn();
      case 'en_MY':
        return GlobalL10NCMSLocalizationsEnMy();
      case 'en_NZ':
        return GlobalL10NCMSLocalizationsEnNz();
      case 'en_PH':
        return GlobalL10NCMSLocalizationsEnPh();
      case 'en_SG':
        return GlobalL10NCMSLocalizationsEnSg();
      case 'es_ES':
        return GlobalL10NCMSLocalizationsEsEs();
      case 'fr_FR':
        return GlobalL10NCMSLocalizationsFrFr();
      case 'id_ID':
        return GlobalL10NCMSLocalizationsIdId();
      case 'it_IT':
        return GlobalL10NCMSLocalizationsItIt();
      case 'ja_JP':
        return GlobalL10NCMSLocalizationsJaJp();
      case 'ko_KR':
        return GlobalL10NCMSLocalizationsKoKr();
      case 'ru_RU':
        return GlobalL10NCMSLocalizationsRuRu();
      case 'th_TH':
        return GlobalL10NCMSLocalizationsThTh();
      case 'vi_VN':
        return GlobalL10NCMSLocalizationsViVn();
      case 'zh_CN':
        return GlobalL10NCMSLocalizationsZhCn();
      case 'zh_HK':
        return GlobalL10NCMSLocalizationsZhHk();
      case 'zh_TW':
        return GlobalL10NCMSLocalizationsZhTw();
    }

    throw ArgumentError('Not Supported locale tag: $localeName');
  }

  @override
  int get buildTimestamp => 1617702255829065;

  @override
  IncrementalGlobalL10NCMSLocalizationsLookup copyWith(
      {bool fetchedIncrementalTextsFromServer}) {
    return GlobalL10NCMSLocalizationsLookupImpl(
        fetchedIncrementalTextsFromServer: fetchedIncrementalTextsFromServer ??
            this.fetchedIncrementalTextsFromServer);
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,non_constant_identifier_names
