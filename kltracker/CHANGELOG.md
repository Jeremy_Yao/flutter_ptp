## 2.0.0

Upgrade to flutter 2.0.1

## 1.3.4
*  不曝光，并且不上报可见性，消除可见性为0的case

## 1.3.3
*  不曝光，不监听可见性

## 1.3.2
*  不管是否曝光，只要是模块就监听可见性，并在sendevent前删除null数据

## 1.3.1
* 曝光时的数据在dective中递归在widget树向上查找，可能会在debug抛出异常。
     对于曝光的数据在didChangeDependencies中先准备好，等需要数据时直接取。
*  重写 trackingData 的 hashCode。

## 1.3.0
* 添加 sync_config_data channel 方法给native更新config数据
* 模块点击是获取曝光范围，并放到事件的数据中

## 1.2.0
* 使用可见性、navigateobserver、applifestate来共同管理flutter的页面事件

## 1.1.1
* sendCustomEventV2 eventInfo optional

## 1.1.0
* add sendCustomEventV2 and uuid for pageId.

## 1.0.0
* Initial release.
