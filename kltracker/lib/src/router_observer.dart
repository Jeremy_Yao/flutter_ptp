import 'package:flutter/widgets.dart';
import 'package:kltracker/kltracker.dart';
import 'tracking_manager.dart';

class InHouseTrackingRouterObserver extends NavigatorObserver {
  @override
  void didPush(Route route, Route previousRoute) {
    if (route.isCurrent) {
      pageEventGenerate(route);
    }
  }

  @override
  void didPop(Route route, Route previousRoute) {
    if (previousRoute.isCurrent) {
      pageEventGenerate(previousRoute);
    }
  }

  @override
  void didReplace({Route newRoute, Route oldRoute}) {
    if (newRoute.isCurrent) {
      pageEventGenerate(newRoute);
    }
  }

  void pageEventGenerate(Route route) {
    if (route is ModalRoute) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        InHouseTrackingManager.instance.currentContext = route.subtreeContext;
      });
    }
  }
}
