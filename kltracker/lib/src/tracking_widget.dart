import 'package:kltracker/kltracker.dart';
import 'widget_util.dart';
import 'package:flutter/widgets.dart';
import 'package:kltracker/src/tracking_exposure_manager.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'tracking_data.dart';

class TrackingWidget extends StatefulWidget {
  /// 页面构造方法
  TrackingWidget.page({
    Key key,
    @required String spmName,
    @required Widget child,
    String objectId,
    TrackingOtherInfoBuilder otherInfoBuilder,
  })  : assert(spmName != null && spmName.isNotEmpty),
        _child = child,
        enableExposure = false,
        _trackingData = TrackingData.page(
            spmName: spmName,
            objectId: objectId,
            otherInfoBuilder: otherInfoBuilder),
        super(key: key);

  /// 模块构造方法
  TrackingWidget.module(
      {Key key,
      @required String spmName,
      @required Widget child,
      String objectId,
      int moduleLength,
      int moduleIndex,
      String requestId,
      TrackingOtherInfoBuilder otherInfoBuilder,
      bool enableExposure})
      : assert(spmName != null && spmName.isNotEmpty),
        _child = child,
        enableExposure = enableExposure ?? false,
        _trackingData = TrackingData.module(
            spmName: spmName,
            objectId: objectId,
            requestId: requestId,
            moduleLength: moduleLength,
            moduleIndex: moduleIndex,
            otherInfoBuilder: otherInfoBuilder),
        super(key: key);

  /// 元素构造方法
  TrackingWidget.item({
    Key key,
    @required String spmName,
    @required Widget child,
    String objectId,
    TrackingOtherInfoBuilder otherInfoBuilder,
  })  : assert(spmName != null && spmName.isNotEmpty),
        _child = child,
        enableExposure = false,
        _trackingData = TrackingData.item(
            spmName: spmName,
            objectId: objectId,
            otherInfoBuilder: otherInfoBuilder),
        super(key: key);

  final Widget _child;
  final TrackingData _trackingData;
  TrackingData get trackingData => _trackingData;
  final bool enableExposure;

  @override
  TrackingWidgetState createState() => TrackingWidgetState();
}

class TrackingWidgetState extends State<TrackingWidget>
    with WidgetsBindingObserver {
  TrackingDataType _type;
  double get visibleFraction => _visibleFraction;
  double _visibleFraction = 0;
  bool _isPagevisible = false;
  bool isNewPage = true;
  // isPagevisible 不让外界修改
  bool get isPagevisible => _isPagevisible;
  Map<TrackingDataType, TrackingData> _dataMap;
  Map<TrackingDataType, TrackingData> get dataMap => _dataMap;

  VisibilityChangedCallback _onVisibilityChanged(BuildContext context) {
    return (info) {
      _visibleFraction = info.visibleFraction;
      if (widget.enableExposure) {
        ExposureTrackingManager.instance
            .recordExposure(context, _dataMap, _visibleFraction);
      }
    };
  }

  VisibilityChangedCallback _onPageVisibilityChanged(BuildContext context) {
    return (info) {
      final bool preVisible = _isPagevisible;
      if (preVisible == false) {
        // 如果之前是不可见,当前判断一下是否全显示(大于0.95认为是完全可见)、如果全显示就设为可见
        _isPagevisible = info.visibleFraction > 0.95;
      } else {
        // 如果之前是可见,当前判断一下是否已经从页面完全消失(不小于0.1都认为是没有完全消失)，设置为不可见
        _isPagevisible = info.visibleFraction > 0.1;
      }
    };
  }

  Key _visibilityKey() {
    return Key(
        '${widget._trackingData.spmName}:${widget._trackingData.hashCode}');
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        if (widget.enableExposure) {
          ExposureTrackingManager.instance.recordExposure(context, _dataMap, 0);
        }
        break;
      case AppLifecycleState.resumed:
        if (widget.enableExposure) {
          if (_visibleFraction != null) {
            ExposureTrackingManager.instance
                .recordExposure(context, _dataMap, _visibleFraction);
          }
        }
        break;
      default:
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    _type ??= widget.trackingData.type;
    if (widget.enableExposure) {
      WidgetsBinding.instance.addObserver(this);
    }
    widget._trackingData.currentContext = context;
  }

  @override
  void didChangeDependencies() {
    widget._trackingData.currentContext = context;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    widget._trackingData.currentContext = context;
    // TrackingWidget rebuild后数据更新，但是Widget.canupdate方法如果返回true这时state不会更新
    if (widget.enableExposure) {
      _dataMap = nonPageTrackingData(context);
    }
    // 对于页面，需要可见性，结合路由和生命周期来触发pageView
    if (widget.trackingData.type == const TrackingDataType.page()) {
      return VisibilityDetector(
          key: _visibilityKey(),
          onVisibilityChanged: _onPageVisibilityChanged(context),
          child: widget._child);
    } else if (widget.trackingData.type == const TrackingDataType.module() &&
        widget.enableExposure) {
      // 模块，需要曝光的模块才监听可见性。
      return VisibilityDetector(
          key: _visibilityKey(),
          onVisibilityChanged: _onVisibilityChanged(context),
          child: widget._child);
    }
    return widget._child;
  }

  @override
  void deactivate() {
    if (widget.enableExposure) {
      ExposureTrackingManager.instance.recordExposure(context, _dataMap, 0);
    }
    super.deactivate();
  }

  @override
  void dispose() {
    if (widget.enableExposure || _type == const TrackingDataType.page()) {
      WidgetsBinding.instance.removeObserver(this);
    }
    super.dispose();
  }
}
