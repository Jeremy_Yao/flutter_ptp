import 'package:flutter/services.dart';

import '../../src/tracking_exposure_manager.dart';

// TODO(RUIJIN): channel 暂时不处理
class ConfigData {
  ConfigData.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    exposureTime = map["exposure_time"];
  }

  int exposureTime;
}

class TrackingChannel {
  final MethodChannel _channel = const MethodChannel('com.klook.platform/in_house_tracking');

  TrackingChannel() {
    _channel.setMethodCallHandler((call) {
      if (call.method == "sync_config_data") {
        syncConfigData(call.arguments);
      }
      return Future.value(true);
    });
  }

  void syncConfigData(dynamic data) {
    if (data is Map) {
      Map info = data;
      ConfigData config = ConfigData.fromJson(Map<String, dynamic>.from(info));
      if (config.exposureTime > 0 &&
          config.exposureTime != ExposureTrackingManager.instance.visiableDuration) {
        ExposureTrackingManager.instance.visiableDuration = config.exposureTime;
      }
    }
  }

  sendEvent(Map<String, dynamic> trackingDatas) async {
    try {
      trackingDatas.removeWhere((key, value) => value == null);
      //return _channel.invokeMethod<bool>('sendEvent', trackingDatas);
    } on PlatformException catch (e) {
      print(e);
    }
  }

  registerCommonProperties(Map<String, String> commonProperties) async {
    try {
      //return _channel.invokeMethod<bool>('registerCommonProperties', commonProperties);
    } on PlatformException catch (e) {
      print(e);
    }
  }

  registerCommonProperty(String propertyKey, String propertyValue) async {
    try {
      //return _channel.invokeMethod<bool>('registerCommonProperties', {propertyKey: propertyValue});
    } on PlatformException catch (e) {
      print(e);
    }
  }

  removeCommonProperty(String propertyKey) async {
    try {
      //return _channel.invokeMethod<bool>('removeCommonProperty', propertyKey);
    } on PlatformException catch (e) {
      print(e);
    }
  }
}
