import 'dart:core';

import 'package:flutter/widgets.dart';

import 'package:kltracker/kltracker.dart';
import 'package:kltracker/src/tracking_manager.dart';

/// 曝光管理
class ExposureTrackingManager {
  static final ExposureTrackingManager instance = ExposureTrackingManager();

  /// 曝光比例
  final double _visiableRate = 0.8;

  /// 曝光时长：毫秒
  int visiableDuration = 500;

  /// 可见视图记录
  Map<String, DateTime> _visiableMap = Map();

  /// 当前 Context 是否在 Module 模块内
  bool isModule(BuildContext context) {
    if (context == null) return false;
    if (context.widget is TrackingWidget) {
      return true;
    }
    Widget trackingWidget =
        context.findAncestorWidgetOfExactType<TrackingWidget>();
    return trackingWidget != null;
  }

  /// 记录&计算曝光
  bool recordExposure(BuildContext context,
      Map<TrackingDataType, TrackingData> expoureData, double visibleRate) {
    assert(expoureData != null && expoureData.isNotEmpty);
    String contextKey = context.hashCode.toString();
    bool isVisiable = visibleRate > _visiableRate;
    DateTime visiableTime = _visiableMap[contextKey];
    bool previouseVisiable = visiableTime != null;

    if (isVisiable) {
      if (!previouseVisiable) {
        _visiableMap[contextKey] = DateTime.now();
      }
    } else {
      _visiableMap.remove(contextKey);
    }

    if (!isVisiable && previouseVisiable) {
      if (visiableTime == null) return false;
      Duration duration = DateTime.now().difference(visiableTime);
      if (duration.inMilliseconds >= visiableDuration) {
        InHouseTrackingManager.instance.collectModuleExpoureDataSendToNative(
            expoureData,
            duration: duration.inMilliseconds,
            startTime: visiableTime.millisecondsSinceEpoch);
      }
      return true;
    }
    return false;
  }
}
