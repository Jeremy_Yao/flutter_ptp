import 'package:flutter/widgets.dart';
import 'package:kltracker/kltracker.dart';

import 'tracking_data.dart';
import 'widget_util.dart' as widget_util;

extension TrackingBuildContextExtension on BuildContext {
  Map<TrackingDataType, TrackingData> nonPageTrackingData() {
    return widget_util.nonPageTrackingData(this);
  }

  Map<TrackingDataType, TrackingData> pageTrackingData() {
    return widget_util.pageTrackingData(this);
  }

  // 返回当前 BuildContext 树往下查找到的第一个可见的page类型的TrackingWidget
  // 如果没有或者类型不是Page类型的数据，返回的都是null
  TrackingWidget pageTrackingWidget() {
    TrackingWidget widget = widget_util.takingDownFindWidget(this, 100);
    if (widget != null &&
        widget.trackingData.type == const TrackingDataType.page()) {
      return widget;
    }
    return null;
  }

  TrackingWidgetState findAncestorModuleState() {
    TrackingWidgetState trackingWidgetState;
    // 方法内的一个遍历函数
    Function visitor = (element) {
      if (element is StatefulElement) {
        State _state = element.state;
        if (_state is TrackingWidgetState &&
            _state.widget.trackingData.type ==
                const TrackingDataType.module()) {
          trackingWidgetState = _state;
          return false;
        }
      }
      return true;
    };
    if (visitor(this) == false || trackingWidgetState == null) {
      visitAncestorElements(visitor);
    }
    return trackingWidgetState;
  }
}

extension TrackingWidgetExtension on Widget {
  Map<TrackingDataType, TrackingData> nonPageTrackingData(
      BuildContext context) {
    return widget_util.nonPageTrackingData(context);
  }
}
