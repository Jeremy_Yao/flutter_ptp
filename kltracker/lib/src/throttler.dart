typedef ThrottlerCallback<T> = void Function(T value);

class Throttler<T> {
  Duration delay;
  ThrottlerCallback<T> callback;

  Throttler(this.delay, this.callback);

  DateTime _lastExec = new DateTime.now();

  void throttle(T value) {
    Duration elapsed = new DateTime.now().difference(_lastExec);

    void exec() {
      _lastExec = new DateTime.now();
      callback(value);
    }

    if (elapsed.compareTo(delay) >= 0) {
      exec();
    }
  }
}
