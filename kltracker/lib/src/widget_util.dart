import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'tracking_widget.dart';
import 'tracking_data.dart';

/// 非页面追踪数据查找（Module/Item 层级）
/// Element 树向上查找
Map<TrackingDataType, TrackingData> nonPageTrackingData(BuildContext context) {
  if (context == null) return {};
  assert(context != null);
  TrackingData module, page, item;
  TrackingWidget currentTrackingWidget;

  if (context.widget is TrackingWidget) {
    currentTrackingWidget = context.widget as TrackingWidget;
  } else {
    currentTrackingWidget =
        context.findAncestorWidgetOfExactType<TrackingWidget>();
  }

  while (page == null && currentTrackingWidget != null) {
    final TrackingData currentTrackingData = currentTrackingWidget.trackingData;
    if (currentTrackingData != null) {
      switch (currentTrackingData.type) {
        case TrackingDataType.item():
          item = item ?? currentTrackingData;
          break;
        case TrackingDataType.module():
          module = module ?? currentTrackingData;
          break;
        case TrackingDataType.page():
          page = page ?? currentTrackingData;
          break;
        default:
          assert(false, '');
      }
    }
    if (currentTrackingData.currentContext != null) {
      currentTrackingWidget = currentTrackingData.currentContext
          .findAncestorWidgetOfExactType<TrackingWidget>();
    } else {
      break;
    }
  }

  final Map<TrackingDataType, TrackingData> trackingMap = {
    const TrackingDataType.item(): item,
    const TrackingDataType.module(): module,
    const TrackingDataType.page(): page
  };
  trackingMap.removeWhere((key, value) => value == null);
  return trackingMap;
}

/// 向下查找页面层级 TrackingWidget
/// depth 递归层级
TrackingWidget takingDownFindWidget(BuildContext context, int depth) {
  if (context == null) return null;
  if (depth < 0) return null;
  if (context.widget is TrackingWidget) {
    final TrackingWidget contextWidget = context.widget;
    if (contextWidget.trackingData.currentContext is StatefulElement) {
      final StatefulElement element = contextWidget.trackingData.currentContext;
      if (element.state is TrackingWidgetState) {
        final TrackingWidgetState state = element.state;
        if (state.isPagevisible) {
          return contextWidget;
        }
      }
    }
  }
  TrackingWidget trackingWidget;
  context.visitChildElements((element) {
    final widget = takingDownFindWidget(element, depth - 1);
    if (widget != null) {
      trackingWidget = widget;
    }
  });
  return trackingWidget;
}

/// 遍历子层查找页面追踪数据
Map<TrackingDataType, TrackingData> pageTrackingData(BuildContext context) {
  final TrackingWidget pageTrackingWidget = takingDownFindWidget(context, 100);
  if (pageTrackingWidget == null) return null;
  TrackingData trackingData;
  switch (pageTrackingWidget.trackingData.type) {
    case TrackingDataType.page():
      trackingData = pageTrackingWidget.trackingData;
      break;
    case TrackingDataType.item():
    case TrackingDataType.module():
      break;
    default:
      assert(false, '');
  }
  if (trackingData == null) return null;
  return {const TrackingDataType.page(): trackingData};
}
