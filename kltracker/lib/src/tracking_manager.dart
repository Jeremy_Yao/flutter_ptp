import 'package:flutter/widgets.dart';
import 'package:kltracker/kltracker.dart';
import 'channel/tracking_channel.dart';

import 'tracking_data.dart';
import 'widget_util.dart';
import 'router_observer.dart';
import 'tracking_extension.dart';
import 'package:visibility_detector/visibility_detector.dart';

enum TrackingEventType { page, exposure, action }

class InHouseTrackingManager with WidgetsBindingObserver {
  InHouseTrackingManager._(TrackingChannel channel)
      : assert(channel != null),
        _channel = channel,
        _superProperties = Map<String, String>() {
    WidgetsBinding.instance.addObserver(this);
    VisibilityDetectorController.instance.updateInterval = Duration.zero;
  }

  /// 通信 Channel
  final TrackingChannel _channel;

  AppLifecycleState _lifeState = AppLifecycleState.resumed;
  // 是否在检查页面的状态
  bool _isChecking = false;
  BuildContext _currentContext;

  set currentContext(BuildContext context) {
    _currentContext = context;
    _checkPageViewDelayed();
  }

  void _checkPageViewDelayed({int milliseconds = 600, int checkCount = 2}) {
    // 等待期间不接收新的触发请求
    if (_isChecking || checkCount < 1) return;
    _isChecking = true;
    // 等状态切换稳定后(600毫秒，通常一个页面切换动画在500毫秒左右)，发送页面事件
    Future.delayed(Duration(milliseconds: milliseconds), () {
      // milliseconds时间到达后，恢复_isChecking的状态
      _isChecking = false;
      if (this._lifeState == AppLifecycleState.resumed &&
          _currentContext != null) {
        // 首页didPush进来是一个tab相关的几个页面的容器，不是包裹的页面，在androids的部分机型，会导致首页的获得可见时的时间晚于
        // collectPageTrackDataSendToNative 调用的时间。所以这里对于失败的情况, milliseconds 时间后重试一次。
        if (false == collectPageTrackDataSendToNative(_currentContext)) {
          _checkPageViewDelayed(
              milliseconds: milliseconds, checkCount: checkCount - 1);
        }
      }
    });
  }

  // 对于无法获取pageContext的地方，调用这个方法告诉sdk页面状态即将发生改变
  // 比如Tab切换，无法获取context，但是点击后会切换页面，这个时候需要调用此方法主动告知sdk在预估时间后会
  // 会有页面的状态变化
  static pageViewStateWillChangeAbout({int milliseconds = 600}) {
    InHouseTrackingManager.instance
        ._checkPageViewDelayed(milliseconds: milliseconds);
  }

  /// 全局 SuperProperty Map
  final Map<String, String> _superProperties;
  Map<String, String> get superProperties =>
      _superProperties ?? Map<String, String>();

  /// 单例
  static final InHouseTrackingManager _instance =
      InHouseTrackingManager._(TrackingChannel());
  static InHouseTrackingManager get instance => _instance;

  final InHouseTrackingRouterObserver _routerObserver =
      InHouseTrackingRouterObserver();
  InHouseTrackingRouterObserver get routerObserver => _routerObserver;

  /// App 生命周期回调
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    _lifeState = state;
    _syncSuperPropertyIfNeed();
    if (state == AppLifecycleState.resumed) {
      _checkPageViewDelayed();
    }
  }

  /// 收集非页面事件(曝光 + 点击)数据并发送到 Native 侧
  void collectNonPageTrackDataSendToNative(
      BuildContext context, TrackingEventType type,
      {int duration,
      int startTime,
      Map<TrackingDataType, TrackingData> precache}) {
    final Map<TrackingDataType, TrackingData> result =
        precache ?? nonPageTrackingData(context);

    if (result == null || result.length < 2) return;
    result.removeWhere((key, value) => value == null);
    if (result.isEmpty) return;

    if (!result.containsKey(const TrackingDataType.module())) {
      result[const TrackingDataType.module()] =
          TrackingData.module(spmName: 'Defalult', objectId: null);
    }

    final Map<String, dynamic> map =
        result.map((k, v) => MapEntry(k.value, v.trackingDataMap()));
    final String typeValue = type.toString().split('.').last;
    final String typeKey = 'event_type';
    map.addAll({typeKey: typeValue});
    if (duration != null) {
      map.addAll({'duration': duration});
      map.addAll({'start_time': startTime});
    }

    // 如果是点击，而且点击的是模块, 带上显示百分比
    if (type == TrackingEventType.action &&
        map.containsKey(const TrackingDataType.module().value) &&
        !map.containsKey(const TrackingDataType.item().value)) {
      final TrackingWidgetState moduleState = context.findAncestorModuleState();
      if (moduleState != null && moduleState.widget.enableExposure) {
        map["module_exposure_ratio"] = moduleState.visibleFraction;
      }
    }
    _channel.sendEvent(map);
  }

  void collectModuleExpoureDataSendToNative(
      Map<TrackingDataType, TrackingData> expoureData,
      {int duration,
      int startTime}) {
    assert(expoureData != null && expoureData.isNotEmpty);
    if (expoureData.length < 2) return;
    expoureData.removeWhere((key, value) => value == null);
    if (expoureData.isEmpty) return;

    if (!expoureData.containsKey(const TrackingDataType.module())) {
      expoureData[const TrackingDataType.module()] =
          TrackingData.module(spmName: 'Defalult', objectId: null);
    }

    final Map<String, dynamic> map =
        expoureData.map((k, v) => MapEntry(k.value, v.trackingDataMap()));
    final String typeValue =
        TrackingEventType.exposure.toString().split('.').last;
    map['event_type'] = typeValue;
    if (duration != null) {
      map['duration'] = duration;
      map['start_time'] = startTime;
    }
    _channel.sendEvent(map);
  }

  /// 收集页面事件数据并发送到 Native 侧
  bool collectPageTrackDataSendToNative(BuildContext context) {
    final TrackingWidget pageWidget = context.pageTrackingWidget();
    // pageWidget不做类型检查，pageTrackingWidget保证的类型是Page才有值
    if (pageWidget == null || pageWidget.trackingData == null) {
      return false;
    }
    final Map<TrackingDataType, TrackingData> result = {
      const TrackingDataType.page(): pageWidget.trackingData
    };
    final Map<String, dynamic> map =
        result.map((k, v) => MapEntry(k.value, v.trackingDataMap()));
    final String typeKey = 'event_type';
    final String typeValue = TrackingEventType.page.toString().split('.').last;
    map.addAll({typeKey: typeValue});

    // 检查pageWidget的state,获取是否为新页面
    if (pageWidget.trackingData.currentContext is StatefulElement) {
      final StatefulElement element = pageWidget.trackingData.currentContext;
      if (element.state is TrackingWidgetState) {
        final TrackingWidgetState state = element.state;
        map['is_new_page'] = state.isNewPage;
        state.isNewPage = false;
      }
    }
    _channel.sendEvent(map);
    return true;
  }

  static void sendCustomEvent(
      BuildContext context, String spmName, Map<String, dynamic> eventInfo) {
    final Map<TrackingDataType, TrackingData> result =
        nonPageTrackingData(context);
    if (result?.isEmpty ?? false) return;
    if (!result.containsKey(const TrackingDataType.page())) return;
    Map<String, dynamic> map =
        result.map((k, v) => MapEntry(k.value, v.trackingDataMap()));
    map['event_type'] = 'custom';
    map['custom_spm_name'] = spmName;
    map['extra_info'] = eventInfo;
    _instance._channel.sendEvent(map);
  }

  /// context和eventInfo参数可选
  static void sendCustomEventV2(String spmName,
      {Map<String, dynamic> eventInfo, BuildContext context}) {
    Map<TrackingDataType, TrackingData> result = {};
    if (context != null) {
      result = nonPageTrackingData(context);
    }
    final Map<String, dynamic> map =
        result.map((k, v) => MapEntry(k.value, v.trackingDataMap()));
    map['event_type'] = 'custom';
    map['custom_spm_name'] = spmName;
    map['extra_info'] = eventInfo;
    _instance._channel.sendEvent(map);
  }

  /// 批量注册 SuperProperty
  void registerSuperProperties(Map<String, String> properties) async {
    assert(properties != null && properties.isNotEmpty);
    _superProperties.addAll(properties);
    bool success = await _channel.registerCommonProperties(properties);
    if (success) {
      for (String key in properties.keys) {
        _superProperties.remove(key);
      }
    }
  }

  /// 注册单个 SuperProperty
  void registerSuperProperty(String key, String value) async {
    assert(key != null && key.isNotEmpty);
    assert(value != null && value.isNotEmpty);
    _superProperties[key] = value;
    bool success = await _channel.registerCommonProperties({key: value});
    if (success) {
      _superProperties.remove(key);
    }
  }

  /// 根据 Key 移除 SuperProperty
  void unregisterSuperProperty(String key) async {
    assert(key != null && key.isNotEmpty);
    bool success = await _channel.removeCommonProperty(key);
    if (success) {
      _superProperties.remove(key);
    }
  }

  /// 根据 key 数组批量移除 SuperProperty
  void unregisterSuperPropertyKeys(List<String> keys) {
    assert(keys != null && keys.isNotEmpty);
    for (String key in keys) {
      unregisterSuperProperty(key);
    }
  }

  /// 前后台切换时强制同步 SuperProperty
  void _syncSuperPropertyIfNeed() {
    if (_superProperties.isEmpty) return;
    Map<String, String> superProperty = Map.from(_superProperties);
    _superProperties.clear();
    registerSuperProperties(superProperty);
  }
}
