import 'package:flutter/widgets.dart';
import 'tracking_manager.dart';
import 'tracking_manager.dart' show TrackingEventType;

typedef ActionCallback<R> = R Function();
typedef ActionCallback1<R, T> = R Function(T value);
typedef ActionCallback2<R, T1, T2> = R Function(T1 value1, T2 value2);
typedef ActionCallback3<R, T1, T2, T3> = R Function(
    T1 value1, T2 value2, T3 value3);

ActionCallback trackingWrapper(BuildContext context, ActionCallback callback) {
  assert(callback != null);
  return () {
    if (callback != null) {
      callback();
    }
    InHouseTrackingManager.instance
        .collectNonPageTrackDataSendToNative(context, TrackingEventType.action);
  };
}

ActionCallback1 trackingWrapper1(
    BuildContext context, ActionCallback1 callback) {
  assert(callback != null);
  return (value) {
    if (callback != null) {
      callback(value);
    }
    InHouseTrackingManager.instance
        .collectNonPageTrackDataSendToNative(context, TrackingEventType.action);
  };
}

ActionCallback2 trackingWrapper2(
    BuildContext context, ActionCallback2 callback) {
  assert(callback != null);
  return (value1, value2) {
    if (callback != null) {
      callback(value1, value2);
    }
    InHouseTrackingManager.instance
        .collectNonPageTrackDataSendToNative(context, TrackingEventType.action);
  };
}

ActionCallback3 trackingWrapper3(
    BuildContext context, ActionCallback3 callback) {
  assert(callback != null);
  return (value1, value2, value3) {
    if (callback != null) {
      callback(value1, value2, value3);
    }
    InHouseTrackingManager.instance
        .collectNonPageTrackDataSendToNative(context, TrackingEventType.action);
  };
}
