import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

typedef Map<String, String> TrackingOtherInfoBuilder(BuildContext context);

/// 追踪数据类型
class TrackingDataType {
  final String _value;

  String get value => _value;

  const TrackingDataType._(String value)
      : _value = value,
        super();
  const TrackingDataType.page() : this._('page');
  const TrackingDataType.module() : this._('module');
  const TrackingDataType.item() : this._('item');

  @override
  String toString() => 'TrackingDataType(value=$_value)';
}

/// 追踪数据结构
class TrackingData {
  TrackingData({
    @required TrackingDataType type,
    @required String spmName,
    String objectId,
    String requestId,
    String spmId,
    String previousPageName,
    int moduleLength,
    int moduleIndex,
    TrackingOtherInfoBuilder otherInfoBuilder,
  })  : assert(spmName != null && spmName.length > 0),
        _spmName = spmName,
        _objectId = objectId,
        _requestId = requestId,
        spmId = spmId,
        _type = type,
        _moduleLength = moduleLength,
        _moduleIndex = moduleIndex,
        _otherInfoBuilder = otherInfoBuilder,
        super();

  TrackingData.module(
      {@required String spmName,
      @required String objectId,
      String requestId,
      int moduleLength,
      int moduleIndex,
      TrackingOtherInfoBuilder otherInfoBuilder})
      : this(
            spmName: spmName,
            objectId: objectId,
            type: const TrackingDataType.module(),
            requestId: requestId,
            moduleLength: moduleLength,
            moduleIndex: moduleIndex,
            otherInfoBuilder: otherInfoBuilder);

  TrackingData.page(
      {@required String spmName,
      @required String objectId,
      TrackingOtherInfoBuilder otherInfoBuilder})
      : this(
            spmName: spmName,
            objectId: objectId,
            spmId: Uuid().v4(),
            type: const TrackingDataType.page(),
            otherInfoBuilder: otherInfoBuilder);

  TrackingData.item(
      {@required String spmName,
      @required String objectId,
      TrackingOtherInfoBuilder otherInfoBuilder})
      : this(
            spmName: spmName,
            objectId: objectId,
            type: const TrackingDataType.item(),
            otherInfoBuilder: otherInfoBuilder);

  final String _spmName;
  final String _objectId;
  final TrackingDataType _type;
  final TrackingOtherInfoBuilder _otherInfoBuilder;
  // Properties only avilable in page
  final String _requestId;
  final String spmId;

  /// Properties only avilable in module
  /// 模块所在列表的长度
  final int _moduleLength;

  /// 模块在列表的位置
  final int _moduleIndex;

  /// 需要更新的額外数据
  Map<String, String> _updateOtherInfo;

  // TODO(Rongan): Delete this property, have the context take a big risk in future.
  BuildContext currentContext;
  String get spmName => _spmName;
  String get objectId => _objectId;
  String get requestId => _requestId;
  TrackingDataType get type => _type;
  TrackingOtherInfoBuilder get otherInfoBuilder => _otherInfoBuilder;
  int get moduleLength => _moduleLength;
  int get moduleIndex => _moduleIndex;
  int _hashcode;

  TrackingData copyWith({
    String spmName,
    String objectId,
    TrackingDataType type,
    String requestId,
    String previousPageName,
    int moduleLength,
    int moduleIndex,
    TrackingOtherInfoBuilder otherInfoBuilder,
  }) {
    return TrackingData(
      spmName: spmName ?? this.spmName,
      objectId: objectId ?? this.objectId,
      type: type ?? this.type,
      requestId: requestId ?? this.requestId,
      moduleLength: moduleLength ?? this.moduleLength,
      moduleIndex: moduleIndex ?? this.moduleIndex,
      otherInfoBuilder: otherInfoBuilder ?? this.otherInfoBuilder,
    );
  }

  /// Modle -> Json
  Map<String, dynamic> trackingDataMap() {
    final otherInfo =
        otherInfoBuilder == null ? null : otherInfoBuilder(currentContext);
    if (_updateOtherInfo != null) {
      otherInfo?.addAll(_updateOtherInfo);
    }

    final Map<String, dynamic> result = {
      'spm': _spmName,
      'object_id': _objectId,
      'data_type': _type.value,
      'extra_info': otherInfo,
    };

    switch (_type) {
      case TrackingDataType.page():
        result.addAll({
          'request_id': _requestId,
          'page_id': spmId,
        });
        break;
      case TrackingDataType.module():
        result.addAll({
          'module_length': _moduleLength,
          'module_index': _moduleIndex,
        });
        break;
      case TrackingDataType.item():
        break;
    }
    result.removeWhere((key, value) => value == null);
    return result;
  }

  @override
  int get hashCode {
    if (_type == const TrackingDataType.module()) {
      _hashcode ??= trackingDataMap().toString().hashCode;
      return _hashcode;
    }
    return super.hashCode;
  }

  @override
  bool operator ==(Object other) {
    return super == other;
  }
}
