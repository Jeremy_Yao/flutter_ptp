import 'dart:async';

import 'package:app_entry_point/src/async_initializer_scope.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets(
    'AsyncInitializerScope show child after initialized',
    (WidgetTester tester) async {
      final initilizedCompleter = Completer<void>();
      await tester.pumpWidget(AsyncInitializerScope(
        ensureInit: () => initilizedCompleter.future,
        child: const AsyncInitializedAwareWidget(
          child: Text(
            'Async initialized',
            textDirection: TextDirection.ltr,
          ),
        ),
      ));

      expect(find.text('Async initialized'), findsNothing);

      initilizedCompleter.complete();

      await tester.pumpAndSettle();

      expect(find.text('Async initialized'), findsOneWidget);
    },
  );
}
