import 'package:app_entry_point/src/root_configuration_change_aware_widget.dart';
import "package:common/common.dart";
import 'package:common/local_event.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockAppLocalizations extends Mock implements AppLocalizations {}

class MockNativeInfoManager extends Mock implements NativeInfoManager {}

class _MockNativeEventChannel extends Mock implements NativeEventChannel {}

class _TestLocalizationText extends StatefulWidget {
  const _TestLocalizationText(
    this._appLocalizations,
  );

  final AppLocalizations _appLocalizations;

  @override
  __TestLocalizationTextState createState() => __TestLocalizationTextState();
}

class __TestLocalizationTextState extends State<_TestLocalizationText>
    with ConfigurationChangeAwareMixin {
  @override
  void refreshUI() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) => Text(
        widget._appLocalizations.text("all"),
        textDirection: TextDirection.ltr,
      );
}

void main() {
  NativeEventDispatcher nativeEventDispatcher;
  MockAppLocalizations mockAppLocalizations;

  setUp(() {
    nativeEventDispatcher = NativeEventDispatcher(
        nativeEventChannel: _MockNativeEventChannel(),
        localEventManager: LocalEventManager());
    mockAppLocalizations = MockAppLocalizations();
  });

  tearDown(() {
    nativeEventDispatcher = null;
    mockAppLocalizations = null;
  });

  group("RootConfigurationChangeAwareWidget", () {
    testWidgets("Change currency", (WidgetTester tester) async {
      when(mockAppLocalizations.text("all")).thenReturn("ALL");
      when(mockAppLocalizations.currentLanguage).thenReturn("en_US");

      await tester.pumpWidget(RootConfigurationChangeAwareWidget(
        appLocalizations: mockAppLocalizations,
        builder: (locale) => _TestLocalizationText(mockAppLocalizations),
      ));

      expect(find.text("ALL"), findsOneWidget);

      nativeEventDispatcher
          .notifyUIRefreshEventListener("event_currency_changed");

      await tester.pump();

      verify(mockAppLocalizations.ensureInit()).called(2);
    });

    testWidgets("Change language en_US -> ja_JP", (WidgetTester tester) async {
      when(mockAppLocalizations.text("all")).thenReturn("ALL");
      when(mockAppLocalizations.currentLanguage).thenReturn("en_US");

      await tester.pumpWidget(RootConfigurationChangeAwareWidget(
        appLocalizations: mockAppLocalizations,
        builder: (locale) => _TestLocalizationText(mockAppLocalizations),
      ));

      await tester.pumpAndSettle();

      expect(find.text("ALL"), findsOneWidget);

      reset(mockAppLocalizations);
      when(mockAppLocalizations.text("all")).thenReturn("すべて");
      when(mockAppLocalizations.currentLanguage).thenReturn("ja_JP");

      nativeEventDispatcher
          .notifyUIRefreshEventListener("event_language_changed");

      await tester.pumpAndSettle();
      await tester.pumpAndSettle();

      verify(mockAppLocalizations.ensureInit()).called(1);
      expect(find.text("すべて"), findsOneWidget);
    });

    testWidgets("Can get correct locale when change language en_US -> ja_JP",
        (WidgetTester tester) async {
      when(mockAppLocalizations.currentLanguage).thenReturn("en_US");

      Locale locale;
      await tester.pumpWidget(RootConfigurationChangeAwareWidget(
        appLocalizations: mockAppLocalizations,
        builder: (tempLocale) {
          locale = tempLocale;
          return Container();
        },
      ));

      await tester.pumpAndSettle();

      expect(locale.languageCode, 'en');
      expect(locale.countryCode, 'US');

      reset(mockAppLocalizations);
      when(mockAppLocalizations.text("all")).thenReturn("すべて");
      when(mockAppLocalizations.currentLanguage).thenReturn("ja_JP");

      nativeEventDispatcher
          .notifyUIRefreshEventListener("event_language_changed");

      await tester.pumpAndSettle();

      expect(locale.languageCode, 'ja');
      expect(locale.countryCode, 'JP');
    });

    testWidgets("Remove listener", (WidgetTester tester) async {
      when(mockAppLocalizations.text("all")).thenReturn("ALL");
      when(mockAppLocalizations.currentLanguage).thenReturn("en_US");

      await tester.pumpWidget(RootConfigurationChangeAwareWidget(
        appLocalizations: mockAppLocalizations,
        builder: (locale) => _TestLocalizationText(mockAppLocalizations),
      ));
    });
  });
}
