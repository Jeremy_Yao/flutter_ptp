import 'dart:async';
import 'dart:ui';

import 'package:app_entry_point/src/app_visibility_scope.dart';
import 'package:app_entry_point/src/async_initializer_scope.dart';
import 'package:built_value/serializer.dart';
import 'package:common/common.dart';
import 'package:common/src/components/rails/component_builder.dart';
import 'package:common/src/route/page_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:l10n_cms/l10n_cms.dart';
import 'package:mockito/mockito.dart';
import 'package:common/module_settings.dart';

import 'package:app_entry_point/app_entry_point.dart';

import 'package:app_entry_point/src/entry_point_app.dart';

class _MockAppLocalizations extends Mock implements AppLocalizations {}

class _TestEntryPointAppModuleSettings extends EntryPointAppModuleSettings {
  @override
  Map<String, ComponentBuilder> get componentsMap => {};

  @override
  List<BaseModuleSettings> get moduleSettings => [];

  @override
  Map<String, PageBuilder> get pageRouteList => {};

  @override
  List<Serializers> get serializersMergeList => [];
}

class _TestGlobalL10NCMSLocalizationsLookup
    extends GlobalL10NCMSLocalizationsLookup {
  @override
  int get buildTimestamp => 1602498555;

  @override
  GlobalL10NCMSLocalizations lookFor(Locale locale) {
    return null;
  }

  @override
  Set<String> get supportedLanguages => const {
        'en',
      };

  @override
  Iterable<Locale> get supportedLocales => const [
        Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      ];
}

class _HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

void main() {
  testWidgets('EntryPointApp smoke test', (WidgetTester tester) async {
    final mockAppLocalizations = _MockAppLocalizations();
    when(mockAppLocalizations.currentLanguage).thenReturn('en_US');

    await tester.pumpWidget(
      AppVisibilityScope(
        child: AsyncInitializerScope(
          ensureInit: () => Future.sync(() => null),
          child: EntryPointApp(
            serializers: Serializers(),
            railsComponentBuilderMap: const {},
            componentContentFactories: const [],
            componentWrapperFactory: null,
            feedCardContentBuilderMap: const {},
            genericFeedCardItemBuilderMap: const {},
            pageRoutes: {"/": (_, __, ___) => _HomePage()},
            localizationsLookup: _TestGlobalL10NCMSLocalizationsLookup(),
            moduleSettings: _TestEntryPointAppModuleSettings(),
          ),
        ),
      ),
    );

    final binding = TestWidgetsFlutterBinding.ensureInitialized();
    binding.handleAppLifecycleStateChanged(AppLifecycleState.resumed);

    await tester.pumpAndSettle(const Duration(milliseconds: 1000));

    // The HomePage will be show for initial route '/'
    expect(find.byType(_HomePage), findsOneWidget);
  });
}
