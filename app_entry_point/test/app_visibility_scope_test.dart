import 'package:app_entry_point/src/app_visibility_scope.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets(
    'AppVisibilityScope show child after AppLifecycleState.resumed',
    (WidgetTester tester) async {
      final binding = TestWidgetsFlutterBinding.ensureInitialized();

      await tester.pumpWidget(
        const AppVisibilityScope(
          child: AppVisible(
            child: Text(
              'App Showed',
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
      );

      expect(find.text('App Showed'), findsNothing);

      binding.handleAppLifecycleStateChanged(AppLifecycleState.paused);

      expect(find.text('App Showed'), findsNothing);

      binding.handleAppLifecycleStateChanged(AppLifecycleState.resumed);
      await tester.pumpAndSettle();
      expect(find.text('App Showed'), findsOneWidget);
    },
  );

  testWidgets(
    'AppVisibilityScope show child when current AppLifecycleState is AppLifecycleState.resumed',
    (WidgetTester tester) async {
      final binding = TestWidgetsFlutterBinding.ensureInitialized();
      binding.handleAppLifecycleStateChanged(AppLifecycleState.resumed);

      await tester.pumpWidget(
        const AppVisibilityScope(
          child: AppVisible(
            child: Text(
              'App Showed',
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
      );

      expect(find.text('App Showed'), findsOneWidget);
    },
  );
}
