# Changlog

## 2.0.0

* Upgrade to flutter 2.0.1

## 1.9.0
* Ensure initialization complete before the route be pushed

## 1.8.2

* Fix `error_handling` hash version to `error_handling-1.2.0`.

## 1.8.1

* Fix hot reload force pop to initial route `/`

## 1.8.0

* Initialize http proxy config, appLocalizations, etc parallelly.
* Upgrade `error_handling` version to `error_handling-1.2.0`.

## 1.7.1

* Fix initial route `/` not show when hot restarted.

## 1.7.0

* Load initial route `/` after `AppLifecycleState.resumed`.

## 1.6.0

* Upgrade `error_handling` version to `error_handling-1.1.0`
* Allow report crash to firebase crashlytics on debug mode.

## 1.5.2

* Add `PageBuilder` assertion for named route.

## 1.5.1

* Move astronomiaNavigatorObserverDelegate construct place from EntryPointApp to DIProvider

## 1.5.0

* Add [RootRestorationScope](https://api.flutter.dev/flutter/widgets/RootRestorationScope-class.html) to entry point app to allow use restoration feature.
* Use `/` for as platform home page
* Remove debug drawer

## 1.4.0

* Upgrade `common` version to `common-1.3.0`
* Migrate error handling logic with `error_handling` package

## 1.3.1

* Upgrade `common` version to `common-1.2.1`
* Upgrade `ui_component` version to `ui_component-0.3.3`

## 1.3.0

* Upgrade `common` version to `common-1.2.0`
* Implement extra headers logic for l10n_cms

## 1.2.2

* Fix can't change language on Android

## 1.2.1

* Remove android scroll overflow effect.

## 1.2.0

* Integrate `ui_component`, version `ui_component-0.3.2`.
* Upgrade `common`, version `common-1.1.1`.

## 1.1.0

Integrate `firebase_crashlytics`.

## 1.0.0

Initial release.
