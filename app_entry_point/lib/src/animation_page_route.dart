import 'package:flutter/material.dart';

class AnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  AnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool animated = true,
  })  : assert(animated != null),
        _animted = animated,
        super(builder: builder, settings: settings);

  final bool _animted;
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (_animted) {
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);
    } else {
      return child;
    }
  }
}
