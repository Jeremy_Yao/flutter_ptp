import 'dart:async';
import 'dart:io';

import 'package:app_entry_point/src/app_visibility_scope.dart';
import 'package:app_entry_point/src/async_initializer_scope.dart';
import 'package:app_entry_point/src/root_configuration_change_aware_widget.dart';
import 'package:astronomia/astronomia.dart';
import 'package:built_value/serializer.dart';
import 'package:common/common.dart';
import 'package:common/common_navigator.dart';
import 'package:common/components.dart';
import 'package:common/debug.dart';
import 'package:common/di_injector.dart';
import 'package:common/error_handling.dart';
import 'package:common/network.dart';
import 'package:design_token/design_token.dart';
import 'package:error_handling/error_handling.dart' as error_handling;
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http_proxy/http_proxy.dart';
import 'package:l10n_cms/l10n_cms.dart';
import 'package:l10n_cms/l10n_cms_text_debug.dart';
import 'package:provider/provider.dart';
import 'package:ui_component/theme.dart';

import 'animation_page_route.dart';
import 'entry_point_app_module_settings.dart';

/// Run `runApp` to start up the [EntryPointApp], which also handle the Crashlytics and other
/// configuration setup. Make sure this function be called at the main entry point. e.g.,
/// ```dart
/// // klook_app_flutter/lib/main.dart
///
/// void main() {
///   startUpApp(
///     componentContentFactories: [
///       PlatformComponentContentFactory(),
///     ],
///     componentWrapperFactory: PlatformComponentWrapperFactory(),
///     moduleSettings: const ModuleSettings(),
///   );
/// }
/// ```
Future<void> startUpApp(
    {@Deprecated('Use moduleSettings instead.')
        Serializers serializers,
    @Deprecated('Use moduleSettings instead.')
        Map<String, ComponentBuilder> railsComponentBuilderMap,
    @required
        List<ComponentContentFactory> componentContentFactories,
    @required
        ComponentContainerFactory componentWrapperFactory,
    @required
        Map<String, FeedCardContentBuilder> feedCardContentBuilderMap,
    @required
        Map<String, GenericFeedCardItemBuilder> genericFeedCardItemBuilderMap,
    @Deprecated('Use moduleSettings instead.')
        Map<String, PageBuilder> pageRoutes,
    @required
        EntryPointAppModuleSettings moduleSettings,
    @required
        GlobalL10NCMSLocalizationsLookup globalL10NCMSLocalizationsLookup,
    @required
        Widget home}) async {
  WidgetsFlutterBinding.ensureInitialized();

  error_handling.runZonedGuaredInErrorHandling(() {
    if (kDebugMode) {
      // TODO(littlegnal): Compatible the deprecated `forceReportCrash`, will be removed this logic
      // after all the error handling logic migrate to `error_handling` package
      // ignore: deprecated_member_use
      forceReportCrash = (dynamic error, StackTrace stackTrace, {bool forceCrash}) {
        return FirebaseCrashlytics.instance.recordError(error, stackTrace);
      };
    }

    _runApp(
        componentContentFactories: componentContentFactories,
        componentWrapperFactory: componentWrapperFactory,
        feedCardContentBuilderMap: feedCardContentBuilderMap,
        genericFeedCardItemBuilderMap: genericFeedCardItemBuilderMap,
        moduleSettings: moduleSettings,
        localizationsLookup: globalL10NCMSLocalizationsLookup,
        home: home);
  });
}

Future<void> _runApp(
    {@required List<ComponentContentFactory> componentContentFactories,
    @required ComponentContainerFactory componentWrapperFactory,
    @required EntryPointAppModuleSettings moduleSettings,
    @required Map<String, FeedCardContentBuilder> feedCardContentBuilderMap,
    @required Map<String, GenericFeedCardItemBuilder> genericFeedCardItemBuilderMap,
    @required GlobalL10NCMSLocalizationsLookup localizationsLookup,
    @required Widget home}) async {
  final app = AppVisibilityScope(
    child: AsyncInitializerScope(
      ensureInit: _ensureInit,
      child: EntryPointApp(
        serializers: moduleSettings.serializers(),
        railsComponentBuilderMap: moduleSettings.railsComponentBuilderMap(),
        componentContentFactories: componentContentFactories,
        componentWrapperFactory: componentWrapperFactory,
        pageRoutes: moduleSettings.pageRoutes(),
        moduleSettings: moduleSettings,
        localizationsLookup: localizationsLookup,
        feedCardContentBuilderMap: feedCardContentBuilderMap,
        genericFeedCardItemBuilderMap: genericFeedCardItemBuilderMap,
        home: home,
      ),
    ),
  );

  runApp(app);

  _initApp();
}

void _initApp() {
  WidgetsBinding.instance.renderView.automaticSystemUiAdjustment = false;
}

Future<void> _ensureInit() async {
  await Future.wait([
    _initHttpProxyConfig(),
    //error_handling.initializeErrorHandling(),
  ]);
}

Future<void> _initHttpProxyConfig() async {
  final enableProxy = await HttpProxyConfig.getEnable();
  if (enableProxy) {
    final HttpProxy httpProxy = await HttpProxy.createHttpProxy();
    HttpOverrides.global = httpProxy;
  }
}

/// Entry point for klook_app_flutter that handle the common configuration setup.
///
/// This widget should not be used directly if you don't intend to add some custom configuration.
///
/// See also:
/// * [startUpApp], run `runApp` to start up the [EntryPointApp].
class EntryPointApp extends StatelessWidget {
  EntryPointApp(
      {@required Serializers serializers,
      @required Map<String, ComponentBuilder> railsComponentBuilderMap,
      @required List<ComponentContentFactory> componentContentFactories,
      @required ComponentContainerFactory componentWrapperFactory,
      @required Map<String, PageBuilder> pageRoutes,
      @required GlobalL10NCMSLocalizationsLookup localizationsLookup,
      @required EntryPointAppModuleSettings moduleSettings,
      @required Map<String, FeedCardContentBuilder> feedCardContentBuilderMap,
      @required Map<String, GenericFeedCardItemBuilder> genericFeedCardItemBuilderMap,
      @required Widget home})
      : _pageRoutes = _adjustPageRoutes(pageRoutes, localizationsLookup),
        _localizationsLookup = localizationsLookup,
        _moduleSettings = moduleSettings,
        _diProvider = DIProvider(
          serializers: serializers,
          railsComponentBuilderMap: railsComponentBuilderMap,
          componentContentFactories: componentContentFactories,
          componentWrapperFactory: componentWrapperFactory,
          feedContentCardBuilderMap: feedCardContentBuilderMap,
          genericFeedCardItemBuilderMap: genericFeedCardItemBuilderMap,
          globalL10NCMSLocalizationsLookup: localizationsLookup,
        ),
        _home = home {
    // TODO(littlegnal): Move back the check test enviroment logic into `overrideErrorWidgetForDebug`
    if (!isFlutterTestEnviroment()) {
      error_handling.overrideErrorWidgetForDebug();
    }
  }

  final DIProvider _diProvider;

  final Map<String, PageBuilder> _pageRoutes;

  final GlobalL10NCMSLocalizationsLookup _localizationsLookup;

  final EntryPointAppModuleSettings _moduleSettings;

  final Widget _home;

  // Add initial route and debug routes to [_pageRoutes].
  static Map<String, PageBuilder> _adjustPageRoutes(
    Map<String, PageBuilder> pageRoutes,
    GlobalL10NCMSLocalizationsLookup localizationsLookup,
  ) {
    // The debug routes should not be passed outside.
    assert(!pageRoutes.keys.any((element) => element.startsWith('/debug')));

    return {
      ...pageRoutes,
      ...debugRoutes,
      '/debug/l10n_cms_debug_page': (_, __, ___) => L10NCMSDebugPage(localizationsLookup),
    };
  }

  @override
  Widget build(BuildContext context) {
    return CommonDIInjector(
      diProvider: _diProvider,
      child: L10NCMSLocalizationsWidget(
        localizationsLookup: _localizationsLookup,
        commonLocalizationsDelegateBuilders:
            _moduleSettings.l10ncmsLocalizationsDelegateBuilderList(),
        commonNavigatorPushNativeCallback: <Void>(context, routeName, {arguments}) async {
          Provider.of<CommonNavigator>(context, listen: false).pushNamedNative(routeName);
          return;
        },
        extraHeadersGetter: _l10nCMSExtraHeadersGetter(_diProvider.nativeInfoManager),
        childBuilder: (localizationsDelegates, supportedLocales) {
          return RootConfigurationChangeAwareWidget(
              appLocalizations: _diProvider.appLocalizations,
              builder: (locale) {
                return ThemeChangedAwareWidget(
                  childBuilder: (BuildContext _, ThemeData themeData) {
                    final adjustedTheme = themeData.copyWith(
                      pageTransitionsTheme: const PageTransitionsTheme(
                        builders: {
                          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
                          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
                        },
                      ),
                    );

                    // TODO(littlegnal): The [RootRestorationScope] can be removed once it is part of [MaterialApp].
                    // https://api.flutter.dev/flutter/widgets/RestorationMixin-mixin.html
                    return RootRestorationScope(
                      restorationId: 'root',
                      child: MaterialApp(
                        // TODO(littlegnal): Remove this when this issuse fixed:
                        // https://bitbucket.org/klook/klook-app-flutter/issues/7/astronomia-unsupported-operation-cannot
                        // ignore: prefer_const_literals_to_create_immutables
                        navigatorObservers:
                            _diProvider.astronomiaNavigatorObserverDelegate.observers,
                        locale: locale,
                        localizationsDelegates: localizationsDelegates,
                        supportedLocales: supportedLocales,
                        builder: (context, child) {
                          return ScrollConfiguration(
                            behavior: const _ScrollBehavior(),
                            child: AstronomiaNavigator.builder(
                                astronomiaNavigatorKey: _diProvider.astronomiaNavigatorKey,
                                navigatorObserverDelegate: _diProvider
                                    .astronomiaNavigatorObserverDelegate)(context, child),
                          );
                        },
                        theme: adjustedTheme,
                        onGenerateRoute: (settings) {
                          return _getPageRoute(settings);
                        },
                        home: _home,
                      ),
                    );
                  },
                  themeData: _defaultAppTheme,
                );
              });
        },
      ),
    );
  }

  MaterialPageRoute _getPageRoute(RouteSettings settings) {
    final pageBuilder = _pageRoutes[settings.name];

    assert(
        pageBuilder != null,
        'Can not find PageBuilder for \'${settings.name}\', '
        'make sure you add the PageBuilder to the routes map.');

    final bool animated = settings is AstronomiaRouteSettings && !settings.isFirstRoute;
    Object actualSettingArguments = settings.arguments;
    if (settings.arguments is Map) {
      final Map arguments = settings.arguments;
      if (arguments.containsKey("route_animated")) {
        actualSettingArguments = arguments["arguments"];
      }
    }

    return AnimationMaterialPageRoute(
        settings: settings,
        animated: animated,
        builder: (context) {
          debugPrint('settings: $settings');
          final Widget page = pageBuilder(
            context,
            settings.name,
            actualSettingArguments,
          );

          //return page;

          return AsyncInitializedAwareWidget(
            child: AppVisible(
              child: page,
            ),
          );
        });
  }

  AsyncValueGetter<Map<String, dynamic>> _l10nCMSExtraHeadersGetter(
      NativeInfoManager nativeInfoManager) {
    return () async {
      return createCommonHeaders(
        nativeInfoManager,
        nativeInfoManager,
        nativeInfoManager,
        nativeInfoManager,
      );
    };
  }
}

ThemeData get _defaultAppTheme {
  final baseTheme = CommonThemeData.light();

  return baseTheme.copyWith(
    // 为了设置键盘颜色, 这里改为light
    // 键盘颜色keyboardAppearance，默认取ThemeData.primaryColorBrightness
    primaryColorBrightness: Brightness.light,
    textSelectionHandleColor: baseTheme.colorScheme.colorPrimary,
  );
}

class _ScrollBehavior extends ScrollBehavior {
  const _ScrollBehavior();

  @override
  Widget buildViewportChrome(BuildContext context, Widget child, AxisDirection axisDirection) {
    // 取消android上滑动时的overflow效果
    return child;
  }
}
