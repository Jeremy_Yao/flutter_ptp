import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class _AppVisibilityNotifier extends ChangeNotifier {
  _AppVisibilityNotifier({bool isVisible = false}) : _isVisible = isVisible;
  bool _isVisible = false;
  bool get isVisible => _isVisible;
  set isVisible(bool isVisible) {
    if (_isVisible == isVisible) return;

    _isVisible = isVisible;
    notifyListeners();
  }
}

class AppVisibilityScope extends StatefulWidget {
  const AppVisibilityScope({@required this.child});
  final Widget child;
  @override
  _AppVisibilityScopeState createState() => _AppVisibilityScopeState();
}

class _AppVisibilityScopeState extends State<AppVisibilityScope>
    with WidgetsBindingObserver {
  _AppVisibilityNotifier _appVisibilityNotifier;

  @override
  void initState() {
    super.initState();
    _appVisibilityNotifier = _AppVisibilityNotifier(
        isVisible: WidgetsBinding.instance.lifecycleState ==
            AppLifecycleState.resumed);

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed &&
        !_appVisibilityNotifier.isVisible) {
      debugPrint(
          '[AppVisibilityScope] Receive didChangeAppLifecycleState callback with state: $state');

      _appVisibilityNotifier.isVisible = true;
      // Remove the `WidgetsBindingObserver` on next build frame to avoid `ConcurrentModificationError`
      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        WidgetsBinding.instance.removeObserver(this);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<_AppVisibilityNotifier>.value(
      value: _appVisibilityNotifier,
      child: widget.child,
    );
  }
}

class AppVisible extends StatelessWidget {
  const AppVisible({this.child});
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Consumer<_AppVisibilityNotifier>(
      builder: (_, value, __) {
        if (value.isVisible) {
          return child;
        } else {
          return Container(
            color: Colors.white,
          );
        }
      },
    );
  }
}
