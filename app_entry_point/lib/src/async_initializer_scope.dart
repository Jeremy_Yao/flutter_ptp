import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class _AsyncInitializerNotifier extends ChangeNotifier {
  _AsyncInitializerNotifier();

  bool _isinitialized = false;
  bool get isinitialized => _isinitialized;
  set isinitialized(bool isinitialized) {
    if (_isinitialized == isinitialized) return;

    _isinitialized = isinitialized;
    notifyListeners();
  }
}

class AsyncInitializerScope extends StatefulWidget {
  const AsyncInitializerScope({
    @required this.ensureInit,
    @required this.child,
  });

  final Widget child;
  final Future<void> Function() ensureInit;

  @override
  _AsyncInitializerScopeState createState() => _AsyncInitializerScopeState();
}

class _AsyncInitializerScopeState extends State<AsyncInitializerScope> {
  _AsyncInitializerNotifier _asyncInitializerNotifier;

  @override
  void initState() {
    super.initState();

    _asyncInitializerNotifier = _AsyncInitializerNotifier();
    _ensureInit();
  }

  Future<void> _ensureInit() async {
    await widget.ensureInit();

    _asyncInitializerNotifier.isinitialized = true;
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<_AsyncInitializerNotifier>.value(
      value: _asyncInitializerNotifier,
      child: widget.child,
    );
  }
}

class AsyncInitializedAwareWidget extends StatelessWidget {
  const AsyncInitializedAwareWidget({@required this.child});
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Consumer<_AsyncInitializerNotifier>(
      builder: (_, value, __) {
        if (value.isinitialized) {
          return child;
        } else {
          return Container(
            color: Colors.white,
          );
        }
      },
    );
  }
}
