import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef RootConfigurationChangeChildBuilder = Widget Function(Locale locale);

/// A [StatefulWidget] that can reload appLocalizations when configuration change (such as
/// language/currency changed).
class RootConfigurationChangeAwareWidget extends StatefulWidget {
  const RootConfigurationChangeAwareWidget({
    @required AppLocalizations appLocalizations,
    @required RootConfigurationChangeChildBuilder builder,
  })  : _appLocalizations = appLocalizations,
        _builder = builder;

  final AppLocalizations _appLocalizations;

  final RootConfigurationChangeChildBuilder _builder;

  @override
  _RootConfigurationChangeAwareWidgetState createState() =>
      _RootConfigurationChangeAwareWidgetState();
}

class _RootConfigurationChangeAwareWidgetState
    extends State<RootConfigurationChangeAwareWidget>
    with ConfigurationChangeAwareMixin {
  Locale _locale;

  @override
  void initState() {
    super.initState();

    _load();
  }

  Future<void> _load() async {
    await widget._appLocalizations.ensureInit();

    _updateLocale(context, widget._appLocalizations.currentLanguage);
  }

  void _updateLocale(BuildContext context, String languageFromNative) {
    setState(() {
      _locale = _getLocale(context, languageFromNative);
    });
  }

  Locale _getLocale(BuildContext context, String languageFromNative) {
    final tempLanguage = languageFromNative.split('_');
    assert(tempLanguage.isNotEmpty);
    assert(tempLanguage.length == 2);

    return Locale.fromSubtags(
      languageCode: tempLanguage[0],
      countryCode: tempLanguage[1],
    );
  }

  @override
  Future<void> refreshUI() async {
    _load();
  }

  @override
  Widget build(BuildContext context) {
    return widget._builder(_locale);
  }
}
