import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:common/common.dart';
import 'package:common/serializers.dart' as s;
import 'package:common/module_settings.dart';

import 'package:l10n_cms/l10n_cms.dart';

abstract class EntryPointAppModuleSettings extends BaseModuleSettings {
  const EntryPointAppModuleSettings();
  List<BaseModuleSettings> get moduleSettings;

  List<Serializers> get serializersMergeList;

  Map<String, PageBuilder> get pageRouteList;

  Map<String, ComponentBuilder> get componentsMap;

  @override
  Serializers serializers() {
    final moduleSettingsSerializers =
        moduleSettings.map((e) => e.serializers()).toList();

    final List<Serializers> serializersMergeList = [
      ...moduleSettingsSerializers,
      s.serializers,
      ...this.serializersMergeList,
    ];

    /// see https://github.com/google/built_value.dart/blob/master/chat_example/lib/data_model/serializers.dart
    final Serializers serializers = (Serializers().toBuilder()
          ..addPlugin(StandardJsonPlugin())
          ..mergeAll(serializersMergeList))
        .build();

    return serializers;
  }

  @override
  Map<String, PageBuilder> pageRoutes() {
    final modulePageRoutes = <String, PageBuilder>{};
    moduleSettings.forEach((element) {
      modulePageRoutes.addAll(element.pageRoutes());
    });

    final Map<String, PageBuilder> pageRouteList = {
      ...modulePageRoutes,
      ...this.pageRouteList
    };

    return pageRouteList;
  }

  @override
  Map<String, ComponentBuilder> railsComponentBuilderMap() {
    final moduleRailsComponentBuilderMap = <String, ComponentBuilder>{};
    moduleSettings.forEach((element) {
      moduleRailsComponentBuilderMap.addAll(element.railsComponentBuilderMap());
    });

    final Map<String, ComponentBuilder> componentsMap = {
      ...moduleRailsComponentBuilderMap,
      ...this.componentsMap
    };

    return componentsMap;
  }

  List<CommonLocalizationsDelegateBuilder>
      l10ncmsLocalizationsDelegateBuilderList() {
    return moduleSettings.map((e) => e.l10ncmsLocalizations).toList();
  }
}
